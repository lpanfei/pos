﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.TestSync
{
    class Program
    {
        static void Main(string[] args)
        {
            ConsoleSync sync = new ConsoleSync(@"Data Source=localhost;Initial Catalog=Inventory_POS;Integrated Security=true;",
                @"Data Source=localhost;Initial Catalog=POSPortal;Integrated Security=true;", 1, 1);
            sync.DoSync();
            Console.ReadLine();
        }
    }

    public class ConsoleSync : DataSync.Sync
    {
        public ConsoleSync(string clientConn, string serverConn, int clientid, int storeid)
            :base(clientConn,serverConn,clientid,storeid)
        {

        }
        public override void WriteLog(string message, DataSync.LogType lt)
        {
            //base.WriteLog(message, lt);
            Console.WriteLine(message);
        }
    }
}
