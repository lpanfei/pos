using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;

namespace Eidolon.Web.DA
{
    /// <summary>
    /// Each provider must conform to this interface.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="Tparms"></typeparam>
	public interface IDataProvider<T, Tparms>
	{
		/// <summary>
		/// Returns a list of items depending on the parms provided
		/// </summary>
		/// <param name="parms"></param>
		/// <returns></returns>
		List<T> Select(Tparms parms);
		/// <summary>
		/// Updates the given item with the optional transaction lock
		/// </summary>
		/// <param name="item"></param>
		/// <param name="lockHolder"></param>
		/// <returns></returns>
		T Update(T item, object lockHolder);
		/// <summary>
		/// Inserts the given item with the optional transaction lock
		/// </summary>
		/// <param name="item"></param>
		/// <param name="lockHolder"></param>
		/// <returns></returns>
		T Insert(T item, object lockHolder);
		/// <summary>
		/// Deletes the given item with the optional transaction lock
		/// </summary>
		/// <param name="item"></param>
		/// <param name="lockHolder"></param>
		/// <returns></returns>
		T Delete(T item, object lockHolder);
		/// <summary>
		/// Gets a transaction lock object
		/// </summary>
		/// <returns></returns>
        object GetLock();
		/// <summary>
		/// Commits a transaction
		/// </summary>
		/// <param name="lockHolder"></param>
        void CommitLock(object lockHolder);
		/// <summary>
		/// Rolls back a transaction
		/// </summary>
		/// <param name="lockHolder"></param>
        void RollbackLock(object lockHolder);
		/// <summary>
		/// By default, clears the _list object and the stored parameters
		/// Forces reget of any data.
		/// </summary>
		void Invalidate();
	}
}
