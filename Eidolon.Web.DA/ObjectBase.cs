using System;
using System.Collections.Generic;
using System.Text;

namespace Eidolon.Web.DA
{
    /// <summary>
    /// The base class for a data object. It supports all the CRUD operation.
    /// </summary>
    /// <typeparam name="T">A data object</typeparam>
    /// <typeparam name="Tparms">Parameters for selecting objects</typeparam>
	[Serializable]
	public class ObjectBase<T, Tparms>
	{
        /// <summary>
        /// Insert an item into the data store
        /// </summary>
        /// <param name="item">The item to be inserted</param>
        /// <param name="lockHolder">
        /// A lock object that the provider understands.
        /// Usually from GetLock().
        /// </param>
        /// <returns></returns>
        protected static T Insert(T item, object lockHolder)
        {
            return DataManagerBase<T, Tparms>.Insert(item, lockHolder);
        }
		/// <summary>
		/// Insert without a transaction
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
        protected static T Insert(T item)
        {
            return Insert(item, null);
        }
        /// <summary>
        /// Update the data object.
        /// </summary>
        /// <param name="item">Object to be updated</param>
        /// <param name="lockHolder">
        /// A lock object that the provider understands.
        /// Usually from GetLock().
        /// </param>
        /// <returns></returns>
        protected static T Update(T item, object lockHolder)
        {
            return DataManagerBase<T, Tparms>.Update(item, lockHolder);
        }
		/// <summary>
		/// Updates the item without a transction
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
        protected static T Update(T item)
        {
            return Update(item, null);
        }
        /// <summary>
        /// Delete the object from the data store
        /// </summary>
        /// <param name="item">object to be deleted</param>
        /// <param name="lockHolder">
        /// A lock object that the provider understands.
        /// Usually from GetLock().
        /// </param>
        /// <returns></returns>
        protected static T Delete(T item, object lockHolder)
        {
            return DataManagerBase<T, Tparms>.Delete(item, lockHolder);
        }
		/// <summary>
		/// Deletes the given item without a transaction
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
        protected static T Delete(T item)
        {
            return Delete(item, null);
        }
        /// <summary>
        /// Get a list of items from the data store
        /// </summary>
        /// <param name="parms">
        /// A set of parameters that the provider understands.
        /// </param>
        /// <returns></returns>
        public static List<T> Select(Tparms parms)
		{
			return DataManagerBase<T, Tparms>.Select(parms);
		}
        /// <summary>
        /// Gets a lock from the provider. The lock is then provided to
        /// all the subsequent operations. For example, for a SQL database
        /// provider, the lockHolder contains the connection and transaction
        /// to be used.
        /// </summary>
        /// <returns></returns>
        public static object GetLock()
        {
            return DataManagerBase<T, Tparms>.GetLock();
        }
        /// <summary>
        /// Commits the lock provided by the lockHolder
        /// </summary>
        /// <param name="lockHolder">The object containing the lock to commit.</param>
        public static void CommitLock(object lockHolder)
        {
            DataManagerBase<T, Tparms>.CommitLock(lockHolder);
        }
        /// <summary>
        /// Rolls back the lock provided by the lockHolder
        /// </summary>
        /// <param name="lockHolder">The object containing the lock to rollback.</param>
        public static void RollbackLock(object lockHolder)
        {
            DataManagerBase<T, Tparms>.RollbackLock(lockHolder);
        }

        /// <summary>
        /// Invalidate causes the provider to clear any cached data
        /// </summary>
        protected static void Invalidate()
		{
			DataManagerBase<T, Tparms>.Invalidate();
		}
	}
}
