﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using POS.Data.DBProvider;

namespace POS.Data
{
    public class DBInitializer
    {
        public static void Initialize(string connectionString)
        {
            DBTools.ConnectionString = connectionString;
            
            DataManagerBase<User, User.Parms>.Provider = new UserDB();
            DataManagerBase<Product, Product.Parms>.Provider = new ProductDB();
            DataManagerBase<Category, Category.Parms>.Provider = new CategoryDB();
            DataManagerBase<Store, Store.Parms>.Provider = new StoreDB();
            DataManagerBase<Setting, Setting.Parms>.Provider = new SettingDB();
            DataManagerBase<Customer, Customer.Parms>.Provider = new CustomerDB();
            DataManagerBase<Supplier, Supplier.Parms>.Provider = new SupplierDB();
            DataManagerBase<Invoice, Invoice.Parms>.Provider = new InvoiceDB();
            DataManagerBase<Log, Log.Parms>.Provider = new LogDB();
            DataManagerBase<Client, Client.Parms>.Provider = new ClientDB();
            DataManagerBase<Warranty, Warranty.Parms>.Provider = new WarrantyDB();
            DataManagerBase<InvoiceProduct, InvoiceProduct.Parms>.Provider = new InvoiceProductDB();
            DataManagerBase<Registeration, Registeration.Parms>.Provider = new RegisterationDB();
            DataManagerBase<Inventory, Inventory.Parms>.Provider = new InventoryDB();
            DataManagerBase<Promotion, Promotion.Parms>.Provider = new PromotionDB();
        }
    }
}
