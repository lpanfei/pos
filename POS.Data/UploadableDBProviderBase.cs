﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Serialization;

namespace POS.Data
{
    public abstract class UploadableDBProviderBase<T, Tparms> : DataProviderDBBase<T, Tparms>
        where T : IUploadable
    {
        protected int Upload(string spUpload, T item)
        {
            if (string.IsNullOrEmpty(UploadConfig.ConnectionString)||!UploadConfig.IsOn)
                return 0;
            int id = 0;
            SqlConnection sqlConn = new SqlConnection(UploadConfig.ConnectionString);
            SqlCommand sqlCmd = sqlConn.CreateCommand();
            System.IO.StringWriter sw = new System.IO.StringWriter();
            try
            {
                XmlSerializer _xmlS = new XmlSerializer(typeof(T));
                _xmlS.Serialize(sw, item);
                string xml = sw.GetStringBuilder().ToString();
                // remove the <?xml...> tag
                xml = System.Text.RegularExpressions.Regex.Replace(xml, "<[?]xml.*?>\r\n", "");
                sqlCmd.Parameters.AddWithValue("@xml", xml);

                sqlCmd.CommandText = spUpload;
                sqlCmd.CommandType = CommandType.StoredProcedure;
                sqlCmd.CommandTimeout = 120; // Increase from default of 30 seconds
                //sqlCmd.Parameters.Add("@ServerID", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
                //sqlCmd.Parameters.Add("@LastUpdatedTime", SqlDbType.DateTime2).Direction = ParameterDirection.ReturnValue;
                
                if (sqlCmd.Connection.State == ConnectionState.Closed) sqlCmd.Connection.Open();
                 SqlDataReader reader = sqlCmd.ExecuteReader();
                 reader.Read();
                 id = reader.GetInt32(0);
                 item.LastUpdatedTime = reader.GetDateTime(1);
                 reader.Dispose();
            }
            catch
            {
                id = 0;
            }
            finally
            {
                sqlConn.Close();
                sqlCmd.Dispose();
                sqlCmd = null;
                sqlConn = null;
                sw.Close();
            }

            return id;
        }

        protected object Update(T item, string sqlUpload, string sqlCommand, object lockHolder)
        {
            int id = Upload(sqlUpload, item);
            if (id != 0 || item.ServerID == 0)
            {
                ((IUploadable)item).ServerID = id;
            }
            return Update(item, sqlCommand, lockHolder);
        }
    }

    public class UploadParms
    {
        public int ClientID { get; set; }
        public int UserID { get; set; }
        public int StoreID { get; set; }
        public string Module { get; set; }
    }
}
