﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Data
{
    public static class StoredProcedures
    {

        public static string updateUploadStatus = "spUpdUploadStatus";

        #region Category
        public static string selCategories = "spSelCategories";
        public static string insUpdCategory = "spInsUpdCategory";
        public static string delCategory = "spDelCategory";
        public static string uploadCategory = "spUploadCategory";
        #endregion

        #region Product
        public static string selProducts = "spSelProducts";
        public static string insUpdProduct = "spInsUpdProduct";
        public static string delProduct = "spDelProduct";
        public static string uploadProduct = "spUploadProduct";
        #endregion

        #region Inventory
        public static string selInventories = "spSelInventories";
        public static string insUpdInventory = "spInsUpdInventory";
        public static string delInventory = "spDelInventory";
        public static string uploadInventory = "spUploadInventory";
        #endregion

        #region Invoice
        public static string selInvoice = "spSelInvoice";
        public static string insUpdInvoice = "spInsUpdInvoice";
        public static string cancelInvoice = "spCancelInvoice";
        public static string selInvoicesByWallet = "spSelInvoiceByWalletID";
        public static string selInvoicesBySMS = "spSelInvoiceBySMS";
        public static string uploadInvoice = "spUploadInvoice";
        #endregion

        #region Supplier
        public static string selSuppliers = "spSelSuppliers";
        public static string delSupplier = "spDelSupplier";
        public static string insUpdSupplier = "spInsUpdSupplier";
        public static string uploadSupplier = "spUploadSupplier";
        #endregion

        #region Warranty
        public static string selWarranties = "spSelWarranties";
        public static string insUpdWarranty = "spInsUpdWarranty";
        public static string delWarranty = "spDelWarranty";
        public static string uploadWarranty = "spUploadWarranty";
        #endregion

        #region Customer
        public static string selCustomers = "spSelCustomers";
        public static string selCustomerByID = "spSelCustomerByID";
        public static string insUpdCustomer = "spInsUpdCustomer";
        public static string delCustomer = "spDelCustomer";
        public static string uploadCustomer = "spUploadCustomer";
        #endregion

        #region Promotion
        public static string selPromotions = "spSelPromotions";
        public static string insUpdPromotion = "spInsUpdPromotion";
        public static string delPromotion = "spDelPromotion";
        public static string uploadPromotion = "spUploadPromotion";
        #endregion

        #region Client
        public static string selClients = "spSelClients";
        public static string delClient = "spDelClient";
        public static string insUpdClient = "spInsUpdClient";
        #endregion

        #region Log
        public static string selLogs = "spSelLogs";
        #endregion

        #region Registeration
        public static string register = "spRegister";
        #endregion

        #region Settings
        public static string selSetting = "spSelSetting";
        public static string insUpdSetting = "spInsUpdSetting";
        #endregion

        #region Store
        public static string selStores = "spSelStores";
        public static string insUpdStore = "spInsUpdStore";
        public static string delStore = "spDelStore";
        #endregion

        #region User
        public static string selUser = "spSelUser";
        public static string selUsers = "spSelUsers";
        public static string selUserByID = "spSelUserByID";
        public static string insUpdUser = "spInsUpdUser";
        public static string delUser = "spDelUser";
        public static string changePassword = "spChangePassword";
        #endregion
    }
}
