﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace POS.Data
{
    public class UploadConfig
    {
        private static string _connectionString;
        /// <summary>
        /// The global connection string (usually set in Global.asax)
        /// </summary>
        public static string ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        private static bool _isOn;
        public static bool IsOn
        {
            get { return _isOn; }
            set { _isOn = value; }
        }
    }
}
