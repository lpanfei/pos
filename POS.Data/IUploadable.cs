﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.Data
{
    public interface IUploadable
    {
        int ServerID { get; set; }
        DateTime LastUpdatedTime { get; set; }
        bool UploadFlag { get; set; }
    }
}
