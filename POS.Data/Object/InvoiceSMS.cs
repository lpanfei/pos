﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class InvoiceSMS : DataManagerBase<InvoiceSMS, InvoiceSMS.Parms>
    {
        public class Parms
        {
            public int InvoiceID { get; set; }
        }

        public int InvoiceID { get; set; }
        public string MobileNo { get; set; }
        public string ValidationCode { get; set; }
    }
}
