﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Log : DataManagerBase<Log, Log.Parms>
    {
        public class Parms
        {
            public int ClientID { get; set; }
            public int StoreID { get; set; }
        }
        public int ID { get; set; }
        public DateTime CreateDate { get; set; }
        public string Content { get; set; }
        public int Category { get; set; }
        public string Module { get; set; }
        public int ClientID { get; set; }
        public int StoreID { get; set; }
    }
}
