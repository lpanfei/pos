﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class User : DataManagerBase<User, User.Parms>
    {
        public class Parms
        {
            public string usr_Name { get; set; }
            public string usr_Password { get; set; }
            public int clientId { get; set; }
            public int storeId { get; set; }
            public int usr_ID { get; set; }
            public RetrieveType RetrieveType { get; set; }
        }

        public enum RetrieveType
        {
            Login,
            User,
            AllUsers
        }
        public bool IsNew { get; set; }
        public int usr_ID { get; set; }
        public string usr_Name { get; set; }
        public string usr_Password { get; set; }
        public DateTime? usr_RegTime { get; set; }
        public string usr_FirstName { get; set; }
        public string usr_LastName { get; set; }
        public string usr_Address { get; set; }
        public string usr_Suburb { get; set; }
        public string usr_State { get; set; }
        public string usr_Postcode { get; set; }
        public string usr_Phone { get; set; }
        public string usr_MoblePhone { get; set; }
        public string usr_Email { get; set; }
        public string usr_Note { get; set; }
        public string usr_Permission { get; set; }
        public string Permission
        {
            get
            {
                switch (usr_Permission)
                {
                    case "0": return "Administrator";
                    case "1": return "Report";
                    case "2": return "Sale";
                    case "3": return "Sale and Warehouse";
                    case "4": return "Service";
                    case "5": return "Technician";
                    case "6": return "Warehouse";
                }
                return "";
            }
        }
        public string usr_Store { get; set; }
        public Store Store { get; set; }
        public UserType UserType { get; set; }
        public Client Client { get; set; }
        public int usr_StoreID { get; set; }
        public int usr_ClientID { get; set; }
        public int usr_UserTypeID { get; set; }
        public int ServerID { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public User Update()
        {
            return Update(this);
        }

        public void Delete()
        {
            Delete(this);
        }

        public void ChangePassword(int id, string password)
        {
            ((IUserProvider)Provider).ChangePassword(id, password);
        }
    }

    public interface IUserProvider : IDataProvider<User, User.Parms>
    {
        void ChangePassword(int id, string password);
    }

}
