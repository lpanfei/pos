﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Location : DataManagerBase<Location,Location.Parms>
    {
        public class Parms
        {
        }

        public int loc_Id { get; set; }
        public string loc_Address { get; set; }
    }
}
