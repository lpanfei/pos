﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Category : DataManagerBase<Category, Category.Parms>,IUploadable
    {
        public class Parms
        {
            public int ClientID { get; set; }
            public int UserID { get; set; }
            public int StoreID { get; set; }
            public string Module { get; set; }
        }

        public int cat_Id { get; set; }
        public string cat_Name { get; set; }
        public Category Parent { get; set; }
        public int ParentID { get; set; }
        public int? BindableParentID
        {
            get
            {
                if (ParentID == 0)
                {
                    return null;
                }
                else
                {
                    return ParentID;
                }
            }
        }
        public bool IsNew { get; set; }
        public List<Category> Children { get; set; }
        public int ServerID { get; set; }
        public bool UploadFlag { get; set; }
        public int cat_ClientID { get; set; }
        public int UserID { get; set; }
        public string ori_Name { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime LastUpdatedTime { get; set; }

        public Category Update()
        {
            return Update(this);
        }

        public void Delete()
        {
            Delete(this);
        }
    }
}
