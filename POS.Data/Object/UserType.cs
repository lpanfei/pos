﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public enum UserType
    {
        SystemAdmin,
        ClientAdmin,
        StoreAdmin,
        User
    }
}
