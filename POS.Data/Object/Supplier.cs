﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Supplier : DataManagerBase<Supplier, Supplier.Parms>,IUploadable
    {
        public class Parms
        {
            public int ClientID { get; set; }
        }
        public bool IsNew { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Contact{get;set;}
        public string Suburb { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Postcode { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public string Country { get; set; }
        public int PaymentMethod { get; set; }
        public int ClientID { get; set; }
        public int ServerID { get; set; }
        public int UserID { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public bool UploadFlag { get; set; }
        public Supplier Update()
        {
            return Update(this);
        }

        public void Delete()
        {
            Delete(this);
        }
    }
}
