﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Invoice : DataManagerBase<Invoice, Invoice.Parms>,IUploadable
    {
        public class Parms
        {
            public int ClientID { get; set; }
            public int StoreID { get; set; }
            public int InvoiceID { get; set; }
            public int InvoiceNo { get; set; }
            public string Customer { get; set; }
            public DateTime? DateFrom { get; set; }
            public DateTime? DateTo { get; set; }
        }

        public int inv_Id { get; set; }
        public int inv_No { get; set; }
        public DateTime? inv_CreateDate { get; set; }
        public string inv_Staff { get; set; }
        public decimal inv_Total { get; set; }
        public decimal inv_GST { get; set; }
        public decimal inv_TotalIncGST { get; set; }
        public string inv_Customer { get; set; }
        public string inv_Address { get; set; }
        public string inv_Phone { get; set; }
        public string inv_Mobile { get; set; }
        public string inv_PaymentMethod { get; set; }
        public decimal inv_PaymentMoney { get; set; }
        public decimal inv_Change { get; set; }
        public string CustomerType { get; set; }
        public decimal cost { get; set; }
        public bool IsGST { get; set; }
        public bool UploadFlag { get; set; }
        public bool IsCancel { get; set; }
        public string Reason { get; set; }
        public string Country { get; set; }
        public bool IsTemp { get; set; }
        public string Email { get; set; }
        public string inv_PrintTo { get; set; }
        public string inv_WalletID { get; set; }
        public int inv_ClientID { get; set; }
        public int inv_StoreID { get; set; }
        public int ServerID { get; set; }
        public int UserID { get; set; }
        public InvoiceStatus Status { get; set; }
        public string OrderStatus
        {
            get
            {
                int t = DateTime.Now.CompareTo(this.inv_CreateDate.Value.AddMinutes(30));
                if (DateTime.Now.CompareTo(this.inv_CreateDate.Value.AddMinutes(30))>= 0)
                {
                    return "high-order";
                }
                else if (DateTime.Now.CompareTo(this.inv_CreateDate.Value.AddMinutes(15)) >= 0)
                {
                    return "medium-order";
                }
                else
                {
                    return "normal-order";
                }
            }
        }
        public int TableNo
        {
            get
            {
                try
                {
                    int tn = 0;
                    int.TryParse(inv_Customer, out tn);
                    return tn;
                }
                catch { return 0; }
            }
        }
        public List<InvoiceProduct> Products { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public List<Inventory> Inventories { get; set; }
        public List<InvoiceSMS> SMS { get; set; }
        public decimal Total
        {
            get
            {
                if (IsGST)
                {
                    return inv_Total + inv_GST;
                }
                else
                {
                    return inv_Total;
                }
            }
        }
        public int Take { get; set; }
        public Invoice Update()
        {
            return Update(this);
        }

        public void Cancel()
        {
           ((IInvoiceProvider)Provider).CancelInvoice(this.inv_Id,this.Reason);
        }

        public List<Invoice> GetWalletInvoices(string walletId)
        {
            return ((IInvoiceProvider)Provider).GetWalletInvoices(walletId);
        }

        public List<Invoice> GetSMSInvoices(string mobileno, string validationcode)
        {
            return ((IInvoiceProvider)Provider).GetSMSInvoice(mobileno, validationcode);
        }
    }

    public interface IInvoiceProvider : IDataProvider<Invoice, Invoice.Parms>
    {
        void CancelInvoice(int id,string reason);
        List<Invoice> GetWalletInvoices(string walletId);
        List<Invoice> GetSMSInvoice(string mobileno, string validationcode);
    }

    public enum InvoiceStatus
    {
        
        UnPaid,
        Paid
    }

    public enum InvoiceProductStatus
    {
       
        InComplete,
        Complete
    }
}
