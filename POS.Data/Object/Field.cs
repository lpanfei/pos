﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Field : DataManagerBase<Field, Field.Parms>
    {
        public class Parms
        {
        }
        public int fie_Id{get;set;}
        public int fie_ProductID{get;set;}
        public string fie_Name{get;set;}
        public string fie_Value { get; set; }
    }
}
