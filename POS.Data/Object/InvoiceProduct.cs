﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class InvoiceProduct : DataManagerBase<InvoiceProduct,InvoiceProduct.Parms>
    {
        public class Parms
        {
            public int InvoiceID { get; set; }
        }

        public int ip_ID { get; set; }
        public int ip_InvoiceID { get; set; }
        public string ip_Description { get; set; }
        public decimal ip_Price { get; set; }
        public decimal ip_Discount { get; set; }
        public int ip_Quantity { get; set; }
        public decimal ip_SubTotal { get; set; }
        public string ip_Note { get; set; }
        public decimal cost { get; set; }
        public int ip_ProductID { get; set; }
        public bool IsComplete 
        {
            get
            {
                return Status == InvoiceProductStatus.Complete;
            }
            set
            {
                if (value)
                    Status = InvoiceProductStatus.Complete;
                else
                    Status = InvoiceProductStatus.InComplete;
            }
        }
        public bool IsExisting { get; set; }
        public InvoiceProductStatus Status { get; set; }
        public Product Product { get; set; }
    }
}
