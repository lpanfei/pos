﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class ProductPicture : DataManagerBase<ProductPicture, ProductPicture.Parms>
    {
        public class Parms
        {
        }

        public int ID { get; set; }
        public int ProductID { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public bool IsTemp { get; set; }
        public DateTime? CreateDate { get; set; }
    }
}
