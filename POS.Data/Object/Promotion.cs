﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Promotion : DataManagerBase<Promotion, Promotion.Parms>, IUploadable
    {
        public class Parms
        {
            public int ClientID { get; set; }
            public int StoreID { get; set; }
        }
        public bool IsNew { get; set; }
        public int ID { get; set; }
        public int ProductID { get; set; }
        public int ClientID { get; set; }
        public int StoreID { get; set; }
        public int ServerID { get; set; }
        public int UserID { get; set; }
        public decimal Discount { get; set; }
        public bool IsDeleted { get; set; }
        public bool UploadFlag { get; set; }
        public string ProductName { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public string Description { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public Product Product { get; set; }
        public string FromDate
        {
            get
            {
                if (StartTime == null)
                    return "";
                return StartTime.Value.ToShortDateString();
            }
        }
        public string EndDate
        {
            get
            {
                if (EndTime == null)
                    return "";
                return EndTime.Value.ToShortDateString();
            }
        }

        public Promotion Update()
        {
            return Update(this);
        }
    }
}
