﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Inventory : DataManagerBase<Inventory, Inventory.Parms>, IUploadable
    {
        public class Parms
        {
            public int ClientID { get; set; }
            public int StoreID { get; set; }
        }

        public int ID { get; set; }
        public int ProductID { get; set; }
        public int ClientID { get; set; }
        public int Quantity { get; set; }
        public int StoreID { get; set; }
        public int ServerID { get; set; }
        public int UserID { get; set; }
        public Product Product { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public bool UploadFlag { get; set; }
        public string SupplierName { get; set; }
        public int SupplierID { get; set; }
        public Inventory Update()
        {
            return Update(this);
        }
    }
}
