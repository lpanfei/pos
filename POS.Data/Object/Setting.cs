﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Setting : DataManagerBase<Setting, Setting.Parms>
    {
        public class Parms
        {
            public int ClientID { get; set; }
            public int StoreID { get; set; }
        }
        public decimal RetailerDiscount{get;set;}
        public decimal WholesalerDiscount{get;set;}
        public decimal ExportDiscount{get;set;}
        public string ServerName{get;set;}
        public string Username{get;set;}
        public string Password{get;set;}
        public string UploadTime{get;set;}
        public string Prefix{get;set;}
        public string DatabaseName{get;set;}
        public int GST { get; set; }
        public string InvoiceHeader{get;set;}
        public string InvoiceFooter{get;set;}
        public int ClientID { get; set; }
        public int StoreID { get; set; }

        public void Update()
        {
            Update(this);
        }
    }
}
