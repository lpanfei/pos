﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Warranty : DataManagerBase<Warranty, Warranty.Parms>,IUploadable
    {
        public class Parms
        {
            public int ClientID { get; set; }
            public int StoreID { get; set; }
        }
        public bool IsNew { get; set; }
        public int ID{get;set;}
        public int InvoiceID{get;set;}
        public int InvoiceNo { get; set; }
        public int ProductID{get;set;}
        public int Quantity{get;set;}
        public DateTime? Date{get;set;}
        public string Comment{get;set;}
        public string Resolution { get; set; }
        public string ProductName { get; set; }
        public int ClientID { get; set; }
        public int ServerID { get; set; }
        public int UserID { get; set; }
        public bool IsDeleted { get; set; }
        public int StoreID { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public bool UploadFlag { get; set; }
        public Invoice Invoice { get; set; }
        public Product Product { get; set; }
        public Warranty Update()
        {
            return Update(this);
        }

        public void Delete()
        {
            Delete(this);
        }
    }
}
