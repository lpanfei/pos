﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Product : DataManagerBase<Product, Product.Parms>,IUploadable
    {
        public class Parms
        {
            //public int StoreID { get; set; }
            public int ClientID { get; set; }
        }
        public bool IsNew { get; set; }
        public int pro_Id { get; set; }
        public string pro_Name { get; set; }
        public string pro_No { get; set; }
        public DateTime? pro_Time { get; set; }
        public Category Category { get; set; }
        public decimal pro_price { get; set; }
        //public int pro_Quantity { get; set; }
        public decimal cost { get; set; }
        public int ServerID { get; set; }
        //public Store Store { get; set; }
        public bool UploadFlag { get; set; }
        public int pro_ClientID { get; set; }
        public List<Field> Fields { get; set; }
        public List<Product> Relates { get; set; }
        public List<ProductPicture> Pictures { get; set; }
        public bool IsDeleted { get; set; }
        public int UserID { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public int SupplierID { get; set; }
        public string SupplierName { get; set; }
        public Supplier Supplier { get; set; }
        public string pro_Description { get; set; }
        public decimal Discount { get; set; }
        public string ImageUrl
        {
            get
            {
                if (Pictures.Any())
                    return Pictures[0].Name;
                return "";
            }
        }
        public int ImageHeight { get; set; }
        public string BindableImageUrl { get; set; }
        public string cat_Name
        {
            get
            {
                if (Category == null)
                    return string.Empty;
                else
                    return Category.cat_Name;
            }
        }

        public Product Update()
        {
            return Update(this);
        }

        public void Delete()
        {
            Delete(this);
        }
    }
}
