﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Registeration : DataManagerBase<Registeration, Registeration.Parms>
    {
        public class Parms
        {
        }

        public Client Client { get; set; }
        public User User { get; set; }
        public List<Store> Stores { get; set; }

        public void Update()
        {
            Update(this);
        }
    }
}
