﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Customer : DataManagerBase<Customer,Customer.Parms>,IUploadable
    {
        public class Parms
        {
            public int ClientID { get; set; }
            public bool ById { get; set; }
            public int ID { get; set; }
        }
        public bool IsNew { get; set; }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Country { get; set; }
        public string Email { get; set; }
        public string WalletID { get; set; }
        public int ClientID { get; set; }
        public int ServerID { get; set; }
        public int UserID { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime LastUpdatedTime { get; set; }
        public bool UploadFlag { get; set; }
        public Customer Update()
        {
            return Update(this);
        }

        public void Delete()
        {
            Delete(this);
        }
    }
}
