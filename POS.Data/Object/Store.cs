﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Store : DataManagerBase<Store, Store.Parms>
    {
        public class Parms
        {
            public int ClientID { get; set; }
        }

        public int sto_Id { get; set; }
        public string sto_Name { get; set; }
        public string sto_Suburb { get; set; }
        public string sto_Address { get; set; }
        public string sto_Postcode { get; set; }
        public string sto_Phone { get; set; }
        public string sto_MobilePhone { get; set; }
        public string sto_Fax { get; set; }
        public string sto_Email { get; set; }
        public string sto_Note { get; set; }
        public int sto_LocationID { get; set; }
        public int sto_ClientID { get; set; }
        public Location Location { get; set; }
        public Client Client { get; set; }
        public List<Product> Products { get; set; }

        public Store Update()
        {
            return Update(this);
        }

        public void Delete()
        {
            Delete(this);
        }
    }
}
