﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;

namespace POS.Data
{
    public class Client : DataManagerBase<Client, Client.Parms>
    {
        public class Parms
        {
        }

        public int ID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Suburb { get; set; }
        public string State { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }

        public Client Update()
        {
            return Update(this);
        }

        public void Delete()
        {
            Delete(this);
        }
    }
}
