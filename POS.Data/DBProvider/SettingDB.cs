﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class SettingDB : DataProviderDBBase<Setting, Setting.Parms>, IDataProvider<Setting, Setting.Parms>
    {
        
        public List<Setting> Select(Setting.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selSetting, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@cid", parms.ClientID),
                                new System.Data.SqlClient.SqlParameter("@sid", parms.StoreID)
							}, null);
        }

        public Setting Update(Setting item, object lockHolder)
        {
            Update(item, StoredProcedures.insUpdSetting, lockHolder);
            return item;
        }

        public Setting Insert(Setting item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Setting Delete(Setting item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }
}
