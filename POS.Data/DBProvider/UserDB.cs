﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class UserDB : DataProviderDBBase<User, User.Parms>, IDataProvider<User, User.Parms>, IUserProvider
    {
       
        public List<User> Select(User.Parms parms)
        {
            if (parms.RetrieveType==User.RetrieveType.Login)
            {
                return ExecuteCommand(StoredProcedures.selUser, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@uid", parms.usr_Name),
                                new System.Data.SqlClient.SqlParameter("@pwd", parms.usr_Password)
							}, null);
            }
            else if(parms.RetrieveType==User.RetrieveType.AllUsers)
            {
                return ExecuteCommand(StoredProcedures.selUsers, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@cid", parms.clientId),
                                new System.Data.SqlClient.SqlParameter("@sid", parms.storeId)
							}, null);
            }
            else if (parms.RetrieveType == User.RetrieveType.User)
            {
                return ExecuteCommand(StoredProcedures.selUserByID, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@id", parms.usr_ID)
							}, null);
            }

            return new List<User>();
        }

        public User Update(User item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdUser, lockHolder);
            item.usr_ID = id;
            return item;
        }

        public User Insert(User item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public User Delete(User item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delUser;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.usr_ID));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }

        public void ChangePassword(int id, string password)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.changePassword;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@userid", id));
            cmd.Parameters.Add(new SqlParameter("@newpassword", password));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
