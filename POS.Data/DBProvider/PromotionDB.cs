﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class PromotionDB : UploadableDBProviderBase<Promotion, Promotion.Parms>, IDataProvider<Promotion, Promotion.Parms>
    {
        public List<Promotion> Select(Promotion.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selPromotions, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@cid", parms.ClientID),
                                new System.Data.SqlClient.SqlParameter("@sid", parms.StoreID)
							}, null);
        }

        public Promotion Update(Promotion item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.uploadPromotion, StoredProcedures.insUpdPromotion, lockHolder);
            item.ID = id;
            return item;
        }

        public Promotion Insert(Promotion item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Promotion Delete(Promotion item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delPromotion;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.ID));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
