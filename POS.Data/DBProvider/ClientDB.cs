﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class ClientDB : DataProviderDBBase<Client, Client.Parms>, IDataProvider<Client, Client.Parms>
    {
        
        public List<Client> Select(Client.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selClients);
        }

        public Client Update(Client item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdClient, lockHolder);
            item.ID = id;
            return item;
        }

        public Client Insert(Client item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Client Delete(Client item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delClient;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.ID));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
