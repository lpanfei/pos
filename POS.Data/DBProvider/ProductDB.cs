﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class ProductDB : UploadableDBProviderBase<Product, Product.Parms>, IDataProvider<Product, Product.Parms>
    {
       
        public List<Product> Select(Product.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selProducts, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@cid", parms.ClientID)
							}, null);
        }

        public Product Update(Product item, object lockHolder)
        {
            int id = (int)Update(item,StoredProcedures.uploadProduct, StoredProcedures.insUpdProduct, lockHolder);
            item.pro_Id = id;
            return item;
        }

        public Product Insert(Product item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Product Delete(Product item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delProduct;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.pro_Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
