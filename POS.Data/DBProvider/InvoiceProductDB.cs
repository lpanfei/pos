﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class InvoiceProductDB : DataProviderDBBase<InvoiceProduct, InvoiceProduct.Parms>, IDataProvider<InvoiceProduct, InvoiceProduct.Parms>
    {
        string selInvoiceProducts = "spSelInvoiceProducts";
        public List<InvoiceProduct> Select(InvoiceProduct.Parms parms)
        {
            return ExecuteCommand(selInvoiceProducts, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@id", parms.InvoiceID)
							}, null);
        }

        public InvoiceProduct Update(InvoiceProduct item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public InvoiceProduct Insert(InvoiceProduct item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public InvoiceProduct Delete(InvoiceProduct item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }
}
