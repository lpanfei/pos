﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class CategoryDB : UploadableDBProviderBase<Category, Category.Parms>, IDataProvider<Category, Category.Parms>
    {
        public List<Category> Select(Category.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selCategories, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@cid", parms.ClientID)
							}, null);
        }

        public Category Update(Category item, object lockHolder)
        {
            int ID = (int)Update(item,StoredProcedures.uploadCategory, StoredProcedures.insUpdCategory, lockHolder);
            item.cat_Id = ID;
            return item;
        }

        public Category Insert(Category item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Category Delete(Category item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delCategory;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.cat_Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
