﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class StoreDB : DataProviderDBBase<Store, Store.Parms>, IDataProvider<Store, Store.Parms>
    {

        public List<Store> Select(Store.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selStores, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@cid", parms.ClientID)
							}, null);
        }

        public Store Update(Store item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.insUpdStore, lockHolder);
            item.sto_Id = id;
            return item;
        }

        public Store Insert(Store item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Store Delete(Store item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delStore;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.sto_Id));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
