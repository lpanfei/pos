﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;
using System.Data;
using System.Xml.Serialization;

namespace POS.Data.DBProvider
{
    public class InvoiceDB : UploadableDBProviderBase<Invoice, Invoice.Parms>, IDataProvider<Invoice, Invoice.Parms>, IInvoiceProvider
    {
        
        public List<Invoice> Select(Invoice.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selInvoice, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@sid", parms.StoreID),
                                new System.Data.SqlClient.SqlParameter("@cid", parms.ClientID),
                                new System.Data.SqlClient.SqlParameter("@invno", parms.InvoiceNo),
                                new System.Data.SqlClient.SqlParameter("@customer", string.IsNullOrEmpty(parms.Customer)?"":parms.Customer),
                                new System.Data.SqlClient.SqlParameter("@datefrom", parms.DateFrom==null?"1970/1/1":parms.DateFrom.Value.ToShortDateString()),
                                new System.Data.SqlClient.SqlParameter("@dateto", parms.DateTo==null?"1970/1/1":parms.DateTo.Value.ToShortDateString())
							}, null);
        }

        public Invoice Update(Invoice item, object lockHolder)
        {
            int id = (int)Update(item,StoredProcedures.uploadInvoice, StoredProcedures.insUpdInvoice, lockHolder);
            item.inv_Id = id;
            return item;
        }

        public Invoice Insert(Invoice item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Invoice Delete(Invoice item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public void CancelInvoice(int id,string reason)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.cancelInvoice;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", id));
            cmd.Parameters.Add(new SqlParameter("@reason", reason));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        //private void SendToWallet(Invoice item)
        //{
        //    SqlConnection sqlConn = new SqlConnection(item.WalletConnection);
        //    SqlCommand sqlCmd = sqlConn.CreateCommand();
        //    sqlCmd.CommandText = insUpdInvoice;
        //    sqlCmd.CommandType = CommandType.StoredProcedure;

        //    System.IO.StringWriter sw = new System.IO.StringWriter();
        //    try
        //    {
        //        XmlSerializer _xmlS = new XmlSerializer(typeof(Invoice));
        //        _xmlS.Serialize(sw, item);
        //        string xml = sw.GetStringBuilder().ToString();
        //        // remove the <?xml...> tag
        //        xml = System.Text.RegularExpressions.Regex.Replace(xml, "<[?]xml.*?>\r\n", "");
        //        sqlCmd.Parameters.AddWithValue("@xml", xml);
        //    }
        //    finally
        //    {
        //        sw.Close();
        //    }

        //    sqlCmd.CommandTimeout = 120; // Increase from default of 30 seconds
        //    sqlCmd.Parameters.Add("@RETVAL", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
        //    object ID = null;
        //    if (sqlCmd.Connection.State == ConnectionState.Closed) sqlCmd.Connection.Open();
        //    try
        //    {
        //        sqlCmd.ExecuteNonQuery();
        //        ID = sqlCmd.Parameters["@RETVAL"].Value;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;	// a convenient debugging point
        //    }
        //    finally
        //    {
        //        sqlConn.Close();
        //        sqlCmd.Dispose();
        //        sqlCmd = null;
        //        sqlConn = null;
        //    }
        //}


        public List<Invoice> GetWalletInvoices(string walletId)
        {
            return ExecuteCommand(StoredProcedures.selInvoicesByWallet, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@walletid", walletId)
							}, null);
        }


        public List<Invoice> GetSMSInvoice(string mobileno, string validationcode)
        {
            return ExecuteCommand(StoredProcedures.selInvoicesBySMS, new SqlParameter[]
							{
								new System.Data.SqlClient.SqlParameter("@mobileno", mobileno),
                                new System.Data.SqlClient.SqlParameter("@validationcode", validationcode)
							}, null);
        }
    }
}
