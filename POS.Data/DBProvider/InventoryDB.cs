﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class InventoryDB : UploadableDBProviderBase<Inventory, Inventory.Parms>, IDataProvider<Inventory, Inventory.Parms>
    {
        public List<Inventory> Select(Inventory.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selInventories, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@cid", parms.ClientID),
                                new System.Data.SqlClient.SqlParameter("@sid", parms.StoreID)
							}, null);
        }

        public Inventory Update(Inventory item, object lockHolder)
        {
            int ID = (int)Update(item, StoredProcedures.uploadInventory, StoredProcedures.insUpdInventory, lockHolder);
            item.ID = ID;
            return item;
        }

        public Inventory Insert(Inventory item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Inventory Delete(Inventory item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }
}
