﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class LogDB : DataProviderDBBase<Log, Log.Parms>, IDataProvider<Log, Log.Parms>
    {
        
        public List<Log> Select(Log.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selLogs, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@cid", parms.ClientID),
                                new System.Data.SqlClient.SqlParameter("@sid", parms.StoreID)
							}, null);
        }

        public Log Update(Log item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Log Insert(Log item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Log Delete(Log item, object lockHolder)
        {
            throw new NotImplementedException();
        }
    }
}
