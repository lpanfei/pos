﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class CustomerDB : UploadableDBProviderBase<Customer, Customer.Parms>, IDataProvider<Customer, Customer.Parms>
    {
        public List<Customer> Select(Customer.Parms parms)
        {
            if (parms.ById)
            {
                return ExecuteCommand(StoredProcedures.selCustomerByID, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@id", parms.ID)
							}, null);
            }
            else
            {
                return ExecuteCommand(StoredProcedures.selCustomers, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@cid", parms.ClientID)
							}, null);
            }
        }

        public Customer Update(Customer item, object lockHolder)
        {
            int id = (int)Update(item,StoredProcedures.uploadCustomer, StoredProcedures.insUpdCustomer, lockHolder);
            item.ID = id;
            return item;
        }

        public Customer Insert(Customer item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Customer Delete(Customer item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delCustomer;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.ID));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
