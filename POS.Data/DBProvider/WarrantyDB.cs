﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class WarrantyDB : UploadableDBProviderBase<Warranty, Warranty.Parms>, IDataProvider<Warranty, Warranty.Parms>
    {
        
        public List<Warranty> Select(Warranty.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selWarranties, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@cid", parms.ClientID),
                                new System.Data.SqlClient.SqlParameter("@sid", parms.StoreID)
							}, null);
        }

        public Warranty Update(Warranty item, object lockHolder)
        {
            int id = (int)Update(item, StoredProcedures.uploadWarranty, StoredProcedures.insUpdWarranty, lockHolder);
            item.ID = id;
            return item;
        }

        public Warranty Insert(Warranty item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Warranty Delete(Warranty item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delWarranty;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.ID));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
