﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Eidolon.Web.DA;
using System.Data.SqlClient;

namespace POS.Data.DBProvider
{
    public class SupplierDB : UploadableDBProviderBase<Supplier, Supplier.Parms>, IDataProvider<Supplier, Supplier.Parms>
    {
        
        public List<Supplier> Select(Supplier.Parms parms)
        {
            return ExecuteCommand(StoredProcedures.selSuppliers, new SqlParameter[]
							{
                                new System.Data.SqlClient.SqlParameter("@cid", parms.ClientID)
							}, null);
        }

        public Supplier Update(Supplier item, object lockHolder)
        {
            int id = (int)Update(item,StoredProcedures.uploadSupplier, StoredProcedures.insUpdSupplier, lockHolder);
            item.ID = id;
            return item;
        }

        public Supplier Insert(Supplier item, object lockHolder)
        {
            throw new NotImplementedException();
        }

        public Supplier Delete(Supplier item, object lockHolder)
        {
            SqlCommand cmd = GetConnection().CreateCommand();
            cmd.CommandText = StoredProcedures.delSupplier;
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            cmd.Parameters.Add(new SqlParameter("@id", item.ID));
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            finally
            {
                cmd.Connection.Close();
            }
            return item;
        }
    }
}
