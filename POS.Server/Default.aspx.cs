﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POSService;
using POS.Data;

namespace POS
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
        }

        protected void Register(object sender, EventArgs e)
        {
            Response.Redirect("RegisterationPage.aspx");
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            string username = txtUsername.Text.Trim();
            string password = txtPassword.Text.Trim();
            POSService.POSService pos = new POSService.POSService();
            LoginResult lr=pos.Login(username, password);
            //true if login successfully
            if (lr.Result)
            {
                GlobalCache.CurrentUser = lr.User;
                UploadConfig.IsOn = false;
                string url = "";
                switch (lr.User.UserType)
                {
                    case UserType.SystemAdmin:
                        break;
                    case UserType.StoreAdmin:
                        break;
                    case UserType.ClientAdmin:
                        url = "Dashboard.aspx";
                        break;
                    case UserType.User:
                        break;
                }
                if (!string.IsNullOrEmpty(url))
                {
                    Response.Redirect(url);
                }
            }
            else
            {
                windowManager.RadAlert("Login Failed! Please verify your username and password.", 300, 100, "Login Failed", "");
            }
        }
    }
}