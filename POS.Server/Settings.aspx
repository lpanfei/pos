﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Settings.aspx.cs" Inherits="POS.Settings" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function pageLoad(sender, args) {
            $("#syncbtn").click(function () { $('#btnUpload').trigger('click'); });
            $("#testbtn").click(function () {
                //$('#btnTestConnection').trigger('click'); 
                __doPostBack($('#btnTestConnection').attr("name"), '')
            });
        }
   
    </script>
    <style>
        .tr-header
        {
            border-bottom: 1px solid #eee;
            font-size: 13px;
            font-weight: bold;
        }
        
        .tr-header td
        {
            padding-top: 25px;
            padding-bottom: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="mainheader">
        <div class="contextual">
            <div style="height: 10px; width: 85px">
            </div>
        </div>
        <div class="subheader">
            <h2 class="pagetitle icon-settings">
                Settings</h2>
        </div>
    </div>

    <div class="md-modal md-effect-1" id="modal-form">
        <div class="md-content">
            <h3>
                Settings Form</h3>
            <div>
                <table class="form-table">
                    <tr class="tr-header">
                        <td colspan="2" style="padding-top: 5px;">
                            <label>
                                Discount Value Settings</label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Retailer:
                        </td>
                        <td>
                            <asp:TextBox ID="txtRetailer" runat="server"></asp:TextBox>
                            %
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Wholesaler:
                        </td>
                        <td>
                            <asp:TextBox ID="txtWholesaler" runat="server"></asp:TextBox>
                            %
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Export:
                        </td>
                        <td>
                            <asp:TextBox ID="txtExport" runat="server"></asp:TextBox>
                            %
                        </td>
                    </tr>
                    <tr class="tr-header">
                        <td colspan="2">
                            <label>
                                Server Database Connection Settings</label>
                                
                            <a href="#" id="testbtn" style="margin-right: 0px; float: right" title="Testing Connection">
                                <i class="c-icon-test"></i></a><a href="#" id="syncbtn" style="margin-right: 30px;
                                    float: right" title="Immediate Sync"><i class="c-icon-sync"></i></a>
                                    
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Server Name:
                        </td>
                        <td>
                            <asp:TextBox ID="txtServerName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Database Name:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDatabaseName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Username:
                        </td>
                        <td>
                            <asp:TextBox ID="txtUsername" runat="server" Style="float: left; margin-right: 10px;"></asp:TextBox>

                            <asp:Button ID="btnUpload" runat="server" Text="Immediate Sync" Width="0px" Height="0px"
                                OnClick="btnUpload_Click" UseSubmitBehavior="false" class="btn btn-large btn-block btn-info"
                                ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Password:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPassword" runat="server" Style="float: left; margin-right: 10px;"></asp:TextBox>

                            <asp:Button ID="btnTestConnection" runat="server" Text="Testing Connection" OnClick="btnTestConnection_Click"
                                Width="0px" Height="0px" UseSubmitBehavior="false" class="btn btn-large btn-block btn-info"
                                ClientIDMode="Static" />
                                
                        </td>
                    </tr>
                    <tr class="tr-header">
                        <td>
                            <label>
                                Sync Time Settings</label>
                        </td>
                        <td align="right">
                            <%--<a href="#" id="addfiled"><i class="c-icon-add"></i></a>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Sync Time:
                        </td>
                        <td>
                            <asp:DropDownList ID="dropUploadTime" runat="server">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr class="tr-header">
                        <td>
                            <label>
                                Other Settings</label>
                        </td>
                        <td align="right">
                            <%--<a href="#" id="addfiled"><i class="c-icon-add"></i></a>--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Invoice Number Prefix:
                        </td>
                        <td>
                            <asp:TextBox ID="txtPrefix" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            GST Amount:
                        </td>
                        <td>
                            <asp:TextBox ID="txtGST" runat="server"></asp:TextBox>%
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Invoice Header:
                        </td>
                        <td>
                            <asp:TextBox ID="txtInvoiceHeader" runat="server" Rows="5" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Invoice Footer:
                        </td>
                        <td>
                            <asp:TextBox ID="txtInvoiceFooter" runat="server" Rows="5" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                   
                </table>
            </div>
            <div class="md-footer">

            <asp:Button ID="btnSave" runat="server" Text="Save All" OnClick="btnSave_Click" UseSubmitBehavior="false"
                                class="btn btn-large btn-block btn-info btn-center" Height="30" Width="80" />
         
            
            </div>
        </div>
    </div>

</asp:Content>
