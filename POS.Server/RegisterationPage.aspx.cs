﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using POS.Data;

namespace POS.Server
{
    public partial class RegisterationPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
              
            }
            if (string.IsNullOrEmpty(password.Text))
            {
                password.Attributes.Add("value", password.Attributes["value"]);
            }
            else
            {
                password.Attributes.Add("value", password.Text);
            }
            if (string.IsNullOrEmpty(confpassword.Text))
            {
                confpassword.Attributes.Add("value", confpassword.Attributes["value"]);
            }
            else
            {
                confpassword.Attributes.Add("value", confpassword.Text);
            }

        }

        protected void Next(object sender, EventArgs e)
        {
            if (next.Text == "Next")
            {
                if (currentpage.Attributes["value"] == "1" && ValidateFirstStep())
                {
                    next.Text = "Next";
                    secondStep.Visible = true;
                    firstStep.Visible = false;
                    thirdStep.Visible = false;
                    back.CssClass = "btn btn-primary";
                    back.Enabled = true;
                    currentpage.Attributes["value"] = "2";
                }
                else if (currentpage.Attributes["value"] == "2" && ValidateSecondStep())
                {
                    next.Text = "Submit";
                    secondStep.Visible = false;
                    firstStep.Visible = false;
                    thirdStep.Visible = true;
                    back.CssClass = "btn btn-primary";
                    back.Enabled = true;
                    currentpage.Attributes["value"] = "3";
                    
                }
            }
            else if(next.Text=="Submit")
            {
                if (ValidateThirdStep())
                {
                    Register();
                }
            }
        }

        bool ValidateFirstStep()
        {
            if (string.IsNullOrEmpty(clientname.Text.Trim()))
            {
                WindowManager.RadAlert("Client name is mandatory", 300, 100, "Validation", "");
                return false;
            }

            if (string.IsNullOrEmpty(clientcode.Text.Trim()))
            {
                WindowManager.RadAlert("Client code is mandatory", 300, 100, "Validation", "");
                return false;
            }

            return true;
        }

        bool ValidateSecondStep()
        {
            if (string.IsNullOrEmpty(username.Text.Trim()))
            {
                WindowManager.RadAlert("User name is mandatory", 300, 100, "Validation", "");
                return false;
            }
            if (string.IsNullOrEmpty(password.Text.Trim()))
            {
                WindowManager.RadAlert("Password is mandatory", 300, 100, "Validation", "");
                return false;
            }
            if (confpassword.Text.Trim() != password.Text.Trim())
            {
                WindowManager.RadAlert("Password doesnt match", 300, 100, "Validation", "");
                return false;
            }
            return true;
        }

        bool ValidateThirdStep()
        {
            if (string.IsNullOrEmpty(storename.Text.Trim()))
            {
                WindowManager.RadAlert("Store name is mandatory", 300, 100, "Validation", "");
                return false;
            }

            return true;
        }

        protected void Back(object sender, EventArgs e)
        {
            if (currentpage.Attributes["value"] == "3" && ValidateFirstStep())
            {
                next.Text = "Next";
                secondStep.Visible = true;
                firstStep.Visible = false;
                thirdStep.Visible = false;
                back.CssClass = "btn btn-primary";
                back.Enabled = true;
                currentpage.Attributes["value"] = "2";
            }
            else if (currentpage.Attributes["value"] == "2" && ValidateSecondStep())
            {
                next.Text = "Next";
                secondStep.Visible = false;
                firstStep.Visible = true;
                thirdStep.Visible = false;
                back.CssClass = "btn";
                back.Enabled = false;
                currentpage.Attributes["value"] = "1";
            }
        }

        void Register()
        {
            Registeration r = new Registeration();
            r.Client = new Client()
            {
                Code=clientcode.Text.Trim(),
                Name=clientname.Text.Trim(),
                Address=address.Text.Trim(),
                Phone=phone.Text.Trim(),
                Suburb = clientsuburb.Text.Trim(),
                State=state.Text.Trim(),
                Email=email.Text.Trim()
            };
            r.User = new User()
            {
                usr_Name = username.Text.Trim(),
                usr_Password = password.Attributes["value"],
                usr_FirstName = firstname.Text.Trim(),
                usr_LastName=lastname.Text.Trim(),
                usr_Address=useraddress.Text.Trim(),
                usr_Email=useremail.Text.Trim(),
                usr_MoblePhone=usermobile.Text.Trim(),
                usr_Suburb=usersuburb.Text.Trim(),
                usr_State=userstate.Text.Trim(),
                usr_Phone=userphone.Text.Trim(),
                usr_Postcode=userpostcode.Text.Trim()
            };
            r.Stores = new List<Store>();
            r.Stores.Add(new Store() { 
                sto_Name=storename.Text.Trim(),
                sto_Address=storeaddress.Text.Trim(),
                sto_Suburb=suburb.Text.Trim(),
                sto_Postcode=postcode.Text.Trim(),
                sto_Phone=storephone.Text.Trim(),
                sto_MobilePhone=mobile.Text.Trim(),
                sto_Fax=fax.Text.Trim(),
                sto_Email=storeemail.Text.Trim(),
                sto_Note=note.Text.Trim()
            });
            POSService.POSService service = new POSService.POSService();
            POSService.OperationResult os= service.Register(r);
            if (os.Result)
            {
                WindowManager.RadAlert("Registeration is completed successfully. Please logon to the system using your registeration now.", 400, 100, "Register successfully", "redirecttologin"); 
            }
        }
    }
}