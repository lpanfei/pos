﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="WarrantyList.aspx.cs" Inherits="POS.WarrantyList" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script language="javascript" type="text/javascript" src="My97DatePicker/WdatePicker.js"></script>
<script type="text/javascript">

    var previous = null;
    function pageLoad(sender, args) {
        $(document).ready(function () {
            $(".list tr").click(function () {

                var current = $(this).find(".buttons div").eq(0);
                if ($(previous).is(current))
                    return;
                current.toggle("slow");
                if (previous != null) {
                    $(previous).toggle("slow", function () { });
                }
                previous = current;
            }
        );

            if ($("#inpProductID").val() != "-1") {
                $("#dropProduct").val($("#inpProductID").val());
            }
        });
    }

    function confirmclosed(sender) {
        if (sender) {
            $("#delBtn").trigger("click");
        }
    }
    </script>
    <style type="text/css">
        input[type=submit]
        {
            cursor: pointer;
        }
         .displaynone
        {
            display:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
<ContentTemplate>

<div class="mainheader">
        <div class="contextual">
             <asp:Panel ID="listbuttons" runat="server">
                        <div style="height: 10px; width: 75px">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="addLink"
                                Text="Add" OnClick="AddWarranty" />
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="formbuttons" runat="server" Visible="false">
                        <div style="height: 10px; width: 75px">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                                Text="Back" OnClick="BackToList" />
                        </div>
                    </asp:Panel> 
         </div>
        <div class="subheader">
            <h2 class="icon-warranty pagetitle"><asp:Literal ID="ltlPageTitle" Text="Warranty List" runat="server"></asp:Literal></h2>
        </div>
    </div>

  <asp:Panel ID="listpanel" runat="server">
   <asp:Repeater ID="rptList" runat="server" onitemcommand="rptList_ItemCommand">
        <HeaderTemplate>
            <table class="list">
            <thead>
                <tr>
                    <th>Invoice No.</th>
                    <th>Item Description</th>
                    <th>Qty</th>
                    <th>Date</th>
                    <th>Comments</th>
                    <th>Resolution</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
        </HeaderTemplate>
        <ItemTemplate>
                <tr>
                    <td align="center"><%# Eval("InvoiceNo")%></td>
                    <td align="center"><%# Eval("ProductName")%></td>
                    <td align="center"><asp:Label ID="lblQty" runat="server" Text='<%# Eval("Quantity")%>' ></asp:Label></td>
                    <td align="center"><asp:Label id="lblDate" runat="server" Text='<%# Eval("Date")%>'></asp:Label></td>
                    <td align="center"><asp:Label id="lblComment" runat="server" Text='<%# Eval("Comment")%>'></asp:Label></td>
                    <td align="center"><asp:Label id="lblResolution" runat="server" Text='<%# Eval("Resolution")%>'></asp:Label></td>
                    <td class="buttons">
                    <div style="display:none;">
                       <%-- <a href='<%# "WarrantyEdit.aspx?id="+Eval("ID") %>' class="icon icon-edit">Edit</a>--%>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandName="ItemEdit" CommandArgument='<%# Eval("ID") %>' class="icon icon-edit"></asp:LinkButton>
                        &nbsp;&nbsp;
                        <asp:LinkButton ID="lblDel" runat="server" Text="Delete" CommandName="ItemDel" CommandArgument='<%# Eval("ID") %>' class="icon icon-del"></asp:LinkButton>
                        <asp:Label ID="lblProductID" runat="server" Text='<%# Eval("ProductID")%>' Visible="false"></asp:Label>
                        <asp:Label ID="lblInvoiceID" runat="server" Text='<%# Eval("InvoiceID")%>' Visible="false"></asp:Label>
                        <asp:Label ID="lblId" runat="server" Text='<%# Eval("ID")%>' Visible="false"></asp:Label></div>
                    </td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
        </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" 
        onpagechanged="AspNetPager1_PageChanged" ClientIDMode="Static" PageSize="1">
    </webdiyer:AspNetPager>
    </asp:Panel>
    <asp:Panel ID="formpanel" runat="server" Visible="false">
     <div class="md-modal md-effect-1 md-show" id="modal-form">
			<div class="md-content">
				<h3>Warranty Form</h3>
				<div>
    <table class="form-table">

        <tr>
            <td class="form-label">Invoice No.:</td>
            <td><asp:DropDownList ID="dropInvoice" runat="server" DataTextField="inv_No" DataValueField="inv_Id"  AutoPostBack="True" OnSelectedIndexChanged="InvoiceChanged"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="form-label">Item Description:</td>
            <td><asp:DropDownList ID="dropProduct" runat="server" DataTextField="ip_Description" DataValueField="ip_ProductID" ClientIDMode="Static"></asp:DropDownList></td>
        </tr>
        <tr>
            <td class="form-label">Qty:</td>
            <td><asp:TextBox ID="txtQty" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Date:</td>
            <td><asp:TextBox ID="txtDate" runat="server" CssClass="Wdate"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Comments:</td>
            <td><asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Resolution:</td>
            <td><asp:TextBox ID="txtResolution" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox></td>
        </tr>
        <tr  style="display:none;">
            <td colspan="2" align="center">
                <input type="hidden" id="inpProductID" runat="server" clientidmode="Static" value="-1" />
                <asp:Label ID="lblInvoiceID"  runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lblProductID"  runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lblOldQty"  runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lblId"  runat="server" Visible="false"></asp:Label>
            </td>
        </tr>
    </table>
    </div>
    <div class="md-footer"><asp:Button ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click" UseSubmitBehavior="false"  class="btn btn-large btn-block btn-info btn-center" Width="75" Height="30"/></div>
    </div></div>
    </asp:Panel>
 <eid:RadWindowManager runat="server" ID="WindowManager"></eid:RadWindowManager>

    <input type="hidden" id="delid" runat="server" clientidmode="Static" value="" />
    <asp:Button ID="delBtn" runat="server" Width="0" Height="0" CssClass="displaynone" OnClick="Delete" ClientIDMode="Static"></asp:Button>
    
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>

