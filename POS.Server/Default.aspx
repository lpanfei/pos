﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="POS.Default" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Welcome to DST Solutions POS System</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="login/css/style.css" />
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script src="login/js/jquery.infieldlabel.js"></script>
    <link rel="stylesheet" type="text/css" href="window/css/component.css" />
    <script src="login/js/modernizr.custom.js"></script>
    <script type="text/javascript" charset="utf-8">
        function pageLoad(sender, args) {
            var top = Math.max($(window).height() / 2 - $(".container2")[0].offsetHeight / 2 - $(window).height() * 0.15, 0);
            var left = Math.max($(window).width() / 2 - $(".container2")[0].offsetWidth / 2, 0);
            $(".container2").css('top', top + "px");
            $(".container2").css('left', left + "px");
            $(".container2").css('position', 'fixed');

            $(window).resize(function () {
                var top = Math.max($(window).height() / 2 - $(".container2")[0].offsetHeight / 2 - $(window).height() * 0.15, 0);
                var left = Math.max($(window).width() / 2 - $(".container2")[0].offsetWidth / 2, 0);
                $(".container2").css('top', top + "px");
                $(".container2").css('left', left + "px");
                $(".container2").css('position', 'fixed');
            });
        }

        $(function () {

            var top = Math.max($(window).height() / 2 - $(".container2")[0].offsetHeight / 2 - $(window).height() * 0.15, 0);
            var left = Math.max($(window).width() / 2 - $(".container2")[0].offsetWidth / 2, 0);
            $(".container2").css('top', top + "px");
            $(".container2").css('left', left + "px");
            $(".container2").css('position', 'fixed');

            $(window).resize(function () {
                var top = Math.max($(window).height() / 2 - $(".container2")[0].offsetHeight / 2 - $(window).height() * 0.15, 0);
                var left = Math.max($(window).width() / 2 - $(".container2")[0].offsetWidth / 2, 0);
                $(".container2").css('top', top + "px");
                $(".container2").css('left', left + "px");
                $(".container2").css('position', 'fixed');
            });
        });
	</script>
    <script>
        function showSuccess(message, transferTo) {
            $("#modal-success").remove();
            $("body").append($("<div class='md-modal-body md-effect-1 md-show' id='modal-success'><div class='md-content'><h3>Success</h3><div ><p><img style='vertical-align:middle;margin-right:20px;' src='images/success.png' alt=''/>" + message + "</p><input type='button' value='Close' class='md-close btn btn-large btn-block btn-info' style='width:100px;display:block;margin:0 auto;'/></div></div></div>"));
            $("#modal-success").find(".md-close").click(function (ev) {
                ev.stopPropagation(); $("#modal-success").removeClass("md-show");
                if (transferTo != "")
                    location.href = transferTo;
            });
        }
        function showError(message, transferTo) {
            $("#modal-error").remove();
            $("body").append($("<div class='md-modal-body md-effect-1 md-show' id='modal-error'><div class='md-content'><h3>Error</h3><div ><p><img style='vertical-align:middle;margin-right:20px;' src='images/error.png' alt=''/>" + message + "</p><input type='button' value='Close' class='md-close btn btn-large btn-block btn-info' style='width:100px;display:block;margin:0 auto;'/></div></div></div>"));
            $("#modal-error").find(".md-close").click(function (ev) {
                ev.stopPropagation(); $("#modal-error").removeClass("md-show");
                if (transferTo != "")
                    location.href = transferTo;
            });
        }
        function showWarning(message, transferTo) {
            $("#modal-warning").remove();
            $("body").append($("<div class='md-modal-body md-effect-1 md-show' id='modal-warning'><div class='md-content'><h3>Warning</h3><div ><p><img style='vertical-align:middle;margin-right:20px;' src='images/warning.png' alt=''/>" + message + "</p><input type='button' value='Close' class='md-close btn btn-large btn-block btn-info' style='width:100px;display:block;margin:0 auto;'/></div></div></div>"));
            $("#modal-warning").find(".md-close").click(function (ev) {
                ev.stopPropagation(); $("#modal-warning").removeClass("md-show");
                if (transferTo != "")
                    location.href = transferTo;
            });
        }
    </script>
   
    <!--[if lte IE 6]>
		<style type="text/css" media="screen">
			form label {
					background: #fff;
			}
		</style>
	<![endif]-->
    <!--[if lte IE 7]><style>.main{display:none;} .support-note .note-ie{display:block;}</style><![endif]-->
</head>
<body>
<form id="form1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
                                    </asp:ScriptManager>
                                    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                                    <ContentTemplate>
<div class="container2">
        <header>
			
				 <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/LOGO.png" />
           
				
			</header>
        <section class="main">
				<div class="form-1" >
					<p class="field">
                       
						<asp:TextBox ID="txtUsername" runat="server"  name="txtUsername" ClientIDMode="Static" ></asp:TextBox>
						<i class="icon-user icon-large"></i>
					</p>
                    <hr style="border:1px dashed #f4f4f4"/>
						<p class="field">
                           
							<asp:TextBox ID="txtPassword" runat="server" TextMode="Password" name="txtPassword" ClientIDMode="Static"></asp:TextBox>
							<i class="icon-lock icon-large"></i>
					</p>
					<p class="submit">
                    
                     <asp:Button ID="btnLogin" runat="server" Text="Sign In" 
                        onclick="btnLogin_Click" UseSubmitBehavior="false" class="inputbutton"  style="margin-bottom:20px;height:45px;float:left;margin-right:10px;"/>
						 <asp:Button ID="Button1" runat="server" Text="Register" 
                        onclick="Register" UseSubmitBehavior="false" class="inputbutton"  style="margin-bottom:20px;height:45px;"/>
            <span style="opacity:0.7;">Welcome to DST Solutions POS System</span>
            
					</p>
				</div>
			</section>
            
    </div>
    
    <div class='md-overlay'></div>
    <script>
        // this is important for IEs
        var polyfilter_scriptpath = '/js/';
		</script>
           <eid:RadWindowManager ID="windowManager" runat="server" Skin="Metro"></eid:RadWindowManager>
                    </ContentTemplate>
                    </asp:UpdatePanel>
  </form>
</body>
</html>
