﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;
using Telerik.Web.UI;
using System.IO;

namespace POS
{
    public partial class ProductList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialize();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int categoryId = Convert.ToInt32(dropCategory.SelectedValue);
            string name = string.Empty;
            string serialNo = string.Empty;
            switch (dropField.SelectedValue)
            {
                case "1":
                    name = txtKey.Text.Trim();
                    break;
                case "2":
                    serialNo = txtKey.Text.Trim();
                    break;
            }

            GlobalCache.RefreshProducts(categoryId, name, serialNo);
            Bind();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            rptList.DataSource = null;
            rptList.DataSource = GlobalCache.Products.TakeFrom((currentPage - 1) * pageSize, pageSize);
            rptList.DataBind();
        }

        protected void Delete(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(delid.Attributes["value"]);
            POSService.POSService service = new POSService.POSService();
            service.DeleteProduct(new Product() { pro_Id = id });
            GlobalCache.RefreshProducts(0, "", "");
            GlobalCache.RefreshInventories(0, "", "", 0);
            Initialize();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                delid.Attributes["value"] = e.CommandArgument.ToString();
                WindowManager.RadConfirm("Are you sure to delete the item?", "confirmclosed", 300, 100, null, "Delete Product");
            }
            else if (e.CommandName == "ItemEdit")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                List<Product> products = GlobalCache.Products;
                if (products != null)
                {
                    Product p = products.FirstOrDefault(p2 => p2.pro_Id == id);
                    if (p != null)
                    {
                        pageform.Visible = true;
                        pagelist.Visible = false;
                        ltlPageTitle.Text = "Products / Edit ("+p.pro_Name+")";
                        addLink.Text = "Back";
                        lblId.Text = p.pro_Id.ToString();
                        if (p.Category != null)
                        {
                            foreach (ListItem obj in dropCategory2.Items)
                            {
                                if (obj.Value == p.Category.cat_Id.ToString())
                                {
                                    obj.Selected = true;
                                }
                                else
                                {
                                    obj.Selected = false;
                                }
                            }
                        }
                        if (p.SupplierID != 0)
                        {
                            foreach (ListItem obj in dropSupplier.Items)
                            {
                                if (obj.Value == p.SupplierID.ToString())
                                {
                                    obj.Selected = true;
                                }
                                else
                                {
                                    obj.Selected = false;
                                }
                            }
                        }
                       
                        txtName.Text = p.pro_Name;
                        txtNo.Text = p.pro_No;
                        txtPrice.Text = p.pro_price.ToString();
                        txtCost.Text = p.cost.ToString();
                        Repeater1.DataSource = null;
                        Repeater1.DataSource = p.Pictures;
                        Repeater1.DataBind();
                        if (p.Fields != null)
                        {
                            List<Field> fields = new List<Field>();
                            int i = 1;
                            foreach (Field f in p.Fields)
                            {
                                fields.Add(new Field() { fie_Name = f.fie_Name, fie_Value = f.fie_Value, fie_Id = i });
                                i++;
                            }
                            rptDynamicFields.DataSource = null;
                            rptDynamicFields.DataSource = fields;
                            rptDynamicFields.DataBind();
                        }
                        foreach (RepeaterItem ri in rptRelateProduct.Items)
                        {
                            Label lblRelateID = ri.FindControl("lblID") as Label;
                            int pid = Convert.ToInt32(lblRelateID.Text);
                            if (p.pro_Id == pid)
                            {
                                ri.Visible = false;
                                break;
                            }
                        }
                        if (p.Relates != null)
                        {
                            foreach (RepeaterItem ri in rptRelateProduct.Items)
                            {
                                Label lblRelateID = ri.FindControl("lblID") as Label;
                                int pid = Convert.ToInt32(lblRelateID.Text);
                                if (p.Relates.Any(r => r.pro_Id == pid))
                                {
                                    CheckBox chkSelect = ri.FindControl("chkSelect") as CheckBox;
                                    chkSelect.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        void Bind()
        {
            AspNetPager1.RecordCount = GlobalCache.Products.Count;
            
            rptList.DataSource = null;
            rptList.DataSource = GlobalCache.Products.TakeFrom(0,this.AspNetPager1.PageSize);
            AspNetPager1.CurrentPageIndex = 1;
            AspNetPager1.GoToPage(this.AspNetPager1.CurrentPageIndex);
            rptList.DataBind();
        }

        void Initialize()
        {
           
            Bind();
            List<Product> plist = new List<Product>();
            
            GlobalCache.Products.ForEach(p => plist.Add(p));
            
            rptRelateProduct.DataSource = null;
            rptRelateProduct.DataSource = plist;
            rptRelateProduct.DataBind();

            rptDynamicFields.DataSource = null;
            rptDynamicFields.DataSource=new List<Field>();
            rptDynamicFields.DataBind();

            BindSuppliers();
            BindCategories();
            txtCost.Text = txtName.Text = txtNo.Text = txtPrice.Text = string.Empty;
            ltlPageTitle.Text = "Products";
            addLink.Text = "Add";
            pagelist.Visible = true;
            pageform.Visible = false;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            #region validations
            #endregion

            #region upload pictures
            int pid = Convert.ToInt32(lblId.Text);
            List<string> pictures = new List<string>();
            List<ProductPicture> pps = new List<ProductPicture>();
            foreach (RepeaterItem item in Repeater1.Items)
            {
                ProductPicture pp = new ProductPicture()
                {
                    ProductID = pid,
                    Name = (item.FindControl("lblName") as Label).Text,
                    Url = (item.FindControl("img") as Image).ImageUrl,
                    IsTemp = (item.FindControl("lblTemp") as Label).Text.Equals("true", StringComparison.CurrentCultureIgnoreCase) ? true : false
                };
                if (!pp.IsTemp)
                {
                    pictures.Add((item.FindControl("lblName") as Label).Text);
                }
                pps.Add(pp);
            }

            List<string> delpictures = new List<string>();
            
            if (pid > 0)
            {
                List<Product> products = GlobalCache.Products;
                if (products != null)
                {
                    Product prod = products.FirstOrDefault(p2 => p2.pro_Id == pid);
                    if (prod != null && prod.Pictures != null)
                    {
                        foreach (ProductPicture pp in prod.Pictures)
                        {
                            if (!pictures.Any(pic => pic == pp.Name))
                            {
                                delpictures.Add(pp.Name);
                            }
                        }
                    }
                }
            }

            string upload = Server.MapPath("~/Upload");
            string temp = Server.MapPath("~/Upload/Temp");
            //foreach (UploadedFile file in fileupload.UploadedFiles)
            //{

            //    if (!CheckExtension(file.FileName))
            //    {
            //        WindowManager.RadAlert("You can only upload .jpg/.jpeg/.gif/.png/.bmp file of type.", 400, 200, "Upload Picture", "");
            //        return;
            //    }

            //    if (file.ContentLength > 1024 * 100)
            //    {
            //        WindowManager.RadAlert("You can only update picture less than 100kb.", 400, 200, "Upload Picture", "");
            //        return;
            //    }
            //}

            if (pps.Any(p2 => p2.IsTemp))
            {
                foreach (ProductPicture file in pps.Where(p2 => p2.IsTemp))
                {
                    string filename = DateTime.Now.ToString("yyyMMddHHmmff") + file.Name;
                    if (File.Exists(Path.Combine(temp, file.Name)))
                    {
                        File.Copy(Path.Combine(temp, file.Name), Path.Combine(upload, filename), true);
                        File.Delete(Path.Combine(temp, file.Name));
                    }
                    pictures.Add(filename);
                }
            }
            #endregion

            #region create/update product
            Product p = new Product();
            p.pro_Id = Convert.ToInt32(lblId.Text);
            p.Category = new Category() { cat_Id=Convert.ToInt32(dropCategory2.SelectedValue)};
            p.SupplierID = Convert.ToInt32(this.dropSupplier.SelectedValue);
            p.pro_Name = txtName.Text.Trim();
            p.pro_No = txtNo.Text.Trim();
            p.pro_ClientID = this.UserClientID;
            if (!string.IsNullOrEmpty(txtPrice.Text.Trim()))
            {
                p.pro_price = Convert.ToDecimal(txtPrice.Text.Trim());
            }

            if (!string.IsNullOrEmpty(txtCost.Text.Trim()))
            {
                p.cost = Convert.ToDecimal(txtCost.Text.Trim());
            }
            p.Relates = new List<Product>();
            foreach (RepeaterItem ri in rptRelateProduct.Items)
            {
                CheckBox chkSelect = ri.FindControl("chkSelect") as CheckBox;
                Label lblRelateID = ri.FindControl("lblID") as Label;
                if (chkSelect.Checked)
                {
                    p.Relates.Add(new Product() { pro_Id=Convert.ToInt32(lblRelateID.Text.Trim())});
                }
            }
            p.Fields = new List<Field>();
            foreach (RepeaterItem ri in rptDynamicFields.Items)
            {
                string name = (ri.FindControl("lblName") as TextBox).Text.Trim();
                string value = (ri.FindControl("txtValue") as TextBox).Text.Trim();
                p.Fields.Add(new Field() { fie_Name = name, fie_Value = value });
            }
            p.Pictures = new List<ProductPicture>();
            foreach (string s in pictures)
            {
                p.Pictures.Add(new ProductPicture() { Name=s});
            }

            try
            {
                POSService.POSService pos = new POSService.POSService();
                pos.AddProduct(p);
                GlobalCache.RefreshProducts(0, "", "");
                GlobalCache.RefreshInventories(0, "", "", 0);
            }
            catch (Exception ex)
            {
                DeleteUnusedPictures(pictures);
                WindowManager.RadAlert("Create product failed: " + ex.Message, 600, 300, "Product Error", "");
                return;
            }
            #endregion
            Initialize();
        }

        void DeleteUnusedPictures(List<string> pictures)
        {
            try
            {
                string path = Server.MapPath("~/Upload");
                if (!Directory.Exists(path))
                {
                    return;
                }

                foreach (string filename in pictures)
                {
                    if (File.Exists(Path.Combine(path, filename)))
                    {
                        File.Delete(Path.Combine(path, filename));
                    }
                }
            }
            catch
            {
            }

        }

        protected void btnAddField_Click(object sender, EventArgs e)
        {
        }

        protected void rptDynamicFields_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                List<Field> fields = new List<Field>();
                int index = Convert.ToInt32(e.CommandArgument);
                int i = 1;
                bool check=false;
                foreach (RepeaterItem ri in rptDynamicFields.Items)
                {
                    if (!check && index == i)
                    {
                        check = true;
                        continue;
                    }
                    string name = (ri.FindControl("lblName") as TextBox).Text.Trim();
                    string value = (ri.FindControl("txtValue") as TextBox).Text.Trim();
                    fields.Add(new Field() { fie_Name = name, fie_Value = value, fie_Id = i });
                    i++;
                }
                rptDynamicFields.DataSource = null;
                rptDynamicFields.DataSource = fields;
                rptDynamicFields.DataBind();
            }
        }

        protected void dlPictureList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            List<ProductPicture> pps = new List<ProductPicture>();
            if (e.CommandName == "ItemDel")
            {
                foreach (RepeaterItem item in Repeater1.Items)
                {

                    if (item != e.Item)
                    {
                        pps.Add(new ProductPicture() { Name = (item.FindControl("lblName") as Label).Text });
                    }
                }

                if ((e.Item.FindControl("lblTemp") as Label).Text.Equals("true", StringComparison.CurrentCultureIgnoreCase))
                {
                    string path = Server.MapPath("~/Upload/Temp");
                    string file = Path.Combine(path, (e.Item.FindControl("lblName") as Label).Text);
                    if (File.Exists(file))
                    {
                        File.Delete(file);
                    }
                }
            }
            Repeater1.DataSource = null;
            Repeater1.DataSource = pps;
            Repeater1.DataBind();
        }
 

        protected void AddDynamicFields(object sender, EventArgs e)
        {
            List<Field> fields = new List<Field>();
            int i = 1;
            foreach (RepeaterItem ri in rptDynamicFields.Items)
            {
                string name=(ri.FindControl("lblName") as TextBox).Text.Trim();
                string value = (ri.FindControl("txtValue") as TextBox).Text.Trim();
                fields.Add(new Field() { fie_Name=name,fie_Value=value,fie_Id=i});
                i++;
            }
            fields.Add(new Field() { fie_Id=i});
            rptDynamicFields.DataSource = null;
            rptDynamicFields.DataSource = fields;
            rptDynamicFields.DataBind();
        }

        void BindCategories()
        {
           
            List<Category> cats = GlobalCache.Categories.GetHiarachy();
            List<Category> cats2 = new List<Category>();
            cats.ForEach(c => cats2.Add(c));
            cats.Insert(0, new Category() { cat_Id = 0, cat_Name = "-- All Category --" });
            dropCategory.DataSource = cats;
            dropCategory.DataBind();
            dropCategory2.DataSource = cats2;
            dropCategory2.DataBind();
        }

        void BindSuppliers()
        {
            dropSupplier.DataSource =GlobalCache.Suppliers;
            dropSupplier.DataBind();
        }

        protected void AddProduct(object sender, EventArgs e)
        {
            if (addLink.Text == "Add")
            {
                lblId.Text = "0";
                pageform.Visible = true;
                pagelist.Visible = false;
                ltlPageTitle.Text = "Products / Add";
                addLink.Text = "Back";
            }
            else
            {
                Initialize();
            }
        }

        private bool CheckExtension(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return false;
            }

            bool flag = false;
            string extension = fileName.Substring(fileName.LastIndexOf('.')).ToLower();
            string[] allowExtension = { ".jpg", ".jpeg", ".gif", ".png", ".bmp" };

            for (int i = 0; i < allowExtension.Length; i++)
            {
                if (allowExtension[i] == extension)
                {
                    flag = true;
                    break;
                }
            }

            return flag;
        }

        protected void ImageUploaded(object sender, FileUploadedEventArgs e)
        {
            int pid = Convert.ToInt32(lblId.Text);
            List<ProductPicture> pps = new List<ProductPicture>();

            foreach (RepeaterItem item in this.Repeater1.Items)
            {
                pps.Add(new ProductPicture()
                {
                    ProductID = pid,
                    Name = (item.FindControl("lblName") as Label).Text,
                    Url = (item.FindControl("img") as Image).ImageUrl,
                    IsTemp = (item.FindControl("lblTemp") as Label).Text.Equals("true", StringComparison.CurrentCultureIgnoreCase) ? true : false
                });
            }

            pps.Add(new ProductPicture()
            {
                ProductID = pid,
                Name = e.File.FileName,
                Url = HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/Temp/" + e.File.FileName),
                IsTemp = true
            });


            Repeater1.DataSource = null;
            Repeater1.DataSource = pps;
            Repeater1.DataBind();
        }
    }
}