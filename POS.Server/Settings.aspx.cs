﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;
using System.Data.SqlClient;
using System.Data;

namespace POS
{
    public partial class Settings :BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialize();
            }
        }

        void Initialize()
        {
            for (int i = 0; i < 24; i++)
            {
                ListItem li = new ListItem();
                li.Text = Convert.ToDecimal(i).ToString("00") + ":00";
                li.Value = li.Text;
                dropUploadTime.Items.Add(li);
            }

            List<Setting> settings = Setting.Select(new Setting.Parms() { ClientID=this.UserClientID,StoreID=this.UserStoreID});
            if (settings.Any())
            {
                txtRetailer.Text = settings[0].RetailerDiscount.ToString();
                txtWholesaler.Text = settings[0].WholesalerDiscount.ToString();
                txtExport.Text = settings[0].ExportDiscount.ToString();
                txtServerName.Text = settings[0].ServerName;
                txtDatabaseName.Text = settings[0].DatabaseName;
                txtPassword.Text = settings[0].Password;
                txtUsername.Text = settings[0].Username;
                txtInvoiceFooter.Text = settings[0].InvoiceFooter;
                txtInvoiceHeader.Text = settings[0].InvoiceHeader;
                txtGST.Text = settings[0].GST.ToString();
                txtPrefix.Text = settings[0].Prefix;
                dropUploadTime.SelectedValue = settings[0].UploadTime;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                new Setting()
                {
                    RetailerDiscount = Convert.ToDecimal(txtRetailer.Text.Trim()),
                    WholesalerDiscount = Convert.ToDecimal(txtWholesaler.Text.Trim()),
                    ExportDiscount = Convert.ToDecimal(txtExport.Text.Trim()),
                    ServerName = txtServerName.Text,
                    Password = txtPassword.Text,
                    Username = txtUsername.Text,
                    Prefix = txtPrefix.Text,
                    GST = Convert.ToInt32(txtGST.Text.Trim()),
                    UploadTime = dropUploadTime.SelectedValue,
                    DatabaseName = txtDatabaseName.Text,
                    InvoiceFooter = txtInvoiceFooter.Text,
                    InvoiceHeader = txtInvoiceHeader.Text,
                    ClientID=this.UserClientID,
                    StoreID=this.UserStoreID
                }.Update();
                Initialize();
                ShowDialog(DialogType.Success, "Settings are saved successfully!", "");
            }
            catch (Exception ex)
            {
                ShowDialog(DialogType.Error, ex.Message, "");
            }
        }

        protected void btnTestConnection_Click(object sender, EventArgs e)
        {
            string serverName = txtServerName.Text.Trim();
            string databaseName = txtDatabaseName.Text.Trim();
            string username = txtUsername.Text.Trim();
            string password = txtPassword.Text.Trim();
            string connectionString = "server=" + serverName + ";database=" + databaseName + ";uid=" + username + ";pwd=" + password;
            SqlConnection conn = new SqlConnection(connectionString);

            try
            {
                conn.Open();
            }
            catch
            {
                //Page.ClientScript.RegisterStartupScript(GetType(), "", "alert('Connection is failed.');", true);
                ShowDialog(DialogType.Error, "Connection is failed!", "");
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    //Page.ClientScript.RegisterStartupScript(GetType(), "", "alert('Connection is successful.');", true);
                    ShowDialog(DialogType.Success, "Connection is successful!", "");
                }

                conn.Close();
                conn.Dispose();

            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            string server = txtServerName.Text.Trim();
            string database = txtDatabaseName.Text.Trim();
            string username = txtUsername.Text.Trim();
            string password = txtPassword.Text.Trim();
            string connectionString = "server=" + server + ";database=" + database + ";uid=" + username + ";pwd=" + password;
            SqlConnection conn = new SqlConnection(connectionString);

            try
            {
                //测试数据器端数据库连接是否正确
                conn.Open();
            }
            catch
            {
                //连接失败
                //Page.ClientScript.RegisterStartupScript(GetType(), "", "alert('Connection is failed.');", true);
                ShowDialog(DialogType.Error, "Connection is failed!", "");
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    //连接成功，开始上传数据
                    try
                    {
                        Uploader uploader = new Uploader(conn,this.UserClientID,this.UserStoreID);
                        uploader.Upload();

                        //Page.ClientScript.RegisterStartupScript(GetType(), "", "alert('upload is done.');", true);
                        ShowDialog(DialogType.Success, "Upload is done!", "");
                    }
                    catch(Exception ex)
                    {
                        //Page.ClientScript.RegisterStartupScript(GetType(), "", "alert('upload is failed.');", true);
                        ShowDialog(DialogType.Error, ex.Message, "");
                    }
                }

                conn.Close();
                conn.Dispose();

            }
        }
    }
}