﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;
using System.Text.RegularExpressions;
using Telerik.Web.UI;

namespace POS
{
    public partial class InvoiceList : BasePage
    {
        public string InvoiceNumberPrefix
        {
            get
            {
                return Session["prefix"].ToString();
            }
            set
            {
                Session["prefix"] = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialize();
                txtDateFrom.Attributes.Add("onFocus", "WdatePicker({lang:'en',dateFmt:'dd-MM-yyyy'})");
                txtDateTo.Attributes.Add("onFocus", "WdatePicker({lang:'en',dateFmt:'dd-MM-yyyy'})");
            }
        }

        void Initialize()
        {
            pagelist.Visible = true;
            pageform.Visible = false;
            addLink.Visible = true;
            searcharea.Visible = true;
            IntialSetting();
            InitialInvoices(0,null,null,null,false);

            POSService.POSService pos = new POSService.POSService();
            List<Category> cats = pos.GetCategories(this.UserClientID).GetHiarachy();
            dropCategory.DataSource = cats;
            dropCategory.DataBind();
            dropCategory.SelectedIndex = 0;
            dropCategory_SelectedIndexChanged(null, null);

            dropCustomer.DataSource = new POSService.POSService().GetCustomers(this.UserClientID);
            dropCustomer.DataBind();
            dropCustomer.SelectedIndex = 0;
            this.dropCustomer_SelectedIndexChanged(null, null);
            
            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        void InitialInvoices(int invno, string customer, DateTime? datefrom, DateTime?dateto,bool isCancel)
        {
            POSService.POSService pos = new POSService.POSService();
            List<Invoice> invoices = pos.GetInvoices(this.UserStoreID, this.UserClientID,invno,customer,datefrom,dateto);
            Session["invoices"] = invoices.Where(i=>i.IsCancel==isCancel).ToList();
            this.AspNetPager1.RecordCount = (Session["invoices"] as List<Invoice>).Count();
            if (invoices.Any())
            {
                lblNo.Text = (invoices.First().inv_No + 1).ToString();
            }
            else
            {
                lblNo.Text = "100000";
            }
            rptList.DataSource = null;
            rptList.DataSource = (Session["invoices"] as List<Invoice>).TakeFrom(0, this.AspNetPager1.PageSize);
            this.AspNetPager1.CurrentPageIndex = 1;
            this.AspNetPager1.GoToPage(this.AspNetPager1.CurrentPageIndex);
            rptList.DataBind();
        }

        void IntialSetting()
        {
            POSService.POSService pos = new POSService.POSService();
            List<Setting> settings=pos.GetSetting(this.UserClientID,this.UserStoreID);
            if (settings != null && settings.Any())
            {
                lblGSTSetting.Text = settings[0].GST.ToString();
                dropType.Items.Add(new ListItem() { Text = "Cash", Value = "100" });
                dropType.Items.Add(new ListItem() { Text = "Retailer", Value = settings[0].RetailerDiscount.ToString("0.00") });
                dropType.Items.Add(new ListItem() { Text = "Wholesaler", Value = settings[0].WholesalerDiscount.ToString("0.00") });
                dropType.Items.Add(new ListItem() { Text = "Export", Value = settings[0].ExportDiscount.ToString("0.00") });
                lblPrefix.Text = settings[0].Prefix;
                InvoiceNumberPrefix = settings[0].Prefix;
            }
            else
            {
                InvoiceNumberPrefix = "";
                lblGSTSetting.Text = "0";
                dropType.Items.Add(new ListItem() { Text = "Cash", Value = "100" });
                dropType.Items.Add(new ListItem() { Text = "Retailer", Value = "100" });
                dropType.Items.Add(new ListItem() { Text = "Wholesaler", Value = "100" });
                dropType.Items.Add(new ListItem() { Text = "Export", Value = "100" });
            }
        }

        protected void DeleteInvoiceItem(object sender, EventArgs e)
        {
            RepeaterItem currentri = (sender as LinkButton).Parent as RepeaterItem;
           
            int id = Convert.ToInt32(lblId.Text);
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            int i = 0;
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                if (ri == currentri)
                    continue;
                string desc = (ri.FindControl("lblName") as Label).Text;
                decimal price = Convert.ToDecimal((ri.FindControl("txtPrice") as TextBox).Text);
                decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = price * quantity;
                string note = (ri.FindControl("txtNote") as TextBox).Text;
                ips.Add(new InvoiceProduct() { ip_Description = desc, ip_Discount = discount, ip_InvoiceID = id, ip_Note = note, ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = cost, ip_ProductID = pid, ip_ID = i });
                i++;
                total += subtotal;
            }
            lblTotal.Text = total.ToString();
            lblGST.Text = (total * Convert.ToInt32(lblGSTSetting.Text) / 100).ToString();
            if (chkIsGST.Checked)
            {
                lblTotalGST.Text = (total + total * Convert.ToInt32(lblGSTSetting.Text) / 100).ToString();
            }
            else
            {
                lblTotalGST.Text = lblTotal.Text;
            }
            rptProductList.DataSource = ips;
            rptProductList.DataBind();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemEdit")
            {
                if (Session["invoices"] != null)
                {
                    List<Invoice> invoices = Session["invoices"] as List<Invoice>;
                    if (invoices != null)
                    {
                        Invoice invoice = invoices.FirstOrDefault(i => i.inv_Id == Convert.ToInt32(e.CommandArgument));
                        txtAddress.Text = invoice.inv_Address;
                        txtCountry.Text = invoice.Country;
                        txtEmail.Text = invoice.Email;
                        txtMobile.Text = invoice.inv_Mobile;
                        txtPhone.Text = invoice.inv_Phone;
                        txtWalletID.Text = invoice.inv_WalletID;
                        bool dropCustomerSelected = false;
                        if (dropCustomer.Items != null)
                        {
                            foreach (ListItem item in dropCustomer.Items)
                            {
                                if (item.Text == invoice.inv_Customer)
                                {
                                    item.Selected = true;
                                    dropCustomerSelected = true;
                                }
                                else
                                {
                                    item.Selected = false;
                                }
                            }
                        }
                        dropCustomer.Visible = dropCustomerSelected;
                        txtName.Visible = !dropCustomerSelected;
                        if (!dropCustomerSelected)
                        {
                            editImageBtn.ImageUrl = "~/Images/test.png";
                            txtName.Text = invoice.inv_Customer;
                        }
                        else
                        {
                            editImageBtn.ImageUrl = "~/Images/edit.png";
                        }

                        foreach (ListItem item in dropType.Items)
                        {
                            if (item.Text == invoice.CustomerType)
                            {
                                item.Selected = true;
                            }
                            else
                            {
                                item.Selected = false;
                            }
                        }
                        foreach (ListItem item in dropPayMethod.Items)
                        {
                            if (item.Text == invoice.inv_PaymentMethod)
                            {
                                item.Selected = true;
                            }
                            else
                            {
                                item.Selected = false;
                            }
                        }
                        foreach (ListItem item in dropPrintTo.Items)
                        {
                            if (item.Text == invoice.inv_PrintTo)
                            {
                                item.Selected = true;
                            }
                            else
                            {
                                item.Selected = false;
                            }
                        }
                        lblGST.Text = invoice.inv_GST.ToString("0.00");
                        lblTotal.Text = invoice.inv_Total.ToString("0.00");
                        if (invoice.IsGST)
                        {
                            chkIsGST.Checked = true;
                            lblTotalGST.Text = invoice.inv_TotalIncGST.ToString("0.00");
                        }
                        else
                        {
                            chkIsGST.Checked = false;
                            lblTotalGST.Text = invoice.inv_Total.ToString("0.00");
                        }
                        lblNo.Text = invoice.inv_No.ToString();
                        lblDate.Text = invoice.inv_CreateDate.Value.ToShortDateString();
                        rptProductList.DataSource = invoice.Products;
                        rptProductList.DataBind();
                        pageform.Visible = true;
                        pagelist.Visible = false;
                        formbuttons.Visible = true;
                        listbuttons.Visible = false;
                        lblId.Text = invoice.inv_Id.ToString();
                        ltlPageTitle.Text = "Invoices / Edit";
                    }
                }
            }
            else if (e.CommandName == "ItemCancel")
            {
                //RadWindow1.Visible = true;
               // RadWindow1.VisibleOnPageLoad = true;
            }
            else if (e.CommandName == "ItemPrint")
            {
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            List<Invoice> invoices = Session["invoices"] as List<Invoice>;
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            rptList.DataSource = null;
            rptList.DataSource = invoices.TakeFrom((currentPage - 1) * pageSize, pageSize);
            rptList.DataBind();
            if (this.ltlPageTitle.Text == "Cancelled List")
            {
                if (rptList.Controls.Count > 0)
                {
                    var obj = rptList.Controls[0].FindControl("reasonColumn");
                    if (obj != null)
                    {
                        Label lbl = obj as Label;
                        if (lbl != null)
                        {
                            lbl.Text = "Reason";
                        }
                    }
                }
            }
            else
            {
                if (rptList.Controls.Count > 0)
                {
                    var obj = rptList.Controls[0].FindControl("reasonColumn");
                    if (obj != null)
                    {
                        Label lbl = obj as Label;
                        if (lbl != null)
                        {
                            lbl.Text = "";
                        }
                    }
                }
            }
        }

        protected void Cancel(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(lblCancelID.Value);
            new POSService.POSService().CancelInvoice(new Invoice() { inv_Id = id, Reason = txtReason.Text});
            Initialize();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                InitialInvoices(string.IsNullOrEmpty(this.txtNo.Text) ? 0 : Convert.ToInt32(this.txtNo.Text.Replace(InvoiceNumberPrefix, "")), this.txtCustomer.Text.Trim(),
                    string.IsNullOrEmpty(this.txtDateFrom.Text) ? new DateTime?() : GetDateTime(this.txtDateFrom.Text), string.IsNullOrEmpty(this.txtDateTo.Text) ? new DateTime?() : GetDateTime(this.txtDateTo.Text), false);
            }
            catch (Exception ex)
            {
            }
        }

        protected void CancelledList(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (btn.Text == "Cancelled List")
            {
                btn.Text = "Invoice List";
                InitialInvoices(0, null, null, null, true);
                this.ltlPageTitle.Text = "Cancelled List";
                addLink.Visible = false;
                if (rptList.Controls.Count>0)
                {
                    var obj = rptList.Controls[0].FindControl("reasonColumn");
                    if (obj != null)
                    {
                        Label lbl = obj as Label;
                        if (lbl != null)
                        {
                            lbl.Text = "Reason";
                        }
                    }
                }
                searcharea.Visible = false;
            }
            else
            {
                btn.Text = "Cancelled List";
                searcharea.Visible = true;
                addLink.Visible = true;
                InitialInvoices(0, null, null, null, false);
                this.ltlPageTitle.Text = "Invoice List";
                if (rptList.Controls.Count > 0)
                {
                    var obj = rptList.Controls[0].FindControl("reasonColumn");
                    if (obj != null)
                    {
                        Label lbl = obj as Label;
                        if (lbl != null)
                        {
                            lbl.Text = "";
                        }
                    }
                }
            }
        }

        DateTime GetDateTime(string s)
        {
            string[] str = s.Split('-');
            int year = Convert.ToInt32(str[2]);
            int month = Convert.ToInt32(str[1]);
            int day = Convert.ToInt32(str[0]);
            return new DateTime(year, month, day, 0, 0, 0);
        }

        protected void AddInvoice(object sender, EventArgs e)
        {
            pageform.Visible = true;
            pagelist.Visible = false;
            formbuttons.Visible = true;
            listbuttons.Visible = false;
            dropCustomer.Visible = true;
            txtName.Visible = false;
            lblId.Text = "0";
            ltlPageTitle.Text = "Invoices / Add";
            Literal lbl = this.Master.FindControl("ltlNav") as Literal;
            lbl.Visible = false;
            dropCategory.SelectedIndex = 0;
            dropCategory_SelectedIndexChanged(null, null);
            dropCustomer.SelectedIndex = 0;
            this.dropCustomer_SelectedIndexChanged(null, null);
        }

        protected void BackToList(object sender, EventArgs e)
        {
            pageform.Visible = false;
            pagelist.Visible = true;
            formbuttons.Visible = false;
            listbuttons.Visible = true;
            lblId.Text = "0";
            
            txtAddress.Text = txtCountry.Text = txtName.Text = txtEmail.Text = txtMobile.Text = txtPhone.Text = txtWalletID.Text = "";
            dropCategory.SelectedIndex = 0;
            dropCustomer.SelectedIndex = 0;
            dropPayMethod.SelectedIndex = 0;
            dropPrintTo.SelectedIndex = 0;
            dropType.SelectedIndex = 0;
            rptProductList.DataSource = null;
            rptProductList.DataBind();
            lblGST.Text = "";
            lblTotal.Text = "";
            lblTotalGST.Text = "";
            txtCustomer.Text = "";
            txtDateFrom.Text = "";
            txtDateTo.Text = "";
            txtNo.Text = "";
            this.Master.FindControl("ltlNav").Visible = true;
            if (Button1.Text == "Invoice List")
            {
                ltlPageTitle.Text = "Cancelled List";
                //Button1.Text = "Cancelled List";
                searcharea.Visible = false;
                addLink.Visible = false;
                InitialInvoices(0, null, null, null, true);
            }
            else
            {
                ltlPageTitle.Text = "Invoices";
                //Button1.Text = "Cancelled List";
                searcharea.Visible = true;
                addLink.Visible = true;
                InitialInvoices(0, null, null, null, false);
            }
        }

        protected void dropCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            POSService.POSService pos = new POSService.POSService();
            List<Product> products=pos.GetProducts(this.UserClientID);
            productsListView.DataSource = products.Where(p => p.Category != null && p.Category.cat_Id == Convert.ToInt32(dropCategory.SelectedValue)).ToList();
            productsListView.DataBind();
        }

        protected void dropCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(dropCustomer.SelectedValue))
                return;
            int id = Convert.ToInt32(dropCustomer.SelectedValue);
            Customer customer = new POSService.POSService().GetCustomerByID(id);
            txtAddress.Text = customer.Address;
            txtCountry.Text = customer.Country;
            txtPhone.Text = customer.Phone;
            txtMobile.Text = customer.Mobile;
            txtEmail.Text = customer.Email;
            txtWalletID.Text = customer.WalletID;
        }

        protected void OnEdit(object sender, EventArgs e)
        {
            txtName.Visible = !txtName.Visible;
            dropCustomer.Visible = !dropCustomer.Visible;
            if (txtName.Visible)
            {
                editImageBtn.ImageUrl = "~/Images/test.png";
                txtName.Text = dropCustomer.SelectedItem!=null?dropCustomer.SelectedItem.Text:"";
            }
            else
            {
                editImageBtn.ImageUrl = "~/Images/edit.png";
                dropCustomer.SelectedIndex = 0;
                this.dropCustomer_SelectedIndexChanged(null, null);
            }
        }

        protected void dropProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        void DoInvoice()
        {
            
        }

        protected void btnAddProduct_Click(object sender, EventArgs e)
        {
            
            var obj=productsListView.DataSourceObject;
            int id = Convert.ToInt32(lblId.Text);
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            int i = 0;
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                string desc = (ri.FindControl("lblName") as Label).Text;
                decimal price = Convert.ToDecimal((ri.FindControl("txtPrice") as TextBox).Text);
                decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = Convert.ToDecimal((ri.FindControl("lblSubTotal") as Label).Text);
                string note = (ri.FindControl("txtNote") as TextBox).Text;
                ips.Add(new InvoiceProduct() { ip_Description = desc, ip_Discount = discount, ip_InvoiceID = id, ip_Note = note, ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = cost, ip_ProductID=pid, ip_ID = i });
                i++;
                total += subtotal;
            }

            RadListViewDataItem item = (sender as ImageButton).Parent as RadListViewDataItem;
            if (item != null)
            {
                decimal discount = 100;
                if (dropType.SelectedIndex > -1)
                {
                    discount = Convert.ToDecimal(dropType.SelectedValue);
                }
                string desc = (item.FindControl("lblPName") as Label).Text;
                decimal price = Convert.ToDecimal(Regex.Replace((item.FindControl("lblPPrice") as Label).Text, @"[^\d.]", ""));
                int quantity = 1;
                decimal cost = Convert.ToDecimal(Regex.Replace((item.FindControl("lblPCost") as Label).Text, @"[^\d.]", ""));
                total += price;
                int pid=Convert.ToInt32((item.FindControl("lblPId") as Label).Text);
                ips.Add(new InvoiceProduct() { ip_ID=i,ip_SubTotal=price,ip_Quantity=quantity,ip_ProductID=pid,ip_Price=price,ip_Description=desc,ip_InvoiceID=id,ip_Note="",cost=cost,ip_Discount=discount});
            }

            lblTotal.Text = total.ToString();
            lblGST.Text = (total * Convert.ToInt32(lblGSTSetting.Text)/100).ToString();
            if (chkIsGST.Checked)
            {
                lblTotalGST.Text = (total + total * Convert.ToInt32(lblGSTSetting.Text)/100).ToString();
            }
            else
            {
                lblTotalGST.Text = lblTotal.Text;
            }
            rptProductList.DataSource = ips;
            rptProductList.DataBind();
        }

        protected void chkIsGST_CheckedChanged(object sender, EventArgs e)
        {
            decimal total = Convert.ToDecimal(lblTotal.Text);

            if (chkIsGST.Checked)
            {
                lblTotalGST.Text = (total  + total * Convert.ToInt32(lblGSTSetting.Text) / 100).ToString();
            }
            else
            {
                lblTotalGST.Text = lblTotal.Text;
            }
        }

        protected void dropType_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        protected void txtQuantity_TextChanged(object sender, EventArgs e)
        {
            UpdatePendingList();
        }

        protected void txtPrice_TextChanged(object sender, EventArgs e)
        {
            UpdatePendingList();
        }

        protected void txtDiscount_TextChanged(object sender, EventArgs e)
        {
            UpdatePendingList();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if ((dropPrintTo.Text == "Wallet(App)" || dropPrintTo.Text == "Print & Wallet (App)")&&string.IsNullOrEmpty(txtWalletID.Text))
            {
                WindowManager.RadAlert("Please input Wallet ID if you want to receive receipt by Wallet App", 300, 100, "Validation", "");
            }
            Invoice invoice=new Invoice()
            {
                inv_Id = Convert.ToInt32(lblId.Text),
                inv_Staff = this.CurrentUser,
                inv_Address = txtAddress.Text.Trim(),
                inv_ClientID = this.UserClientID,
                inv_Customer =  txtName.Visible?this.txtName.Text.Trim(): (dropCustomer.SelectedItem==null?"": dropCustomer.SelectedItem.Text),
                inv_GST = Convert.ToDecimal(this.lblGST.Text),
                inv_Total = Convert.ToDecimal(this.lblTotal.Text),
                inv_TotalIncGST = Convert.ToDecimal(this.lblGST.Text) + Convert.ToDecimal(this.lblTotal.Text),
                inv_Mobile = this.txtMobile.Text.Trim(),
                inv_No = Convert.ToInt32(this.lblNo.Text),
                inv_PaymentMethod = dropPayMethod.Text,
                inv_PrintTo = dropPrintTo.Text,
                CustomerType = dropType.SelectedItem.Text,
                inv_WalletID = txtWalletID.Text.Trim(),
                inv_Phone = txtPhone.Text.Trim(),
                inv_StoreID = this.UserStoreID,
                Email=txtEmail.Text,
                Country=txtCountry.Text,
                IsGST = chkIsGST.Checked,
            };
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                string desc = (ri.FindControl("lblName") as Label).Text;
                decimal price = Convert.ToDecimal((ri.FindControl("txtPrice") as TextBox).Text);
                decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = price * quantity;
                string note = (ri.FindControl("txtNote") as TextBox).Text;
                ips.Add(new InvoiceProduct() { ip_Description = desc, ip_Discount = discount, ip_Note = note, ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = cost, ip_ProductID = pid});
                total += cost;
            }
            invoice.cost = total;
            invoice.Products = ips;
           // invoice.WalletConnection = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["WalletDBConnection"].ConnectionString;
            new POSService.POSService().DoInvoice(invoice);
            if (invoice.inv_PrintTo == "Wallet(App)" || invoice.inv_PrintTo == "Print & Wallet (App)")
            {
                int clientid = this.UserClientID;
                int storeid = this.UserStoreID;
                int invoiceno = invoice.inv_No;
                //notify the wallet app to get receipt by client id, store id, invoice no
            }
            BackToList(null, null);
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
        }

        void UpdatePendingList()
        {
            int id = Convert.ToInt32(lblId.Text);
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            int i = 0;
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                string desc = (ri.FindControl("lblName") as Label).Text;
                decimal price = Convert.ToDecimal((ri.FindControl("txtPrice") as TextBox).Text);
                decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = price * quantity;
                string note = (ri.FindControl("txtNote") as TextBox).Text;
                ips.Add(new InvoiceProduct() { ip_Description = desc, ip_Discount = discount, ip_InvoiceID = id, ip_Note = note, ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = cost, ip_ProductID = pid, ip_ID = i });
                i++;
                total += subtotal;
            }
            lblTotal.Text = total.ToString();
            lblGST.Text = (total * Convert.ToInt32(lblGSTSetting.Text)/100).ToString();
            if (chkIsGST.Checked)
            {
                lblTotalGST.Text = (total + total * Convert.ToInt32(lblGSTSetting.Text)/100).ToString();
            }
            else
            {
                lblTotalGST.Text = lblTotal.Text;
            }
            rptProductList.DataSource = ips;
            rptProductList.DataBind();
            
        }
    }
}