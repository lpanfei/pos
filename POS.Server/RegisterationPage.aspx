﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RegisterationPage.aspx.cs"
    Inherits="POS.Server.RegisterationPage" %>
    <%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>

<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <!-- Apple devices fullscreen -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- Apple devices fullscreen -->
    <meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <title>POS -  Registeration</title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Bootstrap responsive -->
    <link rel="stylesheet" href="css/bootstrap-responsive.min.css">
    <!-- jQuery UI -->
    <link rel="stylesheet" href="css/plugins/jquery-ui/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="css/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- Color CSS -->
    <link rel="stylesheet" href="css/themes.css">
  
    <style>
    #header {
height: 50px;
margin: 0;
padding: 12px 8px 5px 6px;
position: relative;
background-color: #3894E7;
color:White;
}

input[type=text],input[type=password]
{
    height:30px;
}

.form-wizard .wizard-steps {

background: transparent;
}
    </style>
    <script type="text/javascript">
        function redirecttologin() {
            window.location = "Default.aspx";
        }
    </script>
</head>
<body>

   
    <div id="header" >
    <asp:Image ID="Image1" runat="server" ImageAlign="Middle" ImageUrl="~/Images/LOGO_w.png" />&nbsp;<b
                                    style="font-size: 20px; margin-left: 20px; padding-top: 20px;">Distributed POS System</b>
                                &nbsp;&nbsp; <b>Client 2011</b>
    </div>
    <div class="container-fluid" id="content">
    <div id="main" style="margin:0 auto; width:800px;">
			<div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <div class="box">
                    <div class="box-title">
                        <h3>
                            <i class="icon-magic"></i>Client Registeration
                        </h3>
                    </div>
                    <div class="box-content">
                        <form id="form1" runat="server" class='form-horizontal form-wizard'>
                         <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
                        <asp:UpdatePanel ID="updatepanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                        <asp:Panel CssClass="step" ID="firstStep" ClientIDMode="Static" runat="server">
                        
                            <ul class="wizard-steps steps-4">
                                <li class='active'>
                                    <div class="single-step">
                                        <span class="title">1</span> <span class="circle"><span class="active"></span></span>
                                        <span class="description">Basic Information </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">2</span> <span class="circle"></span><span class="description">Create User
                                            </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">3</span> <span class="circle"></span><span class="description">Add Store
                                            </span>
                                    </div>
                                </li>
                            </ul>
                            <div class="step-forms">
                               
                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        Client name</label>
                                    <div class="controls">
                                        <asp:TextBox ID="clientname" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        Client code</label>
                                    <div class="controls">
                                        <asp:TextBox ID="clientcode" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                               
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        Suburb</label>
                                    <div class="controls">
                                        <asp:TextBox ID="clientsuburb" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        State</label>
                                    <div class="controls">
                                        <asp:TextBox ID="state" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        Address</label>
                                    <div class="controls">
                                        <asp:TextBox ID="address" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        Phone</label>
                                    <div class="controls">
                                        <asp:TextBox ID="phone" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        Email</label>
                                    <div class="controls">
                                        <asp:TextBox ID="email" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                         <asp:Panel CssClass="step" ID="secondStep" ClientIDMode="Static" runat="server" Visible="false">
                        
                            <ul class="wizard-steps steps-4">
                                 <li>
                                    <div class="single-step">
                                        <span class="title">1</span> <span class="circle"><span class="active"></span></span>
                                        <span class="description">Basic Information </span>
                                    </div>
                                </li>
                                <li class='active'>
                                    <div class="single-step">
                                        <span class="title">2</span> <span class="circle"><span class="active"></span></span>
                                        <span class="description">Create User </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">3</span> <span class="circle"></span><span class="description">Add Store
                                            </span>
                                    </div>
                                </li>
                             
                            </ul>
                            <div class="step-forms">
                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        User Name</label>
                                    <div class="controls">
                                        <asp:TextBox ID="username" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="anotherelem" class="control-label">
                                       Password</label>
                                    <div class="controls">
                                        <asp:TextBox ID="password" TextMode="Password" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="anotherelem" class="control-label">
                                       Confirm Password</label>
                                    <div class="controls">
                                        <asp:TextBox ID="confpassword" TextMode="Password" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        First name</label>
                                    <div class="controls">
                                        <asp:TextBox ID="firstname" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="anotherelem" class="control-label">
                                        Last name</label>
                                    <div class="controls">
                                        <asp:TextBox ID="lastname" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        Suburb</label>
                                    <div class="controls">
                                        <asp:TextBox ID="usersuburb" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        State</label>
                                    <div class="controls">
                                        <asp:TextBox ID="userstate" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        Address</label>
                                    <div class="controls">
                                        <asp:TextBox ID="useraddress" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        Phone</label>
                                    <div class="controls">
                                        <asp:TextBox ID="userphone" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        Mobile</label>
                                    <div class="controls">
                                        <asp:TextBox ID="usermobile" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        Postcode</label>
                                    <div class="controls">
                                        <asp:TextBox ID="userpostcode" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="additionalfield" class="control-label">
                                        Email</label>
                                    <div class="controls">
                                        <asp:TextBox ID="useremail" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel CssClass="step" ID="thirdStep" ClientIDMode="Static" runat="server" Visible="false">
                            <ul class="wizard-steps steps-4">
                                <li>
                                    <div class="single-step">
                                        <span class="title">1</span> <span class="circle"></span><span class="description">Basic
                                            Information </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-step">
                                        <span class="title">2</span> <span class="circle"><span class="active"></span></span>
                                        <span class="description">Create User </span>
                                    </div>
                                </li>
                                <li class='active'>
                                    <div class="single-step">
                                        <span class="title">3</span> <span class="circle"><span class="active"></span></span>
                                        <span class="description">Add Store</span>
                                    </div>
                                </li>
                                
                            </ul>
                            <div class="step-forms">
                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        Store Name</label>
                                    <div class="controls">
                                        <asp:TextBox ID="storename" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        Suburb</label>
                                    <div class="controls">
                                        <asp:TextBox ID="suburb" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        Address</label>
                                    <div class="controls">
                                        <asp:TextBox ID="storeaddress" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        Postcode</label>
                                    <div class="controls">
                                        <asp:TextBox ID="postcode" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                 <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        Phone</label>
                                    <div class="controls">
                                        <asp:TextBox ID="storephone" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        Mobile</label>
                                    <div class="controls">
                                        <asp:TextBox ID="mobile" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        Fax</label>
                                    <div class="controls">
                                        <asp:TextBox ID="fax" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        Email</label>
                                    <div class="controls">
                                        <asp:TextBox ID="storeemail" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="control-group">
                                    <label for="firstname" class="control-label">
                                        Note</label>
                                    <div class="controls">
                                        <asp:TextBox ID="note" CssClass="input-xlarge" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                       
                        <div class="form-actions">
                        <asp:Button Text="Back" Enabled="false" CssClass="btn" ID="back" ClientIDMode="Static" runat="server" OnClick="Back" />
                         <asp:Button Text="Next" Enabled="true" CssClass="btn btn-primary" ID="next" ClientIDMode="Static" runat="server" OnClick="Next" />
                        </div>
                          <eid:RadWindowManager runat="server" ID="WindowManager" Skin="Metro"></eid:RadWindowManager>
                          <input type="hidden" id="currentpage" runat="server" clientidmode="Static" value="1" />
                        </ContentTemplate>
                        </asp:UpdatePanel>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        </div></div>
    </div>

</body>
</html>
