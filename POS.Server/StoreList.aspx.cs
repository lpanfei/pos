﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;
using Telerik.Web.UI;

namespace POS
{
    public partial class StoreList : BasePage
    {
        private List<Store> Stores
        {
            get
            {
                return GlobalCache.Stores;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind();
            }
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                delid.Attributes["value"] = e.CommandArgument.ToString();
                WindowManager.RadConfirm("Are you sure to delete the item?","confirmclosed",300,100,null,"Delete Store");
            }
            else if (e.CommandName == "ItemEdit")
            {
                RepeaterItem item = e.Item;
                formbuttons.Visible = true;
                listbuttons.Visible = false;
                formpanel.Visible = true;
                listpanel.Visible = false;
                ltlPageTitle.Text = "Store / Add";
                txtAddress.Text = (item.FindControl("lblAddress") as Label).Text;
                txtEmail.Text = (item.FindControl("lblEmail") as Label).Text;
                txtMobilePhone.Text = (item.FindControl("lblMobile") as Label).Text;
                txtName.Text = (item.FindControl("lblName") as Label).Text;
                txtNote.Text = (item.FindControl("lblNote") as Label).Text;
                txtFax.Text = (item.FindControl("lblFax") as Label).Text;
                txtPhone.Text = (item.FindControl("lblPhone") as Label).Text;
                txtPostcode.Text = (item.FindControl("lblPostcode") as Label).Text;
                txtSuburb.Text = (item.FindControl("lblSuburb") as Label).Text;
                lblId.Text = (item.FindControl("lblSID") as Label).Text;
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            rptList.DataSource = null;
            rptList.DataSource = Stores.TakeFrom((currentPage - 1) * pageSize, pageSize);
            rptList.DataBind();
        }

        protected void AddStore(object sender, EventArgs e)
        {
            formbuttons.Visible = true;
            listbuttons.Visible = false;
            formpanel.Visible = true;
            listpanel.Visible = false;
            ltlPageTitle.Text = "Store / Add";
            txtAddress.Text = "";
            txtEmail.Text = "";
            txtMobilePhone.Text = "";
            txtName.Text = "";
            txtNote.Text = "";
            txtFax.Text = "";
            txtPhone.Text = "";
            txtPostcode.Text = "";
            txtSuburb.Text = "";
            lblId.Text = "0";
        }

        protected void BackToList(object sender, EventArgs e)
        {
            listbuttons.Visible = true;
            formbuttons.Visible = false;
            formpanel.Visible = false;
            listpanel.Visible = true;
            ltlPageTitle.Text = "Store List";
            GlobalCache.RefreshStores();
            Bind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            new POSService.POSService().UpdateStore(new Data.Store()
            {
                sto_Id = Convert.ToInt32(lblId.Text),
                sto_Name = txtName.Text,
                sto_Suburb = txtSuburb.Text,
                sto_Address = txtAddress.Text,
                sto_ClientID = this.UserClientID,
                sto_Email = txtEmail.Text,
                sto_Fax = txtFax.Text,
                sto_MobilePhone = txtMobilePhone.Text,
                sto_Phone = txtPhone.Text,
                sto_Postcode = txtPostcode.Text,
                sto_Note = txtNote.Text
            });

            BackToList(null, null);
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = Stores.TakeFrom(0, this.AspNetPager1.PageSize);
            this.AspNetPager1.RecordCount = Stores.Count;
            this.AspNetPager1.CurrentPageIndex = 1;
            this.AspNetPager1.GoToPage(this.AspNetPager1.CurrentPageIndex);
            rptList.DataBind();
        }

        protected void DeleteStore(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delid.Attributes["value"]);
            new POSService.POSService().DeleteStore(id);
            GlobalCache.RefreshStores();
            Bind();
        }
    }
}