﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Dashboard.aspx.cs" Inherits="POS.Dashboard" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .toprow
        {
        }
        .subrow
        {
            display: none;
        }
        input[type=submit]
        {
            cursor: pointer;
        }
    </style>
    <script type="text/javascript">

    //var previous = null;
    function pageLoad() {
        $(".catlist tbody tr").click(function () {
            var current = $(this).find(".buttons div").eq(0);
            $(".catlist tr").each(function () {
                if ($(this).find(".parentid").eq(0).val() == current.find(".classid").eq(0).val()) {
                    $(this).toggle();
                    $(this).css("background-color", "#EFF3F4");
                    $(this).css("color", "#3596E7");
                }

            });
        }
        );
    }

    function tileclick(sender, args) {
             var tile = args.get_tile();
            if(tile.get_name()=="Invoices"||tile.get_name()=="Warranties"){
                $get('<%= timer.ClientID %>').control._stopTimer();
                $(".detailspanel").css("display", "none");
            
                $(".pagetitle").text(tile.get_name());
                $("#" + tile.get_name().toLowerCase() + "panel").css("display", "");
                $("#tilearea").animate({ opacity: 0, paddingTop: "0px" }, 800, "linear", function () { $("#tilearea").css("display", "none"); });
                $("#list").animate({ opacity: 1 }, 800, "linear", function () { $("#list").css("display", ""); });
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="list" style="opacity: 0; height: 0">
                <div class="mainheader">
                    <div class="contextual">
                        <asp:Panel ID="formbuttons" runat="server">
                            <div style="height: 10px; width: 75px">
                                <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                                    Text="Back" ClientIDMode="Static" OnClick="Back" />
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="subheader">
                        <h2 class="icon-products pagetitle">
                            <asp:Literal ID="ltlPageTitle" Text="Store List" runat="server" ClientIDMode="Static"></asp:Literal></h2>
                    </div>
                </div>
                <asp:Panel ID="storespanel" runat="server" ClientIDMode="Static" CssClass="detailspanel">
                    <asp:Repeater ID="storeList" runat="server">
                        <HeaderTemplate>
                            <table class="list" id="stores">
                                <thead>
                                    <tr>
                                        <th>
                                            Store
                                        </th>
                                        <th>
                                            Suburb
                                        </th>
                                        <th>
                                            Address
                                        </th>
                                        <th>
                                            Phone
                                        </th>
                                        <th>
                                            Mobile
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblName" Text='<%# Eval("sto_Name")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblSuburb" Text='<%# Eval("sto_Suburb")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblAddress" Text='<%# Eval("sto_Address")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblPhone" Text='<%# Eval("sto_Phone")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblMobile" Text='<%# Eval("sto_MobilePhone")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblEmail" Text='<%# Eval("sto_Email")%>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" ClientIDMode="Static" PageSize="10">
                    </webdiyer:AspNetPager>
                </asp:Panel>
                <asp:Panel runat="server" ID="productspanel" ClientIDMode="Static" CssClass="detailspanel">
                    <asp:Repeater ID="productList" runat="server">
                        <HeaderTemplate>
                            <table class="list">
                                <thead>
                                    <tr>
                                        <th>
                                            No.
                                        </th>
                                        <th>
                                            Category
                                        </th>
                                        <th>
                                            Name of Product
                                        </th>
                                        <th>
                                            Serial No
                                        </th>
                                        <th>
                                            Price
                                        </th>
                                        <th>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <%# Container.ItemIndex+1 %>
                                </td>
                                <td align="center">
                                    <%# Eval("cat_Name")%>
                                </td>
                                <td align="center">
                                    <%# Eval("pro_Name") %>
                                </td>
                                <td align="center">
                                    <%# Eval("pro_No") %>
                                </td>
                                <td align="center">
                                    <%# Convert.ToDecimal(Eval("pro_Price")).ToString("0.00") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <webdiyer:AspNetPager ID="AspNetPager2" runat="server" ClientIDMode="Static" PageSize="1">
                    </webdiyer:AspNetPager>
                </asp:Panel>
                <asp:Panel runat="server" ID="invoicespanel" ClientIDMode="Static" CssClass="detailspanel">
                    <asp:Repeater ID="invoiceList" runat="server">
                        <HeaderTemplate>
                            <table class="list">
                                <thead>
                                    <tr>
                                        <th>
                                            Invoice No
                                        </th>
                                        <th>
                                            Date
                                        </th>
                                        <th>
                                            Staff
                                        </th>
                                        <th>
                                            Customer
                                        </th>
                                        <th>
                                            Address
                                        </th>
                                        <th>
                                            Phone
                                        </th>
                                        <th>
                                            Mobile
                                        </th>
                                        <th>
                                            Total (inc GST)
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center" class="invno">
                                    <%# Eval("inv_No")%>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblDate" runat="server" Text='<%# Eval("inv_CreateDate")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <%# Eval("inv_Staff")%>
                                </td>
                                <td align="center">
                                    <%# Eval("inv_Customer")%>
                                </td>
                                <td align="center">
                                    <%# Eval("inv_Address")%>
                                </td>
                                <td align="center">
                                    <%# Eval("inv_Phone") %>
                                </td>
                                <td align="center">
                                    <%# Eval("inv_Mobile")%>
                                </td>
                                <td align="center">
                                    $<asp:Label ID="lblTotalIncGST" runat="server" Text='<%# Eval("inv_TotalIncGST")%>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <webdiyer:AspNetPager ID="AspNetPager3" runat="server" PageSize="10" ClientIDMode="Static" OnPageChanged="InvoicePageChanged">
                    </webdiyer:AspNetPager>
                </asp:Panel>
                <asp:Panel runat="server" ID="customerspanel" ClientIDMode="Static" CssClass="detailspanel">
                    <asp:Repeater ID="customerList" runat="server">
                        <HeaderTemplate>
                            <table class="list">
                                <thead>
                                    <tr>
                                        <th>
                                            No.
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Address
                                        </th>
                                        <th>
                                            Phone
                                        </th>
                                        <th>
                                            Mobile
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Country
                                        </th>
                                        <th>
                                            Wallet ID
                                        </th>
                                        <th>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <%# Container.ItemIndex+1 %>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("Phone")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblMobile" runat="server" Text='<%# Eval("Mobile")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Country")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblWalletID" runat="server" Text='<%# Eval("WalletID")%>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <webdiyer:AspNetPager ID="AspNetPager4" runat="server" PageSize="1" ClientIDMode="Static">
                    </webdiyer:AspNetPager>
                </asp:Panel>
                <asp:Panel ID="supplierspanel" runat="server" ClientIDMode="Static" CssClass="detailspanel">
                    <asp:Repeater ID="supplierList" runat="server">
                        <HeaderTemplate>
                            <table class="list">
                                <thead>
                                    <tr>
                                        <th>
                                            No.
                                        </th>
                                        <th>
                                            Supplier
                                        </th>
                                        <th>
                                            Contact
                                        </th>
                                        <th>
                                            Address
                                        </th>
                                        <th>
                                            Phone
                                        </th>
                                        <th>
                                            Mobile
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <%# Container.ItemIndex+1 %>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblName" Text='<%# Eval("Name")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblContact" Text='<%# Eval("Contact")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblAddress" Text='<%# Eval("Address")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblPhone" Text='<%# Eval("Phone")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblMobile" Text='<%# Eval("Mobile")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label runat="server" ID="lblEmail" Text='<%# Eval("Email")%>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <webdiyer:AspNetPager ID="AspNetPager5" runat="server" ClientIDMode="Static" PageSize="1">
                    </webdiyer:AspNetPager>
                </asp:Panel>
                <asp:Panel ID="userspanel" runat="server" ClientIDMode="Static" CssClass="detailspanel">
                    <asp:Repeater ID="userList" runat="server">
                        <HeaderTemplate>
                            <table class="list">
                                <thead>
                                    <tr>
                                        <th>
                                            No.
                                        </th>
                                        <th>
                                            Username
                                        </th>
                                        <th>
                                            Pemission
                                        </th>
                                        <th>
                                            First Name
                                        </th>
                                        <th>
                                            Last Name
                                        </th>
                                        <th>
                                            Address
                                        </th>
                                        <th>
                                            Phone
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <%# Container.ItemIndex+1 %>
                                </td>
                                <td align="center">
                                    <%# Eval("usr_Name")%>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblPermission" runat="server" Text='<%# Eval("Permission")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <%# Eval("usr_FirstName")%>
                                </td>
                                <td align="center">
                                    <%# Eval("usr_LastName")%>
                                </td>
                                <td align="center">
                                    <%# Eval("usr_Address")%>
                                </td>
                                <td align="center">
                                    <%# Eval("usr_Phone") %>
                                </td>
                                <td align="center">
                                    <%# Eval("usr_Email") %>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <webdiyer:AspNetPager ID="AspNetPager6" runat="server" ClientIDMode="Static" PageSize="1">
                    </webdiyer:AspNetPager>
                </asp:Panel>
                <asp:Panel ID="warrantiespanel" runat="server" ClientIDMode="Static" CssClass="detailspanel">
                    <asp:Repeater ID="warrantyList" runat="server">
                        <HeaderTemplate>
                            <table class="list">
                                <thead>
                                    <tr>
                                        <th>
                                            Invoice No.
                                        </th>
                                        <th>
                                            Item Description
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Date
                                        </th>
                                        <th>
                                            Comments
                                        </th>
                                        <th>
                                            Resolution
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <%# Eval("InvoiceNo")%>
                                </td>
                                <td align="center">
                                    <%# Eval("ProductName")%>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblQty" runat="server" Text='<%# Eval("Quantity")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblDate" runat="server" Text='<%# Eval("Date")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblComment" runat="server" Text='<%# Eval("Comment")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblResolution" runat="server" Text='<%# Eval("Resolution")%>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <webdiyer:AspNetPager ID="AspNetPager7" runat="server" ClientIDMode="Static" PageSize="10" OnPageChanged="WarrantyPageChanged">
                    </webdiyer:AspNetPager>
                </asp:Panel>
                <asp:Panel ID="categoriespanel" runat="server" ClientIDMode="Static" CssClass="detailspanel">
                    <asp:Repeater ID="categoryList" runat="server">
                        <HeaderTemplate>
                            <table class="list catlist">
                                <thead>
                                    <tr>
                                        <th width="40px">
                                        </th>
                                        <th align="left">
                                            Category Name
                                        </th>
                                        <th>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr class='<%# Convert.ToInt32(Eval("parentID"))==0? "toprow":"subrow"%> '>
                                <td align="right">
                                    <%--<i class='<%# Eval("CssClass")%>'></i>--%>
                                </td>
                                <%--<td align="center"><%# Container.ItemIndex+1 %></td>--%>
                                <td align="left">
                                    <asp:Label runat="server" ID="lblName" Text='<%# Eval("cat_Name")%>'></asp:Label>
                                </td>
                                <td class="buttons">
                                    <div style="display: none;">
                                        <input type="hidden" class="parentid" value='<%# Eval("ParentID")%>' />
                                        <input type="hidden" class="classid" value='<%# Eval("cat_Id")%>' />
                                        <input type="hidden" class="catname" value='<%# Eval("ori_Name")%>' />
                                    </div>
                                </td>
                                <td style="display: none">
                                    <asp:Label runat="server" ID="lblParentID" Text='<%# Eval("ParentID")%>'></asp:Label>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </asp:Panel>
            </div>
            <div style="padding-top: 10px" id="tilearea">
                <eid:RadTileList runat="server" ID="tilelist" AutoPostBack="false" EnableDragAndDrop="true"
                    ClientIDMode="Static" SelectionMode="Single" OnClientTileClicked="tileclick">
                    <Groups>
                        <eid:TileGroup>
                            <eid:RadIconTile ID="RadIconTile1" Width="200" Height="100" runat="server" Name="Stores"
                                Target="_self" Shape="Wide" ImageUrl="Images/products.png" BackColor="#3894E7"
                                ForeColor="White" NavigateUrl="StoreList.aspx">
                                <Title Text="Stores"></Title>
                            </eid:RadIconTile>
                            <eid:RadIconTile ID="RadIconTile5" Width="200" Height="100" runat="server" Name="Invoices"
                                Target="_blank" Shape="Wide" ImageUrl="Images/invoice.png" BackColor="#3894E7"
                                ForeColor="White">
                                <Title Text="Invoices"></Title>
                            </eid:RadIconTile>
                            <eid:RadIconTile ID="RadIconTile2" Width="200" Height="100" runat="server" Name="Products"
                                Target="_self" Shape="Wide" ImageUrl="Images/products.png" BackColor="#3894E7"
                                ForeColor="White" NavigateUrl="ProductList.aspx">
                                <Title Text="Products"></Title>
                            </eid:RadIconTile>
                            <eid:RadIconTile ID="RadIconTile3" Width="200" Height="100" runat="server" Name="Categories"
                                Target="_self" Shape="Wide" ImageUrl="Images/categories.png" BackColor="#3894E7"
                                ForeColor="White" NavigateUrl="CategoryList.aspx">
                                <Title Text="Categories"></Title>
                            </eid:RadIconTile>
                            <eid:RadIconTile ID="RadIconTile4" Width="200" Height="100" runat="server" Name="Users"
                                Target="_self" Shape="Wide" ImageUrl="Images/user.png" BackColor="#3894E7" ForeColor="White"
                                NavigateUrl="UserList.aspx">
                                <Title Text="Users"></Title>
                            </eid:RadIconTile>
                            <eid:RadIconTile ID="RadIconTile6" Width="200" Height="100" runat="server" Name="Customers"
                                Target="_self" Shape="Wide" ImageUrl="Images/group.png" BackColor="#3894E7" ForeColor="White"
                                NavigateUrl="CustomerList.aspx">
                                <Title Text="Customers"></Title>
                            </eid:RadIconTile>
                            <eid:RadIconTile ID="RadIconTile7" Width="200" Height="100" runat="server" Name="Suppliers"
                                Target="_self" Shape="Wide" ImageUrl="Images/suppliers.png" BackColor="#3894E7"
                                ForeColor="White" NavigateUrl="SupplierList.aspx">
                                <Title Text="Suppliers"></Title>
                            </eid:RadIconTile>
                            <eid:RadIconTile ID="RadIconTile8" Width="200" Height="100" runat="server" Name="Warranties"
                                Target="_blank" Shape="Wide" ImageUrl="Images/warranty.png" BackColor="#3894E7"
                                ForeColor="White">
                                <Title Text="Warranties"></Title>
                            </eid:RadIconTile>
                        </eid:TileGroup>
                    </Groups>
                </eid:RadTileList>
            </div>
            <asp:Timer Interval="2000" OnTick="RefreshTileList" runat="server" ID="timer">
            </asp:Timer>
            <eid:RadNotification ID="batteryNotification" runat="server" Width="380" Height="100"
                Animation="Fade" EnableRoundedCorners="true" VisibleTitlebar="false" LoadContentOn="EveryShow">
            </eid:RadNotification>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
