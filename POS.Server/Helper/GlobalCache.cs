﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POS.Data;

namespace POS
{
    public class GlobalCache
    {
        private static List<Client> _clients;
        public static List<Client> Clients
        {
            get
            {
                if (_clients == null)
                    RefreshClients();
                return _clients;
            }
        }

        private static List<Store> _stores;
        public static List<Store> Stores
        {
            get
            {
                if (_stores == null)
                    RefreshStores();
                return _stores;
            }
        }


        private static List<Category> _categories;
        public static List<Category> Categories
        {
            get
            {
                if (_categories == null)
                {
                    RefreshCategories();
                }

                return _categories;
            }
        }

        private static List<Product> _products;
        public static List<Product> Products
        {
            get
            {
                if (_products == null)
                {
                    RefreshProducts(0, "", "");
                }

                return _products;
            }
        }

        private static List<Invoice> _invoices;
        public static List<Invoice> Invoices
        {
            get
            {
                if (_invoices == null)
                {
                    RefreshInvoices(0, "", null, null, false);
                }

                return _invoices;
            }
        }

        private static List<Inventory> _inventories;
        public static List<Inventory> Inventories
        {
            get
            {
                if (_inventories == null)
                {
                    RefreshInventories(0, "", "", 0);
                }

                return _inventories;
            }
        }

        private static List<User> _users;
        public static List<User> Users
        {
            get
            {
                if (_users == null)
                {
                    RefreshUsers();
                }

                return _users;
            }
        }

        private static List<Supplier> _suppliers;
        public static List<Supplier> Suppliers
        {
            get
            {
                if (_suppliers == null)
                {
                    RefreshSuppliers();
                }

                return _suppliers;
            }
        }

        private static List<Warranty> _warranties;
        public static List<Warranty> Warranties
        {
            get
            {
                if (_warranties == null)
                {
                    RefreshWarranties();
                }

                return _warranties;
            }
        }

        private static List<Customer> _customers;
        public static List<Customer> Customers
        {
            get
            {
                if (_customers == null)
                {
                    RefreshCustomers();
                }

                return _customers;
            }
        }

        private static List<Log> _logs;
        public static List<Log> Logs
        {
            get
            {
                if (_logs == null)
                {
                    RefreshLogs();
                }

                return _logs;
            }
        }

        private static Setting _setting;
        public static Setting Setting
        {
            get
            {
                if (_setting == null)
                {
                    RefreshSetting();
                }

                return _setting;
            }
        }

        public static User CurrentUser { get; set; }

        public static int UserStoreID
        {
            get
            {
                return GlobalCache.CurrentUser.Store == null ? 0 : GlobalCache.CurrentUser.Store.sto_Id;
            }
        }

        public static int UserClientID
        {
            get
            {
                return GlobalCache.CurrentUser.Client == null ? 0 : GlobalCache.CurrentUser.Client.ID;
            }
        }

        public static void RefreshClients()
        {
            _clients = new POSService.POSService().GetClients();
        }

        public static void RefreshStores()
        {
            _stores = new POSService.POSService().GetStores(UserClientID);
        }

        public static void RefreshCategories()
        {
            _categories = new POSService.POSService().GetCategories(GlobalCache.UserClientID);
        }

        public static void RefreshProducts(int categoryID, string name, string serialNo)
        {
            _products = new POSService.POSService().GetProducts(GlobalCache.UserClientID);
            if (categoryID != 0 && _products != null)
            {
                IEnumerable<Product> list = _products.Where(p => p.Category != null && p.Category.cat_Id == categoryID);
                if (list == null)
                {
                    _products = null;
                }
                else
                {
                    _products = list.ToList();
                }
            }
            if (!string.IsNullOrEmpty(name) && _products != null)
            {
                IEnumerable<Product> list = _products.Where(p => p.pro_Name.Contains(name));
                if (list == null)
                {
                    _products = null;
                }
                else
                {
                    _products = list.ToList();
                }
            }
            if (!string.IsNullOrEmpty(serialNo) && _products != null)
            {
                IEnumerable<Product> list = _products.Where(p => p.pro_No.Contains(serialNo));
                if (list == null)
                {
                    _products = null;
                }
                else
                {
                    _products = list.ToList();
                }
            }

            if (_products == null)
                _products = new List<Product>();
        }

        public static void RefreshInvoices(int invno, string customer, DateTime? datefrom, DateTime? dateto, bool isCancel)
        {
            _invoices = new POSService.POSService().GetInvoices(GlobalCache.UserStoreID, GlobalCache.UserClientID, invno, customer, datefrom, dateto);
            IEnumerable<Invoice> list = _invoices.Where(i => i.IsCancel == isCancel);
            if (list == null)
            {
                _invoices = new List<Invoice>();
            }
            else
            {
                _invoices = list.ToList();
            }
        }

        public static void RefreshInventories(int categoryID, string name, string serialNo, int quantity)
        {
            _inventories = new POSService.POSService().GetInventories(GlobalCache.UserClientID, GlobalCache.UserStoreID);

            if (categoryID != 0 && _products != null)
            {
                IEnumerable<Inventory> list = _inventories.Where(p => p.Product.Category != null && p.Product.Category.cat_Id == categoryID);
                if (list == null)
                {
                    _inventories = null;
                }
                else
                {
                    _inventories = list.ToList();
                }
            }
            if (!string.IsNullOrEmpty(name) && _inventories != null)
            {
                IEnumerable<Inventory> list = _inventories.Where(p => p.Product.pro_Name.Contains(name));
                if (list == null)
                {
                    _inventories = null;
                }
                else
                {
                    _inventories = list.ToList();
                }
            }
            if (!string.IsNullOrEmpty(serialNo) && _inventories != null)
            {
                IEnumerable<Inventory> list = _inventories.Where(p => p.Product.pro_No.Contains(serialNo));
                if (list == null)
                {
                    _inventories = null;
                }
                else
                {
                    _inventories = list.ToList();
                }
            }

            if (quantity != 0 && _inventories != null)
            {
                IEnumerable<Inventory> list = null;
                if (quantity <= 100)
                {
                    list = _inventories.Where(p => p.Quantity <= quantity);
                }
                else
                {
                    list = _inventories.Where(p => p.Quantity >= quantity);
                }

                if (list == null)
                {
                    _inventories = null;
                }
                else
                {
                    _inventories = list.ToList();
                }
            }

            if (_inventories == null)
                _inventories = new List<Inventory>();
        }

        public static void RefreshUsers()
        {
            _users = new POSService.POSService().GetUsers(GlobalCache.UserClientID, GlobalCache.UserStoreID);
            IEnumerable<POS.Data.User> source3 = _users.Where(u => u.usr_UserTypeID == 2 || u.usr_UserTypeID == 3);
            if (source3 != null)
            {
                _users = source3.ToList();
            }
            else
            {
                _users = new List<User>();
            }
        }

        public static void RefreshSuppliers()
        {
            _suppliers = new POSService.POSService().GetSuppliers(GlobalCache.UserClientID);
        }

        public static void RefreshWarranties()
        {
            _warranties = new POSService.POSService().GetWarranties(GlobalCache.UserClientID, GlobalCache.UserStoreID);
        }

        public static void RefreshCustomers()
        {
            _customers = new POSService.POSService().GetCustomers(GlobalCache.UserClientID);
        }

        public static void RefreshLogs()
        {
            _logs = new POSService.POSService().GetLogs(GlobalCache.UserClientID, GlobalCache.UserStoreID);
        }

        public static void RefreshSetting()
        {
            List<Setting> settings = new POSService.POSService().GetSetting(GlobalCache.UserClientID, GlobalCache.UserStoreID);
            if (settings.Any())
            {
                _setting = settings[0];
            }
        }

        public static void RefreshAll()
        {
            RefreshCategories();
            RefreshProducts(0, "", "");
            RefreshInvoices(0, "", null, null, false);
            RefreshInventories(0, "", "", 0);
            RefreshUsers();
            RefreshSuppliers();
            RefreshWarranties();
            RefreshCustomers();
            RefreshLogs();
            RefreshSetting();
        }
    }
}