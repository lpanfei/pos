﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Text;

namespace POS
{
    public class BasePage : Page
    {

        public int UserStoreID
        {
            get
            {
                return GlobalCache.CurrentUser.Store == null ? 0 : GlobalCache.CurrentUser.Store.sto_Id;
            }
        }

        public int UserClientID
        {
            get
            {
                return GlobalCache.CurrentUser.Client == null ? 0 : GlobalCache.CurrentUser.Client.ID;
            }
        }

        public BasePage()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        protected override void OnPreLoad(EventArgs e)
        {
            base.OnPreLoad(e);
            if (GlobalCache.CurrentUser == null)
            {
                Response.Redirect("Default.aspx");
            }
        }

        public void Logout()
        {
            GlobalCache.CurrentUser = null;
            Response.Redirect("Default.aspx");
        }
    }
}