﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="InvoiceList.aspx.cs" Inherits="POS.InvoiceList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript" src="My97DatePicker/WdatePicker.js"></script>
    <script type="text/javascript">
        var previous = null;
        function GetRadWindow() {
            var oWindow = null; if (window.radWindow)
                oWindow = window.radWindow; else if (window.frameElement.radWindow)
                oWindow = window.frameElement.radWindow; return oWindow;
        }
        function pageLoad(sender, args) {
            if ($("#pagelist").length == 0) {
                $("#sidebar").parent().css("display", "none");
            } else {
                $("#sidebar").parent().css("display", "");
            }
            $(".list tr").unbind("click");
            $(".list tr").click(function () {

                var current = $(this).find(".buttons div").eq(0);
                if ($(previous).is(current))
                    return;
                current.toggle("slow");
                var $td = $(this).find(".invcancel").eq(0);
                if ($.trim($td.text()) == "True") {
                    $(this).find(".icon-cancel").css("display", "none");
                }
                if (previous != null) {
                    $(previous).toggle("slow", function () { });
                }
                previous = current;
            }
            );

            $(".txtchanged").change(function () {
                __doPostBack($(this).attr("name"), "");
            });

            $("#Button3").click(function () {
                
               var radwindow = $find('<%=RadWindow1.ClientID %>');
                radwindow.close();
                $("#btnCancelInvoice").trigger("click");
            });

            $(".icon-cancel").click(function () {
                var radwindow = $find('<%=RadWindow1.ClientID %>');
                radwindow.show();
                var $tr = $(this).closest("tr");
                var $invid = $tr.find(".invid").eq(0);
                var $invno = $tr.find(".invno").eq(0);
                $("#lblCancelID").val($.trim($invid.text()));
                $("#lblCancelNo").text($.trim($invno.text()));
            });
        }

        

    </script>
    <style type="text/css">
        .displaynone
        {
            display: none;
        }
        .displayit
        {
            display:;
        }
        .btn-add
        {
            cursor: pointer;
        }
        .inputtype
        {
            height: 30px;
            width: 120px;
            padding-left: 10px;
        }
        .nonelabel
        {
            display: none;
        }
        .pdiv
        {
            border: 1px solid #3894E7;
        }
        .pdiv:hover
        {
            border: 1px solid black;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="mainheader">
                <div class="contextual">
                    <asp:Panel ID="formbuttons" Visible="false" runat="server">
                        <div style="float: left; width: 75px; height: 10px;">
                            <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click" UseSubmitBehavior="false"
                                class="btn btn-large btn-block btn-info" />
                        </div>
                        <%--<div style="float: left; margin-left: 10px; width: 150px; height: 10px;">--%>
                            <%--<asp:Button ID="Button1" runat="server" Text="Save and Print" OnClick="btnPrint_Click"
                                UseSubmitBehavior="false" class="btn btn-large btn-block btn-info" /></div>--%>
                        <div style="width: 85px; float: left; display: block; margin-left: 10px;">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button2"
                                Text="Back" OnClick="BackToList" />
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="listbuttons" runat="server">
                        <div style="height: 10px; width: 85px; float: left;">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="addLink"
                                Text="Add" OnClick="AddInvoice" />
                        </div>
                        <div style="height: 10px; width: 150px; float: left; margin-left: 10px;">
                           <%-- <a href="InvoiceCancelList.aspx" class="btn btn-large btn-block btn-info">Cancelled
                                List</a>--%>
                                <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                                Text="Cancelled List" OnClick="CancelledList" />
                        </div>
                    </asp:Panel>
                </div>
                <div class="subheader">
                    <h3 class="icon-invoice pagetitle">
                        <asp:Literal ID="ltlPageTitle" Text="Invoices" runat="server"></asp:Literal>
                    </h3>
                </div>
            </div>
            <asp:Panel runat="server" ID="pagelist" ClientIDMode="Static">
            <asp:Panel runat="server" ID="searcharea">
                <fieldset>
                    <legend>Search</legend>
                    <div style="padding: 5px">
                        <table>
                            <tr>
                                <td width="120px">
                                    Invoice No:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNo" runat="server"></asp:TextBox>
                                </td>
                                <td width="100px" style="padding-left: 20px;">
                                    Date From:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDateFrom" runat="server" CssClass="Wdate"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Customer:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomer" runat="server"></asp:TextBox>
                                </td>
                                <td width="100px" style="padding-left: 20px;">
                                    Date To:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDateTo" runat="server" CssClass="Wdate"></asp:TextBox>
                                </td>
                            </tr>
                            <%--<tr>
                <td>Date:</td>
                <td><asp:TextBox ID="txtDate" runat="server" CssClass="Wdate"></asp:TextBox></td>
                <td></td>
                <td></td>
            </tr>--%>
                            <tr>
                                <td colspan="4" style="padding-top: 5px;">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"
                                        Width="100" UseSubmitBehavior="false" class="btn btn-large btn-block btn-info"
                                        Style="float: left;" />
                                   <%-- <asp:Button ID="btnPrint" runat="server" Text="Print" Width="100" UseSubmitBehavior="false"
                                        class="btn btn-large btn-block btn-info" Style="float: left; margin-left: 10px;" />
                                    <asp:Button ID="btnPrintSoldInventory" runat="server" Text="Print Sold Inventory"
                                        Width="200" UseSubmitBehavior="false" class="btn btn-large btn-block btn-info"
                                        Style="float: left; margin-left: 10px;" />--%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </fieldset>
                </asp:Panel>
                <br />
                <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand">
                    <HeaderTemplate>
                        <table class="list">
                            <thead>
                                <tr>
                                    <th>
                                        Invoice No
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th>
                                        Staff
                                    </th>
                                    <th>
                                        Customer
                                    </th>
                                    <th>
                                        Address
                                    </th>
                                    <th>
                                        Phone
                                    </th>
                                    <th>
                                        Mobile
                                    </th>
                                    <th>
                                        Total (inc GST)
                                    </th>
                                    <th class="reasoncol"><asp:Label ID="reasonColumn" runat="server"></asp:Label></th>
                                    <th>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center" class="invno">
                                <%# InvoiceNumberPrefix + Eval("inv_No")%>
                            </td>
                            <td align="center">
                                <asp:Label ID="lblDate" runat="server" Text='<%# Eval("inv_CreateDate")%>'></asp:Label>
                            </td>
                            <td align="center">
                                <%# Eval("inv_Staff")%>
                            </td>
                            <td align="center">
                                <%# Eval("inv_Customer")%>
                            </td>
                            <td align="center">
                                <%# Eval("inv_Address")%>
                            </td>
                            <td align="center">
                                <%# Eval("inv_Phone") %>
                            </td>
                            <td align="center">
                                <%# Eval("inv_Mobile")%>
                            </td>
                            <td align="center">
                                $<asp:Label ID="lblTotalIncGST" runat="server" Text='<%# Eval("inv_TotalIncGST")%>'></asp:Label>
                            </td>
                            <td  align="center" class='<%# Convert.ToBoolean(Eval("IsCancel"))? "displayit":"displaynone"%> '>
                                <%# Eval("Reason")%>
                            </td>
                            <td class="buttons">
                                <div style="display: none;">
                                   <%-- <asp:LinkButton ID="lbtnPrint" runat="server" Text="Reprint" class="icon icon-print"
                                        CommandName="ItemPrint" CommandArgument='<%# Eval("inv_Id") %>'></asp:LinkButton>--%>
                                    &nbsp;&nbsp;<asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandName="ItemEdit"
                                        CommandArgument='<%# Eval("inv_Id") %>' CssClass="icon icon-edit"></asp:LinkButton>&nbsp;&nbsp;
                                    <%--<asp:LinkButton ID="lblDel" runat="server" Text="Cancel"
                                         CssClass="icon icon-cancel"></asp:LinkButton>--%>
                                    <a href="#" class="icon icon-cancel">Cancel</a>
                                    <asp:Label ID="lblNo" runat="server" Text='<%# Eval("inv_Id")%>' Visible="false"></asp:Label></div>
                            </td>
                            <td style="display:none" class="invid">
                            
                            <%# Eval("inv_Id")%>
                            </td>
                            <td style="display:none" class="invcancel">
                            
                            <%# Eval("IsCancel")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
                <webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged" PageSize="1"
                    ClientIDMode="Static">
                </webdiyer:AspNetPager>
               <asp:Button ID="btnCancelInvoice"  UseSubmitBehavior="false"  OnClick="Cancel" runat="server" CssClass="displaynone" ClientIDMode="Static" Text="Cancel" ></asp:Button>
            </asp:Panel>
            <asp:Panel runat="server" ID="pageform" ClientIDMode="Static">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                     
                        <table width="100%" height="100%">
                            <tr>
                                <td width="45%" valign="top">
                                    <div class="box" style="height: 290px;">
                                        <table style="float: left; margin-right: 20px;">
                                            <%-- <tr style="height: 50px;">
                                <td colspan="2">
                                    <span style="font-weight: bold; font-size: 20px;">Customer Info.</span>
                                </td>
                            </tr>--%>
                                            <tr>
                                                <td style="width: 120px;">
                                                    Customer Name:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtName" runat="server" Width="115" Height="30"></asp:TextBox>
                                                    <asp:DropDownList ID="dropCustomer" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dropCustomer_SelectedIndexChanged"
                                                        Width="115" Height="30" ClientIDMode="Static" DataTextField="Name" DataValueField="ID">
                                                    </asp:DropDownList>
                                                    <asp:ImageButton ID="editImageBtn" runat="server" ImageUrl="~/Images/edit.png" OnClick="OnEdit" />
                                                    <%-- <eid:RadComboBox ID="dropCustomer" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dropCustomer_SelectedIndexChanged"
                                                        ViewStateMode="Enabled" Width="130" Skin="Windows7" EnableViewState="true" DataTextField="Name"
                                                        DataValueField="ID" AllowCustomText="true">
                                                    </eid:RadComboBox>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Address:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtAddress" runat="server" CssClass="inputtype"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Country:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtCountry" runat="server" CssClass="inputtype"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Phone:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPhone" runat="server" CssClass="inputtype"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Mobile:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtMobile" runat="server" CssClass="inputtype"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Email:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtEmail" runat="server" CssClass="inputtype"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Wallet ID:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtWalletID" runat="server" CssClass="inputtype"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <table style="margin-left: 20px;">
                                            <tr>
                                                <td style="width: 100px; height: 35px;">
                                                    <span>Invoice No.</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblPrefix" runat="server"></asp:Label><asp:Label ID="lblNo" runat="server"></asp:Label><asp:Label
                                                        ID="lblIsNew" runat="server" Visible="false"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 35px;">
                                                    <span>Date:</span>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 35px;">
                                                    <span>Type:</span>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="dropType" Height="30" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dropType_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div>
                                        <span style="font-weight: bold; font-size: 20px; float: left;">Product List</span>
                                    </div>
                                    <table style="width: 100%; margin-top: 10px;" class="list">
                                        <thead>
                                            <tr>
                                                <th align="center">
                                                </th>
                                                <th style="width: 50px; display: none;">
                                                    Item
                                                </th>
                                                <th>
                                                    Product name & Description
                                                </th>
                                                <th style="display: none;">
                                                    Serial No
                                                </th>
                                                <th>
                                                    Price (Inc GST)
                                                </th>
                                                <th>
                                                    Discount
                                                </th>
                                                <th>
                                                    Qty
                                                </th>
                                                <th>
                                                    Sub Total
                                                </th>
                                                <th style="width: 200px; display: none;">
                                                    Note
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptProductList" runat="server">
                                                <HeaderTemplate>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:LinkButton ID="lbtnDel" runat="server" Text="Delete" class="icon icon-del" OnClick="DeleteInvoiceItem"></asp:LinkButton>
                                                            <asp:Label ID="lblId" runat="server" Text='<%# Eval("ip_ID")%>' Visible="false"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("ip_Description")%>'></asp:Label>
                                                        </td>
                                                        <td align="center" style="display: none;">
                                                            <asp:Label ID="lblSerialNo" runat="server" Text=''></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            $<asp:TextBox ID="txtPrice" runat="server" Width="80px" Text='<%# Convert.ToDecimal(Eval("ip_Price")).ToString("0.00")%>'
                                                                OnTextChanged="txtPrice_TextChanged" CssClass="txtchanged"></asp:TextBox>
                                                        </td>
                                                        <td align="center">
                                                            <asp:TextBox ID="txtDiscount" runat="server" Width="50px" Text='<%# Convert.ToDecimal(Eval("ip_Discount")).ToString("0.00")%>'
                                                                AutoPostBack="true" OnTextChanged="txtDiscount_TextChanged"></asp:TextBox>
                                                        </td>
                                                        <td align="center">
                                                            <asp:TextBox ID="txtQuantity" runat="server" Width="50px" Text='<%# Eval("ip_Quantity")%>'
                                                                OnTextChanged="txtQuantity_TextChanged" CssClass="txtchanged"></asp:TextBox>
                                                            <asp:Label ID="lblProductId" runat="server" Text='<%# Eval("ip_ProductID")%>' Visible="false"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            $<asp:Label ID="lblSubTotal" runat="server" Text='<%# Eval("ip_SubTotal")%>'></asp:Label><asp:Label
                                                                ID="lblCost" runat="server" Visible="false" Text='<%# Eval("cost")%>'></asp:Label>
                                                        </td>
                                                        <td align="center" style="display: none;">
                                                            <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="4" Text='<%# Eval("ip_Note")%>'></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                    <table style="float: right">
                                        <tr style="font-weight: bold; border-spacing: 0;">
                                            <td colspan="7" style="text-align: right;">
                                                Total:
                                            </td>
                                            <td colspan="2">
                                                $<asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td colspan="7" style="text-align: right;">
                                                GST:
                                            </td>
                                            <td colspan="2">
                                                $<asp:Label ID="lblGST" runat="server" Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td colspan="7" style="text-align: right;">
                                                Total(<asp:CheckBox ID="chkIsGST" runat="server" Text="With GST" AutoPostBack="True"
                                                    OnCheckedChanged="chkIsGST_CheckedChanged" />):
                                            </td>
                                            <td colspan="2">
                                                $<asp:Label ID="lblTotalGST" runat="server" Text="0"></asp:Label>
                                                <asp:Label ID="lblTotalCost" runat="server" Text="0" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td colspan="7" style="text-align: right;">
                                                Payment Method:
                                            </td>
                                            <td colspan="2">
                                                <asp:DropDownList ID="dropPayMethod" runat="server">
                                                    <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                    <asp:ListItem Text="Credit Card" Value="Credit Card"></asp:ListItem>
                                                    <asp:ListItem Text="Nets" Value="Nets"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style="font-weight: bold;">
                                            <td colspan="7" style="text-align: right;">
                                                Payment To:
                                            </td>
                                            <td colspan="2">
                                                <asp:DropDownList ID="dropPrintTo" runat="server">
                                                    <asp:ListItem Text="SMS/MMS" Value="SMS/MMS"></asp:ListItem>
                                                    <asp:ListItem Text="Print & Wallet (App)" Value="Print & Wallet (App)"></asp:ListItem>
                                                    <asp:ListItem Text="Wallet(App)" Value="Wallet(App)"></asp:ListItem>
                                                    <asp:ListItem Text="Print" Value="Print"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td valign="top">
                                    <table style="width: 100%; height: 100%; margin-left: 10px;">
                                        <tr style="height: 40px;">
                                            <td valign="top">
                                                <asp:DropDownList Height="30" Width="180" Style="float: left;" ID="dropCategory"
                                                    runat="server" AutoPostBack="True" OnSelectedIndexChanged="dropCategory_SelectedIndexChanged"
                                                    DataTextField="cat_Name" DataValueField="cat_Id">
                                                </asp:DropDownList>
                                                <asp:DropDownList Height="30" Width="280" Style="float: left; margin-left: 10px;
                                                    display: none;" ID="dropProduct" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dropProduct_SelectedIndexChanged">
                                                </asp:DropDownList>
                                                <asp:Panel ID="pnlAddProduct" runat="server" Style="float: left; margin-left: 10px;
                                                    display: none;">
                                                    <div style="float: left; margin-left: 10px; vertical-align: middle; padding-top: 7px;
                                                        display: none;">
                                                        Price:
                                                        <asp:Label ID="lblPrice" runat="server" ClientIDMode="Static" CssClass="nonelabel"></asp:Label>
                                                        &nbsp;&nbsp; Quantity:
                                                        <asp:Label ID="lblQuantity" runat="server" ClientIDMode="Static" CssClass="nonelabel"></asp:Label>
                                                        &nbsp;<asp:Label ID="lblCost" runat="server" CssClass="nonelabel" ClientIDMode="Static"></asp:Label>
                                                        <asp:Label ID="lblSerialNo" runat="server" CssClass="nonelabel" ClientIDMode="Static"></asp:Label>
                                                        &nbsp;<asp:Label ID="lblProductName" runat="server" CssClass="nonelabel" ClientIDMode="Static"></asp:Label>
                                                        &nbsp;<asp:Label ID="lblProductID" runat="server" CssClass="nonelabel" ClientIDMode="Static"></asp:Label>
                                                        &nbsp;
                                                        <input type="hidden" runat="server" id="input_price" clientidmode="Static" />
                                                        <input type="hidden" runat="server" id="input_quantity" clientidmode="Static" />
                                                        <input type="hidden" runat="server" id="input_cost" clientidmode="Static" />
                                                        <input type="hidden" runat="server" id="input_no" clientidmode="Static" />
                                                        <input type="hidden" runat="server" id="input_name" clientidmode="Static" />
                                                        <input type="hidden" runat="server" id="input_id" clientidmode="Static" />
                                                    </div>
                                                </asp:Panel>
                                                <asp:Button ID="btnAddProduct" ClientIDMode="Static" runat="server" Text="Add" OnClick="btnAddProduct_Click"
                                                    UseSubmitBehavior="false" class="btn btn-large btn-block btn-info" Width="50"
                                                    Style="float: left; margin-left: 10px; display: none;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top">
                                                <eid:RadListView ID="productsListView" runat="server" ItemPlaceholderID="ProductsHolder"
                                                    DataKeyNames="pro_Id">
                                                    <LayoutTemplate>
                                                        <fieldset style="width: 97%; height: 100%;" id="FieldSet1">
                                                            <legend>Products</legend>
                                                            <asp:Panel ID="ProductsHolder" runat="server">
                                                            </asp:Panel>
                                                        </fieldset>
                                                    </LayoutTemplate>
                                                    <ItemTemplate>
                                                        <div style="float: left; width: 45%; margin: 12px; min-width: 200px; background-color: #3894E7;
                                                            color: White; padding: 8px;" class="pdiv">
                                                            <table width="100%" height="100%">
                                                                <tr>
                                                                    <td>
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td>
                                                                                    Name:
                                                                                </td>
                                                                                <td style="padding-left: 5px;" class="pro_name">
                                                                                    <asp:Label ID="lblPName" Text='<%# Eval("pro_Name") %>' runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    Quantity:
                                                                                </td>
                                                                                <td style="padding-left: 5px;" class="pro_quantity">
                                                                                    <asp:Label ID="lblPQuantity" Text='<%# Eval("pro_Quantity") %>' runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    Price:
                                                                                </td>
                                                                                <td style="padding-left: 5px;" class="pro_price">
                                                                                    <asp:Label ID="lblPPrice" Text='<%#DataBinder.Eval(Container.DataItem, "pro_Price", "{0:C}")%>'
                                                                                        runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    Cost:
                                                                                </td>
                                                                                <td style="padding-left: 5px;" class="pro_cost">
                                                                                    <asp:Label ID="lblPCost" Text='<%# DataBinder.Eval(Container.DataItem, "cost", "{0:C}")%>'
                                                                                        runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td>
                                                                                    Serial No:
                                                                                </td>
                                                                                <td style="padding-left: 5px;" class="pro_no">
                                                                                    <asp:Label ID="lblPNo" Text='<%# Eval("pro_no")%>' runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="display: none;">
                                                                                <td class="pro_id">
                                                                                    <asp:Label ID="lblPId" Text='<%# Eval("pro_id")%>' runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="right" valign="bottom">
                                                                        <asp:ImageButton ImageUrl="Images/add.png" AlternateText="Add To Invoice" runat="server"
                                                                            OnClick="btnAddProduct_Click" />
                                                                        <%--<img src="Images/add.png" alt="Add To Invoice" class="pro_add_btn" style="cursor:pointer"/>--%>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </ItemTemplate>
                                                </eid:RadListView>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table style="width: 100%">
                            <tr>
                                <td>
                                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblIsGST" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblGSTSetting" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </asp:Panel>
            <eid:RadWindow runat="server" ID="RadWindow1" Skin="Hay" Overlay="true" Modal="true"
                Title="Cancel Invoice" VisibleStatusbar="false" AutoSize="true" Width="600" Height="400">
                <ContentTemplate>
                    <asp:UpdatePanel ID="Updatepanel11" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table class="form-table">
                                <tr>
                                    <td class="form-label">
                                        Invoice No.:
                                    </td>
                                    <td>
                                        <div style="border: 1px solid #D1E4F6; height: 30px; padding-top: 9px; padding-left: 10px;">
                                            <asp:Label ID="lblCancelNo" runat="server" ClientIDMode="Static"></asp:Label></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="form-label">
                                        Reason:
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" Rows="6" Width="400"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">
                                    <input type="hidden" id="lblCancelID" name="cancelid" runat="server" clientidmode="Static" value="" />
                                    <input type="button" id="Button3" value="Submit" class="btn btn-large btn-block btn-info" />
                                       <%-- <asp:Button ID="Button3" runat="server" Text="Submit"  UseSubmitBehavior="false"
                                            class="btn btn-large btn-block btn-info" ClientIDMode="Static" />--%>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </eid:RadWindow>
           <eid:RadWindowManager runat="server" ID="WindowManager"></eid:RadWindowManager>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
