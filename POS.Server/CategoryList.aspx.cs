﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS
{
    public partial class CategoryList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialize();
            }
        }
        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                delid.Attributes["value"] = e.CommandArgument.ToString();
                WindowManager.RadConfirm("Are you sure to delete the item?", "confirmclosed", 300, 100, null, "Delete Category");
            }
            else if (e.CommandName == "ItemEdit")
            {
                 RepeaterItem item = e.Item;
                 formbuttons.Visible = true;
                 listbuttons.Visible = false;
                 formpanel.Visible = true;
                 listpanel.Visible = false;
                 ltlPageTitle.Text = "Category / Edit";
                catid.Attributes["value"] = e.CommandArgument.ToString();
                txtName.Text = (item.FindControl("lblName") as Label).Text.Replace("├", "").Replace("─", ""); ;
                dropParent.SelectedValue = (item.FindControl("lblParentID") as Label).Text;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                WindowManager.RadAlert("Category name is mandatory", 300, 100, "Validation", "");
                return;
            }
            POSService.POSService pos = new POSService.POSService();
            Category Parent = new Category();
            if (Convert.ToInt32(dropParent.SelectedValue) != 0)
            {
                Parent = GlobalCache.Categories.FirstOrDefault(c => c.cat_Id == Convert.ToInt32(dropParent.SelectedValue));
            }
            pos.AddCategory(new Category() { cat_Id=Convert.ToInt32(catid.Value),cat_Name=txtName.Text.Trim(),ParentID=Convert.ToInt32(dropParent.SelectedValue),cat_ClientID=this.UserClientID,Parent=Parent});

            BackToList(null,null);
        }

        void Initialize()
        {
            catid.Value = "0";
            delid.Attributes["value"] = "-1";
            txtName.Text = "";
            BindCategories(0);
            Bind();
        }

        void BindCategories(int catid)
        {
            List<Category> cats2 = new List<Category>();
            cats2.Add(new Category() { cat_Id = 0, cat_Name = "--No Parent--" });
            GlobalCache.Categories.GetHiarachy().ForEach(c =>
            {
                if (c.cat_Id != catid)
                {
                    cats2.Add(c);
                }
            });
            dropParent.DataSource = cats2;
            dropParent.DataBind();
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = GlobalCache.Categories.GetHiarachy();
            rptList.DataBind();
        }

        protected void AddCategory(object sender, EventArgs args)
        {
            formbuttons.Visible = true;
            listbuttons.Visible = false;
            formpanel.Visible = true;
            listpanel.Visible = false;
            ltlPageTitle.Text = "Category / Add";
            dropParent.SelectedIndex = 0;
            txtName.Text = "";
            catid.Value = "0";
        }

        protected void BackToList(object sender, EventArgs e)
        {
            listbuttons.Visible = true;
            formbuttons.Visible = false;
            formpanel.Visible = false;
            listpanel.Visible = true;
            ltlPageTitle.Text = "Category List";
            Initialize();
        }

        protected void DeleteCategory(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delid.Attributes["value"]);
            POSService.POSService service = new POSService.POSService();
            service.DeleteCategory(new Category() { cat_Id = id });
            GlobalCache.RefreshCategories();
            Initialize();
        }
    }
}