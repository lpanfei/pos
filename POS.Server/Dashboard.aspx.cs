﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using POS.Data;

namespace POS
{
    public partial class Dashboard : BasePage
    {
        public static List<Invoice> Invoices
        {
            get;
            set;
        }
        public static List<Store> Stores
        {
            get;
            set;
        }
        public static List<User> Users
        {
            get;
            set;
        }
        public static List<Customer> Customers
        {
            get;
            set;
        }
        public static List<Supplier> Suppliers
        {
            get;
            set;
        }
        public static List<Product> Products
        {
            get;
            set;
        }
        public static List<Warranty> Warranties
        {
            get;
            set;
        }
        public static List<Category> Categories
        {
            get;
            set;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RefreshTileList(null,null);
                Dashboard.Invoices = null;
                Dashboard.Categories = null;
                Dashboard.Warranties = null;
                Dashboard.Products = null;
                Dashboard.Suppliers = null;
                Dashboard.Customers = null;
                Dashboard.Users = null;
                Dashboard.Stores = null;
            }
        }

        protected void Back(object sender, EventArgs args)
        {
            RefreshTileList(null, null);
        }

        protected void RefreshTileList(object sender, EventArgs args)
        {
            POSService.POSService service = new POSService.POSService();

            List<Store> stores = service.GetStores(this.UserClientID);
            List<Product> products = service.GetProducts(this.UserClientID);
            List<Category> categories = service.GetCategories(this.UserClientID);
            List<Invoice> invoices = service.GetInvoices(0, this.UserClientID, 0, null, null, null);
            List<Customer> customers = service.GetCustomers(this.UserClientID);
            List<Warranty> warranties = service.GetWarranties(this.UserClientID,0);
            List<User> users = service.GetUsers(this.UserClientID, 0);
            List<Supplier> suppliers = service.GetSuppliers(this.UserClientID);
            if (Dashboard.Stores != null)
            {
                if (Dashboard.Stores.Count > stores.Count)
                {
                    batteryNotification.Text = "New Store Available";
                    batteryNotification.Show();
                    Dashboard.Stores = stores;
                    tilelist.GetTileByName("Stores").Badge.Value = stores.Count;
                }
            }
            else
            {
                Dashboard.Stores = stores;
                tilelist.GetTileByName("Stores").Badge.Value = stores.Count;
            }

            if (Dashboard.Products != null)
            {
                if (Dashboard.Products.Count > products.Count)
                {
                    batteryNotification.Text = "New Product Available";
                    batteryNotification.Show();
                    Dashboard.Products = products;
                    tilelist.GetTileByName("Products").Badge.Value = products.Count;
                }
            }
            else
            {
                Dashboard.Products = products;
                tilelist.GetTileByName("Products").Badge.Value = products.Count;
            }

            if (Dashboard.Categories != null)
            {
                if (Dashboard.Categories.Count > categories.Count)
                {
                    batteryNotification.Text = "New Category Available";
                    batteryNotification.Show();
                    Dashboard.Categories = categories;
                    tilelist.GetTileByName("Categories").Badge.Value = categories.Count;
                }
            }
            else
            {
                Dashboard.Categories = categories;
                tilelist.GetTileByName("Categories").Badge.Value = categories.Count;
            }

            if (Dashboard.Customers != null)
            {
                if (Dashboard.Customers.Count > customers.Count)
                {
                    batteryNotification.Text = "New Customer Available";
                    batteryNotification.Show();
                    Dashboard.Customers = customers;
                    tilelist.GetTileByName("Customers").Badge.Value = customers.Count;
                }
            }
            else
            {
                Dashboard.Customers = customers;
                tilelist.GetTileByName("Customers").Badge.Value = customers.Count;
            }

            if (Dashboard.Users != null)
            {
                if (Dashboard.Users.Count > users.Count)
                {
                    batteryNotification.Text = "New Customer Available";
                    batteryNotification.Show();
                    Dashboard.Users = users;
                    tilelist.GetTileByName("Users").Badge.Value = users.Count;
                }
            }
            else
            {
                Dashboard.Users = users;
                tilelist.GetTileByName("Users").Badge.Value = users.Count;
            }

            if (Dashboard.Suppliers != null)
            {
                if (Dashboard.Suppliers.Count > suppliers.Count)
                {
                    batteryNotification.Text = "New Customer Available";
                    batteryNotification.Show();
                    Dashboard.Suppliers = suppliers;
                    tilelist.GetTileByName("Suppliers").Badge.Value = suppliers.Count;
                }
            }
            else
            {
                Dashboard.Suppliers = suppliers;
                tilelist.GetTileByName("Suppliers").Badge.Value = suppliers.Count;
            }

            if (Dashboard.Invoices != null)
            {
                if (Dashboard.Invoices.Count > invoices.Count)
                {
                    batteryNotification.Text = "New Invoice Available";
                    batteryNotification.Show();
                    Dashboard.Invoices = invoices;
                    tilelist.GetTileByName("Invoices").Badge.Value = invoices.Count;
                }
            }
            else
            {
                Dashboard.Invoices = invoices;
                tilelist.GetTileByName("Invoices").Badge.Value = invoices.Count;
            }

            if (Dashboard.Warranties != null)
            {
                if (Dashboard.Warranties.Count > warranties.Count)
                {
                    batteryNotification.Text = "New Customer Available";
                    batteryNotification.Show();
                    Dashboard.Warranties = warranties;
                    tilelist.GetTileByName("Warranties").Badge.Value = warranties.Count;
                }
            }
            else
            {
                Dashboard.Warranties = warranties;
                tilelist.GetTileByName("Warranties").Badge.Value = warranties.Count;
            }
            
            
            //storeList.DataSource = null;
            //storeList.DataSource = stores;
            //storeList.DataBind();

            //productList.DataSource = null;
            //productList.DataSource = products;
            //productList.DataBind();

            invoiceList.DataSource = null;
            invoiceList.DataSource = invoices;
            invoiceList.DataBind();

            warrantyList.DataSource = null;
            warrantyList.DataSource = warranties ;
            warrantyList.DataBind();

            //customerList.DataSource = null;
            //customerList.DataSource = customers;
            //customerList.DataBind();

            //supplierList.DataSource = null;
            //supplierList.DataSource = suppliers;
            //supplierList.DataBind();

            //userList.DataSource = null;
            //userList.DataSource = users;
            //userList.DataBind();
            //categoryList.DataSource = null;
            //categoryList.DataSource = categories.GetHiarachy();
            //categoryList.DataBind();

            if (Dashboard.Invoices != null)
            {
                if (invoices.Count > Dashboard.Invoices.Count)
                {
                    batteryNotification.Text = "New Invoice Available";
                    batteryNotification.Show();
                }
            }
            Dashboard.Invoices = invoices;
        }

        protected void InvoicePageChanged(object sender, EventArgs args)
        {
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            invoiceList.DataSource = null;
            invoiceList.DataSource = Dashboard.Invoices.TakeFrom((currentPage - 1) * pageSize, pageSize);
            invoiceList.DataBind();
        }

        protected void WarrantyPageChanged(object sender, EventArgs args)
        {
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            invoiceList.DataSource = null;
            invoiceList.DataSource = Dashboard.Warranties.TakeFrom((currentPage - 1) * pageSize, pageSize);
            invoiceList.DataBind();
        }
    }
}