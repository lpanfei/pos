﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS
{
    public partial class WarrantyList : BasePage
    {
        List<Warranty> Warranties
        {
            get
            {
                if (Session["Warranties"] == null)
                {
                    GetWarranties();
                }

                return Session["Warranties"] as List<Warranty>;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeDropdowns();
                Bind();
                txtDate.Attributes.Add("onFocus", "WdatePicker({lang:'en',dateFmt:'dd-MM-yyyy'})");
            }
        }

        void InitializeDropdowns()
        {
            POSService.POSService service = new POSService.POSService();
            dropInvoice.DataSource = service.GetInvoices(this.UserStoreID, this.UserClientID, 0, null, null, null);
            dropInvoice.DataBind();
            dropInvoice.SelectedIndex = 0;
            InvoiceChanged(null, null);
        }

        protected void InvoiceChanged(object sender, EventArgs args)
        {
            POSService.POSService service = new POSService.POSService();
            dropProduct.DataSource = service.GetInvoiceProducts(Convert.ToInt32(dropInvoice.SelectedValue));
            dropProduct.DataBind();
        }

        void GetWarranties()
        {
            Session["Warranties"] = new POSService.POSService().GetWarranties(this.UserClientID, this.UserStoreID);
        }

        void Bind()
        {
            GetWarranties();
            this.AspNetPager1.RecordCount = Warranties.Count;
            rptList.DataSource = null;
            rptList.DataSource = Warranties.TakeFrom(0, this.AspNetPager1.PageSize);
            rptList.DataBind();
            this.AspNetPager1.CurrentPageIndex = 1;
            this.AspNetPager1.GoToPage(this.AspNetPager1.CurrentPageIndex);
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            rptList.DataSource = null;
            rptList.DataSource = Warranties.TakeFrom((currentPage - 1) * pageSize, pageSize);
            rptList.DataBind();
        }

        protected void AddWarranty(object sender, EventArgs e)
        {
            formbuttons.Visible = true;
            listbuttons.Visible = false;
            formpanel.Visible = true;
            listpanel.Visible = false;
            ltlPageTitle.Text = "Warranty / Add";
            dropInvoice.SelectedIndex = 0;
            //InvoiceChanged(null, null);
            txtComment.Text = "";
            txtDate.Text = "";
            txtQty.Text = "";
            txtResolution.Text = "";
            lblId.Text = "0";
            inpProductID.Attributes["value"] = "-1";
        }

        protected void BackToList(object sender, EventArgs e)
        {
            listbuttons.Visible = true;
            formbuttons.Visible = false;
            formpanel.Visible = false;
            listpanel.Visible = true;
            ltlPageTitle.Text = "Warranty List";
            inpProductID.Attributes["value"] = "-1";
            Bind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            POSService.POSService service = new POSService.POSService();
            Warranty w = new Warranty()
            {
                InvoiceID=Convert.ToInt32(dropInvoice.SelectedValue),
                ID=Convert.ToInt32(lblId.Text),
                ProductID=Convert.ToInt32(dropProduct.SelectedValue),
                Comment=txtComment.Text,
                Quantity = string.IsNullOrEmpty(txtQty.Text)?0:Convert.ToInt32(txtQty.Text),
                Resolution=txtResolution.Text,
                ClientID=this.UserClientID,
                StoreID=this.UserStoreID
            };
            if (string.IsNullOrEmpty(txtDate.Text))
            {
                w.Date = null;
            }
            else
            {
                try
                {
                    w.Date = DateTime.Parse(txtDate.Text);
                }
                catch
                {
                    w.Date = GetDateTime(txtDate.Text);
                }
            }
            service.AddWarranty(w);
            BackToList(null, null);
        }

        DateTime GetDateTime(string s)
        {
            string[] str = s.Split('-');
            int year = Convert.ToInt32(str[2]);
            int month = Convert.ToInt32(str[1]);
            int day = Convert.ToInt32(str[0]);
            return new DateTime(year, month, day, 0, 0, 0);
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemEdit")
            {
                RepeaterItem item = e.Item;
                formbuttons.Visible = true;
                listbuttons.Visible = false;
                formpanel.Visible = true;
                listpanel.Visible = false;
                ltlPageTitle.Text = "Warranty / Edit";
                dropInvoice.SelectedValue = (item.FindControl("lblInvoiceID") as Label).Text;
                txtComment.Text = (item.FindControl("lblComment") as Label).Text;
                txtDate.Text = (item.FindControl("lblDate") as Label).Text;
                txtQty.Text = (item.FindControl("lblQty") as Label).Text;
                txtResolution.Text = (item.FindControl("lblResolution") as Label).Text;
                lblId.Text = (item.FindControl("lblId") as Label).Text;
                inpProductID.Attributes["value"] = (item.FindControl("lblProductID") as Label).Text;
            }
            else if (e.CommandName == "ItemDel")
            {
                delid.Attributes["value"] = e.CommandArgument.ToString();
                WindowManager.RadConfirm("Are you sure to delete the item?", "confirmclosed", 300, 100, null, "Delete Warranty");
            }
        }

        protected void Delete(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delid.Attributes["value"]);
            POSService.POSService service = new POSService.POSService();
            service.DeleteWarranty(id);
            Bind();
        }


    }
}