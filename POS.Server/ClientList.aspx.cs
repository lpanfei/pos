﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS
{
    public partial class ClientList : BasePage
    {
        List<Client> Clients
        {
            get
            {
                if (Session["Clients"] == null)
                {
                    GetClients();
                }

                return Session["Clients"] as List<Client>;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetClients();
                Bind();
            }
        }

        void GetClients()
        {
            Session["Clients"] = new POSService.POSService().GetClients();
        }

        protected void Delete(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delid.Attributes["value"]);
            POSService.POSService service = new POSService.POSService();
            service.DeleteClient(id);
            GetClients();
            Bind();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                delid.Attributes["value"] = e.CommandArgument.ToString();
                WindowManager.RadConfirm("Are you sure to delete the item?", "confirmclosed", 300, 100, null, "Delete Client");
            }
            else if (e.CommandName == "ItemEdit")
            {
                RepeaterItem item = e.Item;
                formbuttons.Visible = true;
                listbuttons.Visible = false;
                formpanel.Visible = true;
                listpanel.Visible = false;
                ltlPageTitle.Text = "Client / Add";
               
                txtName.Text = (item.FindControl("lblName") as Label).Text;
                txtCode.Text = (item.FindControl("lblCode") as Label).Text;
                lblId.Text = (item.FindControl("lblSID") as Label).Text;
                txtSuburb.Text = (item.FindControl("lblSuburb") as Label).Text;
                txtState.Text = (item.FindControl("lblState") as Label).Text;
                txtAddress.Text = (item.FindControl("lblAddress") as Label).Text;
                txtPhone.Text = (item.FindControl("lblPhone") as Label).Text;
                txtEmail.Text = (item.FindControl("lblEmail") as Label).Text;
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            rptList.DataSource = null;
            rptList.DataSource = Clients.TakeFrom((currentPage - 1) * pageSize, pageSize);
            rptList.DataBind();
        }

        protected void AddClient(object sender, EventArgs e)
        {
            formbuttons.Visible = true;
            listbuttons.Visible = false;
            formpanel.Visible = true;
            listpanel.Visible = false;
            ltlPageTitle.Text = "Client / Add";
            txtCode.Text = "";
            txtName.Text = "";
            delid.Attributes["value"] = "0";
            lblId.Text = "0";
        }

        protected void BackToList(object sender, EventArgs e)
        {
            listbuttons.Visible = true;
            formbuttons.Visible = false;
            formpanel.Visible = false;
            listpanel.Visible = true;
            ltlPageTitle.Text = "Client List";
            Bind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                WindowManager.RadAlert("Client name is mandatory", 300, 100, "Validation", "");
                return;
            }

            new POSService.POSService().AddClient(new Data.Client()
            {
                ID = Convert.ToInt32(lblId.Text),
                Name = txtName.Text,
                Code=txtCode.Text,
                Address=txtAddress.Text,
                Email=txtEmail.Text,
                Phone=txtPhone.Text,
                State=txtState.Text,
                Suburb=txtSuburb.Text
            });

            BackToList(null, null);
        }

        void Bind()
        {
            GetClients();
            this.AspNetPager1.RecordCount = Clients.Count;
            rptList.DataSource = null;
            rptList.DataSource = Clients.TakeFrom(0,this.AspNetPager1.PageSize);
            rptList.DataBind();
            this.AspNetPager1.CurrentPageIndex = 1;
            this.AspNetPager1.GoToPage(this.AspNetPager1.CurrentPageIndex);
        }
    }
}