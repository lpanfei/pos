﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="CategoryList.aspx.cs" Inherits="POS.CategoryList" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    var previous = null;
    function pageLoad() {
        $(".list tbody tr").click(function () {
            //            var parentcat = $(this).find(".parentcat");
            //            if (parentcat.length > 0) {
            //                $(parentcat).toggleClass("parentexpanded");
            //            }
            var current = $(this).find(".buttons div").eq(0);
            $(".list tr").each(function () {
                if ($(this).find(".parentid").eq(0).val() == current.find(".classid").eq(0).val()) {
                    $(this).toggle();
                    $(this).css("background-color", "#EFF3F4");
                    $(this).css("color", "#3596E7");
                }

            });
            if ($(previous).is(current)) {
                return;
            }
            current.toggle("slow");
            if (previous != null) {
                $(previous).toggle("slow", function () { });
            }

            previous = current;

        }
        );

        var input = $.find(".classid[value='" + $("#delid").val() + "']");
        if (input.length > 0) {
            var tr = $(input).eq(0).closest("tr");
            var parentid = tr.find(".parentid").eq(0).val();
            if (parentid != "0") {
                $(".list tr").each(function () {
                    if ($(this).find(".classid").eq(0).val() == $("#delid").val()) {
                        $(this).toggle();
                        $(this).css("background-color", "#EFF3F4");
                        $(this).css("color", "#3596E7");
                    } else if ($(this).find(".parentid").eq(0).val() == parentid) {
                        $(this).toggle();
                    }

                });
            } else {
                $(".list tr").each(function () {
                    if ($(this).find(".parentid").eq(0).val() == $("#delid").val()) {
                        $(this).toggle();
                    }
                 });
            }
        }
        
    }
    

    function confirmclosed(sender) {
        if (sender) {
            $("#delBtn").trigger("click");
        } else {
            $("#delid").val("-1");
        }
    }
    </script>
    <style>
    .toprow
    {
        
    }
    .subrow
    {
        display:none;
    }
    .parentcat:before
    {
        content:url(images/expand.png);
    }
    
    .parentexpanded:before
    {
        content:url(images/collapse.png);
    }
    .leafcat:before
    {
    }
    .displaynone
    {
        display:none;
    }
     input[type=submit]
        {
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
    <div class="mainheader">
        <div class="contextual">
            <asp:Panel ID="listbuttons" runat="server">
                        <div style="height: 10px; width: 75px">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="addLink"
                                Text="Add" OnClick="AddCategory" />
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="formbuttons" runat="server" Visible="false">
                        <div style="height: 10px; width: 75px">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                                Text="Back" OnClick="BackToList" />
                        </div>
                    </asp:Panel> 
        </div>
        <div class="subheader">
            <h2 class="icon-categories pagetitle">
                <asp:Literal ID="ltlPageTitle" Text="Category List" runat="server"></asp:Literal></h2>
        </div>
    </div>


   <asp:Panel ID="listpanel" runat="server">

    <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand">
        <HeaderTemplate>
            <table  class="list">
            <thead>
                <tr>
                    <th width="40px"></th>
                    <%--<th width="150px" align="center">No.</th>--%>
                    <th align="left">Category Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
        </HeaderTemplate>
        <ItemTemplate>
                <tr class='<%# Convert.ToInt32(Eval("parentID"))==0? "toprow":"subrow"%> '>
                    <td align="right"><%--<i class='<%# Eval("CssClass")%>'></i>--%></td>
                    <%--<td align="center"><%# Container.ItemIndex+1 %></td>--%>
                    <td align="left"><asp:Label runat="server" ID="lblName" Text='<%# Eval("cat_Name")%>'></asp:Label></td>
                    <td class="buttons">
                        <div style="display:none;">
                            <input type="hidden" class="parentid" value='<%# Eval("ParentID")%>' />
                            <input type="hidden" class="classid" value='<%# Eval("cat_Id")%>' />
                            <input type="hidden"  class="catname" value='<%# Eval("ori_Name")%>' />
                          <%--  <a href='#' class="icon icon-edit">Edit</a>--%>
                            <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CommandName="ItemEdit" CommandArgument='<%# Eval("cat_Id") %>' class="icon icon-edit"></asp:LinkButton>
                            &nbsp;&nbsp;
                            <%--<a href='#' class="icon icon-del">Delete</a>--%>
                            <asp:LinkButton ID="lblDel" runat="server" Text="Delete" CommandName="ItemDel" CommandArgument='<%# Eval("cat_Id") %>' class="icon icon-del"></asp:LinkButton>
                        </div>
                    </td>
                    <td style="display:none"><asp:Label runat="server" ID="lblParentID" Text='<%# Eval("ParentID")%>'></asp:Label></td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
</asp:Panel>
<asp:Panel ID="formpanel" runat="server" Visible="false">
        <div class="md-modal md-effect-1 md-show" id="modal-form" >
			<div class="md-content">
				<h3>Category Form</h3>
				<div>
        <table class="form-table">
            <tr>
                <td class="form-label">
                    Name:
                </td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" ClientIDMode="Static"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="form-label">
                    Parent Category:
                </td>
                <td>
                    <asp:DropDownList ID="dropParent" runat="server" DataTextField="cat_Name" DataValueField="cat_Id" ClientIDMode="Static">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lblUploadFlag" runat="server" Visible="false"></asp:Label>
                    <input type="hidden" id="catid" runat="server" style="display:none" class="catid" />     
                </td>
            </tr>
        </table>
        </div>
         <div class="md-footer">
        
         <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                    UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center" Height="30" Width="75" />
         </div>
        </div></div>
</asp:Panel>
<eid:RadWindowManager runat="server" ID="WindowManager" Skin="Metro"></eid:RadWindowManager>

    <input type="hidden" id="delid" runat="server" clientidmode="Static" value="-1" />
    <asp:Button ID="delBtn" runat="server" Width="0" Height="0" CssClass="displaynone" OnClick="DeleteCategory" ClientIDMode="Static"></asp:Button>

    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
