﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using POS.Data;
using System.ServiceModel.Activation;

namespace POSService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract(Namespace = "")]
    public interface IPOSService
    {
        [OperationContract]
        LoginResult Login(string uid, string pwd);

        [OperationContract]
        List<Product> GetProducts(int clientID);

        [OperationContract]
        List<Category> GetCategories(int clientID);

        [OperationContract]
        List<Store> GetStores(int clientID);

        [OperationContract]
        Category AddCategory(Category cat);

        [OperationContract]
        Category UpdateCategory(Category cat);

        [OperationContract]
        OperationResult DeleteCategory(Category cat);

        [OperationContract]
        Product AddProduct(Product cat);

        [OperationContract]
        Product UpdateProduct(Product cat);

        [OperationContract]
        OperationResult DeleteProduct(Product cat);

        [OperationContract]
        List<Setting> GetSetting(int clientID, int storeID);

        [OperationContract]
        List<Invoice> GetInvoices(int storeID, int clientID, int invNo, string customer, DateTime? datefrom, DateTime? dateto);

        [OperationContract]
        List<Customer> GetCustomers(int clientID);

        [OperationContract]
        Customer GetCustomerByID(int id);

        [OperationContract]
        Customer AddCustomer(Customer customer);

        [OperationContract]
        Customer UpdateCustomer(Customer customer);

        [OperationContract]
        OperationResult DeleteCustomer(Customer customer);

        [OperationContract]
        OperationResult DeleteCustomerById(int id);

        [OperationContract]
        OperationResult DeleteCategoryById(int id);

        [OperationContract]
        OperationResult SetSetting(Setting setting);

        [OperationContract]
        List<User> GetUsers(int clientId, int storeId);

        [OperationContract]
        OperationResult AddUser(User user);

        [OperationContract]
        OperationResult UpdateUser(User user);

        [OperationContract]
        User GetUser(int userId);

        [OperationContract]
        OperationResult DeleteUserById(int userId);

        [OperationContract]
        OperationResult DeleteUser(User user);

        [OperationContract]
        List<Supplier> GetSuppliers(int clientID);

        [OperationContract]
        Supplier AddSupplier(Supplier supplier);

        [OperationContract]
        Supplier UpdateSupplier(Supplier supplier);

        [OperationContract]
        OperationResult DeleteSupplierById(int id);

        [OperationContract]
        OperationResult DeleteSupplier(Supplier supplier);

        [OperationContract]
        Store AddStore(Store store);

        [OperationContract]
        Store UpdateStore(Store store);

        [OperationContract]
        List<Warranty> GetWarranties(int clientID, int storeID);

        [OperationContract]
        Warranty AddWarranty(Warranty warranty);

        [OperationContract]
        Warranty UpdateWarranty(Warranty warranty);

        [OperationContract]
        List<Promotion> GetPromotions(int clientID, int storeID);

        [OperationContract]
        Promotion AddPromotion(Promotion promotion);

        [OperationContract]
        Promotion UpdatePromotion(Promotion promotion);

        [OperationContract]
        OperationResult DoInvoice(Invoice invoice);

        [OperationContract]
        OperationResult CancelInvoice(Invoice invoice);

        [OperationContract]
        OperationResult DeleteStore(int id);

        [OperationContract]
        List<Log> GetLogs(int clientID, int storeID);

        [OperationContract]
        Client AddClient(Client client);

        [OperationContract]
        Client UpdateClient(Client client);

        [OperationContract]
        OperationResult DeleteClient(int id);

        [OperationContract]
        List<Client> GetClients();

        [OperationContract]
        OperationResult DeleteWarranty(int id);

        [OperationContract]
        List<InvoiceProduct> GetInvoiceProducts(int invoiceID);

        [OperationContract]
        OperationResult Register(Registeration reg);

        [OperationContract]
        [WebInvoke]
        List<Invoice> GetWalletInvoice(string walletId);

        [OperationContract]
        Invoice GetLatestWalletInvoice(string walletId);

        [OperationContract]
        OperationResult ChangePassword(int id, string password);

        [OperationContract]
        List<Inventory> GetInventories(int clientid, int storeid);

        [OperationContract]
        OperationResult StockToInventory(Inventory inventory);
    }


    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class LoginResult
    {
        [DataMember]
        public bool Result { get; set; }

        [DataMember]
        public User User { get; set; }
    }

    [DataContract]
    public class OperationResult
    {
        [DataMember]
        public bool Result { get; set; }

        [DataMember]
        public string Message { get; set; }
    }
}
