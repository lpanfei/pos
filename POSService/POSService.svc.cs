﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using POS.Data;
using System.ServiceModel.Activation;

namespace POSService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
     [ServiceContract(Namespace = "")]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(IncludeExceptionDetailInFaults = true)]
    public class POSService
    {
        public LoginResult Login(string uid, string pwd)
        {
            LoginResult lr = new LoginResult() { Result=false,User=null};
            List<User> users = POS.Data.User.Select(new User.Parms() { usr_Name=uid,usr_Password=pwd,RetrieveType=User.RetrieveType.Login});
            if (users.Any())
            {
                lr.Result = true;
                lr.User = users[0];
            }
            return lr;
        }


        public List<Product> GetProducts(int clientID)
        {
            return Product.Select(new Product.Parms() { ClientID=clientID});
        }


        public List<Category> GetCategories(int clientID)
        {
            return Category.Select(new Category.Parms() { ClientID=clientID});
        }


        public List<Store> GetStores(int clientID)
        {
            return Store.Select(new Store.Parms() { ClientID=clientID});
        }


        public Category AddCategory(Category cat)
        {
            return cat.Update();
        }

        public Category UpdateCategory(Category cat)
        {
            return cat.Update();
        }

        public OperationResult DeleteCategory(Category cat)
        {
            try
            {
                cat.Delete();
                return new OperationResult() { Result=true};
            }
            catch(Exception ex)
            {
                return new OperationResult() { Result=false,Message=ex.Message};
            }
        }

        public Product AddProduct(Product p)
        {
            return p.Update();
        }

        public Product UpdateProduct(Product p)
        {
            return p.Update();
        }

        public OperationResult DeleteProduct(Product p)
        {
            try
            {
                p.Delete();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }

        public List<Setting> GetSetting(int clientID, int storeID)
        {
            return Setting.Select(new Setting.Parms() { ClientID=clientID,StoreID=storeID});
        }


        public List<Invoice> GetInvoices(int storeID, int clientID,int invNo, string customer, DateTime? datefrom, DateTime? dateto)
        {
            return Invoice.Select(new Invoice.Parms() { StoreID=storeID,ClientID=clientID,InvoiceNo=invNo,Customer=customer,DateFrom=datefrom,DateTo=dateto});
        }


        public List<Customer> GetCustomers(int clientID)
        {
            return Customer.Select(new Customer.Parms() { ClientID=clientID});
        }

        public Customer AddCustomer(Customer customer)
        {
            return customer.Update();
        }

        public Customer UpdateCustomer(Customer customer)
        {
            return customer.Update();
        }


        public OperationResult DeleteCustomer(Customer customer)
        {
            try
            {
                customer.Delete();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }


        public OperationResult DeleteCustomerById(int id)
        {
            try
            {
                new Customer() { ID=id}.Delete();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }


        public OperationResult DeleteCategoryById(int id)
        {
            try
            {
                new Category() { cat_Id = id }.Delete();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }


        public OperationResult SetSetting(Setting setting)
        {
            try
            {
                setting.Update();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }


        public List<User> GetUsers(int clientId, int storeId)
        {
            return User.Select(new User.Parms() { clientId=clientId,storeId=storeId,RetrieveType=User.RetrieveType.AllUsers});
        }


        public OperationResult AddUser(User user)
        {
            try
            {
                user.Update();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }

        public OperationResult UpdateUser(User user)
        {
            return AddUser(user);
        }


        public User GetUser(int userId)
        {
            List<User> users = User.Select(new User.Parms() { usr_ID=userId,RetrieveType=User.RetrieveType.User});
            if (users.Any())
            {
                return users[0];
            }
            return null;
        }


        public OperationResult DeleteUserById(int userId)
        {
            return DeleteUser(new User() { usr_ID=userId});
        }

        public OperationResult DeleteUser(User user)
        {
            try
            {
                user.Delete();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }


        public List<Supplier> GetSuppliers(int clientID)
        {
            return Supplier.Select(new Supplier.Parms() { ClientID=clientID});
        }

        public Supplier AddSupplier(Supplier supplier)
        {
            return supplier.Update();
        }

        public Supplier UpdateSupplier(Supplier supplier)
        {
            return supplier.Update();
        }

        public OperationResult DeleteSupplierById(int id)
        {
            return DeleteSupplier(new Supplier() { ID=id});
        }

        public OperationResult DeleteSupplier(Supplier supplier)
        {
            try
            {
                supplier.Delete();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }


        public Store AddStore(Store store)
        {
            return store.Update();
        }

        public Store UpdateStore(Store store)
        {
            return store.Update();
        }


        public List<Warranty> GetWarranties(int clientID, int storeID)
        {
            return Warranty.Select(new Warranty.Parms() { ClientID = clientID, StoreID = storeID });
        }

        public Warranty AddWarranty(Warranty warranty)
        {
            return warranty.Update();
        }

        public Warranty UpdateWarranty(Warranty warranty)
        {
            return warranty.Update();
        }

        public List<Promotion> GetPromotions(int clientID, int storeID)
        {
            return Promotion.Select(new Promotion.Parms() { ClientID = clientID, StoreID = storeID });
        }

        public Promotion AddPromotion(Promotion promotion)
        {
            return promotion.Update();
        }

        public Promotion UpdatePromotion(Promotion promotion)
        {
            return promotion.Update();
        }


        public Customer GetCustomerByID(int id)
        {
            List<Customer> customers = Customer.Select(new Customer.Parms() { ById=true,ID=id});
            if (customers.Any())
                return customers[0];
            return new Customer() { ID=0};
        }


        public OperationResult DoInvoice(Invoice invoice)
        {
            try
            {
                invoice.Update();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }


        public OperationResult CancelInvoice(Invoice invoice)
        {
            try
            {
                invoice.Cancel();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }


        public OperationResult DeleteStore(int id)
        {
            try
            {
                new Store() {sto_Id=id }.Delete();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }


        public List<Log> GetLogs(int clientID, int storeID)
        {
            return Log.Select(new Log.Parms() { ClientID=clientID,StoreID=storeID});
        }


        public Client AddClient(Client client)
        {
           return client.Update();
        }

        public Client UpdateClient(Client client)
        {
            return AddClient(client);
        }

        public OperationResult DeleteClient(int id)
        {
            try
            {
                new Client() { ID = id }.Delete();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }

        public List<Client> GetClients()
        {
            return Client.Select(new Client.Parms());
        }


        public OperationResult DeleteWarranty(int id)
        {
            try
            {
                new Warranty() { ID = id }.Delete();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }



        public List<InvoiceProduct> GetInvoiceProducts(int invoiceID)
        {
            return InvoiceProduct.Select(new InvoiceProduct.Parms() { InvoiceID=invoiceID});
        }


        public OperationResult Register(Registeration reg)
        {
            try
            {
                reg.Update();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }

        [OperationContract]
        [WebInvoke]
        public List<Invoice> GetWalletInvoice(string walletId)
        {
            return new Invoice().GetWalletInvoices(walletId);
        }

        public Invoice GetLatestWalletInvoice(string walletId)
        {
            return new Invoice().GetWalletInvoices(walletId).FirstOrDefault();
        }

        public Invoice GetSMSInvoice(string mobileno,string validationcode)
        {
            return new Invoice().GetSMSInvoices(mobileno, validationcode).FirstOrDefault();
        }


        public OperationResult ChangePassword(int id, string password)
        {
            try
            {
                new User().ChangePassword(id, password);
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }


        public List<Inventory> GetInventories(int clientid, int storeid)
        {
            return Inventory.Select(new Inventory.Parms() { ClientID=clientid,StoreID=storeid});
        }

        public OperationResult StockToInventory(Inventory inventory)
        {
            try
            {
                inventory.Update();
                return new OperationResult() { Result = true };
            }
            catch (Exception ex)
            {
                return new OperationResult() { Result = false, Message = ex.Message };
            }
        }

    }
}
