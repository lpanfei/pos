﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Telerik.Web.UI;
using System.Web.Services;

namespace POS
{
    public partial class POSPage : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (GlobalCache.CurrentUser != null)
                {
                    ltlUser.Text = GlobalCache.CurrentUser.usr_Name;

                    if (string.IsNullOrEmpty(txtOld.Text))
                    {
                        txtOld.Attributes.Add("value", txtOld.Attributes["value"]);
                    }
                    else
                    {
                        txtOld.Attributes.Add("value", txtOld.Text);
                    }
                    if (string.IsNullOrEmpty(txtNew.Text))
                    {
                        txtNew.Attributes.Add("value", txtNew.Attributes["value"]);
                    }
                    else
                    {
                        txtNew.Attributes.Add("value", txtNew.Text);
                    }
                    if (string.IsNullOrEmpty(txtConfirm.Text))
                    {
                        txtConfirm.Attributes.Add("value", txtConfirm.Attributes["value"]);
                    }
                    else
                    {
                        txtConfirm.Attributes.Add("value", txtConfirm.Text);
                    }
                }
                
            }
        }

        protected void InitControls(object sender, EventArgs args)
        {
            ControlBase current = GetControl(controlid.Value);
            ControlBase previous = GetControl(previouscontrolid.Value);
            current.Init();
            previous.UnInit();
        }

        ControlBase GetControl(string control)
        {
            ControlBase cb=null;
            switch (control.ToLower())
            {
                case "settings":
                    cb = (ControlBase)this.SettingControl;
                    break;
                case "categories":
                    cb = (ControlBase)this.CategoryControl;
                    break;
                case "invoice":
                    cb = (ControlBase)this.InvoiceControl;
                    break;
                case "credit":
                    cb = (ControlBase)this.InventoryControl;
                    break;
                case "products":
                    cb = (ControlBase)this.ProductControl;
                    break;
                case "users":
                    cb = (ControlBase)this.UsersControl;
                    break;
                case "suppliers":
                    cb = (ControlBase)this.SupplierControl;
                    break;
                case "warranty":
                    cb = (ControlBase)this.WarrantyControl;
                    break;
                case "groups":
                    cb = (ControlBase)this.CustomerControl;
                    break;
                case "logs":
                    cb = (ControlBase)this.LogControl;
                    break;
                case "task":
                    cb = (ControlBase)this.TaskControl;
                    break;
                case "promotions":
                    cb=(ControlBase)this.PromotionControl;
                    break;
            }
            return cb;
        }

        protected void Click(object sender, EventArgs args)
        {
            if (string.IsNullOrEmpty(txtOld.Text.Trim()) || string.IsNullOrEmpty(txtNew.Text.Trim()) || string.IsNullOrEmpty(txtConfirm.Text.Trim()))
            {
                windowManager.RadAlert("Please fill the form completely!", 300, 100, "Validation", "");
                return;
            }

            if (txtOld.Text.Trim() != GlobalCache.CurrentUser.usr_Password)
            {
                windowManager.RadAlert("Old passwords does not match!", 300, 100, "Validation", "");
                return;
            }

            if (txtNew.Text.Trim() != txtConfirm.Text.Trim())
            {
                windowManager.RadAlert("New passwords do not match!", 300, 100, "Validation", "");
                return;
            }

            POSService.OperationResult or = new POSService.POSService().ChangePassword(GlobalCache.CurrentUser.usr_ID, txtNew.Text.Trim());
            if (or.Result)
            {
                GlobalCache.CurrentUser = null;
                windowManager.RadAlert("Password is modified successfully. Please re-login!", 300, 100, "Validation", "relogin");
            }
        }

        public override RadWindowManager WindowManager
        {
            get
            {
                return this.windowManager;
            }
        }

       
    }
}