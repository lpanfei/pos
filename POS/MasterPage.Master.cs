﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Telerik.Web.UI;

namespace POS
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (GlobalCache.CurrentUser!=null)
                {
                    string role = GlobalCache.CurrentUser.UserType.ToString();
                    string username = GlobalCache.CurrentUser.usr_Name;

                    StringBuilder nav = new StringBuilder();
                    ltlUser.Text = role;
                    string file = this.Page.AppRelativeVirtualPath;
                    switch (role)
                    {
                        case "SystemAdmin":
                            if (file.Contains("ClientList.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='supplierssel'></td><td></i><a href=\"ClientList.aspx\">Clients</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='suppliers'></td><td></i><a href=\"ClientList.aspx\">Clients</a></td></tr></table></li>");
                            }

                            if (file.Contains("UserList.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='usersel'></td><td></i><a href=\"UserList.aspx\">Users</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='users'></td><td></i><a href=\"UserList.aspx\">Users</a></td></tr></table></li>");
                            }

                            break;
                        case "ClientAdmin":
                            if (file.Contains("StoreList.aspx"))
                            {
                                nav.Append("<li class='current'><table><tr><td><i class='productssel'></td><td></i><a href=\"StoreList.aspx\">Stores</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='products'></td><td></i><a href=\"StoreList.aspx\">Stores</a></td></tr></table></li>");
                            }

                            if (file.Contains("UserList.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='usersel'></td><td></i><a href=\"UserList.aspx\">Users</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='users'></td><td></i><a href=\"UserList.aspx\">Users</a></td></tr></table></li>");
                            }
                            break;
                        case "StoreAdmin":
                            if (file.Contains("Settings.aspx"))
                            {
                                nav.Append("<li class='current'><table><tr><td><i class='settingssel'></td><td></i><a href=\"Settings.aspx\">Settings</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='settings'></td><td></i><a href=\"Settings.aspx\">Settings</a></td></tr></table></li>");
                            }

                            if (file.Contains("OrderingMode.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='invoicesel'></td><td></i><a href=\"OrderingMode.aspx\">Order Mode</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='invoice'></td><td></i><a href=\"OrderingMode.aspx\">Order Mode</a></td></tr></table></li>");
                            }

                            if (file.Contains("TaskMode.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='invoicesel'></td><td></i><a href=\"TaskMode.aspx\">Task Mode</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='invoice'></td><td></i><a href=\"TaskMode.aspx\">Task Mode</a></td></tr></table></li>");
                            }

                            if ((file.Contains("InvoiceList.aspx") && !file.Contains("TempInvoiceList.aspx")) || file.Contains("InvoiceEdit2.aspx") || ((file.Contains("InvoiceCancel.aspx") || file.Contains("InvoiceCancelList")) && !file.Contains("Temp")))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='invoicesel'></td><td></i><a href=\"InvoiceList.aspx\">Invoices</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='invoice'></td><td></i><a href=\"InvoiceList.aspx\">Invoices</a></td></tr></table></li>");
                            }

                            if (file.Contains("InventoryList.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='creditsel'></td><td></i><a href=\"InventoryList.aspx\">Inventory</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='credit'></td><td></i><a href=\"InventoryList.aspx\">Inventory</a></td></tr></table></li>");
                            }
                            if (file.Contains("ProductList.aspx") || file.Contains("RestockEdit.aspx") || file.Contains("ProductEdit.aspx"))
                            {
                                nav.Append("<li class='current'><table><tr><td><i class='productssel'></td><td></i><a href=\"ProductList.aspx\">Products</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='products'></td><td></i><a href=\"ProductList.aspx\">Products</a></td></tr></table></li>");
                            }

                            if (file.Contains("CategoryList.aspx") || file.Contains("CategoryEdit.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='categoriessel'></td><td></i><a href=\"CategoryList.aspx\">Categories</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='categories'></td><td></i><a href=\"CategoryList.aspx\">Categories</a></td></tr></table></li>");
                            }

                            if (file.Contains("UserList.aspx") || file.Contains("UserEdit.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='usersel'></td><td></i><a href=\"UserList.aspx\">Users</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='users'></td><td></i><a href=\"UserList.aspx\">Users</a></td></tr></table></li>");
                            }
                            //if (file.Contains("TempInvoiceList.aspx") || file.Contains("TempInvoiceEdit.aspx") || file.Contains("TempInvoiceCancelList.aspx") || file.Contains("TempInvoiceCancel.aspx"))
                            //{
                            //    nav.Append("<li  class='current'><table><tr><td><i class='creditsel'></td><td></i><a href=\"TempInvoiceList.aspx\">Temporary<br/>Loan/Credit</a></td></tr></table></li>");
                            //}
                            //else
                            //{
                            //    nav.Append("<li><table><tr><td><i class='credit'></td><td></i><a href=\"TempInvoiceList.aspx\">Temporary<br/>Loan/Credit</a></td></tr></table></li>");
                            //}

                            if (file.Contains("SupplierList.aspx") || file.Contains("SupplierEdit.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='supplierssel'></td><td></i><a href=\"SupplierList.aspx\">Suppliers</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='suppliers'></td><td></i><a href=\"SupplierList.aspx\">Suppliers</a></td></tr></table></li>");
                            }

                            if (file.Contains("WarrantyList.aspx") || file.Contains("WarrantyEdit.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='warrantysel'></td><td></i><a href=\"WarrantyList.aspx\">Warranties</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='warranty'></td><td></i><a href=\"WarrantyList.aspx\">Warranties</a></td></tr></table></li>");
                            }

                            if (file.Contains("CustomerList.aspx") || file.Contains("CustomerEdit.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='groupsel'></td><td></i><a href=\"CustomerList.aspx\">Customers</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='groups'></td><td></i><a href=\"CustomerList.aspx\">Customers</a></td></tr></table></li>");
                            }
                            if (file.Contains("LogList.aspx"))
                            {
                                nav.Append("<li  class='current'><table><tr><td><i class='logssel'></td><td></i><a href=\"LogList.aspx\">Logs</a></td></tr></table></li>");
                            }
                            else
                            {
                                nav.Append("<li><table><tr><td><i class='logs'></td><td></i><a href=\"LogList.aspx\">Logs</a></td></tr></table></li>");
                            }
                            //if (file.Contains("RestockList.aspx") || file.Contains("Restock.aspx"))
                            //{
                            //    nav.Append("<li  class='current'><table><tr><td><i class='stocksel'></td><td></i><a href=\"RestockList.aspx\">Restocks</a></td></tr></table></li>");
                            //}
                            //else
                            //{
                            //    nav.Append("<li><table><tr><td><i class='stock'></td><td></i><a href=\"RestockList.aspx\">Restocks</a></td></tr></table></li>");
                            //}
                            break;
                    }
                    
                    ltlNav.Text = nav.ToString();
                }
            }
            if (string.IsNullOrEmpty(txtOld.Text))
            {
                txtOld.Attributes.Add("value", txtOld.Attributes["value"]);
            }
            else
            {
                txtOld.Attributes.Add("value", txtOld.Text);
            }
            if (string.IsNullOrEmpty(txtNew.Text))
            {
                txtNew.Attributes.Add("value", txtNew.Attributes["value"]);
            }
            else
            {
                txtNew.Attributes.Add("value", txtNew.Text);
            }
            if (string.IsNullOrEmpty(txtConfirm.Text))
            {
                txtConfirm.Attributes.Add("value", txtConfirm.Attributes["value"]);
            }
            else
            {
                txtConfirm.Attributes.Add("value", txtConfirm.Text);
            }
        }

        protected void Click(object sender, EventArgs args)
        {
            if (string.IsNullOrEmpty(txtOld.Text.Trim()) || string.IsNullOrEmpty(txtNew.Text.Trim()) || string.IsNullOrEmpty(txtConfirm.Text.Trim()))
            {
                windowManager.RadAlert("Please fill the form completely!", 300, 100, "Validation", "");
                return;
            }

            if (txtOld.Text.Trim() != GlobalCache.CurrentUser.usr_Password)
            {
                windowManager.RadAlert("Old passwords does not match!", 300, 100, "Validation", "");
                return;
            }

            if (txtNew.Text.Trim() != txtConfirm.Text.Trim())
            {
                windowManager.RadAlert("New passwords do not match!", 300, 100, "Validation", "");
                return;
            }

            POSService.OperationResult or = new POSService.POSService().ChangePassword(GlobalCache.CurrentUser.usr_ID,txtNew.Text.Trim());
            if (or.Result)
            {
                GlobalCache.CurrentUser = null;
                windowManager.RadAlert("Password is modified successfully. Please re-login!", 300, 100, "Validation", "relogin");
            }
        }

        public RadWindowManager WM
        {
            get
            {
                return this.windowManager;
            }
        } 
    }
}