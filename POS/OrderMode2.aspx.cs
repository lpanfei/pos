﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS
{
    public partial class OrderMode2 : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Repeater3.DataSource = GetPromotedProducts(GlobalCache.Products);
            Repeater3.DataBind();
        }

        public List<Product> GetPromotedProducts(List<Product> products)
        {
            List<Product> list = new List<Product>();
            if (GlobalCache.Promotions != null && products != null)
            {
                foreach (Promotion promotion in GlobalCache.Promotions)
                {
                    if (DateTime.Now >= promotion.StartTime && DateTime.Now <= promotion.EndTime)
                    {
                        Product product = products.FirstOrDefault(p => p.pro_Id == promotion.ProductID);
                        if (product != null)
                        {
                            product.Discount = promotion.Discount;
                        }
                        list.Add(product);
                    }
                }
            }
            return list;
        }
    }
}