﻿var app = Sys.Application;
var c_win = null;
var postbackeleid = "";
app.add_init(ApplicationInit);

function ApplicationInit(sender) {
    var prm = Sys.WebForms.PageRequestManager.getInstance();
    if (!prm.get_isInAsyncPostBack()) {
        prm.add_beginRequest(BeginRequest);
        prm.add_endRequest(EndRequest);
    }
}

function BeginRequest(sender, args) {
    postbackElement = args.get_postBackElement();
    postbackeleid = postbackElement.id;
    if (postbackElement.id != "btnInitControls" && postbackeleid != $("#timerclientid").val()) {
        c_win = radalert('System is processing, please wait...', 300, 100, 'PROCESSING', '', 'Images/busy.gif');
        $(".rwPopupButton").css("visibility", "hidden");
        $(".rwCloseButton").css("visibility", "hidden");
        $(".rwDialogText").css("margin-top", "10px");
    }
}

function EndRequest(sender, args) {
    if (postbackeleid != "btnInitControls" && postbackeleid != $("#timerclientid").val()) {
        closeCurrentWindow();
        $(".rwPopupButton").css("visibility", "visible");
        $(".rwCloseButton").css("visibility", "visible");
        $(".rwDialogText").css("margin-top", "0px");
        if (args.get_error() != undefined) {
            handleError(args.get_error().description);
            args.set_errorHandled(true);
        }
    }
}

function closeCurrentWindow() {
    if (c_win != null) {
        c_win.close();
    }
}

function handleError(error) {
    radalert('Error occured during processing. Error details: ' + error, 400, 200, 'ERROR', '', 'Images/error.png');
}