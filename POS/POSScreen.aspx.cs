﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace POS
{
    public partial class POSScreen : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ControlBase current = GetControl(controlid.Value);
                current.Init();
            }
        }

        protected void InitControls(object sender, EventArgs args)
        {
            ControlBase current = GetControl(controlid.Value);
            ControlBase previous = GetControl(previouscontrolid.Value);
            current.Init();
            previous.UnInit();
        }

        ControlBase GetControl(string control)
        {
            ControlBase cb = null;
            switch (control.ToLower())
            {
                case "settings":
                    cb = (ControlBase)this.SettingControl;
                    break;
                case "categories":
                    cb = (ControlBase)this.CategoryControl;
                    break;
                case "invoice":
                    cb = (ControlBase)this.InvoiceControl;
                    break;
                case "credit":
                    cb = (ControlBase)this.InventoryControl;
                    break;
                case "products":
                    cb = (ControlBase)this.ProductControl;
                    break;
                case "users":
                    cb = (ControlBase)this.UsersControl;
                    break;
                case "suppliers":
                    cb = (ControlBase)this.SupplierControl;
                    break;
                case "warranty":
                    cb = (ControlBase)this.WarrantyControl;
                    break;
                case "groups":
                    cb = (ControlBase)this.CustomerControl;
                    break;
                case "logs":
                    cb = (ControlBase)this.LogControl;
                    break;
                case "task":
                    cb = (ControlBase)this.TaskControl;
                    break;
                case "promotions":
                    cb = (ControlBase)this.PromotionControl;
                    break;
            }
            return cb;
        }

        

        public override RadWindowManager WindowManager
        {
            get
            {
                return this.windowManager;
            }
        }
    }
}