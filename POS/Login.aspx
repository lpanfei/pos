﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="POS.Login" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Welcome to DST Solutions POS System</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="login/css/style.css" />
    <script src="Scripts/jquery-1.9.1.js"></script>
   
    <link rel="stylesheet" type="text/css" href="window/css/component.css" />
    <script src="login/js/modernizr.custom.js"></script>
    <script type="text/javascript" charset="utf-8">

        function pageLoad(sender, args) {
            var top = Math.max($(window).height() / 2 - $(".container2")[0].offsetHeight / 2 - $(window).height() * 0.15, 0);
            var left = Math.max($(window).width() / 2 - $(".container2")[0].offsetWidth / 2, 0);
            $(".container2").css('top', top + "px");
            $(".container2").css('left', left + "px");
            $(".container2").css('position', 'fixed');

            $(window).resize(function () {
                var top = Math.max($(window).height() / 2 - $(".container2")[0].offsetHeight / 2 - $(window).height() * 0.15, 0);
                var left = Math.max($(window).width() / 2 - $(".container2")[0].offsetWidth / 2, 0);
                $(".container2").css('top', top + "px");
                $(".container2").css('left', left + "px");
                $(".container2").css('position', 'fixed');
            });
        }
        $(function () {

            var top = Math.max($(window).height() / 2 - $(".container2")[0].offsetHeight / 2 - $(window).height() * 0.15, 0);
            var left = Math.max($(window).width() / 2 - $(".container2")[0].offsetWidth / 2, 0);
            $(".container2").css('top', top + "px");
            $(".container2").css('left', left + "px");
            $(".container2").css('position', 'fixed');

            $(window).resize(function () {
                var top = Math.max($(window).height() / 2 - $(".container2")[0].offsetHeight / 2 - $(window).height() * 0.15, 0);
                var left = Math.max($(window).width() / 2 - $(".container2")[0].offsetWidth / 2, 0);
                $(".container2").css('top', top + "px");
                $(".container2").css('left', left + "px");
                $(".container2").css('position', 'fixed');
            });

            $("#txtPassword").keypress(function (event) {
                if (event.which == 13) {
                    $("#btnLogin").trigger("click");
                }
            });
        });
	</script>
  
</head>
<body>

<form  runat="server" id="form1">
<eid:RadScriptManager runat="server" ID="RadScriptManager1" />
                                    <eid:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <eid:AjaxSetting AjaxControlID="Panel1">
                <UpdatedControls>
                    <eid:AjaxUpdatedControl ControlID="Panel1" LoadingPanelID="RadAjaxLoadingPanel1">
                    </eid:AjaxUpdatedControl>
                </UpdatedControls>
            </eid:AjaxSetting>
           
        </AjaxSettings>
    </eid:RadAjaxManager>
                                
                                    
<div class="container2">
        <header>
			
				 <asp:Image ID="Image1" runat="server" ImageUrl="~/Images/LOGO.png" />
           
				
			</header>

        <asp:Panel ID="Panel1" runat="server" Width="500px" style="margin:0 auto;padding:20px;"  >
				<div class="form-1">
               
					<p class="field">
						<asp:TextBox ID="txtUsername" runat="server"  name="txtUsername" ClientIDMode="Static" ></asp:TextBox>
						<i class="icon-user icon-large"></i>
					</p>
                    <hr style="border:1px dashed #f4f4f4"/>
                   
						<p class="field">
                           
							<asp:TextBox ID="txtPassword" runat="server" TextMode="Password" name="txtPassword" ClientIDMode="Static" ></asp:TextBox>
							<i class="icon-lock icon-large"></i>
					</p>
					  
                    
                 </div>
                 <p style="width:400px;margin:0 auto;padding:0 auto;text-align:center">
                    
                     <asp:Button ID="btnLogin" runat="server" Text="Sign In" 
                        onclick="btnLogin_Click" UseSubmitBehavior="false" ClientIDMode="Static" class="inputbutton"  style="margin-bottom:20px;height:45px;margin-top:20px;"/>
						 
                        Welcome to DST Solutions POS System
					</p>
                  </asp:Panel>
		
    </div>
    
    <eid:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro" Style="position: absolute; top: 0; left: 0; height: 100%; width: 100%;" IsSticky="true">
    </eid:RadAjaxLoadingPanel>
   <eid:RadWindowManager ID="windowManager" runat="server" Skin="Metro"></eid:RadWindowManager>
   
                 
				</form>
</body>
</html>
