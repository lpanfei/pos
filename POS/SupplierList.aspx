﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="SupplierList.aspx.cs" Inherits="POS.SupplierList" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script type="text/javascript">
    var previous = null;
    function pageLoad(sender, args) {
        $(document).ready(function () {
            $(".list tr").click(function () {

                var current = $(this).find(".buttons div").eq(0);
                if ($(previous).is(current))
                    return;
                current.toggle("slow");
                if (previous != null) {
                    $(previous).toggle("slow", function () { });
                }
                previous = current;
            }
        );
        });
    }

    function confirmclosed(sender) {
        if (sender) {
            $("#delBtn").trigger("click");
        }
    }
    </script>
    <style type="text/css">
        input[type=submit]
        {
            cursor: pointer;
        }
        .autoresize
        {
            height:100%;
        }
        
        .displaynone
        {
            display:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdatePanel runat="server" UpdateMode="Conditional">
<ContentTemplate>

<div class="mainheader">
        <div class="contextual">
             <asp:Panel ID="listbuttons" runat="server">
                        <div style="height: 10px; width: 75px">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="addLink"
                                Text="Add" OnClick="AddSupplier" />
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="formbuttons" runat="server" Visible="false">
                        <div style="height: 10px; width: 75px">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                                Text="Back" OnClick="BackToList" />
                        </div>
                    </asp:Panel> 
         </div>
        <div class="subheader">
            <h2 class="icon-suppliers pagetitle"><asp:Literal ID="ltlPageTitle" Text="Supplier List" runat="server"></asp:Literal></h2>
        </div>
    </div>

  <asp:Panel ID="listpanel" runat="server" CssClass="autoresize">
    <asp:Repeater ID="rptList" runat="server" onitemcommand="rptList_ItemCommand">
        <HeaderTemplate>
            <table class="list">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Supplier</th>
                    <th>Contact</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Mobile</th>
                    <th>Email</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
        </HeaderTemplate>
        <ItemTemplate>
                <tr>
                    <td align="center"><%# Container.ItemIndex+1 %></td>
                    <td align="center"><asp:Label runat="server" ID="lblName" Text='<%# Eval("Name")%>'></asp:Label></td>
                    <td align="center"><asp:Label runat="server" ID="lblContact" Text='<%# Eval("Contact")%>'></asp:Label></td>
                    <td align="center"><asp:Label runat="server" ID="lblAddress" Text='<%# Eval("Address")%>'></asp:Label></td>
                    <td align="center"><asp:Label runat="server" ID="lblPhone" Text='<%# Eval("Phone")%>'></asp:Label></td>
                    <td align="center"><asp:Label runat="server" ID="lblMobile" Text='<%# Eval("Mobile")%>'></asp:Label></td>
                    <td align="center"><asp:Label runat="server" ID="lblEmail" Text='<%# Eval("Email")%>'></asp:Label></td>
                    <td class="buttons">
                    <div style="display:none;">
                       <%-- <a href='<%# "SupplierEdit.aspx?id="+Eval("Id") %>' class="icon icon-edit">Edit</a>--%>
                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandName="ItemEdit" CommandArgument='<%# Eval("Id") %>' class="icon icon-edit"></asp:LinkButton>
                        &nbsp;&nbsp;
                        <asp:LinkButton ID="lblDel" runat="server" Text="Delete" CommandName="ItemDel" CommandArgument='<%# Eval("Id") %>' class="icon icon-del"></asp:LinkButton></div>
                    </td>
                    <td style="display:none"><asp:Label runat="server" ID="lblSuburb" Text='<%# Eval("Suburb")%>'></asp:Label></td>
                    <td style="display:none"><asp:Label runat="server" ID="lblState" Text='<%# Eval("State")%>'></asp:Label></td>
                    <td style="display:none"><asp:Label runat="server" ID="lblCountry" Text='<%# Eval("Country")%>'></asp:Label></td>
                    <td style="display:none"><asp:Label runat="server" ID="lblPostcode" Text='<%# Eval("Postcode")%>'></asp:Label></td>
                    <td style="display:none"><asp:Label runat="server" ID="lblFax" Text='<%# Eval("Fax")%>'></asp:Label></td>
                    <td style="display:none"><asp:Label runat="server" ID="lblNote" Text='<%# Eval("Note")%>'></asp:Label></td>
                     <td style="display:none"><asp:Label runat="server" ID="lblPayment" Text='<%# Eval("PaymentMethod")%>'></asp:Label></td>
                    
                      <td style="display:none"><asp:Label runat="server" ID="lblSID" Text='<%# Eval("ID")%>'></asp:Label></td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
        </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" 
        onpagechanged="AspNetPager1_PageChanged" ClientIDMode="Static" PageSize="10">
    </webdiyer:AspNetPager>
    </asp:Panel>
    <asp:Panel ID="formpanel" runat="server" Visible="false">
     <div class="md-modal md-effect-1 md-show" id="modal-form">
			<div class="md-content">
				<h3>Supplier Form</h3>
				<div>
    <table class="form-table">
       
        <tr>
            <td  class="form-label">Supplier:</td>
            <td><asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Contact:</td>
            <td><asp:TextBox ID="txtContact" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Suburb:</td>
            <td><asp:TextBox ID="txtSuburb" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Address:</td>
            <td><asp:TextBox ID="txtAddress" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">State:</td>
            <td><asp:TextBox ID="txtState" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Country:</td>
            <td><asp:TextBox ID="txtCountry" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Postcode:</td>
            <td><asp:TextBox ID="txtPostcode" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Phone:</td>
            <td><asp:TextBox ID="txtPhone" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">MobilePhone:</td>
            <td><asp:TextBox ID="txtMobilePhone" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Fax:</td>
            <td><asp:TextBox ID="txtFax" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Email:</td>
            <td><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Note:</td>
            <td><asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox></td>
        </tr>
        <tr>
            <td class="form-label">Payment Method:</td>
            <td>
                <asp:DropDownList ID="dropPaymentMethod" runat="server">
                    <asp:ListItem Text="Electronic Debit" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Cheque" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Telegraphic Transfer" Value="3"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr style="display:none;">
            <td colspan="2" align="center">
               
                <asp:Label ID="lblUploadFlag"  runat="server" Visible="false"></asp:Label>

                <asp:Label ID="lblId"  runat="server" Visible="false"></asp:Label>
                
            </td>
        </tr>
    </table>
  </div>
   <div class="md-footer">
    <asp:Button ID="btnSubmit" runat="server" Text="Submit" onclick="btnSubmit_Click" UseSubmitBehavior="false"  class="btn btn-large btn-block btn-info btn-center" Width="75" Height="30"/>
   </div></div></div>
    </asp:Panel>

    <input type="hidden" id="delid" runat="server" clientidmode="Static" value="" />
    <asp:Button ID="delBtn" runat="server" Width="0" Height="0" CssClass="displaynone" OnClick="Delete" ClientIDMode="Static"></asp:Button>
    
</ContentTemplate>
</asp:UpdatePanel>
</asp:Content>
