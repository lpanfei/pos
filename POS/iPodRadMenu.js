(function ($) {

// enforces context over function
var bind = function(context, name) {
	return function(e) {
		if (e && e.preventDefault)
		    e.preventDefault();
		
		return context[name].apply(context, arguments);
	};
};

// enforces "top" coordinate of element to be within container
var constrain = function (element, container) {
	var endTop = parseInt($(element).css("top"));
	var maxScrollTop = container.offsetHeight - element.offsetHeight;
		
	if (endTop > 0 || maxScrollTop > 0) {
		$(element).animate ({
			top: 0
		}, "slow");
	} else if (endTop < maxScrollTop) {
		$(element).animate ({
			top: maxScrollTop
		}, "slow");
	}
}

// <dragging>
var dragStartPos = 0;
var dragStartTop = 0;

var dragElement = null;

var hasDragged = false;

var dragStart = function(e) {
	if (this.offsetHeight > $get('RadMenu1').parentNode.offsetHeight) {  /* disable scrolling for menus that do not need it */
		$(document.body)
			.bind ("mousemove", compositeDrag)
			.bind ("mouseup", dragEnd);
		
		dragElement = $(this).stop();
			
		dragStartPos = e.pageY;
		var top = parseInt(dragElement.css("top")) || 0;
		dragStartTop = top < 0 ? top : 0;
	}
		
	e.preventDefault();
	e.stopPropagation();
	
	hasDragged = false;
}

var simpleDrag = function (e) {
	dragElement
		.css ("top", dragStartTop - (dragStartPos - e.pageY) + 'px');
};

// hit-test
var compositeDrag = function (e) {
	if (Math.abs (dragStartPos - e.pageY) > 5) /* threshold value */
	{
		simpleDrag (e);
	
		var container = $get("RadMenu_iPhone_Content");
	
		$("<div id='overlay'></div>")
			.css({
				height: container.offsetHeight,
				width: container.offsetWidth,
				position: "absolute",
				top: 0,
				zIndex: 7100
			})
			.appendTo (container);
		
		hasDragged = true;
		
		$(document.body)
			.unbind("mousemove", compositeDrag)
			.bind ("mousemove", simpleDrag)
	}
};

var drag = compositeDrag;

var dragEnd = function (e) {
	e.preventDefault();
	e.stopPropagation();
	
	$(document.body)
		.unbind("mousemove", simpleDrag)
		.unbind("mousemove", compositeDrag)
		.unbind("mouseup", dragEnd);
			
	constrain (dragElement[0], $get('RadMenu_iPhone_Content'));
	
	$('#overlay').remove();
}
// </dragging>
    
$.fn.reverse = [].reverse;

window.iPodMenu = function (RadMenuInstance)
{
    this._animating = false;
    this._menu = RadMenuInstance.get_element();
    this._backButton =
        $('a.backButton', this._menu.parentNode.parentNode)
            .bind("click", bind(this, "navigateBack"));
    
    /* go to selected item */        
    var selectedItem = $('.rmSelected', this._menu);
    var parentItems = selectedItem.parents('.rmItem');
    this._level = parentItems.length - 1;
    
    var me = this;
    
    if (this._level > 0) {
        this._backButton.css('display', 'inline');
        
        $.each(parentItems.reverse(), function() {
            $('> .rmSlide', this)
                .css({
                    display: 'block',
			        left: me._menu.offsetWidth + 'px',
			        top: -this.offsetTop + 'px'
                })
                .find('> .rmGroup')
                    .css('display', 'block');
        });
        
        this._lastChild = selectedItem.parent().parent()[0];
        
        $(this._menu).css ('left', -(this._menu.offsetWidth * me._level));
    }
    
	RadMenuInstance.add_itemClicking(bind(this, "navigateForward"));
    
    /* necessary for 2nd level menus */
	RadMenuInstance.add_itemOpening(
		function (sender, args) {
			args.set_cancel(true);
		});
	
	if (!navigator.userAgent.match(/iPod/i)) {
		/* init drag interface for non-iphone users */
		$(".rmVertical", this._menu)
			.mousedown(dragStart);
	}
	
	$(".rmTemplateLink", this._menu)
		.click(function (e) {
			if (hasDragged || $(this).attr('href') == '#')
				e.preventDefault();
		});
}

window.iPodMenu.prototype =
{
    _animateTransition: function (direction, onAnimationEnded)
    {
		var that = this;
		
        this._animating = true;
        
        var finalPosition = parseInt($telerik.getCurrentStyle(this._menu, 'left', '0px')) || 0;
        
        if (direction == Telerik.Web.UI.jSlideDirection.Left)
            finalPosition -= this._menu.offsetWidth;
        else
            finalPosition += this._menu.offsetWidth;
            
        $(this._menu)
			.animate ({
				left: finalPosition
			}, 300, "linear",
			function () {
				if (onAnimationEnded)
					onAnimationEnded();
					
				that._animating = false;
			});
	},
	
	changeOrientation: function(orientation)
	{
        var frame = $(this._menu).parents('#iPhoneFrame');
        
        frame.removeClass('OrientationVertical')
			 .removeClass('OrientationHorizontal')
			 .addClass('Orientation'+orientation.substr(0,1).toUpperCase()+orientation.substr(1));
        
        if (!this._lastChild) {
			this._lastChild = this._menu;
        }
        
		$('.rmSlide', this._menu).css({
			left: this._menu.offsetWidth + 'px'
		});
		
		$(this._menu).css({
			left: -this._menu.offsetWidth * (this._level + 1) + 'px'
		});
		
		if (this._lastChild.offsetHeight < this._menu.parentNode.offsetHeight) {
			if (this._level == -1)
				$('.rmRootGroup', this._lastChild).css ( 'top', '0px' );
			else
				$('.rmGroup', this._lastChild).css ( 'top', '0px' );
		}
	},

    hideLastChild: function ()
    {
		this._lastChild =
			$(this._lastChild)
				.hide()
				.parent().parent().parent().get(0);
    },

    navigateBack: function (e)
    {
        if (this._animating) return;

		if (this._lastChild.parentNode.parentNode.offsetHeight < this._menu.parentNode.offsetHeight) {
			$(this._lastChild.parentNode.parentNode).css ({
				top: '0px'
            });
        }
        
        this._animateTransition(Telerik.Web.UI.jSlideDirection.Right, bind(this, "hideLastChild"));
        
        --this._level;
        
        if (this._level < 0)
        {
            this._backButton.fadeOut("fast");
        }
    },

    navigateForward: function (sender, args)
    {
		if (hasDragged) {
			args.set_cancel(true);
			return;
		}
        else if (this._animating) {
			return;
		}
        
        var clickedItem = args.get_item();
        var childList = clickedItem.get_childListElement();
        
        if (childList)
        {   
            // update back button
            
            this._backButton.fadeIn("fast");
            
            // show child list on the right
            
            childList.style.display = "block";
            
            this._lastChild = childList.parentNode;
            
            $(this._lastChild).css ({
				display: "block",
				left: this._menu.offsetWidth + 'px',
				top: -clickedItem.get_element().offsetTop - clickedItem.get_parent().get_childListElement().offsetTop + 'px'
            });
            
            // animate left
            
            this._animateTransition(Telerik.Web.UI.jSlideDirection.Left);   
         
            this._level++;
        }
        else
        {
            if (clickedItem.get_navigateUrl() == '#')
			    args.set_cancel (true);
        }
    }
};
})($telerik.$);