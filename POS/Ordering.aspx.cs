﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;
using Telerik.Web.UI;
using System.Text;

namespace POS
{
    public partial class Ordering : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Menu.DataSource = GlobalCache.Categories;
                Menu.DataBind();

                List<Product> products = GetProductsWithDiscount(GlobalCache.Products);
                foodRepeater.DataSource = products;
                foodRepeater.DataBind();

                Repeater1.DataSource = products;
                Repeater1.DataBind();

                GlobalCache.RefreshInvoices(0, null, null, null, false);
                if (GlobalCache.Invoices.Any())
                {
                    lblNo.Text = (GlobalCache.Invoices.First().inv_No + 1).ToString();
                }
                else
                {
                    lblNo.Text = "100000";
                }

                //ScriptManager.RegisterStartupScript(this, GetType(), "refreshWaterfall", "refreshWaterfall();", true);

                //normalmenu.InnerHtml = "<div>aaa</div";
            }
        }

        public override RadWindowManager WindowManager
        {
            get
            {
                return this.windowManager;
            }
        }

        public List<Product> GetProductsWithDiscount(List<Product> products)
        {
            if (products == null)
                return null;
            products.ForEach(p=>{p.Discount=100;});
            if (GlobalCache.Promotions != null)
            {
                foreach (Promotion promotion in GlobalCache.Promotions)
                {
                    if (DateTime.Now >= promotion.StartTime && DateTime.Now <= promotion.EndTime)
                    {
                        Product product = products.FirstOrDefault(p => p.pro_Id == promotion.ProductID);
                        if (product != null)
                        {
                            product.Discount = promotion.Discount;
                        }
                    }
                }
            }
            return products;
        }

        protected void RefreshAll(object sender, EventArgs args)
        {
            List<Product> products = GetProductsWithDiscount(GlobalCache.Products);
            foodRepeater.DataSource = products;
            foodRepeater.DataBind();

            Repeater1.DataSource = products;
            Repeater1.DataBind();

            ScriptManager.RegisterStartupScript(this, GetType(), "refreshWaterfall", "refreshWaterfall();", true);
            breadcrumb.Text = "";
        }

        protected void CategorySelected(object sender, RadMenuEventArgs e)
        {
            foodRepeater.DataSource = null;
            Telerik.Web.UI.RadMenuItem ItemClicked = e.Item;
            if (GlobalCache.Products.Any(p => p.Category.cat_Id == Convert.ToInt32(ItemClicked.Value)))
            {
                List<Product> products = GetProductsWithDiscount(GlobalCache.Products.Where(p => p.Category.cat_Id == Convert.ToInt32(ItemClicked.Value)).ToList());
                foodRepeater.DataSource = products;
                foodRepeater.DataBind();
                Repeater1.DataSource = products;
                Repeater1.DataBind();

            }
            else
            {
                foodRepeater.DataSource = new List<Product>();
                foodRepeater.DataBind();
                Repeater1.DataSource = new List<Product>();
                Repeater1.DataBind();
            }

            BuildBreadcrumb(GlobalCache.Categories.FirstOrDefault(c => c.cat_Id == Convert.ToInt32(ItemClicked.Value)));
            ScriptManager.RegisterStartupScript(this, GetType(), "refreshWaterfall", "refreshWaterfall();", true);
        }


        void BuildBreadcrumb(Category currentCat)
        {
            breadcrumb.Text = " > " + GetParentCategory(currentCat, currentCat.cat_Name);
        }

        string GetParentCategory(Category cat, string bc)
        {

            if (GlobalCache.Categories.Any(c => c.cat_Id == cat.ParentID))
            {
                Category parent = GlobalCache.Categories.FirstOrDefault(c => c.cat_Id == cat.ParentID);
                bc = parent.cat_Name + " > " + bc;
                return GetParentCategory(parent, bc);
            }
            else
            {
                return bc;
            }
        }


        protected void UpdateOrder(object sender, EventArgs e)
        {
            UpdatePendingList();
        }

        protected void UpdateBox(object sender, EventArgs e)
        {

            OpenImageLightBox.Items.Clear();
            int id = Convert.ToInt32(pid.Value);
            Product p = GlobalCache.Products.FirstOrDefault(f => f.pro_Id == id);
            System.Drawing.Image image = System.Drawing.Image.FromFile(Server.MapPath("~/Upload/" + p.ImageUrl));
            int width = image.Width;
            var imageWd = width;
            int height = image.Height;
            if (width < 400)
            {
                width = 400;
            }
            if (width > 800)
            {
                imageWd = 800;
                height = (height * 800) / width;
                width = 800;
            }



            int bottom = 270;
            string src = HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + p.ImageUrl);
            string text = "<img src=\"" + src + "\" alt='" + p.pro_Name + "'style='width:" + imageWd.ToString() + "px'/>";

            StringBuilder sb = new StringBuilder();
            sb.Append("<span style='color: #3596E7;font-size: 20px;font-family: Trebuchet MS, Verdana, sans-serif;position:absolute;top:" + (height + 20).ToString() + "px;width:" + width.ToString() + "px;text-align:center'>");
            sb.Append(p.pro_Name);
            sb.Append("&nbsp;&nbsp;/&nbsp;&nbsp;");
            sb.Append(String.Format("{0:C}", p.pro_price));
            sb.Append("&nbsp;&nbsp;</span>");
            sb.Append("<img id='addtocart' src='images/plus-outline.png' style='width:32px;height:32px;cursor:pointer;z-index:3000;position:absolute;top:" + (height + 20).ToString() + "px;left:" + (width - 40).ToString() + "px;'/>");
            sb.Append("<table style='z-index:3000;width:");
            sb.Append(width.ToString());
            sb.Append("px;height:");
            sb.Append((height + bottom).ToString());
            sb.Append("px'><tr height='" + height.ToString() + "px'><td align='center' colspan='2'>");
            sb.Append(text);
            sb.Append("</td></tr><tr height='40px'>");
            sb.Append("<td colspan='2' width='40px' align='right'>");
            sb.Append("</td></tr><tr height='30px'><td align='left'></td><td align='right'><div id='fblike'></div></td></tr><tr height='210px' valign='top'><td colspan='2'><div id='fbcomments'></div></td></tr></table>");

            RadLightBoxItem item = new RadLightBoxItem();
            item.Width = width + 20;
            item.Height = height + bottom;

            string modifiedText = string.Format("<asp:UpdatePanel runat=\"server\"><ContentTemplate>{0}</ContentTemplate></asp:UpdatePanel>", sb.ToString());
            var control = Page.ParseControl(modifiedText);
            var updatePanel = control.Controls[0] as UpdatePanel;
            var template = updatePanel.ContentTemplate;
            item.ItemTemplate = template;
            OpenImageLightBox.Items.Add(item);
            OpenImageLightBox.CurrentItemIndex = 0;

            ScriptManager.RegisterStartupScript(this, GetType(), "OpenImageBox", "OpenImageBox('" + width.ToString() + "');", true);
        }

        protected void Order(object sender, EventArgs e)
        {
            int id = 0;
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            int i = 0;
            decimal total = 0;

            foreach (RepeaterItem ri in rptProductList.Items)
            {
                string desc = (ri.FindControl("lblName") as Label).Text;
                
                // decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int ppid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                Product p = GlobalCache.Products.FirstOrDefault(p2 => p2.pro_Id == ppid);
                decimal price = p.pro_price;
                price = price * p.Discount / 100;
                //decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = price * quantity;
                //string note = (ri.FindControl("txtNote") as TextBox).Text;
                ips.Add(new InvoiceProduct() { ip_Description = desc, ip_Discount = p.Discount, ip_InvoiceID = id, ip_Note = "", ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = p.cost, ip_ProductID = ppid, ip_ID = i });
                i++;
                total += subtotal;

            }

            string name = pname.Value;
            
            int pppid = Convert.ToInt32(pid.Value);
            if (ips.Any(p => p.ip_ProductID == pppid))
            {
                InvoiceProduct ipp = ips.FirstOrDefault(p => p.ip_ProductID == pppid);
                Product p3 = GlobalCache.Products.FirstOrDefault(p2 => p2.pro_Id == pppid);
                decimal iprice = p3.pro_price;
                iprice = iprice * p3.Discount / 100;
                ipp.ip_SubTotal += iprice;
                ipp.ip_Quantity += 1;
                total += iprice;
                
            }
            else
            {
                Product p3 = GlobalCache.Products.FirstOrDefault(p2 => p2.pro_Id == pppid);
                decimal iprice = p3.pro_price;
                iprice = iprice * p3.Discount / 100;
                total += iprice;

                ips.Add(new InvoiceProduct() { ip_Description = name, ip_Discount = p3.Discount, ip_InvoiceID = id, ip_Note = "", ip_Price = iprice, ip_Quantity = 1, ip_SubTotal = iprice, cost = p3.cost, ip_ProductID = pppid, ip_ID = i });

            }
            rptProductList.DataSource = ips;
            rptProductList.DataBind();

            lblTotal.Text = "Total: " + String.Format("{0:C}", total);
            //ltlItems.Text = "Total: " + ips.Count.ToString() + " Items";
        }

        void UpdatePendingList()
        {
            int id = 0;
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            int i = 0;
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                string desc = (ri.FindControl("lblName") as Label).Text;
                //decimal price = Convert.ToDecimal((ri.FindControl("txtPrice") as Label).Text);
                
                // decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                Product p = GlobalCache.Products.FirstOrDefault(p2 => p2.pro_Id == pid);
                decimal price = p.pro_price;
                //if(GlobalCache.Promotions!=null)
                //{
                //    Promotion pro=GlobalCache.Promotions.FirstOrDefault(prom=>prom.ProductID==pid&&
                //}
                price = price * p.Discount / 100;
                //decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = price * quantity;
                //string note = (ri.FindControl("txtNote") as TextBox).Text;
                ips.Add(new InvoiceProduct() { ip_Description = desc, ip_Discount = p.Discount, ip_InvoiceID = id, ip_Note = "", ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = p.cost, ip_ProductID = pid, ip_ID = i });
                i++;
                total += subtotal;

            }
            lblTotal.Text = "Total: " + String.Format("{0:C}", total);
            //ltlItems.Text = "Total: " + ips.Count.ToString() + " Items";
            rptProductList.DataSource = ips;
            rptProductList.DataBind();
        }

        protected void DeleteItem(object sender, EventArgs e)
        {
            RepeaterItem currentri = (sender as LinkButton).Parent as RepeaterItem;

            int id = 0;
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            int i = 0;
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                if (ri == currentri)
                    continue;
                string desc = (ri.FindControl("lblName") as Label).Text;
                decimal price = Convert.ToDecimal((ri.FindControl("txtPrice") as Label).Text);
                // decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                Product p = GlobalCache.Products.FirstOrDefault(p2 => p2.pro_Id == pid);
                //decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = price * quantity;
                //string note = (ri.FindControl("txtNote") as TextBox).Text;
                ips.Add(new InvoiceProduct() { ip_Description = desc, ip_Discount = p.Discount, ip_InvoiceID = id, ip_Note = "", ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = p.cost, ip_ProductID = pid, ip_ID = i });
                i++;
                total += subtotal;
            }
            lblTotal.Text = "Total: " + String.Format("{0:C}", total);
            // ltlItems.Text = "Total: " + ips.Count.ToString() + " Items";
            rptProductList.DataSource = ips;
            rptProductList.DataBind();
        }

        protected void DoInvoice(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                this.ShowError("Table No. is required to make order.");
                txtName.Focus();
                return;
            }

            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                string desc = (ri.FindControl("lblName") as Label).Text;
                decimal price = Convert.ToDecimal((ri.FindControl("txtPrice") as Label).Text);
                //decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                //decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = price * quantity;
                //string note = (ri.FindControl("txtNote") as TextBox).Text;

                InvoiceProduct ipp = new InvoiceProduct() { ip_Description = desc, ip_Discount = 100, ip_Note = "", ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, ip_ProductID = pid };
                ipp.Product = GlobalCache.Products.FirstOrDefault(p => p.pro_Id == ipp.ip_ProductID);
                ipp.ip_Discount = ipp.Product.Discount;
                ipp.cost = ipp.Product.cost;
                ips.Add(ipp);
                decimal cost = ipp.cost;
                total += cost;
            }
            if (!ips.Any())
            {
                this.ShowError("Nothing is ordered!");
                return;
            }
            Invoice invoice = new Invoice()
            {
                inv_Id = 0,
                inv_Staff = GlobalCache.CurrentUser.usr_Name,
                inv_Address = "",
                inv_ClientID = this.UserClientID,
                inv_Customer = this.txtName.Text.Trim(),
                inv_GST = 0,
                inv_Total = Convert.ToDecimal(this.lblTotal.Text.Substring(8)),
                inv_TotalIncGST = Convert.ToDecimal(this.lblTotal.Text.Substring(8)),
                inv_Mobile = "",
                inv_No = Convert.ToInt32(this.lblNo.Text),
                inv_PaymentMethod = "",
                inv_PrintTo = "",
                CustomerType = "",
                inv_WalletID = txtWalletID.Text.Trim(),
                inv_Phone = "",
                inv_StoreID = this.UserStoreID,
                Email = "",
                Country = "",
                IsGST = false,
            };

            invoice.cost = total;
            invoice.Products = ips;
            invoice.UserID = GlobalCache.CurrentUser.usr_ID;
            invoice.LastUpdatedTime = DateTime.Now;
            //invoice.WalletConnection = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["WalletDBConnection"].ConnectionString;
            new POSService.POSService().DoInvoice(invoice);
            if (invoice.inv_PrintTo == "Wallet(App)" || invoice.inv_PrintTo == "Print & Wallet (App)")
            {
                int clientid = this.UserClientID;
                int storeid = this.UserStoreID;
                int invoiceno = invoice.inv_No;
                //notify the wallet app to get receipt by client id, store id, invoice no
            }
            this.ShowSuccess("Thank you. Order is made successfully and will be served soon");
            rptProductList.DataSource = null;
            rptProductList.DataSource = new List<InvoiceProduct>();
            rptProductList.DataBind();
            txtName.Text = "";
            txtWalletID.Text = "";
            lblTotal.Text = "";
            ScriptManager.RegisterStartupScript(this, GetType(), "closeorder", "closeOrder();", true);
            //ltlItems.Text = "";
        }
    }
}