﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ordering.aspx.cs" Inherits="POS.Ordering" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head runat="server">
    <title>Distributed POS System - Client 2011</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.2)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.2)">
    <link href="themes/cupertino/jquery-ui.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pos.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script src="Scripts/jquery-ui.js"></script>
    <script src="Scripts/jquery.ui.touch-punch.min.js"></script>
    <script src="window/js/modernizr.custom.js"></script>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/waterfall.css">
    <link rel="stylesheet" href="css/idangerous.swiper.css">
    <link rel="stylesheet" href="css/ordering.css">
    <script src="Scripts/idangerous.swiper-2.4.1.js"></script>
    <%-- <script src="Scripts/jquery.mobile-1.4.0.js"></script>--%>
    <style>
        .hname, hprice
        {
            display: none;
        }
        .displaynone
        {
            display: none;
        }
        
        .fb_edge_widget_with_comment span.fb_edge_comment_widget iframe.fb_ltr
        {
            display: none !important;
        }
        .rltbDescriptionBox
        {
            display: none;
        }
        #waterfall-loading
        {
            display: none;
        }
        .ui-effects-transfer
        {
            border: 2px dotted gray;
        }
        .drag-cart-item
        {
            z-index: 3000;
        }
        .oproductid
        {
            display: none;
        }
        
        .checkstatus
        {
            display: none;
        }
        div::-webkit-scrollbar,body::-webkit-scrollbar
        {
            width:0px;
        }

    </style>
</head>
<body style="height:100%">
    <div id="fb-root">
    </div>
    <script>        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=226988780808773";
            fjs.parentNode.insertBefore(js, fjs);
        } (document, 'script', 'facebook-jssdk'));</script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true">
        <Scripts>
            <asp:ScriptReference Path="common.js" />
        </Scripts>
    </asp:ScriptManager>
    <div>
        <telerik:RadSkinManager ID="QsfSkinManager" runat="server" ShowChooser="false" Skin="Metro" />
        <%--<telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All"
            EnableRoundedCorners="false" Skin="Silk" />--%>
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">
           
            </script>
        </telerik:RadCodeBlock>
        <asp:Button ID="submitBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="Order" ClientIDMode="Static" UseSubmitBehavior="false"></asp:Button>
        <asp:Button ID="updateBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="UpdateOrder" ClientIDMode="Static" UseSubmitBehavior="false"></asp:Button>
        <asp:Button ID="updateBoxBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="UpdateBox" ClientIDMode="Static" UseSubmitBehavior="false"></asp:Button>
        <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <table style="width: 100%; height: 76px; table-layout: fixed;  top: 0px;
                    z-index: 1000" id="fixedheader">
                    <tr style="height: 36px;">
                        <td>
                            <div id="header2" style="width: 100%">
                                <telerik:RadMenu ID="Menu" runat="server" Skin="MetroTouch" DataTextField="cat_Name"
                                    DataFieldID="cat_Id" DataFieldParentID="BindableParentID" Width="100%" OnItemClick="CategorySelected"
                                    DataValueField="cat_Id" style="z-index:1500">
                                </telerik:RadMenu>
                                <asp:Button ID="refreshAllBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
                                    OnClick="RefreshAll" ClientIDMode="Static"></asp:Button>
                            </div>
                        </td>
                        <td style="width: 300px; background: #25a0da; border-left: 1px solid white; color: White"
                            valign="middle">
                            <table style="width: 100%; height: 100%; table-layout: fixed; font-weight: bold;
                                cursor: pointer" id="cartsummary" class="ui-droppable">
                                <tr>
                                    <td style="width: 40px" align="right">
                                        <img src="images/icon_basket.png" width="30px" height="30px" style="cursor: pointer"
                                            alt="Cart" />
                                    </td>
                                    <td align="left" style="padding-left: 10px">
                                        <asp:Label ID="ltlItems" runat="server" CssClass="totalitems"></asp:Label>
                                    </td>
                                    <td style="width: 40px">
                                        <a href="javascript:void(0)" class="hpcExpand" id="sc_opener" title="Expand" style="display: block;
                                            background: transparent url(images/hpc12.png) no-repeat; overflow: hidden; background-position: -128px -53px;
                                            width: 32px; height: 32px"></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 40px; background-color: #3894E7; color: White; font-weight: bold;
                        vertical-align: middle;">
                        <td>
                            <div style="margin-left: 20px">
                                <a href="javascript:void" style="color: White" id="refreshAllLink">ALL</a>
                                <asp:Literal ID="breadcrumb" runat="server"></asp:Literal>
                            </div>
                        </td>
                        <td valign="middle" style="height: 40px; width: 100%; padding-right: 10px;" align="right">
                            <%--<a href="#" id="allfolds" style="color: White">ALL FOODS</a>--%>
                            <a class="listview" style="color: White" href="javascript:void">List View</a> &nbsp;|
                            &nbsp;<a class="galleryview" style="color: White" href="javascript:void">Gallery View</a>
                            &nbsp;|&nbsp;<a class="menuview" style="color: White" href="javascript:void">Menu View</a>
                        </td>
                    </tr>
                </table>
                <div id="productcontainer">
               
                <div id="waterfall" style="margin-top:10px">
                </div>
                <div class="slideshow" style="display: none">
                    <a class="arrow-left" href="#"></a><a class="arrow-right" href="#"></a>
                    <div class="swiper-container thumbs-cotnainer">
                        <div class="swiper-wrapper">
                            <asp:Repeater ID="Repeater1" runat="server">
                                <ItemTemplate>
                                    <div class="swiper-slide">
                                        <div class="fooditem">
                                            <div class="foodcheckicon checkstatus" style="position: absolute; top: 0px; margin-left: 263px;
                                                cursor: pointer; width: 32px; height: 32px; display: none;">
                                                <img src="images/check.png" style="width: 32px; height: 32px;">
                                            </div>
                                            <div class="foodaddicon" style="position: absolute; top: 200px; margin-left: 255px;
                                                cursor: pointer; opacity: 0.7; width: 40px; height: 40px; background: gray">
                                                <img src="images/plus-outline.png" style="width: 32px; height: 32px; margin-top: 4px;
                                                    margin-left: 4px;">
                                            </div>
                                            <div style="height: 50px;">
                                                <div class="app-title">
                                                    <%#Eval("pro_Name") %></div>
                                                <div class="app-discount" style="color: Red">
                                                    (<span class="discountvalue"><%#Eval("Discount") %></span>% Discount)
                                                </div>
                                            </div>
                                            <asp:Image ID="Image2" ImageUrl='<%# HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + Eval("ImageUrl")) %>'
                                                runat="server" Width="295" CssClass="foodimage" Height="345" Style="overflow: hidden" />
                                            <div class="pdescription" style="width: 155px; position: absolute; top: 300px; background: white;
                                                color: Black; border: 1px solid white; height: 150px; box-shadow: 0px 0px 5px #fff;
                                                border-radius: 10px; margin-left: 5px; opacity: 0.7">
                                                <%#Eval("pro_Description") %>
                                            </div>
                                            <div class="slideaction">
                                                <a class="fbcomment" href="#" style="background-repeat: no-repeat; background-position: 0 0;
                                                    background-image: url(../images/icon-comments.png); padding-left: 15px; color: #fff;
                                                    aligh: right">0</a> <a class="fblike" href="#" style="background-repeat: no-repeat;
                                                        background-position: 0 0; background-image: url(../images/icon-hearts.png); color: #fff;
                                                        align: right; padding-left: 15px; margin-left: 10px;">0</a>
                                            </div>
                                            <div class="foodlike" style="float: right">
                                            </div>
                                            <div class="foodcomment" style="height: 150px; overflow: auto; clear: both">
                                            </div>
                                            <div class="app-price" style="display: none">
                                                <%#Eval("pro_price")%></div>
                                            <div class="app-id" style="display: none">
                                                <%#Eval("pro_Id")%></div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <div id="normalmenu" style="display: none; margin-top: 10px;">
                    <table class="menuviewcontainer list" style="margin: 0 auto; width: auto; border: 0px solid white">
                    </table>
                </div>
                <div style="visibility: hidden">
                    <asp:Repeater ID="foodRepeater" runat="server">
                        <ItemTemplate>
                            <div>
                                <asp:Image CssClass="himage" ID="Image1" runat="server" ImageUrl='<%# HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + Eval("ImageUrl")) %>'
                                    Width="250" Style="overflow: hidden" />
                                <asp:Label ID="Label1" CssClass="hname" Text='<%#Eval("pro_Name") %>' runat="server"></asp:Label>
                                <asp:Label ID="Label4" CssClass="hdiscount" Text='<%#Eval("Discount") %>' runat="server"></asp:Label>
                                <asp:Label ID="Label2" CssClass="hprice" Text='<%#String.Format("{0:C}",Eval("pro_Price") ) %>'
                                    runat="server"></asp:Label>
                                <asp:Label ID="Label3" CssClass="hid" Text='<%#Eval("pro_Id")  %>' runat="server"></asp:Label>
                                <asp:Label ID="Label5" CssClass="hcatname" Text='<%#Eval("Category.cat_Name")  %>'
                                    runat="server"></asp:Label>
                                <asp:Label ID="Label6" CssClass="hdescription" Text='<%#Eval("pro_Description")  %>'
                                    runat="server"></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                 </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div id="detail" style="position: absolute; right: 0px; width: 600px; height: 500px;
            background-color: gray; top: 36px; z-index: 2000; display: none; border: 1px solid black">
            <table style="width: 100%">
                <tr style="height: 40px">
                    <td colspan="2">
                        <div style="width: 588px; height: 14px; background-color: black; color: White; vertical-align: middle;
                            padding-top: 13px; padding-bottom: 13px; padding-left: 13px;">
                            <div style="float: left">
                                <label class="dname" style="color: White">
                                </label>
                                &nbsp; / &nbsp;
                                <label class="dprice" style="color: White">
                                </label>
                                <label class="did" style="color: White; display: none">
                                </label>
                            </div>
                            <div style="float: right; margin-right: 10px; margin-top: -8px;">
                                <input type="button" value="Back" id="backbtn" class="btn btn-large btn-block btn-info btn-add"
                                    style="width: 100px" />
                            </div>
                            <div style="float: right; margin-right: 10px; margin-top: -8px;">
                                <input type="button" value="Order" id="orderbtn" class="btn btn-large btn-block btn-info btn-add"
                                    style="width: 100px" />
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <div style="height: 460px; border-right: 1px solid #3894E7; width: 300px; overflow: hidden;
                            display: table-cell; vertical-align: middle; text-align: center">
                            <img src="" alt="" style="width: 300px; height: 460px;" class="dimg" />
                        </div>
                    </td>
                    <td width="50%" style="vertical-align: top">
                        <div style="height: 400px; width: 300px; overflow: auto;" class="commentsarea" id="comments">
                        </div>
                        <div style="height: 60px; width: 300px; border-top: 1px solid white; overflow: hidden;">
                            <div style="height: 20px; text-align: left; overflow: hidden; margin-top: 15px; width: 70px;"
                                class="likearea" id="like">
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server" class="cartcontainer"
            style="display: none">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="submitBtn" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="updateBtn" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <div class="rltbOverlay" style="z-index: 3000;">
                </div>
                <div id="cartdetails" class="RadLightBox RadLightBox_Metro" style="position: fixed;
                    left: 50%; top: 50%; z-index: 3001; width: 420px; height: 670px; margin-left: -210px;
                    margin-top: -345px; visibility: visible; overflow: visible;">
                    <div class="rltbGlowEffect">
                    </div>
                    <div class="rltbClose orderclose" style="">
                        <button type="button" class="rltbActionButton rltbCloseButton">
                            <span class="rltbIcon rltbCloseIcon"></span><span class="rltbButtonText">Close</span></button>
                    </div>
                    <div class="rltbWrapper">
                        <div class="rltbItemBox">
                            <table style="width: 100%; height: 100%;" id="cart">
                                <tr style="height: 60px;">
                                    <td style="margin-bottom: 10px;">
                                        <table>
                                            <tr>
                                                <td>
                                                    <table style="width: 100%; height: 100%">
                                                        <tr>
                                                            <td style="width: 120px;">
                                                                Table NO.:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtName" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Wallet ID:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWalletID" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="center" style="text-align: center">
                                                    <asp:Button ID="Button1" Text="Submit" runat="server" OnClick="DoInvoice" CssClass="btn btn-large btn-block btn-info btn-add"
                                                        Width="100" Height="30" Style="margin-left: 10px; float: left;" UseSubmitBehavior="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <div id="orderdetail" style="width: 400px; z-index: 1000;height:560px;overflow:auto">
                                            <table style="width: 100%;" class="list olist">
                                                <thead>
                                                    <tr>
                                                        <th align="center">
                                                        </th>
                                                        <th>
                                                            Name
                                                        </th>
                                                        <th>
                                                            Price
                                                        </th>
                                                        <th>
                                                            Qty
                                                        </th>
                                                        <th>
                                                            Sub Total
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:Repeater ID="rptProductList" runat="server">
                                                        <HeaderTemplate>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td align="center">
                                                                    <asp:LinkButton ID="lbtnDel" runat="server" class="icon icon-del" OnClick="DeleteItem"></asp:LinkButton>
                                                                    <asp:Label ID="lblId" CssClass="opid" runat="server" Text='<%# Eval("ip_ID")%>' Visible="false"></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:Label ID="lblName" CssClass="opname" runat="server" Text='<%# Eval("ip_Description")%>'></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    $<asp:Label ID="txtPrice" CssClass="opprice" runat="server" Text='<%# Convert.ToDecimal(Eval("ip_Price")).ToString("0.00")%>'></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:TextBox ID="txtQuantity" CssClass="opquantity" runat="server" Width="50px" Text='<%# Eval("ip_Quantity")%>'></asp:TextBox>
                                                                    <asp:Label ID="lblProductId" runat="server" Text='<%# Eval("ip_ProductID")%>' CssClass="oproductid"></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    $<asp:Label ID="lblSubTotal" CssClass="opsubtotal" runat="server" Text='<%# Eval("ip_SubTotal")%>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height: 50px">
                                    <td style="height: 50px">
                                        <div style="height: 50px; text-align: right; color: White; padding-top: 10px;">
                                            <asp:Label ID="lblTotal" CssClass="totalcost" runat="server" Style="margin-right: 10px;
                                                color: Black;"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <telerik:RadLightBox ID="OpenImageLightBox" runat="server" Modal="true" Skin="Metro"
                    ShowCloseButton="true">
                    <ClientSettings>
                        <AnimationSettings HideAnimation="Resize" NextAnimation="Fade" PrevAnimation="Fade"
                            ShowAnimation="Resize" />
                    </ClientSettings>
                </telerik:RadLightBox>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="updateBoxBtn" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <script src="js/libs/handlebars/handlebars.js"></script>
        <script src="js/libs/jquery.easing.js"></script>
        <script src="js/waterfall.js"></script>
        <script type="text/javascript">
            var isrefresh = false;
            var isshowlb = false;
            var fbwidth = "";
            var wf;
            function OpenImageBox(fwidth) {
                isshowlb = true;
                fbwidth = fwidth;
            }

            jQuery.fn.outerHTML = function (s) {
                return s
        ? this.before(s).remove()
        : jQuery("<p>").append(this.eq(0).clone()).html();
            };

            function InCartContainer(ele) {
                var parent = ele.parent();
                if (parent.is("body")) {
                    return false;
                } else if (parent.attr("class") == "cartcontainer") {
                    return true;
                } else {
                    return InCartContainer(parent);
                }
            }

            var mySwiper = null;
            function pageLoad(sender, args) {
                refreshWaterfall();
                $("html").css("height", "100%");
                //$("form").css("height", "100%");
                if (mySwiper != null) {
                    mySwiper.destroy();
                    mySwiper = null;
                }

                if ($(".slideshow").css("display") != "none") {
                    var isiPad = navigator.userAgent.match(/iPad/i) != null;
                    if (isiPad) {
                        $(".slideshow").css("width", "945px");
                        mySwiper = $('.thumbs-cotnainer').swiper({
                            slidesPerView: 3,
                            offsetPxBefore: 10,
                            offsetPxAfter: 10,
                            calculateHeight: true,
                            slidesPerGroup: 3
                        });
                    } else {
                        mySwiper = $('.thumbs-cotnainer').swiper({
                            slidesPerView: 'auto',
                            offsetPxBefore: 5,
                            offsetPxAfter: 5,
                            calculateHeight: true,
                            slidesPerGroup: 3
                        });
                    }
                }

                if (navigator.userAgent.match(/iPad/i) != null) {
                    $("#productcontainer").css("height", "690px");
                    $("#productcontainer").css("overflow-y", "auto");
                    $("#productcontainer").css("overflow-x", "hidden");
                    $("#waterfall").css("margin-top", "10px");
                    $(".slideshow").css("margin-top", "10px");
                    $("#normalmenu").css("margin-top", "10px");
                } else {
                    $("#fixedheader").css("position", "fixed");
                    $("#waterfall").css("margin-top", "91px");
                    $(".slideshow").css("margin-top", "91px");
                    $("#normalmenu").css("margin-top", "91px");
                }


                $(".listview").unbind("click");
                $(".listview").click(function () {
                    $(".slideshow").css("display", "none");
                    $('#waterfall').css("display", "");
                    $("#normalmenu").css("display", "none");
                    $("#currentview").val("listview");
                    $(window).trigger("resize");
                });

                $(".galleryview").unbind("click");
                $(".galleryview").click(function () {
                    $(".slideshow").css("display", "");
                    $('#waterfall').css("display", "none");
                    $("#normalmenu").css("display", "none");
                    $("#currentview").val("galleryview");
                    if (mySwiper != null) {
                        mySwiper.destroy();
                        mySwiper = null;
                    }
                    if (mySwiper == null) {
                        var isiPad = navigator.userAgent.match(/iPad/i) != null;
                        if (isiPad) {
                            $(".slideshow").css("width", "945px");
                            mySwiper = $('.thumbs-cotnainer').swiper({
                                slidesPerView: 3,
                                offsetPxBefore: 10,
                                offsetPxAfter: 10,
                                calculateHeight: true,
                                slidesPerGroup: 3
                            });
                        } else {
                            mySwiper = $('.thumbs-cotnainer').swiper({
                                slidesPerView: 'auto',
                                offsetPxBefore: 5,
                                offsetPxAfter: 5,
                                calculateHeight: true,
                                slidesPerGroup: 3
                            });
                        }
                    }
                });

                $("#refreshAllLink").click(function () {
                    $("#refreshAllBtn").trigger("click");
                });

                $(".menuview").unbind("click");
                $(".menuview").click(function () {
                    $(".slideshow").css("display", "none");
                    $('#waterfall').css("display", "none");
                    $("#normalmenu").css("display", "");
                    $("#currentview").val("menuview");
                });

                $("." + $("#currentview").val()).trigger("click");

                //                $(".fooditem").unbind("hover");
                //                $(".fooditem").hover(function () {
                //                    $(this).find(".foodaddicon").eq(0).css("display", "");
                //                }, function () {
                //                    $(this).find(".foodaddicon").eq(0).css("display", "none");
                //                });

                $(".foodaddicon").click(function () {
                    var fitem = $(this).closest(".fooditem");

                    $("#pid").val($.trim(fitem.find(".app-id").eq(0).text()));
                    $("#pname").val($.trim(fitem.find(".app-title").eq(0).text()));
                    $("#pprice").val("$" + $.trim(fitem.find(".app-price").eq(0).text()));
                    //fitem.effect("shake", {}, 500, function () { });
                    $("#submitBtn").trigger("click");
                    //$("#cartsummary").effect("highlight", {}, 500, function () { });
                    e.stopPropagation();
                });


                $('.arrow-left').click(function (e) {
                    e.preventDefault();
                    mySwiper.swipePrev();
                });

                $('.arrow-right').click(function (e) {
                    e.preventDefault();
                    mySwiper.swipeNext(true);
                });

                var totalquantity = 0;
                $(".olist tbody > tr").each(function () {
                    totalquantity += parseInt($(this).find(".opquantity").eq(0).val());
                });
                if ($.trim($(".totalcost").text()) != "") {
                    $(".totalitems").text($.trim($(".totalcost").text()) + ", " + totalquantity.toString() + " items");
                } else {
                    $(".totalitems").text("");
                }

                if ($("#cart").parent().width() > 0) {
                    $("#cart").css("display", "");
                    $("#cart").parent().css("display", "");
                    $("#cart").parent().css("width", "400px");
                    $('#waterfall').waterfall('resize');
                }

                $(".opquantity").change(function () { $("#updateBtn").trigger("click"); });
                if (isshowlb) {
                    var lightBox = $find('<%= OpenImageLightBox.ClientID %>');
                    lightBox.show();
                    var src = $("#detail").find(".dimg").eq(0).attr("src");
                    $('#fbcomments').html(' <fb:comments href="' + src.replace("localhost", "127.0.0.1") + '" width="' + fbwidth + '" numposts="5" colorscheme="light"></fb:comments>');
                    if (typeof FB !== 'undefined') {
                        FB.XFBML.parse(document.getElementById('fbcomments'));
                    }

                    $('#fblike').html('<fb:like href="' + src.replace("localhost", "127.0.0.1") + '" layout="button_count" show_faces="false" width="65" action="like" font="segoe ui" colorscheme="light" />');
                    if (typeof FB !== 'undefined') {
                        FB.XFBML.parse(document.getElementById('fblike'));
                    }

                    $("#addtocart").click(function () {
                        var pname = $.trim($("#detail").find(".dname").eq(0).text());
                        var pprice = $.trim($("#detail").find(".dprice").eq(0).text());
                        var pid = $.trim($("#detail").find(".did").eq(0).text());
                        $("#pid").val(pid);
                        $("#pname").val(pname);
                        $("#pprice").val(pprice);
                        $("#submitBtn").trigger("click");
                        lightBox.hide();
                    });
                }
                isshowlb = false;
                fbwidth = "";
                $("#sc_opener").unbind("click");
                $("#sc_opener").click(function (event) {
                    if ($(this).hasClass("hpcExpand")) {
                        $(this).addClass("hpcCollapse");
                        $(this).removeClass("hpcExpand");
                        $(this).css("background-position", "-96px -53px");
                        $(".cartcontainer").css("display", "");
                        $(this).attr("title", "Collapse");
                    }
                    event.stopPropagation();
                });
//                $("#sc_opener").unbind("mouseup");
//                $("#sc_opener").mouseup(function () {
//                    return false;
//                });
//                $("#cartsummary").unbind("mouseup");
//                $("#cartsummary").mouseup(function () {
//                    return false;
//                });
                $("#backbtn").unbind("click");
                $("#backbtn").click(function () { $("#detail").css("display", "none"); });
                $("#orderbtn").unbind("click");
                $("#orderbtn").click(function () {
                    $("#detail").css("display", "none");

                    var pname = $.trim($("#detail").find(".dname").eq(0).text());
                    var pprice = $.trim($("#detail").find(".dprice").eq(0).text());
                    var pid = $.trim($("#detail").find(".did").eq(0).text());
                    $("#pid").val(pid);
                    $("#pname").val(pname);
                    $("#pprice").val(pprice);
                    $("#submitBtn").trigger("click");
                });
//                $(this).unbind("mouseup");
//                $(this).mouseup(function (login) {
//                    if (!InCartContainer($(login.target))) {
//                        $("#sc_opener").removeClass("hpcCollapse");
//                        $("#sc_opener").addClass("hpcExpand");
//                        $("#sc_opener").css("background-position", "-128px -53px");
//                        $("#cart").css("display", "none");
//                        $("#cart").parent().css("display", "none");
//                        $("#cart").parent().css("width", "0px");
//                        $("#sc_opener").attr("title", "Expand");
//                    }
//                });
                $("#cartsummary").unbind("click");
                $("#cartsummary").click(function (event) {
                    //alert($("#sc_opener").hasClass("hpcExpand"));
                    if ($("#sc_opener").hasClass("hpcExpand")) {
                        $("#sc_opener").addClass("hpcCollapse");
                        $("#sc_opener").removeClass("hpcExpand");
                        $("#sc_opener").css("background-position", "-96px -53px");
                        $(".cartcontainer").css("display", "");
                        $("#sc_opener").attr("title", "Collapse");
                    }
                    event.stopPropagation();
                });

                $(".orderclose").unbind("click");
                $(".orderclose").click(function () {
                    $("#sc_opener").removeClass("hpcCollapse");
                    $("#sc_opener").addClass("hpcExpand");
                    $("#sc_opener").css("background-position", "-128px -53px");
                    $(".cartcontainer").css("display", "none");
                    $("#sc_opener").attr("title", "Expand");
                });

                $(".witem").each(function () {
                    $(this).find(".checkstatus").each(function () {
                        $(this).css("display", "none");
                    });
                });

                $(".fooditem").each(function () {
                    $(this).find(".checkstatus").each(function () {
                        $(this).css("display", "none");
                    });
                });

                $(".menuviewcontainer tbody>tr").each(function () {
                    $(this).find(".checkstatus").each(function () {
                        $(this).css("display", "none");
                    });
                });

                $(".oproductid").each(function () {
                    var productid = $.trim($(this).text());
                    $(".witem").each(function () {
                        //$(this).find(".checkstatus").css("display", "none");
                        if ($(this).attr("data-index") == productid) {
                            $(this).find(".checkstatus").toggle("fade");
                        }
                    });
                    $(".fooditem").each(function () {
                        //$(this).find(".checkstatus").css("display", "none");

                        if ($.trim($(this).find(".app-id").eq(0).text()) == productid) {
                            //alert($(this).find(".checkstatus").eq(0).css("display"));
                            $(this).find(".checkstatus").toggle("fade");
                            //alert($(this).find(".checkstatus").css("display"));
                        }
                    });
                    $(".menuviewcontainer tbody>tr").each(function () {
                        if ($.trim($(this).find(".proid").eq(0).text()) == productid) {
                            //alert($(this).find(".checkstatus").eq(0).css("display"));
                            $(this).find(".checkstatus").toggle("fade");
                            //alert($(this).find(".checkstatus").css("display"));
                        }
                    });
                    
                });
//                wf = $('#waterfall').waterfall({
//                    itemCls: 'witem',
//                    colWidth: 250,
//                    gutterWidth: 20,
//                    gutterHeight: 30,
//                    checkImagesLoaded: false,
//                    path: function (page) {
//                        return '';
//                    },
//                    callbacks: {
//                        loadingError: function ($message, xhr) {
//                            //$message.html('Data load faild, please try again later.');
//                        }
//                    }
//                });
               

                //$(".productcontainer").css("width", ($(".productcontainer").width + 16).toString() + "px");
                //alert("ok");
                //                if (wf != null) {
                //                    wf.waterfall('destroy');
                //                }
            }

            function refreshFaceBook() {
                $(".witem").each(function () {
                    var img = $(this).find("img").eq(0);
                    var item = $(this);
                    $.ajax({
                        type: "GET",
                        url: "https://graph.facebook.com/?ids=" + img.attr("src").replace("localhost", "127.0.0.1"),
                        contentType: "application/json;charset=utf-8",
                        data: null,
                        processData: false,
                        dataType: "json",
                        success: function (result) {
                            for (var prop in result) {
                                if (result[prop].shares != undefined) {
                                    item.find(".fblike").eq(0).text(result[prop].shares.toString());
                                } else {
                                    item.find(".fblike").eq(0).text("0");
                                }
                                if (result[prop].comments != undefined) {
                                    item.find(".fbcomment").eq(0).text(result[prop].comments.toString());
                                } else {
                                    item.find(".fbcomment").eq(0).text("0");
                                }
                                break;
                            }

                        },
                        error: function (textStatus, str, err) {

                        }
                    });
                });

            }

            function closeOrder() {
                $("#sc_opener").removeClass("hpcCollapse");
                $("#sc_opener").addClass("hpcExpand");
                $("#sc_opener").css("background-position", "-128px -53px");
                $(".cartcontainer").css("display", "none");
                $("#sc_opener").attr("title", "Expand");
            }

            function refreshWaterfall() {

                wf = $('#waterfall').waterfall({
                    itemCls: 'witem',
                    colWidth: 250,
                    gutterWidth: 20,
                    gutterHeight: 30,
                    checkImagesLoaded: false,
                    path: function (page) {
                        return '';
                    },
                    callbacks: {
                        loadingError: function ($message, xhr) {
                            //$message.html('Data load faild, please try again later.');
                        }
                    }
                });

                //alert("ok");
                $('#waterfall').waterfall('removeItems', $('.witem:lt(' + $(".witem").length + ')'), function () { });
                var menus = new Array();
                var menuProducts = new Array();
                $(".himage").each(
                     function () {
                         $(this).css("display", "");
                         $(this).parent().css("display", "");
                     }
                 );

                $(".himage").each(function () {
                    var img = new Image();
                    var height = $(this).height();
                    var html = $(this).outerHTML();
                    var src = $(this).attr("src");
                    var name = $.trim($(this).parent().find(".hname").eq(0).text());
                    var price = $.trim($(this).parent().find(".hprice").eq(0).text());
                    var pid = $.trim($(this).parent().find(".hid").eq(0).text());
                    var discount = $.trim($(this).parent().find(".hdiscount").eq(0).text());
                    var catname = $.trim($(this).parent().find(".hcatname").eq(0).text());
                    var pdescription = $.trim($(this).parent().find(".hdescription").eq(0).text());
                    if (menus.indexOf(catname) >= 0) {

                        for (var k = 0; k < menuProducts.length; k++) {
                            if (menuProducts[k].cat == catname) {
                                menuProducts[k].productList.push({ id: pid, productPrice: price, productDiscount: discount, productName: name, description: pdescription });
                                break;
                            }
                        }
                    } else {
                        var products = new Array();
                        products.push({ id: pid, productPrice: price, productDiscount: discount, productName: name, description: pdescription });
                        menuProducts.push({ cat: catname, productList: products });
                        menus.push(catname);
                    }

                    var aa = $(this);
                    img.onerror = function () {
                        aa.css("display", "none");
                        aa.parent().css("display", "none");
                    }
                    img.onload = function () {
                        var fh = Math.ceil(name.length / 33) * 20;
                        var bh = Math.ceil(name.length / 33) * 20 + 30;
                        var item = $('<div class="witem" data-index="' + pid + '" style="cursor:pointer;height:' + (aa.height() + bh) + 'px;width:250px; background: #2b8af8; color: #fff;"></div>');

                        var bottom = $('<div style="position:absolute;top:' + aa.height() + 'px;width:250px;height:' + bh.toString() + 'px;background-color:gray;vertical-align:middle;overflow:hidden"></div>');
                        var comments = $('<a class="fbcomment" href="#" style="position:absolute;top:' + (fh + 10).toString() + 'px;left:5px;background-repeat: no-repeat;background-position: 0 0;background-image:  url(../images/icon-comments.png);padding-left:15px;color:#fff;align:right">0</a>');
                        var hearts = $('<a class="fblike" href="#" style="position:absolute;top:' + (fh + 10).toString() + 'px;left:45px;background-repeat: no-repeat;background-position: 0 0;background-image:  url(../images/icon-hearts.png);padding-left:15px;color:#fff;align:right">0</a>');
                        var ename = $('<span style="position:absolute;top:2px;left:2px;width:200px;">' + name + '</span>');

                        var hid = $('<span style="display:none;" class="pid">' + pid + '</span>');

                        var ah = (bh - 32) / 2;
                        var addbtn = $('<img src="images/plus-outline.png" style="width:32px;height:32px;position:absolute;top:' + ah + 'px;left:210px;"/>');
                        var img = $('<img src="' + src + '" style="width:250px;overflow:hidden"></img>');

                        var eprice = $('<div style="position:absolute;width:50px;height:15px;background-color:gray;top:0px;">' + price + '</div>');

                        bottom.append(ename);
                        bottom.append(comments);
                        bottom.append(hearts);
                        bottom.append(addbtn);

                        item.append(img);
                        item.append(bottom);
                        item.append(eprice);
                        if (discount != "100") {
                            var ediscount = $('<div style="position:absolute;width:90px;height:15px;background-color:gray;top:15px;left:0px; color:red">' + discount + '% Discount</div>');
                            item.append(ediscount);
                        }
                        var pdisplay = "none";
                        $(".oproductid").each(function () {
                            var productid = $.trim($(this).text());
                            if (pid == productid) {
                                pdisplay = "";
                            }
                        });

                        var cbtn = $('<img class="checkstatus" src="images/check.png" style="display:' + pdisplay + ';width:32px;height:32px;position:absolute;top:0px;right:0px;"/>');
                        item.append(cbtn);

                        comments.mouseover(function () { $(this).css("background-position", "0% 75%"); });
                        comments.mouseout(function () { $(this).css("background-position", "0 0"); });

                        hearts.mouseover(function () { $(this).css("background-position", "0% 120%"); });
                        hearts.mouseout(function () { $(this).css("background-position", "0 0"); });
                        addbtn.click(function (e) {
                            $("#pid").val(pid);
                            $("#pname").val(name);
                            $("#pprice").val(price);
                            //item.effect("shake", {}, 500, function () { });
                            $("#submitBtn").trigger("click");
                            //$("#cartsummary").effect("highlight", {}, 500, function () { });
                            e.stopPropagation();
                        });
                        item.click(function () {


                            $("#detail").find(".dname").eq(0).text(name);
                            $("#detail").find(".dprice").eq(0).text(price);
                            $("#detail").find(".did").eq(0).text(pid);
                            $("#detail").find(".dimg").eq(0).attr("src", src);


                            //$(".lbimage").attr("src", src);
                            //alert($(".rltbActiveImage").attr("src"));
                            $("#pid").val(pid);
                            $("#updateBoxBtn").trigger("click");
                            //$("#detail").css("display", "");
                            //$("#detail").toggle("slide");
                        });

                        $('#waterfall').waterfall('append', item, function () {

                        });
                        $(window).trigger("resize");
                        //wf._resize();
                        aa.css("display", "none");
                        aa.parent().css("display", "none");
                        refreshFaceBook();
                    }
                    img.src = $(this).attr("src");

                });
                //alert(menuProducts.length);
                for (var j = 0; j < menuProducts.length; j++) {
                    var catrow = $("<tr style='height:32px;border:0px solid white'><td colspan='3' align='center' style='font-weight:bold;font-size:20px'>" + menuProducts[j].cat + "</td></tr>");
                    $(".menuviewcontainer").append(catrow);
                    for (var m = 0; m < menuProducts[j].productList.length; m++) {
                        var prow = $("<tr style='height:20px;border:0px solid white;font-weight:bold;font-size:15px;'></tr>");
                        var ntd = $("<td class='proname'>" + menuProducts[j].productList[m].productName + "</td>");
                        var ptd = $("<td class='proprice'>" + menuProducts[j].productList[m].productPrice + "</td>");
                        var pidtd = $("<td class='proid' style='display:none'>" + menuProducts[j].productList[m].id + "</td>");
                        var atd = $('<td><img src="images/plus-outline.png" style="width:32px;height:32px;"/></td>');
                        var ctd = $('<td><img class="checkstatus" src="images/check.png" style="width:32px;height:32px;"/></td>');
                        prow.append(ntd);
                        prow.append($("<td style='width:50px'></td>"));
                        prow.append(ptd);
                        prow.append($("<td style='width:20px'></td>"));
                        prow.append(atd);
                        prow.append(ctd);
                        prow.append(pidtd);

                        atd.click(function (e) {
                            var protr = $(this).closest("tr");
                            var proname = $.trim($(this).closest("tr").find(".proname").text());
                            var proprice = $.trim($(this).closest("tr").find(".proprice").text());
                            var proid = $.trim($(this).closest("tr").find(".proid").text());
                            //alert(proid + "ok" + proname + proprice);
                            $("#pid").val(proid);
                            $("#pname").val(proname);
                            $("#pprice").val(proprice);
                            $(this).closest("tr").effect("highlight", {}, 500, function () { });
                            $("#submitBtn").trigger("click");
                            //$("#cartsummary").effect("highlight", {}, 500, function () { });
                            e.stopPropagation();
                        });

                        var descriptionrow = $("<tr style='height:12px;border:0px solid white;font-size:10px;font-style: italic;'></tr>");
                        var descriptiontd = $("<td colspan='5' style='height:12px;margin-bottom:10px;'>" + menuProducts[j].productList[m].description + "</td>");
                        descriptionrow.append(descriptiontd);
                        //                             prow.draggable({
                        //                                 helper: function (event) {
                        //                                     return $('<div class="drag-cart-item"><table></table></div>').find('table').append($(event.target).closest('tr').clone()).end().insertAfter($(".menuviewcontainer"));
                        //                                 },
                        //                                 cursorAt: {
                        //                                     left: -5,
                        //                                     bottom: 5
                        //                                 },
                        //                                 cursor: 'move',
                        //                                 distance: 10,
                        //                                 delay: 100,
                        //                                 scope: 'cart-item',
                        //                                 revert: 'invalid'
                        //                             });
                        $(".menuviewcontainer").append(prow);
                        $(".menuviewcontainer").append(descriptionrow);
                    }
                }

                var i = 0;
                if ($(".fooditem").length == 0) {
                    $(".arrow-left").css("display", "none");
                    $(".arrow-right").css("display", "none");
                } else {
                    $(".arrow-left").css("display", "");
                    $(".arrow-right").css("display", "");
                }
                $(".fooditem").each(function () {
                    var src = $(this).find(".foodimage").eq(0).attr("src");
                    $(this).find('.foodcomment').eq(0).html(' <fb:comments href="' + src.replace("localhost", "127.0.0.1") + '" width="250" numposts="3" colorscheme="light"></fb:comments>');
                    $(this).find('.foodcomment').eq(0).attr("id", "ftm" + i.toString());
                    if (typeof FB !== 'undefined') {
                        FB.XFBML.parse(document.getElementById("ftm" + i.toString()));
                    }

                    $(this).find('.foodlike').eq(0).attr("id", "ftl" + i.toString());
                    $(this).find('.foodlike').eq(0).html('<fb:like href="' + src.replace("localhost", "127.0.0.1") + '" layout="button_count" show_faces="false" width="70" action="like" font="segoe ui" colorscheme="light" />');
                    if (typeof FB !== 'undefined') {
                        FB.XFBML.parse(document.getElementById("ftl" + i.toString()));
                    }
                    i++;
                });

                $(".app-discount").each(function () {
                    var discountvalue = $.trim($(this).find(".discountvalue").eq(0).text());
                    if (discountvalue == "100") {
                        $(this).css("display", "none");
                    } else {
                        $(this).css("display", "");
                    }
                });
            }

         
        </script>
        <input type="hidden" runat="server" id="pid" clientidmode="Static" />
        <input type="hidden" runat="server" id="pname" clientidmode="Static" />
        <input type="hidden" runat="server" id="pprice" clientidmode="Static" />
        <input type="hidden" runat="server" id="pwidth" clientidmode="Static" />
        <input type="hidden" runat="server" id="currentview" clientidmode="Static" value="listview" />
        <asp:Label ID="lblNo" runat="server" Visible="false"></asp:Label>
    </div>
    <telerik:RadWindowManager runat="server" ID="windowManager" Skin="Metro" KeepInScreenBounds="true"
        AutoSize="true">
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    </form>
</body>
</html>
