﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Telerik.Web.UI;
using System.IO;

namespace POS.Handlers
{
    /// <summary>
    /// Summary description for UploadHandler
    /// </summary>
    public class UploadHandler : AsyncUploadHandler, System.Web.SessionState.IRequiresSessionState
    {

        protected override IAsyncUploadResult Process(UploadedFile file, HttpContext context, IAsyncUploadConfiguration configuration, string tempFileName)
        {
            // Call the base Process method to save the file to the temporary folder
            // base.Process(file, context, configuration, tempFileName);

            // Populate the default (base) result into an object of type SampleAsyncUploadResult
            POSAsyncUploadResult result = CreateDefaultUploadResult<POSAsyncUploadResult>(file);
            string guid = Guid.NewGuid().ToString();
            string filename=guid+file.FileName;
            string uploadFolder=context.Server.MapPath("~/Upload");
            // Populate any additional fields into the upload result.
            // The upload result is available both on the client and on the server
            file.SaveAs(Path.Combine(uploadFolder, filename), true);
            result.ImageName = filename;
            return result;
        }

        
    }

    public class POSAsyncUploadResult : AsyncUploadResult
    {
        private string imageName;
        public string ImageName
        {
            get
            {
                return imageName;
            }
            set
            {
                imageName = value;
            }
        }
    }
}