﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;
using POSService;

namespace POS
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                string username = txtUsername.Text.Trim();
                string password = txtPassword.Text.Trim();
                POSService.POSService pos = new POSService.POSService();
                LoginResult lr = pos.Login(username, password);
                //true if login successfully
                if (lr.Result)
                {

                    GlobalCache.CurrentUser = lr.User;
                    GlobalCache.RefreshAll();
                    if (GlobalCache.Setting != null)
                    {

                        if (!string.IsNullOrEmpty(GlobalCache.Setting.ServerName) &&
                            !string.IsNullOrEmpty(GlobalCache.Setting.DatabaseName) &&
                            !string.IsNullOrEmpty(GlobalCache.Setting.Username) &&
                            !string.IsNullOrEmpty(GlobalCache.Setting.Password))
                        {
                            string conn = "Data Source=" + GlobalCache.Setting.ServerName + ";Initial Catalog=" + GlobalCache.Setting.DatabaseName + ";Integrated Security=false;User Id=" + GlobalCache.Setting.Username + "; Password=" + GlobalCache.Setting.Password;
                            UploadConfig.ConnectionString = conn;
                            UploadConfig.IsOn = true;
                        }
                    }

                    string url = "";
                    switch (lr.User.UserType)
                    {
                        case UserType.SystemAdmin:
                            break;
                        case UserType.StoreAdmin:
                            url = "index.htm?page=pos";
                            break;
                        case UserType.ClientAdmin:
                            break;
                        case UserType.User:
                            break;
                    }
                    if (!string.IsNullOrEmpty(url))
                    {
                        //Response.Redirect(url);
                        ScriptManager.RegisterStartupScript(this, GetType(), "loginsuccess", "window.parent.location.href='"+url+"';", true);
                    }
                }
                else
                {
                    windowManager.RadAlert("Login Failed! Please verify your username and password.", 300, 100, "Login Failed", "");
                }
            }
            catch
            {
                windowManager.RadAlert("Login Failed! Please contact administrator.", 300, 100, "Login Failed", "");
            }
        }
    }
}