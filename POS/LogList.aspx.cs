﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS
{
    public partial class LogList : BasePage
    {
        List<Log> Logs
        {
            get
            {
                return GlobalCache.Logs;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind();
            }
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = Logs.TakeFrom(0, this.AspNetPager1.PageSize);
            this.AspNetPager1.RecordCount = Logs.Count;
            this.AspNetPager1.CurrentPageIndex = 1;
            this.AspNetPager1.GoToPage(this.AspNetPager1.CurrentPageIndex);
            rptList.DataBind();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            rptList.DataSource = null;
            rptList.DataSource = Logs.TakeFrom((currentPage - 1) * pageSize, pageSize);
            rptList.DataBind();
        }

    }
}