﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS
{
    public partial class POSInvoice : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                rptList.DataSource = GetProductsWithDiscount(GlobalCache.Products);
                rptList.DataBind();
                ScriptManager.RegisterStartupScript(this, GetType(), "buildGrid", "buildGrid();", true);
                GlobalCache.RefreshSetting();
                if (GlobalCache.Setting != null)
                {
                    lblGSTSetting.Text = GlobalCache.Setting.GST.ToString();
                }
                else
                {
                    lblGSTSetting.Text = "0";
                }
            }
        }

        public List<Product> GetProductsWithDiscount(List<Product> products)
        {
            if (products == null)
                return null;
            products.ForEach(p => { p.Discount = 100; });
            if (GlobalCache.Promotions != null)
            {
                foreach (Promotion promotion in GlobalCache.Promotions)
                {
                    if (DateTime.Now >= promotion.StartTime && DateTime.Now <= promotion.EndTime)
                    {
                        products.FirstOrDefault(p => p.pro_Id == promotion.ProductID).Discount = promotion.Discount;
                    }
                }
            }
            return products;
        }

        public override Telerik.Web.UI.RadWindowManager WindowManager
        {
            get {
                return null;
            }
        }
    }
}