﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using POS.Data;

namespace POS
{
    public static class ListExtension
    {
        public static List<T> TakeFrom<T>(this List<T> list, int from, int size) 
        {
            if (list == null)
                return list;
            List<T> rl = new List<T>();
            if (from < list.Count)
            {
                int end = from + size > list.Count ? list.Count : from + size;
                for (int i = from; i < end; i++)
                {
                    rl.Add(list[i]);
                }
            }
            return rl; ;
        }

        public static List<Category> GetHiarachy(this List<Category> cats)
        {
            List<Category> copies = new List<Category>();
            cats.OrderByDescending(cc=>cc.cat_Id).ToList().ForEach(c => { copies.Add(new Category() { cat_Id=c.cat_Id,cat_Name=c.cat_Name,ParentID=c.ParentID,ServerID=c.ServerID,Parent=c.Parent,IsDeleted=c.IsDeleted,LastUpdatedTime=c.LastUpdatedTime,ori_Name=c.ori_Name}); });
            
            List<Category> list = new List<Category>();
            foreach (Category cat in copies.Where(c => c.ParentID == 0))
            {
                cat.ori_Name = cat.cat_Name;
                list.Add(cat);
                AddSubCat(cat, list, copies, "├─");
            }
            return list;
        }

        public static void AddSubCat(Category parent, List<Category> hlist, List<Category> cats,string verb)
        {
            foreach (Category cat in cats.Where(c => c.ParentID == parent.cat_Id))
            {
                cat.ori_Name = cat.cat_Name;
                cat.cat_Name = verb + cat.cat_Name;
                hlist.Add(cat);
                AddSubCat(cat, hlist, cats, verb + "──");
            }
        }
    }

  
}