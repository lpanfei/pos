﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Text;
using POS.Data;
using Telerik.Web.UI;

namespace POS
{
    public class BasePage : Page
    {
        public int UserStoreID
        {
            get
            {
                return GlobalCache.CurrentUser == null ? 0 : GlobalCache.CurrentUser.usr_StoreID;
            }
        }

        public int UserClientID
        {
            get
            {
                return GlobalCache.CurrentUser == null ? 0 : GlobalCache.CurrentUser.usr_ClientID;
            }
        }

        public BasePage()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        protected override void OnPreLoad(EventArgs e)
        {
            base.OnPreLoad(e);
            if (GlobalCache.CurrentUser == null)
            {
                //Response.Redirect("Default.aspx");
                List<Client> clients=new POSService.POSService().GetClients();
                if (clients.Any())
                {
                    List<Store> stores = new POSService.POSService().GetStores(clients[0].ID);
                    if (stores.Any())
                    {
                        List<User> users = new POSService.POSService().GetUsers(clients[0].ID, stores[0].sto_Id);
                        if (users.Any(u => u.usr_UserTypeID == 3))
                        {
                            GlobalCache.CurrentUser = users.FirstOrDefault(u => u.usr_UserTypeID == 3);
                            GlobalCache.CurrentUser.UserType = UserType.StoreAdmin;
                            GlobalCache.RefreshAll();
                        }
                    }
                }
            }

            if (GlobalCache.CurrentUser == null)
            {
                Response.Redirect("Default.aspx");
            }
        }

        public void Logout()
        {
            GlobalCache.CurrentUser = null;
            Response.Redirect("Default.aspx");
        }



        public void ShowSuccess(string message)
        {
            (this.Master as MasterPage).WM.RadAlert(message,300,100,"SUCCESS","","Images/Success.png");
        }

        public void ShowError(string message)
        {
            (this.Master as MasterPage).WM.RadAlert(message, 300, 100, "ERROR", "");
        }

        public void ShowConfirm(string message,string handler)
        {
            (this.Master as MasterPage).WM.RadConfirm(message, handler, 300, 100, null, "PLEASE CONFIRM", "Images/Confirm.png");
        }
    }

    public abstract class ControlBase: System.Web.UI.UserControl
    {
        public abstract void Init();
        public virtual void UnInit()
        {
        }
        public int UserStoreID
        {
            get
            {
                return GlobalCache.CurrentUser == null ? 0 : GlobalCache.CurrentUser.usr_StoreID;
            }
        }

        public int UserClientID
        {
            get
            {
                return GlobalCache.CurrentUser == null ? 0 : GlobalCache.CurrentUser.usr_ClientID;
            }
        }

        public ControlBase()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public void ShowSuccess(string message)
        {
            (this.Page as PageBase).ShowSuccess(message);
        }

        public void ShowSuccess(string message,string handler)
        {
            (this.Page as PageBase).ShowSuccess(message,handler);
        }

        public void ShowError(string message)
        {
            (this.Page as PageBase).ShowError(message);
        }

        public void ShowConfirm(string message,string handler)
        {
            (this.Page as PageBase).ShowConfirm(message, handler);
        }
    }

    public abstract class PageBase : Page
    {
        public abstract RadWindowManager WindowManager { get; }
  
        public int UserStoreID
        {
            get
            {
                return GlobalCache.CurrentUser == null ? 0 : GlobalCache.CurrentUser.usr_StoreID;
            }
        }

        public int UserClientID
        {
            get
            {
                return GlobalCache.CurrentUser == null ? 0 : GlobalCache.CurrentUser.usr_ClientID;
            }
        }

        public PageBase()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        protected override void OnPreLoad(EventArgs e)
        {
            base.OnPreLoad(e);
            if (GlobalCache.CurrentUser == null)
            {
                //Response.Redirect("Default.aspx");
                List<Client> clients = new POSService.POSService().GetClients();
                if (clients.Any())
                {
                    List<Store> stores = new POSService.POSService().GetStores(clients[0].ID);
                    if (stores.Any())
                    {
                        List<User> users = new POSService.POSService().GetUsers(clients[0].ID, stores[0].sto_Id);
                        if (users.Any(u => u.usr_UserTypeID == 3))
                        {
                            GlobalCache.CurrentUser = users.FirstOrDefault(u => u.usr_UserTypeID == 3);
                            GlobalCache.CurrentUser.UserType = UserType.StoreAdmin;
                            GlobalCache.RefreshAll();
                        }
                    }
                }
            }

            if (GlobalCache.CurrentUser == null)
            {
                Response.Redirect("Default.aspx");
            }
        }

        public void Logout()
        {
            GlobalCache.CurrentUser = null;
            Response.Redirect("Default.aspx");
        }


        public void ShowSuccess(string message,string handler)
        {
            this.WindowManager.RadAlert(message, 300, 100, "SUCCESS", handler, "Images/Success.png");
        }

        public void ShowSuccess(string message)
        {
            this.WindowManager.RadAlert(message, 300, 100, "SUCCESS", "", "Images/Success.png");
        }

        public void ShowError(string message)
        {
            this.WindowManager.RadAlert(message, 300, 100, "ERROR", "");
        }

        public void ShowConfirm(string message, string handler)
        {
            this.WindowManager.RadConfirm(message, handler, 300, 100, null, "PLEASE CONFIRM", "Images/Confirm.png");
        }
    }
}