﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Net;

namespace POS.Helper
{
    public class BitlyHelper
    {
        public static string Shorten(string url)
        {
            if(string.IsNullOrEmpty(url))
            {
                return null;
            }

            url=System.Web.HttpUtility.UrlEncode(url);

            WebClient client = new WebClient();
            Bitly bitly = JsonHelper.Deserialise<Bitly>(client.DownloadData(System.Web.Configuration.WebConfigurationManager.AppSettings["bitlyurl"] + "&longUrl=" + url));

            if (bitly != null && bitly.status_code == "200" && bitly.data != null)
            {
                return bitly.data.url;
            }

            return null;
        }
    }

    public class JsonHelper
    {
        public static T Deserialise<T>(byte[] bytes)
        {
            T obj = Activator.CreateInstance<T>();
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
                obj = (T)serializer.ReadObject(ms); // <== Your missing line
                return obj;
            }
        }
    }

    [DataContract]
    public class Bitly
    {
        [DataMember]
        public string status_code { get; set; }

        [DataMember]
        public string status_txt { get; set; }

        [DataMember]
        public BitlyData data { get; set; }
    }

    [DataContract]
    public class BitlyData
    {
        [DataMember]
        public string long_url { get; set; }

        [DataMember]
        public string url { get; set; }

        [DataMember]
        public string hash { get; set; }

        [DataMember]
        public string global_hash { get; set; }

        [DataMember]
        public string new_hash { get; set; }
    }
}