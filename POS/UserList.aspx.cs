﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace POS
{
    public partial class UserList : BasePage
    {
        private List<POS.Data.User> Users
        {
            get
            {

                return GlobalCache.Users;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeDropdowns();
                Bind();
            }
        }

        void InitializeDropdowns()
        {
            switch (GlobalCache.CurrentUser.UserType.ToString())
            {
                case "SystemAdmin":
                    userTypedrop.Items.Add(new ListItem() { Text="System", Value="1",Selected=true});
                    userTypedrop.Items.Add(new ListItem() { Text = "Client", Value = "2"});
                    break;
                case "ClientAdmin":
                    userTypedrop.Items.Add(new ListItem() { Text = "Client", Value = "2", Selected = true });
                    userTypedrop.Items.Add(new ListItem() { Text = "Store", Value = "3"});
                    break;
                case "StoreAdmin":
                    userTypedrop.Items.Add(new ListItem() { Text = "Store", Value = "3", Selected = true });
                    break;
            }

            clientDropdown.DataSource = GlobalCache.Clients;
            clientDropdown.DataBind();
            if (GlobalCache.Clients.Any())
            {
                clientDropdown.SelectedIndex = 0;
            }
            storeDropdown.DataSource = GlobalCache.Stores;
            storeDropdown.DataBind();
            if (GlobalCache.Stores.Any())
            {
                storeDropdown.SelectedIndex = 0;
            }
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemEdit")
            {
                formbuttons.Visible = true;
                listbuttons.Visible = false;
                formpanel.Visible = true;
                listpanel.Visible = false;
                ltlPageTitle.Text = "User / Edit";
                POS.Data.User user = new POSService.POSService().GetUser(Convert.ToInt32(e.CommandArgument.ToString()));
                txtAddress.Text = user.usr_Address;
                txtEmail.Text = user.usr_Email;
                txtFirstName.Text = user.usr_FirstName;
                txtLastName.Text = user.usr_LastName;
                txtMoblePhone.Text = user.usr_MoblePhone;
                txtName.Text = user.usr_Name;
                txtNote.Text = user.usr_Note;
                //txtPassword.Text = user.usr_Password;
                txtPhone.Text = user.usr_Phone;
                txtPostcode.Text = user.usr_Postcode;
                txtState.Text = user.usr_State;
                txtSuburb.Text = user.usr_Suburb;
                radlPermission.SelectedValue = user.usr_Permission;
                userTypedrop.SelectedValue = user.usr_UserTypeID.ToString();
                lblId.Text = user.usr_ID.ToString();
            }
            else if (e.CommandName == "ItemDel")
            {
                delid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "confirmclosed");
            }
        }

        protected void Delete(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delid.Attributes["value"]);
            new POSService.POSService().DeleteUserById(id);
            GlobalCache.RefreshUsers();
            Bind();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            rptList.DataSource = null;
            rptList.DataSource = Users.TakeFrom((currentPage - 1) * pageSize, pageSize);
            rptList.DataBind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int storeid = 0;
            int clientid = 0;
            switch (userTypedrop.SelectedValue)
            {
                case "1":
                    break;
                case "2":
                    clientid = Convert.ToInt32(clientDropdown.SelectedValue);
                    break;
                case "3":
                    clientid = this.UserClientID;
                    storeid = Convert.ToInt32(storeDropdown.SelectedValue);
                    break;
            }
            new POS.Data.User()
            {
                usr_FirstName=txtFirstName.Text,
                usr_ID=Convert.ToInt32(lblId.Text.Trim()),
                usr_Address=txtAddress.Text,
                usr_ClientID=clientid,
                usr_Email=txtEmail.Text,
                usr_LastName=txtLastName.Text,
                usr_MoblePhone=txtMoblePhone.Text,
                usr_Name=txtName.Text,
                usr_Note=txtNote.Text,
                usr_Password=txtPassword.Text,
                usr_Permission=radlPermission.SelectedValue,
                usr_Phone=txtPhone.Text,
                usr_Postcode=txtPostcode.Text,
                usr_State=txtState.Text,
                usr_StoreID=storeid,
                usr_Suburb=txtSuburb.Text,
                usr_UserTypeID=Convert.ToInt32(userTypedrop.SelectedValue)
            }.Update();
            BackToList(null, null);
        }

        protected void AddUser(object sender, EventArgs e)
        {
            formbuttons.Visible = true;
            listbuttons.Visible = false;
            formpanel.Visible = true;
            listpanel.Visible = false;
            ltlPageTitle.Text = "User / Add";
            txtAddress.Text = "";
            txtEmail.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtMoblePhone.Text = "";
            txtName.Text = "";
            txtNote.Text = "";
            txtPassword.Text = "";
            txtPhone.Text = "";
            txtPostcode.Text = "";
            txtState.Text = "";
            txtSuburb.Text = "";
            radlPermission.SelectedValue = "0";
            lblId.Text = "0";
            
        }

        protected void BackToList(object sender, EventArgs e)
        {
            listbuttons.Visible = true;
            formbuttons.Visible = false;
            formpanel.Visible = false;
            listpanel.Visible = true;
            ltlPageTitle.Text = "User List";
            GlobalCache.RefreshUsers();
            Bind();
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = Users.TakeFrom(0, this.AspNetPager1.PageSize); ;
            this.AspNetPager1.RecordCount = Users.Count;
            this.AspNetPager1.CurrentPageIndex = 1;
            this.AspNetPager1.GoToPage(this.AspNetPager1.CurrentPageIndex);
            rptList.DataBind();
        }
    }
}