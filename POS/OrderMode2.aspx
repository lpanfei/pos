﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderMode2.aspx.cs" Inherits="POS.OrderMode2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Welcome to DST Solutions POS System</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="login/css/style.css" />
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script src="Scripts/idangerous.swiper-2.4.1.js"></script>
    <link rel="stylesheet" href="css/idangerous.swiper.css">
    <link rel="stylesheet" type="text/css" href="window/css/component.css" />
    <script src="login/js/modernizr.custom.js"></script>
    <script type="text/javascript" charset="utf-8">
    $   (window).resize(function () {
            $("#loadinggif").css("left", ($(window).width() - 400) / 2);
            $("#loadinggif").css("top", ($(window).height() - 530) / 2);
        });
        $(function () {
            $("#loadinggif").css("left", ($(window).width() - 400) / 2);
            $("#loadinggif").css("top", ($(window).height() - 530) / 2);
            $("#iframe").load(function () {
                $("#loadinggif").toggle("fade");
                $(this).toggle("fade");
            });

            $("#iframe").attr("src", "Ordering2.aspx");

            $('.proitem-container').swiper({

                offsetPxBefore: 0,
                offsetPxAfter: 0,
                calculateHeight: true,
                autoplay: 1000,
                loop: true,
                autoplayDisableOnInteraction: false
            });
        });
    </script>
    <style>
        body
        {
            overflow: hidden;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style="width: 400px; height: 530px; position: absolute" id="loadinggif">
            <div class="swiper-container  thumbs-cotnainer proitem-container">
                <div class="swiper-wrapper">
                    <asp:Repeater ID="Repeater3" runat="server">
                        <ItemTemplate>
                            <div class="swiper-slide" style="cursor: pointer">
                                <div>
                                    <table style="border-spacing: 0px;">
                                        <tr>
                                            <td valign="middle" align="center">
                                                <div style="width:400px;height:400px;overflow:hidden">
                                                    <img style="cursor: pointer;min-width:400px;" src="<%# HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + Eval("ImageUrl")) %>" />
                                                </div>
                                                <div style="padding: 10px; font-size: 17px;">
                                                        <%#Eval("pro_Name") %></div>
                                                <div style="color: Red">(<span><%#Eval("Discount") %></span>% Discount)
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div style="width: 400px;height: 25px;box-shadow: 0 0 7px 3px #25a0da;-moz-box-shadow: 0 0 7px 3px #25a0da;-webkit-box-shadow: 0 0 7px 3px #25a0da;border-radius: 25px;-moz-border-radius: 25px;-webkit-border-radius: 25px; margin-top:35px;">
            <div style="font-size:15px;color:#25a0da;margin:0 auto;width:50px;padding-top:3px;">Loading...</div>
            </div>
            <%--<img src="Images/busy.gif" id="Img1" style="display:block;margin:0 auto;margin-top:20px;" />--%>
        </div>
        <iframe src="" id="iframe" style="border: 0; position: absolute; top: 0; left: 0;
            right: 0; bottom: 0; width: 100%; height: 100%; display: none;"></iframe>
    </div>
    </form>
</body>
</html>
