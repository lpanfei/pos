﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCTask.ascx.cs" Inherits="POS.TouchControls.WUCTask" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<style>
    .refreshtaskbtn
    {
        display:none;
    }
    .high-order
    {
        background-color:Red;
    }
    .medium-order
    {
        background-color:#25a0da;
    }
    .normal-order
    {
        background-color:green;
    }
</style>
<div class="mainheader">
    <div class="contextual">
    </div>
    <div class="subheader">
        <h2 class="icon-categories pagetitle">
            <asp:Literal ID="ltlPageTitle" Text="Tasks" runat="server"></asp:Literal></h2>
    </div>
</div>
<div>
    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                width: 1024px">
                <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                    <table style="width: 100%">
                        <tr>
                            <td style="width: 32px">
                                <img src="images/icon_list.png" style="cursor: pointer" class="shownav" />
                            </td>
                            <td align="center">
                            <table>
                            <tr>
                            <td align="center"><span style="color: White; font-size: 13px; vertical-align: top">Task Mode</span></td>
                            </tr>
                            <tr>
                            <td align="center"><span  style="color: White; font-size: 10px; vertical-align: top"><asp:Literal ID="nextrefreshtime" runat="server"></asp:Literal></span></td>
                            </tr>
                            </table>
                                
                            </td>
                            <td style="width: 32px" valign="middle">
                                <img src="images/sync.png" style="width: 20px; height: 20px; cursor: pointer" class="refreshinvoice" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width: 100%; height: 725px; overflow-y: auto;margin-top:2px;">
                    <table style="width: 1024px; margin: 0 auto; height: 725px; border-spacing: 0px;">
                        <tr>
                            <td style="width: 342px">
                                <div style="width: 340px; height: 725px">
                                    <div>
                                    <table  style="width: 340px; height: 70px;font-size:20px;color:white">
                                    <tr>
                                    <td align="center" valign="middle">
                                    <asp:Literal runat="server" ID="TableNo1"></asp:Literal><bt />
                                    
                                    <input type="hidden" runat="server" id="table1status" clientidmode="Static" value="" class="taskmodeorderstatus"  />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td  align="center" valign="middle" style="font-size:12px">
                                    <asp:Literal runat="server" ID="table1date"></asp:Literal>
                                    </td>
                                    </tr>
                                    </table>
                                    </div>
                                    <div style="width: 338px; height: 650px; border: 1px solid #e4e4e4;overflow-y:auto;overflow-x:hidden">
                                        <asp:Repeater ID="rptList" runat="server" OnItemCommand="CompleteInvoiceProduct">
                                            <HeaderTemplate>
                                                <table class="list taskmodelist" style="width: 338px; margin: 0px auto;">
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td align="left" style="padding-left:10px;width: 258px;">
                                                        <div style=" font-size: 12px;">
                                                            <%# Eval("ip_Description")%>
                                                            <input class="ip_status" type="hidden" value='<%# Eval("IsComplete")%>' />
                                                        </div>
                                                    </td>
                                                    <td style="padding-left:10px;width:60px;" valign="middle" align="center">
                                                        <img class="completedstatus" src="images/check.png" style="width: 32px; height: 32px; display: block;" alt="completed">
                                                        <asp:LinkButton ID="lblEdit" runat="server" Text="Complete" CommandName="Complete"
                                                            CommandArgument='<%# Eval("ip_ID") %>' rel="nofollow" CssClass="incompletestatus"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody> </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </td>
                            <td style="width: 340px">
                                <div style="width: 338px; height: 725px">
                                    <div>
                                    <table  style="width: 338px; height: 70px; font-size:20px;color:white">
                                    <tr>
                                    <td align="center"><asp:Literal runat="server" ID="Literal1"></asp:Literal>
                                    <input type="hidden" runat="server" id="table2status" class="taskmodeorderstatus" clientidmode="Static" value="" />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td  align="center" valign="middle" style="font-size:12px">
                                    <asp:Literal runat="server" ID="table2date"></asp:Literal>
                                    </td>
                                    </tr>
                                    </table>
                                    </div>
                                    <div style="width: 336px; height: 650px; border: 1px solid #e4e4e4;overflow-y:auto;overflow-x:hidden">
                                      <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="CompleteInvoiceProduct">
                                            <HeaderTemplate>
                                                <table class="list taskmodelist" style="width: 336px; margin: 0px auto;">
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td align="left"  style="padding-left:10px;width: 256px;">
                                                        <div style=" font-size: 12px;">
                                                            <%# Eval("ip_Description")%>
                                                             <input class="ip_status" type="hidden" value='<%# Eval("IsComplete")%>' />
                                                        </div>
                                                    </td>
                                                    <td style="padding-left:10px;width:60px;" valign="middle" align="center">
                                                         <img class="completedstatus" src="images/check.png" style="width: 32px; height: 32px; display: block;" alt="completed">
                                                        <asp:LinkButton ID="lblEdit" runat="server" Text="Complete" CommandName="Complete"
                                                            CommandArgument='<%# Eval("ip_ProductID") %>' rel="nofollow" CssClass="incompletestatus"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody> </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </td>
                            <td style="width: 342px">
                                <div style="width: 340px; height: 725px">
                                    <div>
                                    <table  style="width: 340px; height: 70px; font-size:20px;color:white">
                                    <tr>
                                    <td align="center"><asp:Literal runat="server" ID="Literal2"></asp:Literal>
                                    <input type="hidden" runat="server" id="table3status" clientidmode="Static" value=""  class="taskmodeorderstatus" />
                                    </td>
                                    </tr>
                                    <tr>
                                    <td  align="center" valign="middle" style="font-size:12px">
                                    <asp:Literal runat="server" ID="table3date"></asp:Literal>
                                    </td>
                                    </tr>
                                    </table>
                                    </div>
                                    <div style="width: 338px; height: 650px; border: 1px solid #e4e4e4;overflow-y:auto;overflow-x:hidden">
                                       <asp:Repeater ID="Repeater2" runat="server" OnItemCommand="CompleteInvoiceProduct">
                                            <HeaderTemplate>
                                                <table class="list taskmodelist" style="width: 338px; margin: 0px auto;">
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td align="left"  style="padding-left:10px;width: 258px;">
                                                        <div style="font-size: 12px;">
                                                            <%# Eval("ip_Description")%>
                                                            <input class="ip_status" type="hidden" value='<%# Eval("IsComplete")%>' />
                                                        </div>
                                                    </td>
                                                    <td style="padding-left:10px;width:60px;" valign="middle" align="center">
                                                         <img class="completedstatus" src="images/check.png" style="width: 32px; height: 32px; display: block;" alt="completed">
                                                        <asp:LinkButton ID="lblEdit" runat="server" Text="Complete" CommandName="Complete"
                                                            CommandArgument='<%# Eval("ip_ProductID") %>' rel="nofollow" CssClass="incompletestatus"></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody> </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <asp:Timer ID="TaskTimer" runat="server" Interval="60000" OnTick="RefreshTask"></asp:Timer>
            <input type="hidden" id="timerclientid" runat="server" clientidmode="Static" value="" />
            <asp:Button ID="refreshTaskBtn" runat="server" Width="0" Height="0" CssClass="refreshtaskbtn"
            OnClick="RefreshTask"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">

    Sys.Application.add_load(taskLoad);
    function taskLoad() {
        $(".taskmodelist>tbody>tr").each(function () {
            if ($(this).find(".ip_status").val().toLowerCase() == "true") {
                $(this).find(".completedstatus").css("display", "block");
                $(this).find(".incompletestatus").css("display", "none");
            } else {
                $(this).find(".completedstatus").css("display", "none");
                $(this).find(".incompletestatus").css("display", "inline");
            }
        });

        $(".taskmodeorderstatus").each(function () {
            $(this).closest("table").addClass($(this).val());
        });

        $(".refreshinvoice").unbind("click");
        $(".refreshinvoice").click(function () {
            $(".refreshtaskbtn").trigger("click");
        });
    }
    </script>
</div>
