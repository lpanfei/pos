﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCProduct.ascx.cs"
    Inherits="POS.TouchControls.WUCProduct" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    var previousproduct = null;
    Sys.Application.add_load(productLoad);
    function productLoad(sender, args) {
        $("#addfiled").unbind("click");
        $("#addfiled").click(function () { $("#btnAddField").trigger("click"); });

        $(".productlist tr").unbind("click");
        $(".productlist tr").click(function () {

            $("#selectedpid").val($(this).find(".proid").eq(0).val());

            $("#btnProductSelected").trigger("click");
        }
        );

        $(".productlist tr").each(function () {
            if ($(this).find(".proid").eq(0).val() == $("#selectedpid").val()) {
                $(this).css("background-color", "#EFF3F4");
            }
        });

        $("#addpicture").unbind("click");
        $("#addpicture").click(function () { $('.ruFileInput').trigger('click'); });

        $("#btnDeleteThis").unbind("click");
        $("#btnDeleteThis").click(function () {
            $("#delproductid").val($("#selectedpid").val());
            radconfirm("Are you sure to delete the item?", deleteProduct, 300, 100, null, "Delete Product", "Images/Confirm.png");
        });

        $(".addproducticon").unbind("click");
        $(".addproducticon").click(function () {
            $("#addLink").trigger("click");
            $("#btnDeleteThis").css("display", "none");
        });

        for (var ic = 0; ic < $(".yixiaimage").length; ic++) {
            var yxi = $(".yixiaimage").eq(ic);
            yxi.attr("data-index", ic);
            yxi.unbind("click");
            yxi.click(function () {
                OpenProductImageRadLigthBox($(this).attr("data-index"));
            });
        }
    }
    function deleteProduct(sender) {
        if (sender) {
            $("#delProductBtn").trigger("click");
        } else {
            $("#delproductid").val("-1");
        }
    }

    function OpenProductImageRadLigthBox(index) {
        var lightBox = $find('<%= productPictureLightbox.ClientID %>');
        lightBox.set_currentItemIndex(parseInt(index));
        lightBox.show();
    }

    function OnClientValidationFailed(sender, args) {
        var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
        if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
            if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                radalert("Wrong Extension! You can only upload .jpg/.jpeg/.gif/.png/.bmp file of type.", 400, 100, "ERROR");
            }
            else {
                radalert("Wrong file size! You can only update picture less than 100kb", 400, 100, "ERROR");
            }
        }
        else {
            radalert("not correct extension!", 300, 100, "ERROR");
        }
        var index = $(args.get_row()).index();
        sender.deleteFileInputAt(index);
    }

    function OnClientFileUploadFailed(sender, args) {
        if (args.get_message() == "error") {
            args.set_handled(true);
            radalert("Upload Failed! " + args.get_message(), 400, 200, "ERROR");
            var index = $(args.get_row()).index();
            sender.deleteFileInputAt(index);
        }
    }

    function FileUploaded(sender, args) {
        $("#refreshImageBtn").trigger("click");
    }

</script>
<style type="text/css">
    .displaynone
    {
        display: none;
    }
    
    input[type=button]
    {
        cursor: pointer;
    }
    
    input[type=file]
    {
        cursor: pointer;
    }
    #ftable .tr-header
    {
        font-size: 13px;
        font-weight: bold;
    }
    
    .padding5
    {
        padding-top: 5px;
    }
    
    
    #ftable .tr-header td
    {
        padding-top: 25px;
        padding-bottom: 10px;
    }
    #ftable
    {
        border-collapse: collapse;
        margin: 0 auto;
    }
    .pageform input[type=text]
    {
        height: 30px;
        width: 280px;
        padding-left: 10px;
    }
    
    .pageform
    {
        margin: 0 auto;
    }
    
    .btn-add
    {
        cursor: pointer;
    }
    
    
    
    .RadUpload .ruBrowse
    {
        color: white;
        background-color: #25a0da;
        background-image: -webkit-linear-gradient( top, #3894E7 0%, #25a0da 100% );
        background-image: -moz-linear-gradient( top, #3894E7 0%, #25a0da 100% );
        background-image: -ms-linear-gradient( top, #3894E7 0%, #25a0da 100% );
        background-image: -o-linear-gradient( top, #3894E7 0%, #25a0da 100% );
        background-image: linear-gradient( top, #3894E7 0%, #25a0da 100% );
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3894E7', endColorstr='#25a0da',GradientType=0 );
    }
    
    .ruButtonHover
    {
        color: white;
    }
    
    .ruRemove
    {
        margin-top: 10px;
    }
    
    .cursorpointer
    {
        cursor: pointer;
    }
    
    .yixiaimage
    {
        cursor: pointer;
    }
    
    table.list
    {
        border: 0px solid #e4e4e4;
    }
</style>
<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="mainheader">
            <div class="contextual">
                <div style="height: 10px; width: 85px">
                    <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="addLink"
                        Text="Add" OnClick="AddProduct" ClientIDMode="Static" />
                </div>
            </div>
            <div class="subheader">
                <h3 class="icon-products pagetitle" id="pagetitle">
                    <asp:Literal ID="ltlPageTitle" Text="Products" runat="server"></asp:Literal>
                </h3>
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <table style="border-spacing: 0px">
                    <tr>
                        <td>
                            <asp:Panel CssClass="pagelist" runat="server" ID="pagelist">
                                <div style="display: none">
                                    <div style="float: left">
                                        <asp:DropDownList ID="dropCategory" runat="server" Height="30" DataTextField="cat_Name"
                                            DataValueField="cat_Id">
                                        </asp:DropDownList>
                                    </div>
                                    <div style="float: left; margin-left: 2px;">
                                        <asp:DropDownList ID="dropField" runat="server" Height="30">
                                            <asp:ListItem Text="Name of Product" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Serial No" Value="2"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                    <div style="float: left; margin-left: 2px; margin-top: -1px; margin-right: 15px">
                                        <asp:TextBox ID="txtKey" runat="server" CssClass="searchbox" Height="28"></asp:TextBox></div>
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" UseSubmitBehavior="false"
                                        class="btn btn-large btn-block btn-info" Width="75" Height="30" Style="margin-left: 15px;" />
                                </div>
                                <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                                    width: 400px">
                                    <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 32px">
                                                    <img src="images/icon_list.png" style="cursor: pointer" class="shownav" />
                                                </td>
                                                <td align="center">
                                                    <span style="color: White; font-size: 15px; vertical-align: middle">Products</span>
                                                </td>
                                                <td style="width: 32px">
                                                    <img src="images/add.png" style="width: 20px; height: 20px; cursor: pointer" class="addproducticon" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="width: 100%; height: 730px; overflow-y: auto">
                                        <asp:Repeater ID="rptList" runat="server">
                                            <HeaderTemplate>
                                                <table class="list productlist" style="width: 400px;">
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <%--<td align="center">
                                <%# Container.ItemIndex+1 %>
                            </td>
                            <td align="center">
                                <%# Eval("cat_Name")%>
                            </td>--%>
                                                    <td align="left">
                                                        <div style="width: 300px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;
                                                            padding-left: 15px; font-size: 12px;">
                                                            <%# Eval("pro_Name") %>
                                                        </div>
                                                    </td>
                                                    <%--<td align="center">
                                <%# Eval("SupplierName") %>
                            </td>
                            <td align="center">
                                <%# Eval("pro_No") %>
                            </td>--%>
                                                    <td align="right">
                                                        <div style="padding-right: 10px">
                                                            <%# Convert.ToDecimal(Eval("pro_Price")).ToString("0.00") %>
                                                            <input type="hidden" value='<%# Eval("pro_Id") %>' class="proid" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody> </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <%--<webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged" PageSize="10" CssClass="aspnetpager">
                </webdiyer:AspNetPager>--%>
                            </asp:Panel>
                        </td>
                        <td>
                            <asp:Panel CssClass="pageform" runat="server" ID="pageform">
                                <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                                    border-left: 0px solid black; width: 620px;">
                                    <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                        <table style="float: right">
                                            <tr>
                                                <%--<td  style="padding:0px">
                                                            <asp:Button ID="addfiled" runat="server" Text="Add New Fields"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center displaynone"
                                        Height="30" Width="120" ClientIDMode="Static" />
                                                            </td>--%>
                                                <td style="padding: 0px; width: 32px; padding-top: 3px;">
                                                    <img id="btnDeleteThis" src="images/trash.png" style="cursor: pointer" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="padding-left: 20px; height: 690px; overflow: auto">
                                        <table id="ftable">
                                            <tr>
                                                <td style="width: 150px;" class="padding5">
                                                    Category:
                                                </td>
                                                <td class="padding5">
                                                    <asp:DropDownList Height="30" ID="dropCategory2" runat="server" Width="290" DataTextField="cat_Name"
                                                        DataValueField="cat_Id">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 150px;" class="padding5">
                                                    Supplier:
                                                </td>
                                                <td class="padding5">
                                                    <asp:DropDownList Height="30" ID="dropSupplier" runat="server" Width="290" DataTextField="Name"
                                                        DataValueField="ID">
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="padding5">
                                                    Name:
                                                </td>
                                                <td class="padding5">
                                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="padding5">
                                                    Serial No:
                                                </td>
                                                <td class="padding5">
                                                    <asp:TextBox ID="txtNo" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="padding5">
                                                    Price:
                                                </td>
                                                <td class="padding5">
                                                    <asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="padding5">
                                                    Cost:
                                                </td>
                                                <td class="padding5">
                                                    <asp:TextBox ID="txtCost" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="padding5">
                                                    Description:
                                                </td>
                                                <td class="padding5">
                                                    <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Rows="3" Width="285"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Repeater ID="rptDynamicFields" runat="server" OnItemCommand="rptDynamicFields_ItemCommand">
                                                        <ItemTemplate>
                                                            <tr style="display: none">
                                                                <td class="padding5">
                                                                    <asp:TextBox ID="lblName" runat="server" Text='<%# Eval("fie_Name") %>' Width="127"></asp:TextBox>:
                                                                </td>
                                                                <td class="padding5">
                                                                    <asp:TextBox ID="txtValue" runat="server" Text='<%# Eval("fie_Value") %>'></asp:TextBox>
                                                                    <asp:LinkButton ID="lbtnDel" runat="server" Text="×" CommandName="ItemDel" CommandArgument='<%# Eval("fie_Id") %>'></asp:LinkButton><br />
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                            <tr style="display: none">
                                                <td class="padding5">
                                                    Related Products:
                                                </td>
                                                <td class="padding5">
                                                    <div style="width: 300px; height: 120px; overflow: auto;">
                                                        <asp:Repeater ID="rptRelateProduct" runat="server">
                                                            <HeaderTemplate>
                                                                <table>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <tr>
                                                                    <td>
                                                                        <table>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                                                                    <asp:Label ID="lblID" runat="server" Text='<%# Eval("pro_ID") %>' Visible="false"></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <div style="width: 200px; overflow: hidden; text-overflow: ellipsis; white-space: nowrap;
                                                                                        padding-left: 15px; font-size: 12px;">
                                                                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("pro_Name") %>'></asp:Label>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                                </table>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                        <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                                                        </asp:CheckBoxList>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr class="tr-header">
                                                <td>
                                                    Pictures
                                                </td>
                                                <td>
                                                    <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="dlPictureList_ItemCommand">
                                                        <HeaderTemplate>
                                                            <div class="RadUpload RadAsyncUpload RadUpload_Metro">
                                                                <ul class="ruInputs">
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <li><span class="ruFileWrap ruStyled" style="vertical-align: middle; display: none">
                                                                <span class="ruUploadProgress ruUploadSuccess"></span></span>
                                                                <asp:Image ID="img" ImageUrl='<%# Eval("Url") %>' CssClass="yixiaimage" runat="server"
                                                                    Height="50" ImageAlign="Middle" />
                                                                <span style="vertical-align: middle">
                                                                    <asp:Button ID="Button1" Text="Remove" CssClass="ruButton ruRemove" runat="server"
                                                                        CommandName="ItemDel" /></span>
                                                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblTemp" runat="server" Text='<%# Eval("IsTemp") %>' Visible="false"></asp:Label>
                                                            </li>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </ul></div></FooterTemplate>
                                                    </asp:Repeater>
                                                    <eid:RadAsyncUpload runat="server" Skin="Metro" ID="fileupload" OnClientValidationFailed="OnClientValidationFailed"
                                                        OnClientFileUploadFailed="OnClientFileUploadFailed" AllowedFileExtensions=".jpeg,.jpg,.png,.gif,.bmp,.JPG,.JPEG,.PNG,.GIF,.BMP"
                                                        TargetFolder="Upload/Temp" MaxFileSize="102400" PostbackTriggers="refreshImageBtn"
                                                        OnClientFileUploaded="FileUploaded" OnFileUploaded="ImageUploaded">
                                                    </eid:RadAsyncUpload>
                                                </td>
                                            </tr>
                                            <tr style="display: none;">
                                                <td colspan="2" align="center">
                                                    <asp:Label ID="lblCategoryID" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblUploadFlag" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblServerID" runat="server" Visible="false"></asp:Label>
                                                    <input type="hidden" id="pid" runat="server" />
                                                </td>
                                            </tr>
                                            <asp:Button ID="btnAddField" runat="server" Text="Update" ClientIDMode="Static" OnClick="AddDynamicFields"
                                                CssClass="displaynone" />
                                        </table>
                                    </div>
                                    <div style="height: 40px; width: 160px; margin: 0 auto;">
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click" UseSubmitBehavior="false"
                                                        class="btn btn-large btn-block btn-info btn-center" Height="30" Width="75" />
                                                </td>
                                                <td style="width: 10px">
                                                </td>
                                                <td>
                                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                                        Height="30" Width="75" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <input type="hidden" id="delproductid" runat="server" clientidmode="Static" value="-1" />
        <asp:Button ID="delProductBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="Delete" ClientIDMode="Static"></asp:Button>
        <asp:Button ID="refreshImageBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            ClientIDMode="Static"></asp:Button>
        <asp:Button ID="btnProductSelected" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="ProductSelected" ClientIDMode="Static"></asp:Button>
        <input type="hidden" id="selectedpid" runat="server" clientidmode="Static" value="0" />
        <div style="clear: both;">
        </div>
        <eid:RadLightBox ID="productPictureLightbox" runat="server" Modal="true" DataImageUrlField="Url"
            DataTitleField="Name">
        </eid:RadLightBox>
    </ContentTemplate>
</asp:UpdatePanel>
