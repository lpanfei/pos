﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCCategory.ascx.cs"
    Inherits="POS.TouchControls.WUCCategory" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    var previouscat = null;
    Sys.Application.add_load(categoryLoad);
    function categoryLoad() {
        $(".categorylist tbody tr").unbind("click");
        $(".categorylist tbody tr").click(function () {

            $("#selectedcatid").val($(this).find(".catcatid").eq(0).val());

            $("#btnCategorySelected").trigger("click");
            }
        );

        $(".categorylist tr").each(function () {
            if ($(this).find(".catcatid").eq(0).val() == $("#selectedcatid").val()) {
                $(this).css("background-color", "#EFF3F4");
            }
        });

        $(".addcategoryicon").unbind("click");
        $(".addcategoryicon").click(function () {
            $("#btnAddCategory").trigger("click");
            $("#btnDeleteCategory").css("display", "none");
        });

        $("#btnDeleteCategory").unbind("click");
        $("#btnDeleteCategory").click(function () {
            $("#delcatid").val($("#selectedcatid").val());
            radconfirm("Are you sure to delete the item?", deleteCategory, 300, 100, null, "Delete Category", "Images/Confirm.png");
        });
    }


    function deleteCategory(sender) {
        if (sender) {
            $("#delCategoryBtn").trigger("click");
        } else {
            $("#delcatid").val("-1");
        }
    }
</script>
<style>
    .toprow
    {
    }
    .subrow
    {
        display: none;
    }
    .parentcat:before
    {
        content: url(images/expand.png);
    }
    
    .parentexpanded:before
    {
        content: url(images/collapse.png);
    }
    .leafcat:before
    {
    }
    .displaynone
    {
        display: none;
    }
    input[type=submit]
    {
        cursor: pointer;
    }
</style>
<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="mainheader">
            <div class="contextual">
                <asp:Panel ID="listbuttons" runat="server">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="btnAddCategory"
                            Text="Add" OnClick="AddCategory" ClientIDMode="Static" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="formbuttons" runat="server" Visible="false">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                            Text="Back" OnClick="BackToList" />
                    </div>
                </asp:Panel>
            </div>
            <div class="subheader">
                <h2 class="icon-categories pagetitle">
                    <asp:Literal ID="ltlPageTitle" Text="Category List" runat="server"></asp:Literal></h2>
            </div>
        </div>
        <table style="border-spacing: 0px">
            <tr>
                <td>
                    <asp:Panel ID="listpanel" runat="server">
                        <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                            width: 400px">
                            <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 32px">
                                            <img src="images/icon_list.png" style="cursor: pointer" class="shownav" />
                                        </td>
                                        <td align="center">
                                            <span style="color: White; font-size: 15px; vertical-align: middle">Categories</span>
                                        </td>
                                        <td style="width: 32px">
                                            <img src="images/add.png" style="width: 20px; height: 20px; cursor: pointer" class="addcategoryicon" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                             <div style="width: 100%; height: 730px; overflow-y: auto">
                            <asp:Repeater ID="rptList" runat="server">
                                <HeaderTemplate>
                                    <table class="list categorylist">
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="left" style="padding-left:15px">
                                            <asp:Label runat="server" ID="lblName" Text='<%# Eval("cat_Name")%>'></asp:Label>
                                            <input type="hidden" value='<%# Eval("cat_Id") %>' class="catcatid" />
                                        </td>
                                        <%--<td class="buttons">
                                            <div style="display: none;">
                                                <input type="hidden" class="parentid" value='<%# Eval("ParentID")%>' />
                                                <input type="hidden" class="classid" value='<%# Eval("cat_Id")%>' />
                                                <input type="hidden" class="catname" value='<%# Eval("ori_Name")%>' />
                                               
                                                <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CommandName="ItemEdit" CommandArgument='<%# Eval("cat_Id") %>'
                                                    class="icon icon-edit"></asp:LinkButton>
                                                &nbsp;&nbsp;
                                                <asp:LinkButton ID="lblDel" runat="server" Text="Delete" CommandName="ItemDel" CommandArgument='<%# Eval("cat_Id") %>'
                                                    class="icon icon-del"></asp:LinkButton>
                                            </div>
                                        </td>
                                        <td style="display: none">
                                            <asp:Label runat="server" ID="lblParentID" Text='<%# Eval("ParentID")%>'></asp:Label>
                                        </td>
                                        <td style="display: none">
                                            <asp:Label runat="server" ID="lblServerID" Text='<%# Eval("ServerID")%>'></asp:Label>
                                        </td>--%>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody> </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            </div>
                        </div>
                    </asp:Panel>
                </td>
                <td>
                    <asp:Panel ID="formpanel" runat="server">
                        <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                            border-left: 0px solid black; width: 620px;">
                            <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                <table style="float: right">
                                    <tr>
                                        <%--<td  style="padding:0px">
                                                            <asp:Button ID="addfiled" runat="server" Text="Add New Fields"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center displaynone"
                                        Height="30" Width="120" ClientIDMode="Static" />
                                                            </td>--%>
                                        <td style="padding: 0px; width: 32px; padding-top: 3px;">
                                            <img id="btnDeleteCategory" src="images/trash.png" style="cursor: pointer" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="padding-left: 20px; height: 690px; overflow: auto">
                                <table class="form-table">
                                    <tr>
                                        <td class="form-label">
                                            Name:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Parent Category:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dropParent" runat="server" DataTextField="cat_Name" DataValueField="cat_Id">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center">
                                            <asp:Label ID="lblUploadFlag" runat="server" Visible="false"></asp:Label>
                                            <input type="hidden" id="catid" runat="server" style="display: none" class="catid" />
                                            <input type="hidden" id="serverid" runat="server" style="display: none" class="serverid" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="height: 40px; width: 160px; margin: 0 auto;">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                                UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                                Height="30" Width="75" />
                                        </td>
                                        <td style="width:10px"></td>
                            <td><asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                        Height="30" Width="75" /></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <input type="hidden" id="delcatid" runat="server" clientidmode="Static" value="-1" />
        <asp:Button ID="delCategoryBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="DeleteCategory" ClientIDMode="Static"></asp:Button>
            <asp:Button ID="btnCategorySelected" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="CategorySelected" ClientIDMode="Static"></asp:Button>
            <input type="hidden" id="selectedcatid" runat="server" clientidmode="Static" value="0" />
    </ContentTemplate>
</asp:UpdatePanel>
