﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCWarranty.ascx.cs"
    Inherits="POS.TouchControls.WUCWarranty" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    var previouswarranty = null;
    Sys.Application.add_load(warrantyLoad);
    function warrantyLoad() {
        $(".warrantylist tr").unbind("click");
        $(".warrantylist tr").click(function () {
            $("#selectedwarrantyid").val($(this).find(".warrantyid").eq(0).val());

            $("#btnWarrantySelected").trigger("click");
        }
             );

        if ($("#inpWarrantyProductID").val() != "-1") {
            $("#dropWarrantyProduct").val($("#inpWarrantyProductID").val());
        }

        $(".warrantylist tr").each(function () {
            if ($(this).find(".warrantyid").eq(0).val() == $("#selectedwarrantyid").val()) {
                $(this).css("background-color", "#EFF3F4");
            }
        });

        $(".addwarrantyicon").unbind("click");
        $(".addwarrantyicon").click(function () {
            $(".btnAddWarranty").trigger("click");
            $("#btnDeleteWarranty").css("display", "none");
        });

        $("#btnDeleteWarranty").unbind("click");
        $("#btnDeleteWarranty").click(function () {
            $("#delwarrantyid").val($("#selectedwarrantyid").val());
            radconfirm("Are you sure to delete the item?", deleteWarranty, 300, 100, null, "Delete Warranty", "Images/Confirm.png");
        });

    }

    function deleteWarranty(sender) {
        if (sender) {
            $("#delWarrantyBtn").trigger("click");
        } else {
            $("#delwarrantyid").val("-1");
        }
    }
</script>
<style type="text/css">
    input[type=submit]
    {
        cursor: pointer;
    }
    .displaynone
    {
        display: none;
    }
</style>
<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="mainheader">
            <div class="contextual">
                <asp:Panel ID="listbuttons" runat="server">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add btnAddWarranty" ID="addLink"
                            Text="Add" OnClick="AddWarranty" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="formbuttons" runat="server" Visible="false">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                            Text="Back" OnClick="BackToList" />
                    </div>
                </asp:Panel>
            </div>
            <div class="subheader">
                <h2 class="icon-warranty pagetitle">
                    <asp:Literal ID="ltlPageTitle" Text="Warranty List" runat="server"></asp:Literal></h2>
            </div>
        </div>
         <table style="border-spacing:0px">
            <tr>
                <td>
        <asp:Panel ID="listpanel" runat="server">
        <div style="height:765px;overflow:auto;border:1px solid #e4e4e4;border-radius:15px;width:400px">
                        <div style="height:35px;width:100%;border-bottom:1px solid #e4e4e4;background-color:#25a0da">
                        <table style="width:100%">
                        <tr>
                        <td style="width:32px"><img src="images/icon_list.png" style="cursor:pointer" class="shownav" />
                        </td>
                        <td align="center"><span style="color:White;font-size:15px;vertical-align:middle">Warranties</span>
                        </td>
                        <td style="width:32px">
                        <img src="images/add.png" style="width:20px;height:20px;cursor:pointer" class="addwarrantyicon" />
                        </td>
                        </tr>
                        </table>
                        
                        
                        </div>
                         <div style="width: 100%; height: 730px; overflow-y: auto">
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table class="list warrantylist">
                       
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            <%# Eval("ProductName")%>

                        </td>
                        
                        <td align="center">
                            <asp:Label ID="lblQty" runat="server" Text='<%# Eval("Quantity")%>'></asp:Label>
                            <input type="hidden" value='<%# Eval("ID") %>' class="warrantyid" />
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
            </div>
            </div>
            <%--<webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged"
                PageSize="10" CssClass="aspnetpager">
            </webdiyer:AspNetPager>--%>
        </asp:Panel>
        </td><td>
        <asp:Panel ID="formpanel" runat="server">
           <div  style="height:765px;overflow:auto;border:1px solid #e4e4e4;border-radius:15px;border-left:0px solid black;width:620px;">
                        <div style="height:35px;width:100%;border-bottom:1px solid #e4e4e4;background-color:#25a0da">
                        <table style="float:right">
                            <tr>
                            <%--<td  style="padding:0px">
                                                            <asp:Button ID="addfiled" runat="server" Text="Add New Fields"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center displaynone"
                                        Height="30" Width="120" ClientIDMode="Static" />
                                                            </td>--%>
                                                            <td  style="padding:0px;width:32px;padding-top:3px;"><img id="btnDeleteWarranty" src="images/trash.png" style="cursor:pointer" /></td>
                                    
                            </tr>
                            </table>
                        </div>
                            <div style="padding-left:20px;height:690px;overflow:auto">
                        <table class="form-table">
                            <tr>
                                <td class="form-label">
                                    Invoice No.:
                                </td>
                                <td>
                                    <asp:DropDownList ID="dropInvoice" runat="server" DataTextField="inv_No" DataValueField="inv_Id"
                                        AutoPostBack="True" OnSelectedIndexChanged="InvoiceChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Item Description:
                                </td>
                                <td>
                                    <asp:DropDownList ID="dropWarrantyProduct" runat="server" DataTextField="ip_Description"
                                        DataValueField="ip_ProductID" ClientIDMode="Static">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Qty:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQty" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Date:
                                </td>
                                <td>
                                    <%--<asp:TextBox ID="txtDate" runat="server" CssClass="Wdate"></asp:TextBox>--%>
                                    <eid:RadDatePicker runat="server" ID="txtDate" Width="295px" Height="35px"
                                        Skin="MetroTouch" ShowPopupOnFocus="true" DateInput-Enabled="false" >
                                        <Calendar ID="Calendar1" runat="server" EnableKeyboardNavigation="true">
                                        </Calendar>
                                    </eid:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Comments:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Resolution:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtResolution" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <td colspan="2" align="center">
                                    <input type="hidden" id="inpWarrantyProductID" runat="server" clientidmode="Static"
                                        value="-1" />
                                    <asp:Label ID="lblInvoiceID" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblProductID" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblOldQty" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                     <div style="height:40px;width:160px;margin:0 auto;">
                            <table>
                            <tr>
                            <td>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                            UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                            Width="75" Height="30" /> </td>
                                        <td style="width:10px"></td>
                            <td><asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                        Height="30" Width="75" /></td>
                            </tr>
                            </table>
                </div>
            </div>
        </asp:Panel></td></tr></table>
        <input type="hidden" id="delwarrantyid" runat="server" clientidmode="Static" value="" />
        <asp:Button ID="delWarrantyBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="Delete" ClientIDMode="Static"></asp:Button>
            <asp:Button ID="btnWarrantySelected" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="WarrantySelected" ClientIDMode="Static"></asp:Button>
            <input type="hidden" id="selectedwarrantyid" runat="server" clientidmode="Static" value="0" />
    </ContentTemplate>
</asp:UpdatePanel>
