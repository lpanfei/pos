﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS.TouchControls
{
    public partial class WUCCategory : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //GlobalCache.RefreshCategories();
                //Initialize();
            }
        }

        public override void Init()
        {
            
                Initialize();
            
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                delcatid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "deleteCategory");
            }
            else if (e.CommandName == "ItemEdit")
            {
                RepeaterItem item = e.Item;
                ltlPageTitle.Text = "Category / Edit";
                catid.Attributes["value"] = e.CommandArgument.ToString();
                serverid.Attributes["value"] = (item.FindControl("lblServerID") as Label).Text;
                txtName.Text = (item.FindControl("lblName") as Label).Text.Replace("├", "").Replace("─", "");
                BindCategories(Convert.ToInt32(e.CommandArgument.ToString()));
                dropParent.SelectedValue = (item.FindControl("lblParentID") as Label).Text;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                ShowError("Category name is mandatory!");
                return;
            }
            POSService.POSService pos = new POSService.POSService();
            Category Parent = new Category();
            if (Convert.ToInt32(dropParent.SelectedValue) != 0)
            {
                Parent = GlobalCache.Categories.FirstOrDefault(c => c.cat_Id == Convert.ToInt32(dropParent.SelectedValue));
            }
            Category category=new Category() { cat_Id = Convert.ToInt32(catid.Value), cat_Name = txtName.Text.Trim(), ParentID = Convert.ToInt32(dropParent.SelectedValue), cat_ClientID = this.UserClientID, UserID = GlobalCache.CurrentUser.usr_ID, ServerID = Convert.ToInt32(serverid.Value), LastUpdatedTime = DateTime.Now, Parent = Parent };
            if (GlobalCache.Categories.Any(c => c.cat_Id == category.cat_Id) && GlobalCache.Categories.FirstOrDefault(c => c.cat_Id == category.cat_Id).IsNew)
            {
                category.cat_Id = 0;
            }
            pos.AddCategory(category);
            
            GlobalCache.RefreshCategories();
            BackToList(null, null);
        }

        void Initialize()
        {
            catid.Value = "0";
            serverid.Value = "0";
            delcatid.Attributes["value"] = "-1";
            txtName.Text = "";
            BindCategories(0);
            Bind();
            if (GlobalCache.Categories != null && GlobalCache.Categories.Any())
            {
                if (selectedcatid.Attributes["value"] == "0" || !GlobalCache.Categories.Any(p => p.cat_Id.ToString() == selectedcatid.Attributes["value"]))
                {
                    selectedcatid.Attributes["value"] = GlobalCache.Categories.GetHiarachy().FirstOrDefault().cat_Id.ToString();
                }
            }
            CategorySelected(null, null);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            CategorySelected(null, null);
        }

        void BindCategories(int catid)
        {
            List<Category> cats2 = new List<Category>();
            cats2.Add(new Category() { cat_Id = 0, cat_Name = "--No Parent--" });
            GlobalCache.Categories.GetHiarachy().ForEach(c =>
            {
                if (c.cat_Id != catid)
                {
                    cats2.Add(c);
                }
            });
            dropParent.DataSource = cats2;
            dropParent.DataBind();
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = GlobalCache.Categories.GetHiarachy();
            rptList.DataBind();
        }

        protected void AddCategory(object sender, EventArgs args)
        {
            ltlPageTitle.Text = "Category / Add";
            dropParent.SelectedIndex = 0;
            txtName.Text = "";
            catid.Value = "0";
            serverid.Value = "0";
            Category category = new Category() { cat_Id = GlobalCache.Categories.Max(p => p.cat_Id) + 1, cat_Name="New Category", IsNew = true };
            GlobalCache.Categories.Add(category);
            selectedcatid.Attributes["value"] = category.cat_Id.ToString();
            rptList.DataSource = GlobalCache.Categories.GetHiarachy();
            rptList.DataBind();
            CategorySelected(null, null);
        }

        protected void CategorySelected(object sender, EventArgs args)
        {
            ltlPageTitle.Text = "Category / Edit";
            int id = Convert.ToInt32(selectedcatid.Attributes["value"]);
            Category cat = GlobalCache.Categories.FirstOrDefault(c => c.cat_Id == id);
            if (cat != null)
            {
                catid.Attributes["value"] = cat.cat_Id.ToString();
                serverid.Attributes["value"] = cat.ServerID.ToString();
                txtName.Text = cat.cat_Name.Replace("├", "").Replace("─", "");
                BindCategories(cat.cat_Id);
                dropParent.SelectedValue = cat.ParentID.ToString();
            }
        }

        protected void BackToList(object sender, EventArgs e)
        {
            ltlPageTitle.Text = "Category List";
            Initialize();
        }

        protected void DeleteCategory(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delcatid.Attributes["value"]);

            Category category = GlobalCache.Categories.FirstOrDefault(c => c.cat_Id == id);
            if (category.IsNew)
            {
                GlobalCache.Categories.Remove(category);
            }
            else
            {
                POSService.POSService service = new POSService.POSService();
                category.IsDeleted = true;
                category.UserID = GlobalCache.CurrentUser.usr_ID;
                category.LastUpdatedTime = DateTime.Now;
                service.UpdateCategory(category);
                GlobalCache.RefreshCategories();
            }
            Initialize();
        }
    }
}