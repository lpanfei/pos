﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCUser.ascx.cs" Inherits="POS.TouchControls.WUCUser" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    var previoususer = null;
    Sys.Application.add_load(usersLoad);
    function usersLoad(sender, args) {
        $(".userslist tr").unbind("click");
        $(".userslist tr").click(function () {
            $("#selecteduserid").val($(this).find(".userid").eq(0).val());

            $("#btnUserSelected").trigger("click");
        }
       );

        $(".userslist>tbody>tr").each(function () {
            if ($(this).find(".userid").eq(0).val() == $("#selecteduserid").val()) {
                $(this).css("background-color", "#EFF3F4");
            }
            $(this).find(".currentusericon").css("display", "none");
            if ($(this).find(".userid").eq(0).val() == $("#loginuserid").val()) {
                $(this).find(".currentusericon").css("display", "");
            }
        });

        $(".addusericon").unbind("click");
        $(".addusericon").click(function () {
            $(".btnAddUser").trigger("click");
            $("#btnDeleteUser").css("display", "none");
        });

        $("#btnDeleteUser").unbind("click");
        $("#btnDeleteUser").click(function () {
            $("#deluserid").val($("#selecteduserid").val());
            radconfirm("Are you sure to delete the item?", deleteUser, 300, 100, null, "Delete User", "Images/Confirm.png");
        });

        $("#userTypedrop").unbind("change");
        $("#userTypedrop").change(function () {
            if ($("#userTypedrop").val() == "1") {
                $(".clientlevel").css("display", "none");
                $(".storelevel").css("display", "none");
                $(".permissionlevel").css("display", "none");
            } else if ($("#userTypedrop").val() == "2") {
                $(".clientlevel").css("display", "");
                $(".storelevel").css("display", "none");
                $(".permissionlevel").css("display", "");

            } else if ($("#userTypedrop").val() == "3") {
                $(".clientlevel").css("display", "none");
                $(".storelevel").css("display", "");
                $(".permissionlevel").css("display", "");
            }
        });
        
        if ($.trim($(".userpagetitle").text()) == "User / Edit") {
            $(".passwordfields").css("display", "none");
        } else {
            $(".passwordfields").css("display", "");
        }

        if ($("#userTypedrop").val() == "1") {
            $(".clientlevel").css("display", "none");
            $(".storelevel").css("display", "none");
            $(".permissionlevel").css("display", "none");
        } else if ($("#userTypedrop").val() == "2") {
            $(".clientlevel").css("display", "");
            $(".storelevel").css("display", "none");
            $(".permissionlevel").css("display", "");

        } else if ($("#userTypedrop").val() == "3") {
            $(".clientlevel").css("display", "none");
            $(".storelevel").css("display", "");
            $(".permissionlevel").css("display", "");
        }

        if ($("#selecteduserid").val() == $("#loginuserid").val()) {
            $("#changepassword").css("display", "block");
        } else {
            $("#changepassword").css("display", "none");
        }

        $("#changepassword").unbind("click");
        $("#changepassword").click(function () {
            $(".typepassword").val("");
            var radwindow = $find('<%=ChangePasswordRadWindow.ClientID %>');
            radwindow.show();
        });
    }

    function deleteUser(sender) {
        if (sender) {
            $("#delUserBtn").trigger("click");
        } else {
            $("#deluserid").val("-1");
        }
    }
</script>
<style type="text/css">
    input[type=submit]
    {
        cursor: pointer;
    }
    
    .displaynone
    {
        display: none;
    }
</style>
<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="mainheader">
            <div class="contextual">
                <asp:Panel ID="listbuttons" runat="server">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add btnAddUser" ID="addLink"
                            Text="Add" OnClick="AddUser" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="formbuttons" runat="server" Visible="false">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                            Text="Back" OnClick="BackToList" />
                    </div>
                </asp:Panel>
            </div>
            <div class="subheader">
                <h2 class="pagetitle icon-user userpagetitle">
                    <asp:Literal ID="ltlPageTitle" Text="User List" runat="server"></asp:Literal></h2>
            </div>
        </div>
        <table style="border-spacing: 0px">
        <tr>
        <td>
        <asp:Panel ID="listpanel" runat="server">
        <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                            width: 400px">
                            <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 32px">
                                            <img src="images/icon_list.png" style="cursor: pointer" class="shownav" />
                                        </td>
                                        <td align="center">
                                            <span style="color: White; font-size: 15px; vertical-align: middle">Users</span>
                                        </td>
                                        <td style="width: 32px">
                                            <img src="images/add.png" style="width: 20px; height: 20px; cursor: pointer" class="addusericon" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                             <div style="width: 100%; height: 730px; overflow-y: auto">
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table class="list userslist">
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            
                            <input type="hidden" value='<%# Eval("usr_ID") %>' class="userid" />
                            <span style="vertical-align:middle">
                                <%# Eval("usr_Name")%>
                                <img src="images/current_user.png" style="vertical-align:middle;width:32px;height:32px;" class="currentusericon" />
                            </span>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
            </div>
            </div>
            <%--<webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged" PageSize="10" CssClass="aspnetpager">
            </webdiyer:AspNetPager>--%>
        </asp:Panel>
        </td>
         <td>
          <asp:Panel ID="formpanel" runat="server">
            <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                            border-left: 0px solid black; width: 620px;">
                            <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                <table style="float: right">
                                    <tr>
                                        <td  style="padding: 0px; width: 32px; padding-top: 3px;">
                                                           <img id="changepassword" src="images/change_password.png" style="cursor: pointer;width:24px;height:24px" />
                                        </td>
                                        <td style="width:30px"
                                        <td style="padding: 0px; width: 32px; padding-top: 3px;">
                                            <img id="btnDeleteUser" src="images/trash.png" style="cursor: pointer" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="padding-left: 20px; height: 690px; overflow: auto">
                        <table class="form-table">
                            <tr>
                                <td class="form-label">
                                    User Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="passwordfields">
                                <td class="form-label">
                                    Password:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    User Level:
                                </td>
                                <td>
                                    <asp:DropDownList ID="userTypedrop" runat="server" ClientIDMode="Static">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="clientlevel" style="display: none">
                                <td class="form-label">
                                    Client:
                                </td>
                                <td>
                                    <asp:DropDownList ID="clientDropdown" runat="server" DataValueField="ID"
                                        DataTextField="Name">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="storelevel" style="display: none">
                                <td class="form-label">
                                    Store:
                                </td>
                                <td>
                                    <asp:DropDownList ID="storeDropdown" runat="server" DataTextField="sto_Name" DataValueField="sto_ID">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="permissionlevel" style="display: none">
                                <td class="form-label">
                                    Permission:
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="radlPermission" runat="server" RepeatDirection="Horizontal"
                                        RepeatColumns="2">
                                        <asp:ListItem Text="Administrator" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Report" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Sale" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Sale and Warehouse" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Service" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Technician" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Warehouse" Value="6"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <%--<tr>
            <td>Store:</td>
            <td>
                <asp:DropDownList ID="dropStore" runat="server"></asp:DropDownList>
            </td>
        </tr>--%>
                            <tr>
                                <td class="form-label">
                                    First Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Last Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Address:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Suburb:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSuburb" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    State:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtState" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Postcode:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPostcode" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Phone:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Moble Phone:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMoblePhone" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Email:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Note:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <td colspan="2" align="center">
                                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="height: 40px; width: 160px; margin: 0 auto;">
                                <table>
                                    <tr>
                                        <td>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                            UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                            Width="75" Height="30" /></td>
                                        <td style="width: 10px">
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                                                UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                                Height="30" Width="75" />
                                        </td>
                                    </tr>
                                </table>
                    </div>
                </div>
        </asp:Panel>
        </td>
        </tr>
        </table>
        <eid:RadWindow runat="server" ID="ChangePasswordRadWindow" Skin="Metro" Overlay="true" Modal="true"
                                        Title="Modify Password" VisibleStatusbar="false" AutoSize="true" Width="600"
                                        Height="400">
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="Updatepanel11" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table class="form-table">
                                                        <tr>
                                                            <td class="form-label">
                                                                Old Password:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtOld" runat="server" Width="200" TextMode="Password" CssClass="typepassword"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="form-label">
                                                                New Password:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNew" runat="server" Width="200" TextMode="Password" CssClass="typepassword"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="form-label">
                                                                Confirm Password:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtConfirm" runat="server" Width="200" TextMode="Password" CssClass="typepassword"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="center">
                                                                <asp:Button ID="Button2" Width="100" runat="server" CssClass="btn btn-large btn-block btn-info"
                                                                    Text="Submit" OnClick="Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </eid:RadWindow>
       
        <input type="hidden" id="deluserid" runat="server" clientidmode="Static" value="" />
        <input type="hidden" id="loginuserid" runat="server" clientidmode="Static" value="" />
        <asp:Button ID="delUserBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="Delete" ClientIDMode="Static"></asp:Button>
            <asp:Button ID="btnUserSelected" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="UserSelected" ClientIDMode="Static"></asp:Button>
            <input type="hidden" id="selecteduserid" runat="server" clientidmode="Static" value="0" />
    </ContentTemplate>
</asp:UpdatePanel>
