﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS.TouchControls
{
    public partial class WUCInventory : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Initialize();
            }
        }

        public override void Init()
        {
            Initialize();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int categoryId = Convert.ToInt32(dropCategory.SelectedValue);
            string name = string.Empty;
            string serialNo = string.Empty;
            switch (dropField.SelectedValue)
            {
                case "1":
                    name = txtKey.Text.Trim();
                    break;
                case "2":
                    serialNo = txtKey.Text.Trim();
                    break;
            }
            int quantity = Convert.ToInt32(dropQuantity.SelectedValue);

            GlobalCache.RefreshInventories(categoryId, name, serialNo, quantity);
            Bind();
        }

        protected void InventorySelected(object sender, EventArgs e)
        {
            int proid = Convert.ToInt32(selectedinventoryid.Attributes["value"]);
            List<Inventory> inventories = GlobalCache.Inventories;
            if (inventories != null)
            {
                Inventory p = inventories.FirstOrDefault(p2 => p2.Product.pro_Id == proid);
                if (p != null)
                {
                    txtName.Text = p.Product.pro_Name;
                    txtQuantity.Text = p.Quantity.ToString();
                    inventorypid.Attributes["value"] = p.Product.pro_Id.ToString();
                }
                else
                {
                    Product p2 = GlobalCache.Products.FirstOrDefault(pp => pp.pro_Id == proid);
                    if (p2 == null)
                        return;
                    txtName.Text = p2.pro_Name;
                    txtQuantity.Text = "0";
                    inventorypid.Attributes["value"] = p2.pro_Id.ToString();
                }
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            InventorySelected(null, null);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Stock(null, null);
        }
        protected void Stock(object sender, EventArgs e)
        {
            int proid = Convert.ToInt32(inventorypid.Attributes["value"]);
            
            POSService.POSService service = new POSService.POSService();
            List<Inventory> inventories = GlobalCache.Inventories;
            if (inventories != null)
            {
                Inventory p = inventories.FirstOrDefault(p2 => p2.Product.pro_Id == proid);
                p.Quantity = Convert.ToInt32(txtQuantity.Text.Trim());
                p.ProductID = proid;
                p.ClientID = this.UserClientID;
                p.StoreID = this.UserStoreID;
                p.UserID = GlobalCache.CurrentUser.usr_ID;
                p.LastUpdatedTime = DateTime.Now;
                service.StockToInventory(p);
            }
            GlobalCache.RefreshInventories(0, "", "", 0);
            Initialize();
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = GlobalCache.Inventories;
            rptList.DataBind();
            if (GlobalCache.Inventories != null && GlobalCache.Inventories.Any())
            {
                if (selectedinventoryid.Attributes["value"] == "0" || !GlobalCache.Inventories.Any(p => p.Product.pro_Id.ToString() == selectedinventoryid.Attributes["value"]))
                {
                    selectedinventoryid.Attributes["value"] = GlobalCache.Inventories.FirstOrDefault().Product.pro_Id.ToString();
                }
            }
            InventorySelected(null, null);
        }

        void Initialize()
        {
            inventoryinvid.Attributes["value"] = "0";
            inventorypid.Attributes["value"] = "0";
            inventoryquantity.Attributes["value"] = "0";

            Bind();
            BindCategories();
        }

        void BindCategories()
        {
            List<Category> cats = GlobalCache.Categories.GetHiarachy();
            cats.Insert(0, new Category() { cat_Id = 0, cat_Name = "-- All Category --" });
            dropCategory.DataSource = cats;
            dropCategory.DataBind();
        }
    }
}