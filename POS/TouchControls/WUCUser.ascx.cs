﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace POS.TouchControls
{
    public partial class WUCUser : ControlBase
    {
        private List<POS.Data.User> Users
        {
            get
            {

                return GlobalCache.Users;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //InitializeDropdowns();
                //Bind();

                if (GlobalCache.CurrentUser != null)
                {
                    //ltlUser.Text = GlobalCache.CurrentUser.usr_Name;

                    if (string.IsNullOrEmpty(txtOld.Text))
                    {
                        txtOld.Attributes.Add("value", txtOld.Attributes["value"]);
                    }
                    else
                    {
                        txtOld.Attributes.Add("value", txtOld.Text);
                    }
                    if (string.IsNullOrEmpty(txtNew.Text))
                    {
                        txtNew.Attributes.Add("value", txtNew.Attributes["value"]);
                    }
                    else
                    {
                        txtNew.Attributes.Add("value", txtNew.Text);
                    }
                    if (string.IsNullOrEmpty(txtConfirm.Text))
                    {
                        txtConfirm.Attributes.Add("value", txtConfirm.Attributes["value"]);
                    }
                    else
                    {
                        txtConfirm.Attributes.Add("value", txtConfirm.Text);
                    }

                    loginuserid.Attributes["value"]=GlobalCache.CurrentUser.usr_ID.ToString();
                }
            }
        }

        protected void Click(object sender, EventArgs args)
        {
            if (string.IsNullOrEmpty(txtOld.Text.Trim()) || string.IsNullOrEmpty(txtNew.Text.Trim()) || string.IsNullOrEmpty(txtConfirm.Text.Trim()))
            {
                this.ShowError("Please fill the form completely!");
                return;
            }

            if (txtOld.Text.Trim() != GlobalCache.CurrentUser.usr_Password)
            {
                ShowError("Old passwords does not match!");
                return;
            }

            if (txtNew.Text.Trim() != txtConfirm.Text.Trim())
            {
                ShowError("New passwords do not match!");
                return;
            }

            POSService.OperationResult or = new POSService.POSService().ChangePassword(GlobalCache.CurrentUser.usr_ID, txtNew.Text.Trim());
            if (or.Result)
            {
                GlobalCache.CurrentUser = null;
                ShowSuccess("Password is modified successfully. Please re-login!", "relogin");
                //ScriptManager.RegisterStartupScript(Button2, GetType(), "relogin", "relogin();", true);
            }
        }

        public override void Init()
        {
            InitializeDropdowns();
            Bind();
        }

        void InitializeDropdowns()
        {
            switch (GlobalCache.CurrentUser.UserType.ToString())
            {
                case "SystemAdmin":
                    userTypedrop.Items.Add(new ListItem() { Text = "System", Value = "1", Selected = true });
                    userTypedrop.Items.Add(new ListItem() { Text = "Client", Value = "2" });
                    break;
                case "ClientAdmin":
                    userTypedrop.Items.Add(new ListItem() { Text = "Client", Value = "2", Selected = true });
                    userTypedrop.Items.Add(new ListItem() { Text = "Store", Value = "3" });
                    break;
                case "StoreAdmin":
                    userTypedrop.Items.Add(new ListItem() { Text = "Store", Value = "3", Selected = true });
                    break;
            }

            clientDropdown.DataSource = GlobalCache.Clients;
            clientDropdown.DataBind();
            if (GlobalCache.Clients.Any())
            {
                clientDropdown.SelectedIndex = 0;
            }
            storeDropdown.DataSource = GlobalCache.Stores;
            storeDropdown.DataBind();
            if (GlobalCache.Stores.Any())
            {
                storeDropdown.SelectedIndex = 0;
            }
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemEdit")
            {
                formbuttons.Visible = true;
                listbuttons.Visible = false;
                formpanel.Visible = true;
                listpanel.Visible = false;
                ltlPageTitle.Text = "User / Edit";
                POS.Data.User user = new POSService.POSService().GetUser(Convert.ToInt32(e.CommandArgument.ToString()));
                txtAddress.Text = user.usr_Address;
                txtEmail.Text = user.usr_Email;
                txtFirstName.Text = user.usr_FirstName;
                txtLastName.Text = user.usr_LastName;
                txtMoblePhone.Text = user.usr_MoblePhone;
                txtName.Text = user.usr_Name;
                txtNote.Text = user.usr_Note;
                //txtPassword.Text = user.usr_Password;
                txtPhone.Text = user.usr_Phone;
                txtPostcode.Text = user.usr_Postcode;
                txtState.Text = user.usr_State;
                txtSuburb.Text = user.usr_Suburb;
                radlPermission.SelectedValue = user.usr_Permission;
                userTypedrop.SelectedValue = user.usr_UserTypeID.ToString();
                lblId.Text = user.usr_ID.ToString();
            }
            else if (e.CommandName == "ItemDel")
            {
                deluserid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "deleteUser");
            }
        }

        protected void Delete(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(deluserid.Attributes["value"]);
            POS.Data.User user = GlobalCache.Users.FirstOrDefault(u => u.usr_ID == id);
            if (user.IsNew)
            {
                GlobalCache.Users.Remove(user);
            }
            else
            {
                new POSService.POSService().DeleteUserById(id);
                GlobalCache.RefreshUsers();
            }
            Bind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            int storeid = 0;
            int clientid = 0;
            switch (userTypedrop.SelectedValue)
            {
                case "1":
                    break;
                case "2":
                    clientid = Convert.ToInt32(clientDropdown.SelectedValue);
                    break;
                case "3":
                    clientid = this.UserClientID;
                    storeid = Convert.ToInt32(storeDropdown.SelectedValue);
                    break;
            }
            POS.Data.User user=new POS.Data.User()
            {
                usr_FirstName = txtFirstName.Text,
                usr_ID = Convert.ToInt32(lblId.Text.Trim()),
                usr_Address = txtAddress.Text,
                usr_ClientID = clientid,
                usr_Email = txtEmail.Text,
                usr_LastName = txtLastName.Text,
                usr_MoblePhone = txtMoblePhone.Text,
                usr_Name = txtName.Text,
                usr_Note = txtNote.Text,
                usr_Password = txtPassword.Text,
                usr_Permission = radlPermission.SelectedValue,
                usr_Phone = txtPhone.Text,
                usr_Postcode = txtPostcode.Text,
                usr_State = txtState.Text,
                usr_StoreID = storeid,
                usr_Suburb = txtSuburb.Text,
                usr_UserTypeID = Convert.ToInt32(userTypedrop.SelectedValue)
            };
            POS.Data.User exu = GlobalCache.Users.FirstOrDefault(u => u.usr_ID == user.usr_ID && u.IsNew);
            if (exu != null)
            {
                user.usr_ID = 0;
            }
            user.Update();
            BackToList(null, null);
        }

        protected void AddUser(object sender, EventArgs e)
        {
            ltlPageTitle.Text = "User / Add";
            txtAddress.Text = "";
            txtEmail.Text = "";
            txtFirstName.Text = "";
            txtLastName.Text = "";
            txtMoblePhone.Text = "";
            txtName.Text = "";
            txtNote.Text = "";
            txtPassword.Text = "";
            txtPhone.Text = "";
            txtPostcode.Text = "";
            txtState.Text = "";
            txtSuburb.Text = "";
            radlPermission.SelectedValue = "0";
            lblId.Text = "0";
            POS.Data.User supplier = new POS.Data.User() { usr_ID = GlobalCache.Users.Max(c => c.usr_ID) + 1, usr_Name = "New User", IsNew = true };
            GlobalCache.Users.Add(supplier);
            selecteduserid.Attributes["value"] = supplier.usr_ID.ToString();
            Bind();
        }

        protected void UserSelected(object sender, EventArgs e)
        {
            
            POS.Data.User user = new POSService.POSService().GetUser(Convert.ToInt32(selecteduserid.Attributes["value"]));
            if (user == null)
            {
                user = GlobalCache.Users.FirstOrDefault(u => u.usr_ID == Convert.ToInt32(selecteduserid.Attributes["value"]));
                ltlPageTitle.Text = "User / Add";
            }
            else
            {
                ltlPageTitle.Text = "User / Edit";
            }
            if (user == null)
                return;
            txtAddress.Text = user.usr_Address;
            txtEmail.Text = user.usr_Email;
            txtFirstName.Text = user.usr_FirstName;
            txtLastName.Text = user.usr_LastName;
            txtMoblePhone.Text = user.usr_MoblePhone;
            txtName.Text = user.usr_Name;
            txtNote.Text = user.usr_Note;
            //txtPassword.Text = user.usr_Password;
            txtPhone.Text = user.usr_Phone;
            txtPostcode.Text = user.usr_Postcode;
            txtState.Text = user.usr_State;
            txtSuburb.Text = user.usr_Suburb;
            if (!user.IsNew)
            {
                radlPermission.SelectedValue = user.usr_Permission;
                userTypedrop.SelectedValue = user.usr_UserTypeID.ToString();
            }
            else
            {
                radlPermission.SelectedIndex = 0;
                userTypedrop.SelectedIndex = 0;
            }
            lblId.Text = user.usr_ID.ToString();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            UserSelected(null, null);
        }

        protected void BackToList(object sender, EventArgs e)
        {
            ltlPageTitle.Text = "User List";
            GlobalCache.RefreshUsers();
            Bind();
        }

        void Bind()
        {
            List<POS.Data.User> list = new List<Data.User>();
            POS.Data.User current = Users.FirstOrDefault(u => u.usr_ID == GlobalCache.CurrentUser.usr_ID);
            list.Add(current);
            foreach (POS.Data.User user in Users.OrderByDescending(u => u.usr_ID))
            {
                if (user != current)
                {
                    list.Add(user);
                }
            }
            rptList.DataSource = null;
            rptList.DataSource = list;
            rptList.DataBind();
            if (list.Any())
            {
                if (selecteduserid.Attributes["value"] == "0" || !list.Any(p => p.usr_ID.ToString() == selecteduserid.Attributes["value"]))
                {
                    selecteduserid.Attributes["value"] = list.FirstOrDefault().usr_ID.ToString();
                }
            }
            UserSelected(null, null);
        }
    }
}