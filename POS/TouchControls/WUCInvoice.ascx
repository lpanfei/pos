﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCInvoice.ascx.cs"
    Inherits="POS.TouchControls.WUCInvoice" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    var previousinvoice = null;
    Sys.Application.add_load(invoiceLoad);

    function invoiceLoad(sender, args) {
        $(".txtchanged").attr("readonly", true);

        $('.txtchanged').keyboard({
            layout: 'num',
            restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
            preventPaste: true,  // prevent ctrl-v and right click
            autoAccept: false
        });

        $(".invoicelist tr").unbind("click");
        $(".invoicelist tr").click(function () {
            $(".btn-addinvoice").trigger("click");

        }
            );
        $(".txtchanged").unbind("change");
        $(".txtchanged").change(function () {
            $("#btnUpdateCart").trigger("click");
        });

        $("#editImageBtn").unbind("click");
        $("#editImageBtn").click(function () {
            $(".customertr").css("display", "none");
            $(".custommessage").text("");
            $(".customername").val("");
            $(".customercountry").val("");
            $(".customeremail").val("");
            $(".customerphone").val("");
            $(".customermobilephone").val("");
            $(".customerwalletid").val("");
            $(".customeraddress").val("");
            var radwindow = $find('<%=AddCustomerWindow.ClientID %>');
            radwindow.show();
        });
        $(".customername").unbind("focusout");
        $(".customername").focusout(function () {
            if ($(".customername").val() == "") {
                $(".customertr").css("display", "");
                $(".custommessage").text("Customer name is required");
                return false;
            } else {
                $(".customertr").css("display", "none");
                $(".custommessage").text("");
                return false;
            }
        });
        $("#btnAddCustomerInfo").unbind("click");
        $("#btnAddCustomerInfo").click(function () {
            if ($(".customername").val() == "") {
                $(".customertr").css("display", "");
                $(".custommessage").text("Customer name is required");
                return false;
            }
            var radwindow = $find('<%=AddCustomerWindow.ClientID %>');
            radwindow.close();
            $("#btnAddCustomer").trigger("click");
        });
        $("#submitCancelInvoice").unbind("click");
        $("#submitCancelInvoice").click(function () {

            var radwindow = $find('<%=InvoiceCancelRadWindow1.ClientID %>');
            radwindow.close();
            $("#btnCancelInvoice").trigger("click");
        });
        $(".icon-cancel").unbind("click");
        $(".icon-cancel").click(function () {
            var radwindow = $find('<%=InvoiceCancelRadWindow1.ClientID %>');
            radwindow.show();
            var $tr = $(this).closest("tr");
            var $invid = $tr.find(".invid").eq(0);
            var $invno = $tr.find(".invno").eq(0);
            $("#lblCancelID").val($.trim($invid.text()));
            $("#lblCancelNo").text($.trim($invno.text()));
        });

        $(".paidstatus").each(function () {
            if ($(this).hasClass("paidselected")) {
                $(this).removeClass("paidselected");
                $(this).addClass("paidunselected");
            }
            //alert($(".dropPaidStatus").val().toLowerCase());
            $(".paid-" + $(".dropPaidStatus").val().toLowerCase()).removeClass("paidunselected");
            $(".paid-" + $(".dropPaidStatus").val().toLowerCase()).addClass("paidselected");
        });

        $(".paidstatus").unbind("click");
        $(".paidstatus").click(function () {
            $(".paidstatus").each(function () {
                if ($(this).hasClass("paidselected")) {
                    $(this).removeClass("paidselected");
                    $(this).addClass("paidunselected");
                }
            });
            $(this).removeClass("paidunselected");
            $(this).addClass("paidselected");
            if ($(this).hasClass("paid-all")) {
                $(".dropPaidStatus").val("All");
            } else if ($(this).hasClass("paid-paid")) {
                $(".dropPaidStatus").val("Paid");
            } else if ($(this).hasClass("paid-unpaid")) {
                $(".dropPaidStatus").val("Unpaid");
            }
            //$(".dropPaidStatus").trigger("change");
        });
    }

    function ReDoInvoice(sender) {
        if (sender) {
            $("#resubmitBtn").trigger("click");
        }
    }
</script>
<style type="text/css">
        .displaynone
        {
            display: none;
        }
        .displayit
        {
            display:;
        }
        .btn-add
        {
            cursor: pointer;
        }
        .inputtype
        {
            height: 30px;
            width: 120px;
            padding-left: 10px;
        }
        .nonelabel
        {
            display: none;
        }
        .pdiv
        {
            border-bottom: 1px solid #3894E7;
        }
        .rpText
        {
            font-size:25px;
        }
        .txtchanged
        {
            margin-top:0px;
            margin-bottom:0px;
            border-bottom-left-radius:0px;
            border-top-right-radius:0px;
            border-top-left-radius:0px;
            border-bottom-right-radius:0px;
        }
        .paidselected
        {
            background-color:#25a0da;
            color:White;
        }
        
        .paidunselected
        {
            background-color:#f6f6f6;
            color:black;
        }
    </style>
<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="mainheader">
            <div class="contextual">
                <asp:Panel ID="formbuttons" Visible="false" runat="server">
                    <div style="float: left; width: 75px; height: 10px;">
                        <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click" UseSubmitBehavior="false"
                            class="btn btn-large btn-block btn-info" />
                    </div>
                    <%--<div style="float: left; margin-left: 10px; width: 150px; height: 10px;">--%>
                    <%--<asp:Button ID="Button1" runat="server" Text="Save and Print" OnClick="btnPrint_Click"
                                UseSubmitBehavior="false" class="btn btn-large btn-block btn-info" /></div>--%>
                    <div style="width: 85px; float: left; display: block; margin-left: 10px;">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button2"
                            Text="Back" OnClick="BackToList" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="listbuttons" runat="server">
                    <div style="height: 10px; width: 85px; float: left;">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add btn-addinvoice" ID="addLink"
                            Text="Add" OnClick="AddInvoice" />
                    </div>
                    <div style="height: 10px; width: 150px; float: left; margin-left: 10px;">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                            Text="Cancelled List" OnClick="CancelledList" />
                    </div>
                </asp:Panel>
            </div>
            <div class="subheader">
                <h3 class="icon-invoice pagetitle">
                    <asp:Literal ID="ltlPageTitle" Text="Invoices" runat="server"></asp:Literal>
                </h3>
            </div>
        </div>
        <asp:Panel runat="server" ID="pagelist">
            <asp:Panel runat="server" ID="searcharea" Style="display: none">
                <fieldset>
                    <legend>Search</legend>
                    <div style="padding: 5px">
                        <table>
                            <tr>
                                <td width="120px">
                                    Invoice No:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNo" runat="server"></asp:TextBox>
                                </td>
                                <td width="100px" style="padding-left: 20px;">
                                    Date From:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDateFrom" runat="server" CssClass="Wdate"></asp:TextBox>
                                </td>
                                <td rowspan="2" valign="middle">
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click"
                                        Width="100" UseSubmitBehavior="false" class="btn btn-large btn-block btn-info"
                                        Style="float: left; margin-left: 15px;" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Customer:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomer" runat="server"></asp:TextBox>
                                </td>
                                <td width="100px" style="padding-left: 20px;">
                                    Date To:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDateTo" runat="server" CssClass="Wdate"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                            </tr>
                        </table>
                    </div>
                </fieldset>
            </asp:Panel>
            <asp:DropDownList ID="dropPaidStatus" runat="server" CssClass="dropPaidStatus"
                AutoPostBack="true" Style="height: 30px; width: 292px; border: 1px solid #D1E4F6;
                display: none;">
                <asp:ListItem Text="All" Value="All" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Paid" Value="Paid"></asp:ListItem>
                <asp:ListItem Text="Unpaid" Value="Unpaid"></asp:ListItem>
            </asp:DropDownList>
            <table style="height: 35px; border-spacing: 0px; border-width: 0px; border-collapse: collapse;display:none;">
                <tr>
                    <td style="width: 60px; height: 30px; font-weight: bold; border-right: 1px solid #ddd;
                        cursor: pointer" valign="middle" align="center" class="paidstatus paid-all paidselected">
                        All
                    </td>
                    <td style="width: 60px; height: 30px; font-weight: bold; border-right: 1px solid #ddd;
                        cursor: pointer" valign="middle" align="center" class="paidstatus paid-paid paidunselected">
                        Paid
                    </td>
                    <td style="width: 60px; height: 30px; font-weight: bold; cursor: pointer" valign="middle"
                        align="center" class="paidstatus paid-unpaid paidunselected">
                        Unpaid
                    </td>
                    <td style="width: 20px">
                    </td>
                    <td>
                        <table>
                            <tr>
                                <td>
                                    From
                                </td>
                                <td>
                                    <eid:RadDatePicker runat="server" ID="RadDatePicker1" Width="130px" Height="35px"
                                        Skin="MetroTouch" ShowPopupOnFocus="true" DateInput-Enabled="true" >
                                        <Calendar ID="Calendar1" runat="server" EnableKeyboardNavigation="true">
                                        </Calendar>
                                    </eid:RadDatePicker>
                                </td>
                                <td valign="middle">
                                    TO
                                </td>
                                <td>
                                    <eid:RadDatePicker runat="server" ID="RadDatePicker2" Width="130px" Height="35px"
                                        Skin="MetroTouch" ShowPopupOnFocus="true" DateInput-Enabled="true">
                                        <Calendar ID="Calendar2" runat="server" EnableKeyboardNavigation="true">
                                        </Calendar>
                                    </eid:RadDatePicker>
                                </td>
                                <td style="width:20px">
                                
                                </td>
                                <td>
                                    <asp:Button ID="FilterButton" runat="server" Text="Search" Width="100" OnClick="FilterByTime"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <br />
            <div>
                <table class="list invoicelist" style="table-layout: fixed; margin-bottom: 0px;">
                    <colgroup>
                        <col>
                        <col>
                        <col>
                        <col>
                        <col>
                        <col>
                    </colgroup>
                    <thead>
                        <tr>
                            <th>
                                Invoice No
                            </th>
                            <th>
                                Created Date
                            </th>
                            <th>
                                Created User
                            </th>
                            <th>
                                Customer Name
                            </th>
                            <th>
                                Total (inc GST)
                            </th>
                            <th>
                                Paid Status
                            </th>
                        </tr>
                    </thead>
                    <tbody style="display: none">
                        <tr>
                            <td colspan="6" valign="middle">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div style="height: 673px; overflow-y: auto">
                <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand">
                    <HeaderTemplate>
                        <table class="list invoicelist" style="table-layout: fixed">
                            <colgroup>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                            </colgroup>
                            <thead style="display: none;">
                                <tr>
                                    <th scope="col">
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center" class="invno" style="width: 100px;">
                                <%# InvoiceNumberPrefix + Eval("inv_No")%>
                            </td>
                            <td align="center" style="width: 100px;">
                                <asp:Label ID="lblDate" runat="server" Text='<%# Eval("inv_CreateDate")%>'></asp:Label>
                            </td>
                            <td align="center" style="width: 100px;">
                                <%# Eval("inv_Staff")%>
                            </td>
                            <td align="center" style="width: 100px;">
                                <%# Eval("inv_Customer")%>
                            </td>
                            <td align="center" style="width: 100px;">
                                $<asp:Label ID="lblTotalIncGST" runat="server" Text='<%# Eval("inv_TotalIncGST")%>'></asp:Label>
                            </td>
                            <td align="center" style="width: 100px;">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("Status")%>'></asp:Label>
                            </td>
                            <td style="display: none" class="invid">
                                <%# Eval("inv_Id")%>
                            </td>
                            <td style="display: none" class="invcancel">
                                <%# Eval("IsCancel")%>
                            </td>
                            <td style="display: none" class="invstatus">
                                <%# Eval("Status")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
            <%-- <webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged"
                PageSize="10" CssClass="aspnetpager">
            </webdiyer:AspNetPager>--%>
        </asp:Panel>
        <asp:Panel runat="server" ID="pageform">
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table width="100%" height="100%">
                        <tr>
                            <td valign="top">
                                <div>
                                    <span style="font-weight: bold; font-size: 20px;">Customer Info</span>
                                </div>
                                <div class="box" style="height: 110px;">
                                    <table style="float: left; margin-right: 20px;">
                                        <tr>
                                            <td style="width: 100px; height: 35px;">
                                                <span>Invoice No.</span>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPrefix" runat="server"></asp:Label><asp:Label ID="lblNo" runat="server"></asp:Label><asp:Label
                                                    ID="lblIsNew" runat="server" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 120px;">
                                                Customer Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtName" runat="server" Width="115" Height="30"></asp:TextBox>
                                                <asp:DropDownList ID="dropCustomer" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dropCustomer_SelectedIndexChanged"
                                                    Width="115" Height="30" DataTextField="Name" DataValueField="ID">
                                                </asp:DropDownList>
                                                <asp:ImageButton ID="editImageBtn" runat="server" ImageUrl="~/Images/edit.png" ClientIDMode="Static" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 35px;">
                                                <span>Discount Type:</span>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="dropType" Height="30" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dropType_SelectedIndexChanged"
                                                    Width="130">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="margin-left: 20px;">
                                        <tr>
                                            <td style="height: 35px;">
                                                <span>Date:</span>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDate" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                Address:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddress" runat="server" CssClass="inputtype"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                Country:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCountry" runat="server" CssClass="inputtype"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                Phone:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPhone" runat="server" CssClass="inputtype"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                Mobile:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMobile" runat="server" CssClass="inputtype"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="display: none">
                                            <td>
                                                Email:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="inputtype"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Wallet ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtWalletID" runat="server" CssClass="inputtype"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div>
                                    <span style="font-weight: bold; font-size: 20px;">Invoice Product List</span>
                                </div>
                                <div id="invoiceProductContainer" style="height: 500px; overflow: auto; border: 1px solid #ebebeb">
                                    <table style="width: 100%;" class="list">
                                        <thead>
                                            <tr>
                                                <th align="center">
                                                </th>
                                                <th style="width: 50px; display: none;">
                                                    Item
                                                </th>
                                                <th>
                                                    Product name & Description
                                                </th>
                                                <th style="display: none;">
                                                    Serial No
                                                </th>
                                                <th>
                                                    Price (Inc GST)
                                                </th>
                                                <th>
                                                    Discount
                                                </th>
                                                <th>
                                                    Qty
                                                </th>
                                                <th>
                                                    Sub Total
                                                </th>
                                                <th style="width: 200px; display: none;">
                                                    Note
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <asp:Repeater ID="rptProductList" runat="server">
                                                <HeaderTemplate>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <tr>
                                                        <td align="center">
                                                            <asp:LinkButton ID="lbtnDel" runat="server" Text="Delete" class="icon icon-del" OnClick="DeleteInvoiceItem"></asp:LinkButton>
                                                            <asp:Label ID="lblId" runat="server" Text='<%# Eval("ip_ID")%>' Visible="false"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("ip_Description")%>'></asp:Label>
                                                        </td>
                                                        <td align="center" style="display: none;">
                                                            <asp:Label ID="lblSerialNo" runat="server" Text=''></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            $<asp:TextBox ID="txtPrice" runat="server" Width="80px" Text='<%# Convert.ToDecimal(Eval("ip_Price")).ToString("0.00")%>'
                                                                CssClass="txtchanged ui-keyboard-lockedinput"></asp:TextBox>
                                                        </td>
                                                        <td align="center">
                                                            <asp:TextBox ID="txtDiscount" runat="server" Width="50px" CssClass="txtchanged ui-keyboard-lockedinput"
                                                                Text='<%# Convert.ToDecimal(Eval("ip_Discount")).ToString("0.00")%>'></asp:TextBox>
                                                        </td>
                                                        <td align="center">
                                                            <asp:TextBox ID="txtQuantity" runat="server" Width="50px" Text='<%# Eval("ip_Quantity")%>'
                                                                CssClass="txtchanged ui-keyboard-lockedinput"></asp:TextBox>
                                                            <asp:Label ID="lblProductId" runat="server" Text='<%# Eval("ip_ProductID")%>' Visible="false"></asp:Label>
                                                        </td>
                                                        <td align="center">
                                                            $<asp:Label ID="lblSubTotal" runat="server" Text='<%# Eval("ip_SubTotal")%>'></asp:Label><asp:Label
                                                                ID="lblCost" runat="server" Visible="false" Text='<%# Eval("cost")%>'></asp:Label>
                                                        </td>
                                                        <td align="center" style="display: none;">
                                                            <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="4" Text='<%# Eval("ip_Note")%>'></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                                <FooterTemplate>
                                                </FooterTemplate>
                                            </asp:Repeater>
                                        </tbody>
                                    </table>
                                </div>
                                <table runat="server" id="totaltable" style="float: right">
                                    <tr style="font-weight: bold; border-spacing: 0;">
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td style="text-align: right;">
                                            Total:
                                        </td>
                                        <td>
                                            $<asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="font-weight: bold;">
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td style="text-align: right;">
                                            GST:
                                        </td>
                                        <td>
                                            $<asp:Label ID="lblGST" runat="server" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="font-weight: bold;">
                                        <td style="text-align: right;">
                                            Payment Method:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dropPayMethod" runat="server">
                                                <asp:ListItem Text="Cash" Value="Cash"></asp:ListItem>
                                                <asp:ListItem Text="Credit Card" Value="Credit Card"></asp:ListItem>
                                                <asp:ListItem Text="Nets" Value="Nets"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="text-align: right; padding-left: 10px;">
                                            Payment To:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dropPrintTo" runat="server">
                                                <asp:ListItem Text="SMS/MMS" Value="SMS/MMS"></asp:ListItem>
                                                <asp:ListItem Text="Print & Wallet (App)" Value="Print & Wallet (App)"></asp:ListItem>
                                                <asp:ListItem Text="Wallet(App)" Value="Wallet(App)"></asp:ListItem>
                                                <asp:ListItem Text="Print" Value="Print"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="text-align: right; padding-left: 10px;">
                                            Total(<asp:CheckBox ID="chkIsGST" runat="server" Text="With GST" AutoPostBack="True"
                                                OnCheckedChanged="chkIsGST_CheckedChanged" />):
                                        </td>
                                        <td>
                                            $<asp:Label ID="lblTotalGST" runat="server" Text="0"></asp:Label>
                                            <asp:Label ID="lblTotalCost" runat="server" Text="0" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr style="font-weight: bold;">
                                    </tr>
                                    <tr style="font-weight: bold;">
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="width: 450px">
                                <table>
                                    <tr>
                                        <td valign="top" style="width: 300px">
                                            <div>
                                                <span style="font-weight: bold; font-size: 20px;">Product List</span>
                                            </div>
                                            <div id="productContainer" style="height: 500px; overflow: auto; border: 1px solid #ebebeb">
                                                <eid:RadListView ID="productsListView" runat="server" ItemPlaceholderID="ProductsHolder"
                                                    DataKeyNames="Product.pro_Id">
                                                    <LayoutTemplate>
                                                        <asp:Panel ID="ProductsHolder" runat="server">
                                                        </asp:Panel>
                                                    </LayoutTemplate>
                                                    <ItemTemplate>
                                                        <div class="pdiv">
                                                            <table style="width: 100%">
                                                                <tr>
                                                                    <td style="width: 60px">
                                                                        <asp:Image ID="Image1" runat="server" Width="60" Height="60" ImageUrl='<%# HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + Eval("Product.ImageUrl")) %>' />
                                                                    </td>
                                                                    <td align="left">
                                                                        <table cellpadding="0" cellspacing="0">
                                                                            <tr>
                                                                                <td style="display: none">
                                                                                    Name:
                                                                                </td>
                                                                                <td style="padding-left: 5px;" class="pro_name">
                                                                                    <asp:Label ID="lblPName" Text='<%# Eval("Product.pro_Name") %>' runat="server"></asp:Label><br />
                                                                                    <br />
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="display: none">
                                                                                <td style="display: none">
                                                                                    Quantity:
                                                                                </td>
                                                                                <td style="padding-left: 5px;" class="pro_quantity">
                                                                                    <asp:Label ID="lblPQuantity" Text='<%# Eval("Quantity") %>' runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td style="display: none">
                                                                                    Price:
                                                                                </td>
                                                                                <td style="padding-left: 5px;" class="pro_price">
                                                                                    <asp:Label ID="lblPPrice" Text='<%#DataBinder.Eval(Container.DataItem, "Product.pro_Price", "{0:C}")%>'
                                                                                        runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="display: none">
                                                                                <td style="display: none">
                                                                                    Cost:
                                                                                </td>
                                                                                <td style="padding-left: 5px;" class="pro_cost">
                                                                                    <asp:Label ID="lblPCost" Text='<%# DataBinder.Eval(Container.DataItem, "Product.cost", "{0:C}")%>'
                                                                                        runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="display: none">
                                                                                <td style="display: none">
                                                                                    Serial No:
                                                                                </td>
                                                                                <td style="padding-left: 5px;" class="pro_no">
                                                                                    <asp:Label ID="lblPNo" Text='<%# Eval("Product.pro_no")%>' runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                            <tr style="display: none;">
                                                                                <td class="pro_id">
                                                                                    <asp:Label ID="lblPId" Text='<%# Eval("Product.pro_id")%>' runat="server"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td align="right" valign="bottom" style="width: 32px">
                                                                        <asp:ImageButton ID="ImageButton1" ImageUrl="../Images/plus-outline.png" AlternateText="Add To Invoice"
                                                                            runat="server" OnClick="btnAddProduct_Click" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </ItemTemplate>
                                                </eid:RadListView>
                                            </div>
                                        </td>
                                        <td valign="top" style="width: 150px">
                                            <%--<asp:DropDownList Height="30" Width="180" Style="float: left;" ID="dropCategory"
                                                runat="server" AutoPostBack="True" OnSelectedIndexChanged="dropCategory_SelectedIndexChanged"
                                                DataTextField="cat_Name" DataValueField="cat_Id">
                                            </asp:DropDownList>--%>
                                            <div>
                                                <span style="font-weight: bold; font-size: 20px;">Category</span>
                                            </div>
                                            <div id="categoryContainer" style="height: 500px; overflow: auto; border: 1px solid #ebebeb">
                                                <eid:RadPanelBar runat="server" ID="catPanelBar" DataValueField="cat_Id" Width="150"
                                                    Style="float: left;" DataFieldID="cat_Id" DataFieldParentID="BindableParentID"
                                                    DataTextField="cat_Name" ExpandMode="SingleExpandedItem" OnItemClick="CategorySelected"
                                                    Skin="MetroTouch">
                                                </eid:RadPanelBar>
                                            </div>
                                            <asp:DropDownList Height="30" Width="280" Style="float: left; margin-left: 10px;
                                                display: none;" ID="dropProduct" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dropProduct_SelectedIndexChanged">
                                            </asp:DropDownList>
                                            <asp:Panel ID="pnlAddProduct" runat="server" Style="float: left; margin-left: 10px;
                                                display: none;">
                                                <div style="float: left; margin-left: 10px; vertical-align: middle; padding-top: 7px;
                                                    display: none;">
                                                    Price:
                                                    <asp:Label ID="lblPrice" runat="server" CssClass="nonelabel"></asp:Label>
                                                    &nbsp;&nbsp; Quantity:
                                                    <asp:Label ID="lblQuantity" runat="server" CssClass="nonelabel"></asp:Label>
                                                    &nbsp;<asp:Label ID="lblCost" runat="server" CssClass="nonelabel"></asp:Label>
                                                    <asp:Label ID="lblSerialNo" runat="server" CssClass="nonelabel"></asp:Label>
                                                    &nbsp;<asp:Label ID="lblProductName" runat="server" CssClass="nonelabel"></asp:Label>
                                                    &nbsp;<asp:Label ID="lblProductID" runat="server" CssClass="nonelabel"></asp:Label>
                                                    &nbsp;
                                                    <input type="hidden" runat="server" id="input_price" clientidmode="Static" />
                                                    <input type="hidden" runat="server" id="input_quantity" clientidmode="Static" />
                                                    <input type="hidden" runat="server" id="input_cost" clientidmode="Static" />
                                                    <input type="hidden" runat="server" id="input_no" clientidmode="Static" />
                                                    <input type="hidden" runat="server" id="input_name" clientidmode="Static" />
                                                    <input type="hidden" runat="server" id="input_id" clientidmode="Static" />
                                                </div>
                                            </asp:Panel>
                                            <asp:Button ID="btnAddProduct" ClientIDMode="Static" runat="server" Text="Add" OnClick="btnAddProduct_Click"
                                                UseSubmitBehavior="false" class="btn btn-large btn-block btn-info" Width="50"
                                                Style="float: left; margin-left: 10px; display: none;" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <table style="width: 100%; display: none;">
                        <tr>
                            <td>
                                <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblIsGST" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblGSTSetting" runat="server" Visible="false"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>

        <eid:RadWindow runat="server" ID="InvoiceCancelRadWindow1" Skin="Metro" Overlay="true"
            Modal="true" Title="Cancel Invoice" VisibleStatusbar="false" AutoSize="true"
            Width="600" Height="400">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table class="form-table">
                            <tr>
                                <td class="form-label">
                                    Invoice No.:
                                </td>
                                <td>
                                    <div style="border: 1px solid #D1E4F6; height: 30px; padding-top: 9px; padding-left: 10px;">
                                        <asp:Label ID="lblCancelNo" runat="server" ClientIDMode="Static"></asp:Label></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Reason:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" Rows="6" Width="400"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <input type="hidden" id="lblCancelID" name="cancelid" runat="server" clientidmode="Static"
                                        value="" />
                                    <input type="button" id="submitCancelInvoice" value="Submit" class="btn btn-large btn-block btn-info" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </eid:RadWindow>
        <eid:RadWindow runat="server" ID="AddCustomerWindow" Skin="Metro" Overlay="true"
            Modal="true" Title="Add Customer" VisibleStatusbar="false" AutoSize="true" Width="600"
            Height="400">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table class="form-table" style="height: 400px">
                            <tr>
                                <td class="form-label">
                                    Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerName" runat="server" CssClass="customername" Width="200"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="Label2" runat="server" ForeColor="Red">*</asp:Label>
                                </td>
                            </tr>
                            <tr style="display: none" class="customertr">
                                <td class="form-label">
                                </td>
                                <td>
                                    <asp:Label ID="txtNameMessage" runat="server" ForeColor="Red" CssClass="custommessage"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Country:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerCountry" runat="server" Width="200" CssClass="customercountry"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Phone:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerPhone" runat="server" Width="200" CssClass="customerphone"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    MobilePhone:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerMobilePhone" runat="server" Width="200" CssClass="customermobilephone"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Email:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerEmail" runat="server" Width="200" CssClass="customeremail"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Address:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerAddress" runat="server" Width="200" CssClass="customeraddress"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Wallet ID:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerWalletID" runat="server" Width="200" CssClass="customerwalletid"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <input type="button" id="btnAddCustomerInfo" value="Submit" class="btn btn-large btn-block btn-info" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </eid:RadWindow>
        <asp:Button ID="resubmitBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="DoInvoice" ClientIDMode="Static"></asp:Button>
        <asp:Button ID="btnCancelInvoice" OnClick="Cancel" runat="server" CssClass="displaynone"
            ClientIDMode="Static" Text="Cancel" Width="0" Height="0"></asp:Button>
        <asp:Button ID="btnAddCustomer" OnClick="AddCustomer" runat="server" CssClass="displaynone"
            ClientIDMode="Static" Text="Cancel" Width="0" Height="0"></asp:Button>
        <asp:Button ID="btnUpdateCart" OnClick="UpdateInvoiceProduct" runat="server" CssClass="displaynone"
            ClientIDMode="Static" Text="Cancel" Width="0" Height="0"></asp:Button>
    </ContentTemplate>
</asp:UpdatePanel>
