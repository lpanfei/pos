﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCSetting.ascx.cs"
    Inherits="POS.TouchControls.WUCSetting" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    Sys.Application.add_load(settingLoad);
    function settingLoad() {
        $("#syncbtn").unbind("click");
        $("#syncbtn").click(function () {
            $('#btnUpload').trigger('click');

        });
        $("#testbtn").unbind("click");
        $("#testbtn").click(function () {
            $('#btnTestConnection').trigger('click');
        });

        $(".settingform>tbody>tr").each(function () {
            $(this).css("display", "none");
        });

        $(".discountsetting").unbind("click");
        $(".discountsetting").click(function () {
            $(".dvs").css("display", "table-row");
            $(".sdcs").css("display", "none");
            $(".sts").css("display", "none");
            $(".oss").css("display", "none");
            $("#selectedsettingsection").val("discountsetting");
            $(this).css("background-color", "#EFF3F4");
            $(".serversetting").css("background-color", "white");
            $(".syncsetting").css("background-color", "white");
            $(".othersetting").css("background-color", "white");
            $(".syncCommand").css("display", "none");
            if (parseInt($(".timeTable").css("marginLeft").replace("px", "")) < 0) {
                $(".timeTable").animate({
                    marginLeft: "+=620px"
                }, 500, function () {
                    //animation = false;
                });
            }
        });

        $(".serversetting").unbind("click");
        $(".serversetting").click(function () {
            $(".dvs").css("display", "none");
            $(".sdcs").css("display", "table-row");
            $(".sts").css("display", "none");
            $(".oss").css("display", "none");
            $("#selectedsettingsection").val("serversetting");
            $(this).css("background-color", "#EFF3F4");
            $(".discountsetting").css("background-color", "white");
            $(".syncsetting").css("background-color", "white");
            $(".othersetting").css("background-color", "white");
            $(".syncCommand").css("display", "block");
            if (parseInt($(".timeTable").css("marginLeft").replace("px", "")) < 0) {
                $(".timeTable").animate({
                    marginLeft: "+=620px"
                }, 500, function () {
                    //animation = false;
                });
            }
        });

        $(".syncsetting").unbind("click");
        $(".syncsetting").click(function () {
            $(".dvs").css("display", "none");
            $(".sdcs").css("display", "none");
            $(".sts").css("display", "table-row");
            $(".oss").css("display", "none");
            $("#selectedsettingsection").val("syncsetting");
            $(this).css("background-color", "#EFF3F4");
            $(".discountsetting").css("background-color", "white");
            $(".serversetting").css("background-color", "white");
            $(".othersetting").css("background-color", "white");
            $(".syncCommand").css("display", "none");
        });

        $(".othersetting").unbind("click");
        $(".othersetting").click(function () {
            $(".dvs").css("display", "none");
            $(".sdcs").css("display", "none");
            $(".sts").css("display", "none");
            $(".oss").css("display", "table-row");
            $("#selectedsettingsection").val("othersetting");
            $(this).css("background-color", "#EFF3F4");
            $(".discountsetting").css("background-color", "white");
            $(".serversetting").css("background-color", "white");
            $(".syncsetting").css("background-color", "white");
            $(".syncCommand").css("display", "none");
            if (parseInt($(".timeTable").css("marginLeft").replace("px", "")) < 0) {
                $(".timeTable").animate({
                    marginLeft: "+=620px"
                }, 500, function () {
                    //animation = false;
                });
            }
        });

        $("." + $("#selectedsettingsection").val()).trigger("click");

        $(".choosetimecontainer").unbind("click");
        $(".choosetimecontainer").click(function () {
            $(".timeTable").animate({
                marginLeft: "-=620px"
            }, 500, function () {
                //animation = false;
            });

            $(".timelist>tbody>tr").each(function () {
                $(this).find(".timechecked").css("display", "none");
                if ($(this).find(".choosetime").val() == $("#dropUploadTime").val()) {
                    $(this).find(".timechecked").css("display", "block");
                    //alert($(this).find(".timechecked").css("display"));
                }
            });
        });

        $(".back-tosyncsetting").unbind("click");
        $(".back-tosyncsetting").click(function () {
            $(".timeTable").animate({
                marginLeft: "+=620px"
            }, 500, function () {
                //animation = false;
            });
        });

        $(".selectedTime").text($("#dropUploadTime").val());

        $(".timelist>tbody>tr").unbind("click");

        $(".timelist>tbody>tr").click(function () {

            $("#dropUploadTime").val($(this).find(".choosetime").val());
            $(".timelist>tbody>tr").each(function () {
                $(this).find(".timechecked").css("display", "none");
            });
            $(this).find(".timechecked").css("display", "block");
            $(".selectedTime").text($(this).find(".choosetime").val());
            $(".timeTable").animate({
                marginLeft: "+=620px"
            }, 500, function () {
                //animation = false;
            });
        });
    }

    function openWindow(msg) {
        $("#lblLog").html(msg);
        var radwindow = $find('<%=SettingRadWindow1.ClientID %>');
        radwindow.show();
    }
</script>
<style>
    .settingform .tr-header
    {
        border-bottom: 1px solid #eee;
        font-size: 13px;
        font-weight: bold;
        display: none;
    }
    
    .settingform .tr-header td
    {
        padding-top: 25px;
        padding-bottom: 10px;
    }
    .displaynone
    {
        display: none;
    }
</style>
<asp:UpdatePanel ID="UpdatePanel44" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="mainheader">
            <div class="contextual">
                <div style="height: 10px; width: 85px">
                </div>
            </div>
            <div class="subheader">
                <h2 class="pagetitle icon-settings">
                    Settings</h2>
            </div>
        </div>
        <table style="border-spacing: 0px">
            <tr>
                <td>
                    <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                        width: 400px">
                        <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                            <table style="width: 100%">
                                <tr>
                                    <td style="width: 32px">
                                        <img src="images/icon_list.png" style="cursor: pointer" class="shownav" />
                                    </td>
                                    <td align="center">
                                        <span style="color: White; font-size: 15px; vertical-align: middle">Settings</span>
                                    </td>
                                    <td style="width: 32px">
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="width: 100%; height: 730px; overflow-y: auto">
                            <table class="list settingslist">
                                <tbody>
                                    <tr class="discountsetting">
                                        <td style="padding-left: 20px">
                                            Discount Value Setting
                                        </td>
                                    </tr>
                                    <tr class="serversetting">
                                        <td style="padding-left: 20px">
                                            Server Database Connection Setting
                                        </td>
                                    </tr>
                                    <tr class="syncsetting">
                                        <td style="padding-left: 20px">
                                            Sync Time Setting
                                        </td>
                                    </tr>
                                    <tr class="othersetting">
                                        <td style="padding-left: 20px">
                                            Other Settings
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </td>
                <td>
                    <div style="height: 765px; overflow-y: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                        border-left: 0px solid black; width: 620px; overflow-x: hidden">
                        <table style="width: 1240px; border-spacing: 0px" class="timeTable">
                            <tr>
                                <td valign="top" style="width: 620px">
                                    <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                        <table style="float: right;margin-right:15px;" class="syncCommand">
                                            <tr>
                                                <td>
                                                    <a href="#" id="syncbtn" style="margin-right: 10px; float: right" title="Immediate Sync">
                                                        <i class="c-icon-sync"></i></a>
                                                </td>
                                                <td style="width: 10px">
                                                </td>
                                                <td>
                                                    <a href="#" id="testbtn" style="margin-right: 0px; float: right" title="Testing Connection">
                                                        <i class="c-icon-test"></i></a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="padding-left: 20px; height: 690px; overflow: auto">
                                        <table class="form-table settingform" style="margin: 0 auto; width: 480px;">
                                            <tr class="tr-header">
                                                <td colspan="2" style="padding-top: 5px;">
                                                    <label>
                                                        Discount Value Settings</label>
                                                </td>
                                            </tr>
                                            <tr class="dvs">
                                                <td>
                                                    Retailer:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtRetailer" runat="server" Width="300"></asp:TextBox>
                                                    %
                                                </td>
                                            </tr>
                                            <tr class="dvs">
                                                <td>
                                                    Wholesaler:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtWholesaler" runat="server" Width="300"></asp:TextBox>
                                                    %
                                                </td>
                                            </tr>
                                            <tr class="dvs">
                                                <td>
                                                    Export:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtExport" runat="server" Width="300"></asp:TextBox>
                                                    %
                                                </td>
                                            </tr>
                                            <tr class="tr-header">
                                                <td colspan="2">
                                                    <label>
                                                        Server Settings</label>
                                                </td>
                                            </tr>
                                            <tr class="sdcs">
                                                <td>
                                                    Server Name:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtServerName" runat="server" Width="300"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="sdcs">
                                                <td>
                                                    Database Name:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtDatabaseName" runat="server" Width="300"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="sdcs">
                                                <td>
                                                    Username:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtUsername" runat="server" Style="float: left; margin-right: 10px;"
                                                        Width="300"></asp:TextBox>
                                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Button ID="btnUpload" runat="server" Text="Immediate Sync" Width="0px" Height="0px"
                                                                class="btn btn-large btn-block btn-info" OnClick="btnUpload_Click" ClientIDMode="Static" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr class="sdcs">
                                                <td>
                                                    Password:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPassword" runat="server" Style="float: left; margin-right: 10px;"
                                                        Width="300"></asp:TextBox>
                                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:Button ID="btnTestConnection" runat="server" Text="Testing Connection" Width="0px"
                                                                Height="0px" class="btn btn-large btn-block btn-info displaynone" OnClick="btnTestConnection_Click"
                                                                ClientIDMode="Static" />
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </td>
                                            </tr>
                                            <tr class="tr-header">
                                                <td style="padding-top: 0px">
                                                    <label>
                                                        Sync Time Settings</label>
                                                </td>
                                                <td align="right">
                                                    <%--<a href="#" id="addfiled"><i class="c-icon-add"></i></a>--%>
                                                </td>
                                            </tr>
                                            <tr class="sts">
                                                <td>
                                                    Sync Time:
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="dropUploadTime" runat="server" Width="300" CssClass="displaynone"
                                                        ClientIDMode="Static">
                                                    </asp:DropDownList>
                                                    <table style="width: 300px; height: 40px; border: 1px solid #e4e4e4; cursor: pointer"
                                                        class="choosetimecontainer">
                                                        <tr>
                                                            <td style="font-weight: bold" class="selectedTime">
                                                                00:00
                                                            </td>
                                                            <td style="width: 32px">
                                                                <img src="images/right.png" style="width: 26px; height: 26px; cursor: pointer" class="choosetimeicon" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="tr-header">
                                                <td>
                                                    <label>
                                                        Other Settings</label>
                                                </td>
                                                <td align="right">
                                                    <%--<a href="#" id="addfiled"><i class="c-icon-add"></i></a>--%>
                                                </td>
                                            </tr>
                                            <tr class="oss">
                                                <td>
                                                    Invoice Number Prefix:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtPrefix" runat="server" Width="300"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="oss">
                                                <td>
                                                    GST Amount:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtGST" runat="server" Width="300"></asp:TextBox>%
                                                </td>
                                            </tr>
                                            <tr class="oss">
                                                <td>
                                                    Invoice Header:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtInvoiceHeader" runat="server" Rows="6" Height="75" TextMode="MultiLine"
                                                        Width="300"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr class="oss">
                                                <td>
                                                    Invoice Footer:
                                                </td>
                                                <td>
                                                    <asp:TextBox ID="txtInvoiceFooter" runat="server" Rows="6" Height="75" TextMode="MultiLine"
                                                        Width="300"></asp:TextBox>
                                                </td>
                                            </tr>
                                        </table>
                                        <div style="margin-top: 20px">
                                            <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Button ID="btnSave" runat="server" Text="Save All" OnClick="btnSave_Click" class="btn btn-large btn-block btn-info btn-center"
                                                        Height="30" Width="80" />
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                </td>
                                <td valign="top" style="width: 620px">
                                    <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da;
                                        cursor: pointer" class="back-tosyncsetting">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="padding: 0px; width: 32px;">
                                                    <div style="height: 26px; width: 26px; font-size: 26px; color: White; font-weight: bold;
                                                        line-height: 26px; text-align: center;">
                                                        <</div>
                                                </td>
                                                <td align="left">
                                                    <span style="color: White; font-size: 15px; vertical-align: middle" class="title">Sync
                                                        Time Setting</span>
                                                </td>
                                                <td style="padding: 0px; width: 32px; padding-top: 3px;">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="width: 100%; height: 730px; overflow-y: auto">
                                    <asp:Repeater ID="timeItems" runat="server">
                                        <HeaderTemplate>
                                            <table class="list timelist">
                                                <tbody>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td align="left" class="choosetimerow" style="padding-left: 20px">
                                                    <asp:Label ID="lblValue" runat="server" Text='<%# Eval("Value")%>'></asp:Label>
                                                    <input type="hidden" value='<%# Eval("Value") %>' class="choosetime" />
                                                </td>
                                                <td style="width: 50px">
                                                    <img src="images/check.png" class="timechecked"  style="width:32px;height:32px" />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            </tbody> </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
        </table>
        <eid:RadWindow runat="server" ID="SettingRadWindow1" Skin="Metro" Overlay="true"
            Modal="true" Title="Sync Data" VisibleStatusbar="false" AutoSize="false" KeepInScreenBounds="true"
            Behaviors="Close" MinWidth="600" MinHeight="400">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:Label ID="lblLog" runat="server" ClientIDMode="Static" Text="Start data sync..."></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </eid:RadWindow>
        <input type="hidden" id="selectedsettingsection" runat="server" clientidmode="Static"
            value="discountsetting" />
    </ContentTemplate>
</asp:UpdatePanel>
