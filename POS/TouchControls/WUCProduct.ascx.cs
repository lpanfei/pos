﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using POS.Data;
using System.IO;

namespace POS.TouchControls
{
    public partial class WUCProduct : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Initialize();
            }
        }

        public override void Init()
        {
            Initialize();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int categoryId = Convert.ToInt32(dropCategory.SelectedValue);
            string name = string.Empty;
            string serialNo = string.Empty;
            switch (dropField.SelectedValue)
            {
                case "1":
                    name = txtKey.Text.Trim();
                    break;
                case "2":
                    serialNo = txtKey.Text.Trim();
                    break;
            }

            GlobalCache.RefreshProducts(categoryId, name, serialNo);
            Bind();
        }

        //protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        //{
        //    int pageSize = AspNetPager1.PageSize;
        //    int currentPage = AspNetPager1.CurrentPageIndex;
        //    rptList.DataSource = null;
        //    rptList.DataSource = GlobalCache.Products.TakeFrom((currentPage - 1) * pageSize, pageSize);
        //    rptList.DataBind();
        //}

        protected void Delete(object sender, EventArgs e)
        {
            
            int id = Convert.ToInt32(delproductid.Attributes["value"]);
            Product p = GlobalCache.Products.FirstOrDefault(p2 => p2.pro_Id == id);
            if (p.IsNew)
            {
                GlobalCache.Products.Remove(p);
            }
            else
            {
                p.IsDeleted = true;
                p.UserID = GlobalCache.CurrentUser.usr_ID;
                p.LastUpdatedTime = DateTime.Now;
                new POSService.POSService().UpdateProduct(p);
                GlobalCache.RefreshProducts(0, "", "");
                GlobalCache.RefreshInventories(0, "", "", 0);
            }
            Initialize();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                delproductid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "deleteProduct");
            }
            else if (e.CommandName == "ItemEdit")
            {
                int id = Convert.ToInt32(e.CommandArgument);
                List<Product> products = GlobalCache.Products;
                if (products != null)
                {
                    Product p = products.FirstOrDefault(p2 => p2.pro_Id == id);
                    if (p != null)
                    {
                        //pageform.Visible = true;
                        //pagelist.Visible = false;
                        ltlPageTitle.Text = "Products / Edit (" + p.pro_Name + ")";
                        addLink.Text = "Back";
                        lblId.Text = p.pro_Id.ToString();
                        lblServerID.Text = p.ServerID.ToString();
                        if (p.Category != null)
                        {
                            foreach (ListItem obj in dropCategory2.Items)
                            {
                                if (obj.Value == p.Category.cat_Id.ToString())
                                {
                                    obj.Selected = true;
                                }
                                else
                                {
                                    obj.Selected = false;
                                }
                            }
                        }
                        if (p.SupplierID != 0)
                        {
                            foreach (ListItem obj in dropSupplier.Items)
                            {
                                if (obj.Value == p.SupplierID.ToString())
                                {
                                    obj.Selected = true;
                                }
                                else
                                {
                                    obj.Selected = false;
                                }
                            }
                        }

                        txtName.Text = p.pro_Name;
                        txtNo.Text = p.pro_No;
                        txtPrice.Text = p.pro_price.ToString();
                        //txtQuantity.Text = p.pro_Quantity.ToString();
                        txtCost.Text = p.cost.ToString();
                        txtDescription.Text = p.pro_Description;
                        if (p.Pictures != null)
                        {
                            foreach (ProductPicture pp in p.Pictures)
                            {
                                pp.Url = HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + pp.Name);
                                pp.IsTemp = false;
                            }
                        }
                        Repeater1.DataSource = null;
                        Repeater1.DataSource = p.Pictures;
                        Repeater1.DataBind();

                        productPictureLightbox.DataSource = null;
                        productPictureLightbox.DataSource = p.Pictures;
                        productPictureLightbox.DataBind();

                        if (p.Fields != null)
                        {
                            List<Field> fields = new List<Field>();
                            int i = 1;
                            foreach (Field f in p.Fields)
                            {
                                fields.Add(new Field() { fie_Name = f.fie_Name, fie_Value = f.fie_Value, fie_Id = i });
                                i++;
                            }
                            rptDynamicFields.DataSource = null;
                            rptDynamicFields.DataSource = fields;
                            rptDynamicFields.DataBind();
                        }
                        foreach (RepeaterItem ri in rptRelateProduct.Items)
                        {
                            Label lblRelateID = ri.FindControl("lblID") as Label;
                            int pid = Convert.ToInt32(lblRelateID.Text);
                            if (p.pro_Id == pid)
                            {
                                ri.Visible = false;
                                break;
                            }
                        }
                        if (p.Relates != null)
                        {
                            foreach (RepeaterItem ri in rptRelateProduct.Items)
                            {
                                Label lblRelateID = ri.FindControl("lblID") as Label;
                                int pid = Convert.ToInt32(lblRelateID.Text);
                                if (p.Relates.Any(r => r.pro_Id == pid))
                                {
                                    CheckBox chkSelect = ri.FindControl("chkSelect") as CheckBox;
                                    chkSelect.Checked = true;
                                }
                            }
                        }
                    }
                }
            }
        }

        void Bind()
        {
            rptList.DataSource = null;
            //List<Product> list = new List<Product>();
           // GlobalCache.Products.ForEach(p => { list.Add(new Product() { pro_Name=p.pro_Name,pro_Id=p.pro_Id,pro_price=p.pro_price}); });
            rptList.DataSource = GlobalCache.Products.OrderByDescending(p=>p.pro_Id);
            rptList.DataBind();
        }

        void Initialize()
        {
            
            List<Product> plist = new List<Product>();

            GlobalCache.Products.ForEach(p => plist.Add(p));

            rptRelateProduct.DataSource = null;
            rptRelateProduct.DataSource = plist;
            rptRelateProduct.DataBind();

            rptDynamicFields.DataSource = null;
            rptDynamicFields.DataSource = new List<Field>();
            rptDynamicFields.DataBind();

            BindSuppliers();
            BindCategories();
            txtCost.Text = txtName.Text = txtNo.Text = txtPrice.Text = string.Empty;
            txtDescription.Text = string.Empty;
            ltlPageTitle.Text = "Products";
            addLink.Text = "Add";
            Repeater1.DataSource = null;
            Repeater1.DataBind();
            productPictureLightbox.DataSource = null;
            productPictureLightbox.DataBind();
            if (GlobalCache.Products != null && GlobalCache.Products.Any())
            {
                if (selectedpid.Attributes["value"] == "0" || !GlobalCache.Products.Any(p => p.pro_Id.ToString() == selectedpid.Attributes["value"]))
                {
                    selectedpid.Attributes["value"] = GlobalCache.Products.OrderByDescending(p => p.pro_Id).FirstOrDefault().pro_Id.ToString();
                }
            }
            ProductSelected(null, null);
            Bind();
        }

        protected void ProductSelected(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(selectedpid.Attributes["value"]);
            List<Product> products = GlobalCache.Products;
            if (products != null)
            {
                Product p = products.FirstOrDefault(p2 => p2.pro_Id == id);
                if (p != null)
                {
                    //btnDeleteThis.Visible = true;
                    //pageform.Visible = true;
                    //pagelist.Visible = false;
                    ltlPageTitle.Text = "Products / Edit (" + p.pro_Name + ")";
                    addLink.Text = "Back";
                    lblId.Text = p.pro_Id.ToString();
                    lblServerID.Text = p.ServerID.ToString();
                    if (p.Category != null)
                    {
                        foreach (ListItem obj in dropCategory2.Items)
                        {
                            if (obj.Value == p.Category.cat_Id.ToString())
                            {
                                obj.Selected = true;
                            }
                            else
                            {
                                obj.Selected = false;
                            }
                        }
                    }
                    if (p.SupplierID != 0)
                    {
                        foreach (ListItem obj in dropSupplier.Items)
                        {
                            if (obj.Value == p.SupplierID.ToString())
                            {
                                obj.Selected = true;
                            }
                            else
                            {
                                obj.Selected = false;
                            }
                        }
                    }

                    txtName.Text = p.pro_Name;
                    txtNo.Text = p.pro_No;
                    txtPrice.Text = Decimal.Round(p.pro_price, 2).ToString();
                    //txtQuantity.Text = p.pro_Quantity.ToString();
                    txtCost.Text = Decimal.Round(p.cost,2).ToString();
                    txtDescription.Text = p.pro_Description;
                    if (p.Pictures != null)
                    {
                        foreach (ProductPicture pp in p.Pictures)
                        {
                            pp.Url = HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + pp.Name);
                            pp.IsTemp = false;
                        }
                    }
                    Repeater1.DataSource = null;
                    Repeater1.DataSource = p.Pictures;
                    Repeater1.DataBind();

                    productPictureLightbox.DataSource = null;
                    productPictureLightbox.DataSource = p.Pictures;
                    productPictureLightbox.DataBind();

                    if (p.Fields != null)
                    {
                        List<Field> fields = new List<Field>();
                        int i = 1;
                        foreach (Field f in p.Fields)
                        {
                            fields.Add(new Field() { fie_Name = f.fie_Name, fie_Value = f.fie_Value, fie_Id = i });
                            i++;
                        }
                        rptDynamicFields.DataSource = null;
                        rptDynamicFields.DataSource = fields;
                        rptDynamicFields.DataBind();
                    }
                    foreach (RepeaterItem ri in rptRelateProduct.Items)
                    {
                        Label lblRelateID = ri.FindControl("lblID") as Label;
                        int pid = Convert.ToInt32(lblRelateID.Text);
                        if (p.pro_Id == pid)
                        {
                            ri.Visible = false;
                            break;
                        }
                    }
                    if (p.Relates != null)
                    {
                        foreach (RepeaterItem ri in rptRelateProduct.Items)
                        {
                            Label lblRelateID = ri.FindControl("lblID") as Label;
                            int pid = Convert.ToInt32(lblRelateID.Text);
                            if (p.Relates.Any(r => r.pro_Id == pid))
                            {
                                CheckBox chkSelect = ri.FindControl("chkSelect") as CheckBox;
                                chkSelect.Checked = true;
                            }
                        }
                    }
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            ProductSelected(null, null);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            #region validations
            if (string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                ShowError("Product Name is required!");
                return;
            }
            if (dropCategory2.SelectedIndex < 0)
            {
                ShowError("Category is required!");
                return;
            }

            if (dropSupplier.SelectedIndex < 0)
            {
                ShowError("Supplier is required!");
                return;
            }
            try
            {
                if (string.IsNullOrEmpty(txtPrice.Text.Trim()))
                {
                    txtPrice.Text = "0";
                }
                decimal price = Convert.ToDecimal(txtPrice.Text.Trim());
            }
            catch
            {
                ShowError("Invalid character in Price field. Only number is allowed!");
                return;
            }
            try
            {
                if (string.IsNullOrEmpty(txtCost.Text.Trim()))
                {
                    txtCost.Text = "0";
                }
                decimal cost = Convert.ToDecimal(txtCost.Text.Trim());
            }
            catch
            {
                ShowError("Invalid character in Cost field. Only number is allowed!");
                return;
            }
            #endregion

            #region upload pictures
            int pid = Convert.ToInt32(lblId.Text);
            List<string> pictures = new List<string>();
            List<ProductPicture> pps = new List<ProductPicture>();

            foreach (RepeaterItem item in Repeater1.Items)
            {
                ProductPicture pp = new ProductPicture()
                {
                    ProductID = pid,
                    Name = (item.FindControl("lblName") as Label).Text,
                    Url = (item.FindControl("img") as Image).ImageUrl,
                    IsTemp = (item.FindControl("lblTemp") as Label).Text.Equals("true", StringComparison.CurrentCultureIgnoreCase) ? true : false
                };
                if (!pp.IsTemp)
                {
                    pictures.Add((item.FindControl("lblName") as Label).Text);
                }
                pps.Add(pp);
            }

            List<string> delpictures = new List<string>();

            if (pid > 0)
            {
                List<Product> products = GlobalCache.Products;
                if (products != null)
                {
                    Product prod = products.FirstOrDefault(p2 => p2.pro_Id == pid);
                    if (prod != null && prod.Pictures != null)
                    {
                        foreach (ProductPicture pp in prod.Pictures)
                        {
                            if (!pictures.Any(pic => pic == pp.Name))
                            {
                                delpictures.Add(pp.Name);
                            }
                        }
                    }
                }
            }

            string upload = Server.MapPath("~/Upload");
            string temp = Server.MapPath("~/Upload/Temp");

            if (pps.Any(p2 => p2.IsTemp))
            {
                foreach (ProductPicture file in pps.Where(p2 => p2.IsTemp))
                {
                    string filename = new string(file.Name.Where(c => Char.IsLetterOrDigit(c) || c == '.').ToArray());
                    filename = DateTime.Now.ToString("yyyMMddHHmmff") + filename;
                    if (File.Exists(Path.Combine(temp, file.Name)))
                    {
                        File.Copy(Path.Combine(temp, file.Name), Path.Combine(upload, filename), true);
                        File.Delete(Path.Combine(temp, file.Name));
                    }
                    pictures.Add(filename);
                }
            }
            #endregion

            #region create/update product
            Product p = new Product();
            p.pro_Id = Convert.ToInt32(lblId.Text);
            
            p.ServerID = Convert.ToInt32(lblServerID.Text);
            p.Category = GlobalCache.Categories.FirstOrDefault(c => c.cat_Id == Convert.ToInt32(dropCategory2.SelectedValue));
            p.SupplierID = Convert.ToInt32(this.dropSupplier.SelectedValue);
            p.Supplier = GlobalCache.Suppliers.FirstOrDefault(s => s.ID == p.SupplierID);
            p.pro_Name = txtName.Text.Trim();
            p.pro_No = txtNo.Text.Trim();
            p.pro_ClientID = this.UserClientID;
            p.UserID = GlobalCache.CurrentUser.usr_ID;
            p.pro_Description = txtDescription.Text.Trim();
            if (!string.IsNullOrEmpty(txtPrice.Text.Trim()))
            {
                p.pro_price = Convert.ToDecimal(txtPrice.Text.Trim());
            }

            if (!string.IsNullOrEmpty(txtCost.Text.Trim()))
            {
                p.cost = Convert.ToDecimal(txtCost.Text.Trim());
            }

            //if (!string.IsNullOrEmpty(txtQuantity.Text.Trim()))
            //{
            //    p.pro_Quantity = Convert.ToInt32(txtQuantity.Text.Trim());
            //}
            p.Relates = new List<Product>();
            foreach (RepeaterItem ri in rptRelateProduct.Items)
            {
                CheckBox chkSelect = ri.FindControl("chkSelect") as CheckBox;
                Label lblRelateID = ri.FindControl("lblID") as Label;
                if (chkSelect.Checked)
                {
                    //new Product() { pro_Id = Convert.ToInt32(lblRelateID.Text.Trim()) }
                    p.Relates.Add(GlobalCache.Products.FirstOrDefault(pr => pr.pro_Id == Convert.ToInt32(lblRelateID.Text.Trim())));
                }
            }
            p.Fields = new List<Field>();
            foreach (RepeaterItem ri in rptDynamicFields.Items)
            {
                string name = (ri.FindControl("lblName") as TextBox).Text.Trim();
                string value = (ri.FindControl("txtValue") as TextBox).Text.Trim();
                p.Fields.Add(new Field() { fie_Name = name, fie_Value = value });
            }
            p.Pictures = new List<ProductPicture>();
            foreach (string s in pictures)
            {
                p.Pictures.Add(new ProductPicture() { Name = s });
            }
            p.LastUpdatedTime = DateTime.Now;
            try
            {
                POSService.POSService pos = new POSService.POSService();
                if (GlobalCache.Products.Any(pppp => pppp.pro_Id == p.pro_Id) && GlobalCache.Products.FirstOrDefault(pppp => pppp.pro_Id == p.pro_Id).IsNew)
                {
                    p.pro_Id = 0;
                }
                pos.AddProduct(p);
                foreach (string s in delpictures)
                {
                    if (File.Exists(Path.Combine(upload, s)))
                    {
                        File.Delete(Path.Combine(upload, s));
                    }
                }
                GlobalCache.RefreshProducts(0, "", "");
                GlobalCache.RefreshInventories(0, "", "", 0);
            }
            catch (Exception ex)
            {
                DeleteUnusedPictures(pictures);
                throw ex;
            }
            #endregion
            Initialize();
        }

        void DeleteUnusedPictures(List<string> pictures)
        {
            try
            {
                string path = Server.MapPath("~/Upload");
                if (!Directory.Exists(path))
                {
                    return;
                }

                foreach (string filename in pictures)
                {
                    if (File.Exists(Path.Combine(path, filename)))
                    {
                        File.Delete(Path.Combine(path, filename));
                    }
                }
            }
            catch
            {
            }

        }


        protected void rptDynamicFields_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                List<Field> fields = new List<Field>();
                int index = Convert.ToInt32(e.CommandArgument);
                int i = 1;
                bool check = false;
                foreach (RepeaterItem ri in rptDynamicFields.Items)
                {
                    if (!check && index == i)
                    {
                        check = true;
                        continue;
                    }
                    string name = (ri.FindControl("lblName") as TextBox).Text.Trim();
                    string value = (ri.FindControl("txtValue") as TextBox).Text.Trim();
                    fields.Add(new Field() { fie_Name = name, fie_Value = value, fie_Id = i });
                    i++;
                }
                rptDynamicFields.DataSource = null;
                rptDynamicFields.DataSource = fields;
                rptDynamicFields.DataBind();
            }
        }

        protected void dlPictureList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            List<ProductPicture> pps = new List<ProductPicture>();
            if (e.CommandName == "ItemDel")
            {
                foreach (RepeaterItem item in Repeater1.Items)
                {
                    if (item != e.Item)
                    {
                        pps.Add(new ProductPicture() { Name = (item.FindControl("lblName") as Label).Text, Url = (item.FindControl("img") as Image).ImageUrl });
                    }
                }

                if ((e.Item.FindControl("lblTemp") as Label).Text.Equals("true", StringComparison.CurrentCultureIgnoreCase))
                {
                    string path = Server.MapPath("~/Upload/Temp");
                    string file = Path.Combine(path, (e.Item.FindControl("lblName") as Label).Text);
                    if (File.Exists(file))
                    {
                        File.Delete(file);
                    }
                }
            }
            Repeater1.DataSource = null;
            Repeater1.DataSource = pps;
            Repeater1.DataBind();

            productPictureLightbox.DataSource = null;
            productPictureLightbox.DataSource = pps;
            productPictureLightbox.DataBind();
        }

        protected void AddDynamicFields(object sender, EventArgs e)
        {
            List<Field> fields = new List<Field>();
            int i = 1;
            foreach (RepeaterItem ri in rptDynamicFields.Items)
            {
                string name = (ri.FindControl("lblName") as TextBox).Text.Trim();
                string value = (ri.FindControl("txtValue") as TextBox).Text.Trim();
                fields.Add(new Field() { fie_Name = name, fie_Value = value, fie_Id = i });
                i++;
            }
            fields.Add(new Field() { fie_Id = i });
            rptDynamicFields.DataSource = null;
            rptDynamicFields.DataSource = fields;
            rptDynamicFields.DataBind();
        }

        void BindSuppliers()
        {
            dropSupplier.DataSource = GlobalCache.Suppliers;
            dropSupplier.DataBind();
        }

        void BindCategories()
        {
            List<Category> cats = GlobalCache.Categories.GetHiarachy();
            List<Category> cats2 = new List<Category>();
            cats.ForEach(c => cats2.Add(c));
            cats.Insert(0, new Category() { cat_Id = 0, cat_Name = "-- All Category --" });
            dropCategory.DataSource = cats;
            dropCategory.DataBind();
            dropCategory2.DataSource = cats2;
            dropCategory2.DataBind();
        }

        protected void AddProduct(object sender, EventArgs e)
        {
            //if (addLink.Text == "Add")
            //{
                lblId.Text = "0";
                lblServerID.Text = "0";
               // pageform.Visible = true;
               // pagelist.Visible = false;
                BindSuppliers();
                BindCategories();
                txtCost.Text = txtName.Text = txtNo.Text = txtPrice.Text = string.Empty;
                txtDescription.Text = string.Empty;
                ltlPageTitle.Text = "Products / Add";
                addLink.Text = "Back";
                
                Repeater1.DataSource = null;
                Repeater1.DataBind();
                productPictureLightbox.DataSource = null;
                productPictureLightbox.DataBind();
                rptList.DataSource = null;
                Product product = new Product() { pro_Id = GlobalCache.Products.Max(p => p.pro_Id) + 1, pro_price = 0, pro_Name = "New Product",IsNew=true };
                GlobalCache.Products.Add(product);
                selectedpid.Attributes["value"] = product.pro_Id.ToString();
                rptList.DataSource = GlobalCache.Products.OrderByDescending(p => p.pro_Id);
                rptList.DataBind();
                ProductSelected(null, null);
            //}
            //else
            //{
            //    Initialize();
            //}
        }

        private bool CheckExtension(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return false;
            }

            bool flag = false;
            string extension = fileName.Substring(fileName.LastIndexOf('.')).ToLower();
            string[] allowExtension = { ".jpg", ".jpeg", ".gif", ".png", ".bmp" };

            for (int i = 0; i < allowExtension.Length; i++)
            {
                if (allowExtension[i] == extension)
                {
                    flag = true;
                    break;
                }
            }

            return flag;
        }

        protected void ImageUploaded(object sender, FileUploadedEventArgs e)
        {
            int pid = Convert.ToInt32(lblId.Text);
            List<ProductPicture> pps = new List<ProductPicture>();

            foreach (RepeaterItem item in this.Repeater1.Items)
            {
                pps.Add(new ProductPicture()
                {
                    ProductID = pid,
                    Name = (item.FindControl("lblName") as Label).Text,
                    Url = (item.FindControl("img") as Image).ImageUrl,
                    IsTemp = (item.FindControl("lblTemp") as Label).Text.Equals("true", StringComparison.CurrentCultureIgnoreCase) ? true : false
                });
            }

            pps.Add(new ProductPicture()
            {
                ProductID = pid,
                Name = e.File.FileName,
                Url = HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/Temp/" + e.File.FileName),
                IsTemp = true
            });


            Repeater1.DataSource = null;
            Repeater1.DataSource = pps;
            Repeater1.DataBind();

            productPictureLightbox.DataSource = null;
            productPictureLightbox.DataSource = pps;
            productPictureLightbox.DataBind();
        }
    }
}