﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS.TouchControls
{
    public partial class WUCLog : ControlBase
    {
        List<Log> Logs
        {
            get
            {
                return GlobalCache.Logs;
            }
        }

        public override void Init()
        {
            Bind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Bind();
            }
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = Logs;
            rptList.DataBind();
        }

    }
}