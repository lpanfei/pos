﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS.TouchControls
{
    public partial class WUCWarranty : ControlBase
    {
        List<Warranty> Warranties
        {
            get
            {
                return GlobalCache.Warranties;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //InitializeDropdowns();
                //Bind();
               // txtDate.Attributes.Add("onFocus", "WdatePicker({lang:'en',dateFmt:'dd-MM-yyyy'})");
            }
        }

        public override void Init()
        {
            InitializeDropdowns();
            Bind();
        }

        void InitializeDropdowns()
        {
            //POSService.POSService service = new POSService.POSService();
            GlobalCache.RefreshInvoices(0, "", null, null, false);
            dropInvoice.DataSource = GlobalCache.Invoices;
            dropInvoice.DataBind();
            if (GlobalCache.Invoices.Any())
            {
                dropInvoice.SelectedIndex = 0;
                InvoiceChanged(null, null);
            }
        }

        protected void InvoiceChanged(object sender, EventArgs args)
        {
            POSService.POSService service = new POSService.POSService();
            dropWarrantyProduct.DataSource = service.GetInvoiceProducts(Convert.ToInt32(dropInvoice.SelectedValue));
            dropWarrantyProduct.DataBind();
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = Warranties.OrderByDescending(w=>w.ID);
            rptList.DataBind();
            if (GlobalCache.Warranties != null && GlobalCache.Warranties.Any())
            {
                if (selectedwarrantyid.Attributes["value"] == "0" || !GlobalCache.Warranties.Any(p => p.ID.ToString() == selectedwarrantyid.Attributes["value"]))
                {
                    selectedwarrantyid.Attributes["value"] = GlobalCache.Warranties.OrderByDescending(c => c.ID).FirstOrDefault().ID.ToString();
                }
            }
            WarrantySelected(null, null);
        }

        protected void AddWarranty(object sender, EventArgs e)
        {
            ltlPageTitle.Text = "Warranty / Add";
            dropInvoice.SelectedIndex = 0;
            //InvoiceChanged(null, null);
            txtComment.Text = "";
            //txtDate.Text = "";
            txtDate.SelectedDate = DateTime.Now; ;
            txtQty.Text = "";
            txtResolution.Text = "";
            lblId.Text = "0";
            inpWarrantyProductID.Attributes["value"] = "-1";
            Warranty supplier = new Warranty() { ID = GlobalCache.Warranties.Max(c => c.ID) + 1, ProductName = "New Warranty", IsNew = true,Date=DateTime.Now };
            GlobalCache.Warranties.Add(supplier);
            selectedwarrantyid.Attributes["value"] = supplier.ID.ToString();
            Bind();
        }

        protected void WarrantySelected(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(selectedwarrantyid.Attributes["value"]);
            Warranty warranty=GlobalCache.Warranties.FirstOrDefault(w=>w.ID==id);
            if (warranty == null)
                return;
            ltlPageTitle.Text = "Warranty / Edit";
            if (warranty.IsNew)
            {
                if (dropInvoice.Items.Count > 0)
                {
                    dropInvoice.SelectedIndex = 0;
                }
            }
            else
            {
                dropInvoice.SelectedValue = warranty.InvoiceID.ToString();
            }
            txtComment.Text = warranty.Comment;
            try
            {
                txtDate.SelectedDate = warranty.Date;
            }
            catch
            {
                txtDate.SelectedDate = null;
            }
            txtQty.Text = warranty.Quantity.ToString();
            txtResolution.Text = warranty.Resolution;
            lblId.Text = id.ToString();
            inpWarrantyProductID.Attributes["value"] = warranty.ProductID.ToString();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            WarrantySelected(null, null);
        }

        protected void BackToList(object sender, EventArgs e)
        {
            ltlPageTitle.Text = "Warranty List";
            inpWarrantyProductID.Attributes["value"] = "-1";
            Bind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            POSService.POSService service = new POSService.POSService();
            Warranty w = new Warranty()
            {
                InvoiceID = Convert.ToInt32(dropInvoice.SelectedValue),
                ID = Convert.ToInt32(lblId.Text),
                ProductID = Convert.ToInt32(dropWarrantyProduct.SelectedValue),
                Comment = txtComment.Text,
                Quantity = string.IsNullOrEmpty(txtQty.Text) ? 0 : Convert.ToInt32(txtQty.Text),
                Resolution = txtResolution.Text,
                ClientID = this.UserClientID,
                UserID = GlobalCache.CurrentUser.usr_ID,
                StoreID = this.UserStoreID
            };
            w.Invoice = GlobalCache.Invoices.FirstOrDefault(i => i.inv_Id == w.InvoiceID);
            w.Product = GlobalCache.Products.FirstOrDefault(p => p.pro_Id == w.ProductID);
            Warranty exw = GlobalCache.Warranties.FirstOrDefault(ww => ww.ID == w.ID&&ww.IsNew);
            if (exw != null)
            {
                w.ID = 0;
            }
            if (w.ID > 0)
            {
                w.ServerID = Warranties.FirstOrDefault(wa => wa.ID == w.ID).ServerID;
            }
            w.Date = txtDate.SelectedDate;
            //if (string.IsNullOrEmpty(txtDate.Text))
            //{
            //    w.Date = null;
            //}
            //else
            //{
            //    try
            //    {
            //        w.Date = DateTime.Parse(txtDate.Text);
            //    }
            //    catch
            //    {
            //        w.Date = GetDateTime(txtDate.Text);
            //    }
            //}
            w.LastUpdatedTime = DateTime.Now;
            service.AddWarranty(w);
            GlobalCache.RefreshWarranties();
            BackToList(null, null);
        }

        DateTime GetDateTime(string s)
        {
            string[] str = s.Split('-');
            int year = Convert.ToInt32(str[2]);
            int month = Convert.ToInt32(str[1]);
            int day = Convert.ToInt32(str[0]);
            return new DateTime(year, month, day, 0, 0, 0);
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemEdit")
            {
                RepeaterItem item = e.Item;
                formbuttons.Visible = true;
                listbuttons.Visible = false;
                formpanel.Visible = true;
                listpanel.Visible = false;
                ltlPageTitle.Text = "Warranty / Edit";
                dropInvoice.SelectedValue = (item.FindControl("lblInvoiceID") as Label).Text;
                txtComment.Text = (item.FindControl("lblComment") as Label).Text;
                //txtDate.Text = (item.FindControl("lblDate") as Label).Text;
                txtQty.Text = (item.FindControl("lblQty") as Label).Text;
                txtResolution.Text = (item.FindControl("lblResolution") as Label).Text;
                lblId.Text = (item.FindControl("lblId") as Label).Text;
                inpWarrantyProductID.Attributes["value"] = (item.FindControl("lblProductID") as Label).Text;
            }
            else if (e.CommandName == "ItemDel")
            {
                delwarrantyid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "deleteWarranty");
            }
        }

        protected void Delete(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delwarrantyid.Attributes["value"]);
            POSService.POSService service = new POSService.POSService();
            Warranty w = Warranties.FirstOrDefault(wa => wa.ID == id);
            if (w.IsNew)
            {
                GlobalCache.Warranties.Remove(w);
            }
            else
            {
                w.IsDeleted = true;
                w.UserID = GlobalCache.CurrentUser.usr_ID;
                w.LastUpdatedTime = DateTime.Now;
                service.UpdateWarranty(w);
                GlobalCache.RefreshWarranties();
            }
            //service.DeleteWarranty(id);
            Bind();
        }

    }
}