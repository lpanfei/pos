﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Configuration;
using System.Text;
using System.Data;
using POS.Data;
using System.Data.SqlClient;

namespace POS.TouchControls
{
    public partial class WUCSetting : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               // Initialize();
            }
        }

        public override void Init()
        {
            Initialize();
        }

        void Initialize()
        {
            List<SyncTimeWrapper> list = new List<SyncTimeWrapper>();
            for (int i = 0; i < 24; i++)
            {
                ListItem li = new ListItem();
                li.Text = Convert.ToDecimal(i).ToString("00") + ":00";
                li.Value = li.Text;
                dropUploadTime.Items.Add(li);
                list.Add(new SyncTimeWrapper() { Value=Convert.ToDecimal(i).ToString("00") + ":00"});
            }
            timeItems.DataSource = null;
            timeItems.DataSource = list;
            timeItems.DataBind();

            if (GlobalCache.Setting != null)
            {
                txtRetailer.Text = GlobalCache.Setting.RetailerDiscount.ToString();
                txtWholesaler.Text = GlobalCache.Setting.WholesalerDiscount.ToString();
                txtExport.Text = GlobalCache.Setting.ExportDiscount.ToString();
                txtServerName.Text = GlobalCache.Setting.ServerName;
                txtDatabaseName.Text = GlobalCache.Setting.DatabaseName;
                txtPassword.Text = GlobalCache.Setting.Password;
                txtUsername.Text = GlobalCache.Setting.Username;
                txtInvoiceFooter.Text = GlobalCache.Setting.InvoiceFooter;
                txtInvoiceHeader.Text = GlobalCache.Setting.InvoiceHeader;
                txtGST.Text = GlobalCache.Setting.GST.ToString();
                txtPrefix.Text = GlobalCache.Setting.Prefix;
                dropUploadTime.SelectedValue = GlobalCache.Setting.UploadTime;
                if (!string.IsNullOrEmpty(GlobalCache.Setting.ServerName) &&
                            !string.IsNullOrEmpty(GlobalCache.Setting.DatabaseName) &&
                            !string.IsNullOrEmpty(GlobalCache.Setting.Username) &&
                            !string.IsNullOrEmpty(GlobalCache.Setting.Password))
                {
                    string conn = "Data Source=" + GlobalCache.Setting.ServerName + ";Initial Catalog=" + GlobalCache.Setting.DatabaseName + ";Integrated Security=false;User Id=" + GlobalCache.Setting.Username + "; Password=" + GlobalCache.Setting.Password;
                    UploadConfig.ConnectionString = conn;
                    UploadConfig.IsOn = true;
                }
                else
                {
                    UploadConfig.IsOn = false;
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            new Setting()
            {
                RetailerDiscount = Convert.ToDecimal(string.IsNullOrEmpty(txtRetailer.Text.Trim()) ? "0" : txtRetailer.Text.Trim()),
                WholesalerDiscount = Convert.ToDecimal(string.IsNullOrEmpty(txtWholesaler.Text.Trim()) ? "0" : txtWholesaler.Text.Trim()),
                ExportDiscount = Convert.ToDecimal(string.IsNullOrEmpty(txtExport.Text.Trim()) ? "0" : txtExport.Text.Trim()),
                ServerName = txtServerName.Text,
                Password = txtPassword.Text,
                Username = txtUsername.Text,
                Prefix = txtPrefix.Text,
                GST = Convert.ToInt32(string.IsNullOrEmpty(this.txtGST.Text.Trim()) ? "0" : txtGST.Text.Trim()),
                UploadTime = dropUploadTime.SelectedValue,
                DatabaseName = txtDatabaseName.Text,
                InvoiceFooter = txtInvoiceFooter.Text,
                InvoiceHeader = txtInvoiceHeader.Text,
                ClientID = this.UserClientID,
                StoreID = this.UserStoreID
            }.Update();
            GlobalCache.RefreshSetting();
            string serverName = txtServerName.Text.Trim();
            string databaseName = txtDatabaseName.Text.Trim();
            string username = txtUsername.Text.Trim();
            string password = txtPassword.Text.Trim();
            string connectionString = "server=" + serverName + ";database=" + databaseName + ";uid=" + username + ";pwd=" + password;

            SqlConnection conn = new SqlConnection(connectionString);
            bool isSuccess = true;
            try
            {
                conn.Open();
            }
            catch
            {
                isSuccess = false;
            }
            finally
            {

                if (conn.State == ConnectionState.Open)
                {
                    isSuccess = true;
                }

                conn.Close();
                conn.Dispose();
            }
            UploadConfig.IsOn = isSuccess;
            ShowSuccess("Settings are saved successfully!");

        }

        protected void btnTestConnection_Click(object sender, EventArgs e)
        {
            string serverName = txtServerName.Text.Trim();
            string databaseName = txtDatabaseName.Text.Trim();
            string username = txtUsername.Text.Trim();
            string password = txtPassword.Text.Trim();
            string connectionString = "server=" + serverName + ";database=" + databaseName + ";uid=" + username + ";pwd=" + password;

            SqlConnection conn = new SqlConnection(connectionString);
            bool isSuccess = true;
            try
            {
                conn.Open();
            }
            catch
            {
                isSuccess = false;
            }
            finally
            {

                if (conn.State == ConnectionState.Open)
                {
                    isSuccess = true;
                }

                conn.Close();
                conn.Dispose();
            }
            if (isSuccess)
            {
                ShowSuccess("Connection is successful!");
            }
            else
            {
                ShowError("Connection is failed!");
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            int count = HttpContext.Current.Cache.Count;
            for (int i = 0; i < count; i++)
            {
                HttpContext.Current.Cache.Remove("Sync" + i.ToString());
            }
            PageDataSync sync = new PageDataSync(WebConfigurationManager.ConnectionStrings["DBConnection"].ConnectionString, UploadConfig.ConnectionString, this.UserClientID, this.UserStoreID);
            sync.ProcessChanged += new PageDataSync.SyncEventHandler(sync_ProcessChanged);
            sync.DoSync();
            GlobalCache.RefreshAll();
            ScriptManager.RegisterStartupScript(btnUpload, GetType(), "openwindow", "openWindow('" + GetStatus() + "');", true);
        }

        void sync_ProcessChanged(object sender, SyncEventArgs args)
        {
            int count = HttpContext.Current.Cache.Count;
            if (HttpContext.Current.Cache.Get("Sync" + count.ToString()) == null)
            {
                HttpContext.Current.Cache.Add("Sync" + count.ToString(), args.Message, null, DateTime.Now.AddMinutes(30), TimeSpan.Zero, System.Web.Caching.CacheItemPriority.Normal, null);
            }
        }

        public string GetStatus()
        {
            StringBuilder sb = new StringBuilder();
            int count = HttpContext.Current.Cache.Count;
            for (int i = 0; i < count; i++)
            {
                object obj = HttpContext.Current.Cache.Get("Sync" + i.ToString());
                if (obj != null)
                {
                    sb.Append(obj.ToString());
                    sb.Append("<br/>");
                }
            }
            return sb.ToString();
        }
    }

    public class SyncTimeWrapper
    {
        public string Value { get; set; }
    }
}