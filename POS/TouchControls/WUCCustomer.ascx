﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCCustomer.ascx.cs" Inherits="POS.TouchControls.WUCCustomer" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
    <script type="text/javascript">
        var previouscustomer = null;
        Sys.Application.add_load(customerLoad);
        function customerLoad(sender, args) {
                $(".customerlist tr").unbind("click");
                $(".customerlist tr").click(function () {

                    $("#selectedcustomerid").val($(this).find(".customerid").eq(0).val());

                    $("#btnCustomerSelected").trigger("click");
                }
                );

                $(".customerlist tr").each(function () {
                    if ($(this).find(".customerid").eq(0).val() == $("#selectedcustomerid").val()) {
                        $(this).css("background-color", "#EFF3F4");
                    }
                });

                $(".addcustomericon").unbind("click");
                $(".addcustomericon").click(function () {
                    $(".btnAddCustomer").trigger("click");
                    $("#btnDeleteCustomer").css("display", "none");
                });

                $("#btnDeleteCustomer").unbind("click");
                $("#btnDeleteCustomer").click(function () {
                    $("#delcustomerid").val($("#selectedcustomerid").val());
                    radconfirm("Are you sure to delete the item?", deleteCustomer, 300, 100, null, "Delete Customer", "Images/Confirm.png");
                });
        }
        function deleteCustomer(sender) {
            if (sender) {
                $("#delCustomerBtn").trigger("click");
            } else {
                $("#delcustomerid").val("-1");
            }
        }
    </script>
    <style type="text/css">
        input[type=submit]
        {
            cursor: pointer;
        }
         .displaynone
        {
            display:none;
        }
    </style>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="mainheader">
                <div class="contextual">
                    <asp:Panel ID="listbuttons" runat="server">
                        <div style="height: 10px; width: 75px">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add btnAddCustomer" ID="btnAddCustomer"
                                Text="Add" OnClick="AddCustomer"/>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="formbuttons" runat="server">
                        <div style="height: 10px; width: 75px">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                                Text="Back" OnClick="BackToList" />
                        </div>
                    </asp:Panel>
                </div>
                <div class="subheader">
                    <h2 class="icon-customer pagetitle">
                        <asp:Literal ID="ltlPageTitle" Text="Customers" runat="server"></asp:Literal></h2>
                </div>
            </div>
 <table style="border-spacing:0px">
            <tr>
                <td>
                <asp:Panel runat="server" ID="listpanel">
                <div style="height:765px;overflow:auto;border:1px solid #e4e4e4;border-radius:15px;width:400px">
                        <div style="height:35px;width:100%;border-bottom:1px solid #e4e4e4;background-color:#25a0da">
                        <table style="width:100%">
                        <tr>
                        <td style="width:32px"><img src="images/icon_list.png" style="cursor:pointer" class="shownav" />
                        </td>
                        <td align="center"><span style="color:White;font-size:15px;vertical-align:middle">Customers</span>
                        </td>
                        <td style="width:32px">
                        <img src="images/add.png" style="width:20px;height:20px;cursor:pointer" class="addcustomericon" />
                        </td>
                        </tr>
                        </table>
                        
                        
                        </div>
                         <div style="width: 100%; height: 730px; overflow-y: auto">
                    <asp:Repeater ID="rptList" runat="server">
                        <HeaderTemplate>
                            <table class="list customerlist">
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                    <input type="hidden" value='<%# Eval("ID") %>' class="customerid" />
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    </div>
<%--                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged" PageSize="10"
                        CssClass="aspnetpager">
                    </webdiyer:AspNetPager>--%>
                    </div>
                </asp:Panel></td>
                <td>
                <asp:Panel runat="server" ID="formpanel">
                    <div  style="height:765px;overflow:auto;border:1px solid #e4e4e4;border-radius:15px;border-left:0px solid black;width:620px;">
                        <div style="height:35px;width:100%;border-bottom:1px solid #e4e4e4;background-color:#25a0da">
                        <table style="float:right">
                            <tr>
                            <%--<td  style="padding:0px">
                                                            <asp:Button ID="addfiled" runat="server" Text="Add New Fields"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center displaynone"
                                        Height="30" Width="120" ClientIDMode="Static" />
                                                            </td>--%>
                                                            <td  style="padding:0px;width:32px;padding-top:3px;"><img id="btnDeleteCustomer" src="images/trash.png" style="cursor:pointer" /></td>
                                    
                            </tr>
                            </table>
                        </div>
                            <div style="padding-left:20px;height:690px;overflow:auto">
                                    <table class="form-table">
                                        <tr>
                                            <td class="form-label">
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width:200px;">
                                                <asp:Label ID="txtNameMessage" runat="server" ForeColor="Red" >*</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form-label">
                                                Country:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form-label">
                                                Phone:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form-label">
                                                MobilePhone:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMobilePhone" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form-label">
                                                Email:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form-label">
                                                Address:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td class="form-label">
                                                Wallet ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtWalletID" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td colspan="2" align="center">
                                                <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="height:40px;width:160px;margin:0 auto;">
                            <table>
                            <tr>
                            <td>
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                        Width="75" Height="30" />
                                        </td>
                                        <td style="width:10px"></td>
                            <td><asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                        Height="30" Width="75" /></td>
                            </tr>
                            </table>
                                        </div>
                            </div>
   
                </asp:Panel></td></tr></table>
    <input type="hidden" id="delcustomerid" runat="server" clientidmode="Static" value="" />
    <asp:Button ID="delCustomerBtn" runat="server" Width="0" Height="0" CssClass="displaynone" OnClick="Delete" ClientIDMode="Static"></asp:Button>
    <asp:Button ID="btnCustomerSelected" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="CustomerSelected" ClientIDMode="Static"></asp:Button>
            <input type="hidden" id="selectedcustomerid" runat="server" clientidmode="Static" value="0" />
        </ContentTemplate>
    </asp:UpdatePanel>