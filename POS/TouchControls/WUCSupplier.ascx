﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCSupplier.ascx.cs"
    Inherits="POS.TouchControls.WUCSupplier" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    var previoussupplier = null;
    Sys.Application.add_load(supplierLoad);
    function supplierLoad(sender, args) {
        $(".supplierlist tr").unbind("click");
        $(".supplierlist tr").click(function () {

                $("#selectedsupplierid").val($(this).find(".supplierid").eq(0).val());

                $("#btnSupplierSelected").trigger("click");
            }
        );

            $(".supplierlist tr").each(function () {
                if ($(this).find(".supplierid").eq(0).val() == $("#selectedsupplierid").val()) {
                    $(this).css("background-color", "#EFF3F4");
                }
            });

            $(".addsuppliericon").unbind("click");
            $(".addsuppliericon").click(function () {
                $(".btnAddSupplier").trigger("click");
                $("#btnDeleteSupplier").css("display", "none");
            });

            $("#btnDeleteSupplier").unbind("click");
            $("#btnDeleteSupplier").click(function () {
                $("#delsupplierid").val($("#selectedsupplierid").val());
                radconfirm("Are you sure to delete the item?", deleteSupplier, 300, 100, null, "Delete Supplier", "Images/Confirm.png");
            });
    }

    function deleteSupplier(sender) {
        if (sender) {
            $("#delSupplierBtn").trigger("click");
        } else {
            $("#delsupplierid").val("-1");
        }
    }
</script>
<style type="text/css">
    input[type=submit]
    {
        cursor: pointer;
    }
    .autoresize
    {
        height: 100%;
    }
    
    .displaynone
    {
        display: none;
    }
</style>
<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="mainheader">
            <div class="contextual">
                <asp:Panel ID="listbuttons" runat="server">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add btnAddSupplier" ID="addLink"
                            Text="Add" OnClick="AddSupplier" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="formbuttons" runat="server" Visible="false">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                            Text="Back" OnClick="BackToList" />
                    </div>
                </asp:Panel>
            </div>
            <div class="subheader">
                <h2 class="icon-suppliers pagetitle">
                    <asp:Literal ID="ltlPageTitle" Text="Supplier List" runat="server"></asp:Literal></h2>
            </div>
        </div>
        <table style="border-spacing: 0px">
            <tr>
                <td>
                    <asp:Panel ID="listpanel" runat="server" CssClass="autoresize">
                        <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                            width: 400px">
                            <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 32px">
                                            <img src="images/icon_list.png" style="cursor: pointer" class="shownav" />
                                        </td>
                                        <td align="center">
                                            <span style="color: White; font-size: 15px; vertical-align: middle">Suppliers</span>
                                        </td>
                                        <td style="width: 32px">
                                            <img src="images/add.png" style="width: 20px; height: 20px; cursor: pointer" class="addsuppliericon" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                             <div style="width: 100%; height: 730px; overflow-y: auto">
                            <asp:Repeater ID="rptList" runat="server">
                                <HeaderTemplate>
                                    <table class="list supplierlist">
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center">
                                            <asp:Label runat="server" ID="lblName" Text='<%# Eval("Name")%>'></asp:Label><input
                                                type="hidden" value='<%# Eval("ID") %>' class="supplierid" />
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody> </table>
                                </FooterTemplate>
                            </asp:Repeater>
                            </div>
                        </div>
                        <%--<webdiyer:AspNetPager ID="AspNetPager1" runat="server" 
        onpagechanged="AspNetPager1_PageChanged" CssClass="aspnetpager"  PageSize="10">
    </webdiyer:AspNetPager>--%>
                    </asp:Panel>
                </td>
                <td>
                    <asp:Panel ID="formpanel" runat="server">
                        <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                            border-left: 0px solid black; width: 620px;">
                            <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                <table style="float: right">
                                    <tr>
                                        <%--<td  style="padding:0px">
                                                            <asp:Button ID="addfiled" runat="server" Text="Add New Fields"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center displaynone"
                                        Height="30" Width="120" ClientIDMode="Static" />
                                                            </td>--%>
                                        <td style="padding: 0px; width: 32px; padding-top: 3px;">
                                            <img id="btnDeleteSupplier" src="images/trash.png" style="cursor: pointer" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="padding-left: 20px; height: 690px; overflow: auto">
                                <table class="form-table">
                                    <tr>
                                        <td class="form-label">
                                            Supplier:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Contact:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtContact" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Suburb:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtSuburb" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Address:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            State:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtState" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Country:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Postcode:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPostcode" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Phone:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            MobilePhone:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtMobilePhone" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Fax:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtFax" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Email:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Note:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Payment Method:
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="dropPaymentMethod" runat="server">
                                                <asp:ListItem Text="Electronic Debit" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Cheque" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="Telegraphic Transfer" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td colspan="2" align="center">
                                            <asp:Label ID="lblUploadFlag" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="height: 40px; width: 160px; margin: 0 auto;">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                                UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                                Width="75" Height="30" />
                                        </td>
                                        <td style="width: 10px">
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                                                UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                                Height="30" Width="75" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <input type="hidden" id="delsupplierid" runat="server" clientidmode="Static" value="" />
        <asp:Button ID="delSupplierBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="Delete" ClientIDMode="Static"></asp:Button>
            <asp:Button ID="btnSupplierSelected" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="SupplierSelected" ClientIDMode="Static"></asp:Button>
            <input type="hidden" id="selectedsupplierid" runat="server" clientidmode="Static" value="0" />
    </ContentTemplate>
</asp:UpdatePanel>
