﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCLog.ascx.cs" Inherits="POS.TouchControls.WUCLog" %>
<%@ Register assembly="AspNetPager" namespace="Wuqi.Webdiyer" tagprefix="webdiyer" %>
<script>
    $(document).ready(function () {
        $("input[value='go']").val("GO");
    });
</script>
<style type="text/css">
 .logpager input[type=submit]
 {
      border: none;
      margin-left:10px;
            color: #ffffff;
            padding: 9px 12px 10px;
            line-height: 22px;
            text-decoration: none;
            text-shadow: none;
            
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            -webkit-transition: 0.25s;
            -moz-transition: 0.25s;
            -o-transition: 0.25s;
            transition: 0.25s;
            -webkit-backface-visibility: hidden;
            text-align: center;
            font-size: 14.994px; /* 15px */
            font-weight: 500;
            font-size: 16.996px; /* 17px */
            line-height: 20px;
            padding: 8px 0px 13px;
            background-color: #3894E7;
            width: 100%;
            padding-right: 0;
            padding-left: 0;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            font-size: 16.996px; /* 17px */
            line-height: 20px;
            padding: 5px 0px 5px;
            cursor:pointer;
            width:35px;
            height:25px;
             -webkit-appearance: none;
             border-radius: 0;
 }
 .logpager input[type=submit]:hover, .logpager input[type=submit]:focus
 {
     background-color: #5dade2;
 }
  .logpager input[type=submit]:active
 {
     background-color: #2c81ba;
 }
</style>
<asp:UpdatePanel runat="server" UpdateMode="Conditional">
<ContentTemplate>

<div class="mainheader">
        <div class="contextual">
            <div style="height: 10px; width: 85px">
            </div>
        </div>
        <div class="subheader">
            <h2 class="pagetitle icon-log">
                Logs</h2>
        </div>
    </div>
    <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                            width: 1020px">
                            <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 32px">
                                            <img src="images/icon_list.png" style="cursor: pointer" class="shownav" />
                                        </td>
                                        <td align="center">
                                            <span style="color: White; font-size: 15px; vertical-align: middle">Logs</span>
                                        </td>
                                        <td style="width: 32px">
                                           <%-- <img src="images/add.png" style="width: 20px; height: 20px; cursor: pointer" class="addsuppliericon" />--%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div style="width:100%;height:730px;overflow-y:auto">
<asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table style="width:100%;" class="list">
            <tbody>
        </HeaderTemplate>
        <ItemTemplate>
                <tr>
                    <td align="center"><%# Eval("CreateDate")%></td>
                    <td align="center"><%# Eval("Content")%></td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
        </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    </div>
    </div>
    <%--<webdiyer:AspNetPager ID="AspNetPager1" runat="server" 
        onpagechanged="AspNetPager1_PageChanged"  PageSize="10" CssClass="logpager aspnetpager">
    </webdiyer:AspNetPager>--%>
    </ContentTemplate>
</asp:UpdatePanel>