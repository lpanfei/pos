﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS.TouchControls
{
    public partial class WUCSupplier : ControlBase
    {
        List<Supplier> Suppliers
        {
            get
            {
                return GlobalCache.Suppliers;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Bind();
            }
        }

        public override void Init()
        {
             Bind();
        }

        protected void Delete(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delsupplierid.Attributes["value"]);
            Supplier supplier = Suppliers.FirstOrDefault(s => s.ID == id);
            if (supplier.IsNew)
            {
                GlobalCache.Suppliers.Remove(supplier);
            }
            else
            {
                supplier.IsDeleted = true;
                supplier.UserID = GlobalCache.CurrentUser.usr_ID;
                supplier.LastUpdatedTime = DateTime.Now;
                new POSService.POSService().UpdateSupplier(supplier);
                //new POSService.POSService().DeleteSupplier(id);
                GlobalCache.RefreshSuppliers();
            }
            Bind();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                delsupplierid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "deleteSupplier");
            }
            else if (e.CommandName == "ItemEdit")
            {
                RepeaterItem item = e.Item;
                ltlPageTitle.Text = "Supplier / Add";
                txtAddress.Text = (item.FindControl("lblAddress") as Label).Text;
                txtEmail.Text = (item.FindControl("lblEmail") as Label).Text;
                txtContact.Text = (item.FindControl("lblContact") as Label).Text;
                txtCountry.Text = (item.FindControl("lblCountry") as Label).Text;
                txtMobilePhone.Text = (item.FindControl("lblMobile") as Label).Text;
                txtName.Text = (item.FindControl("lblName") as Label).Text;
                txtNote.Text = (item.FindControl("lblNote") as Label).Text;
                txtFax.Text = (item.FindControl("lblFax") as Label).Text;
                txtPhone.Text = (item.FindControl("lblPhone") as Label).Text;
                txtPostcode.Text = (item.FindControl("lblPostcode") as Label).Text;
                txtState.Text = (item.FindControl("lblState") as Label).Text;
                txtSuburb.Text = (item.FindControl("lblSuburb") as Label).Text;
                dropPaymentMethod.SelectedValue = (item.FindControl("lblPayment") as Label).Text;
                lblId.Text = (item.FindControl("lblSID") as Label).Text;
            }
        }


        protected void AddSupplier(object sender, EventArgs e)
        {
            ltlPageTitle.Text = "Supplier / Add";
            txtAddress.Text = "";
            txtEmail.Text = "";
            txtContact.Text = "";
            txtCountry.Text = "";
            txtMobilePhone.Text = "";
            txtName.Text = "";
            txtNote.Text = "";
            txtFax.Text = "";
            txtPhone.Text = "";
            txtPostcode.Text = "";
            txtState.Text = "";
            txtSuburb.Text = "";
            dropPaymentMethod.SelectedValue = "1";
            lblId.Text = "0";
            Supplier supplier = new Supplier() { ID = GlobalCache.Suppliers.Max(c => c.ID) + 1, Name = "New Supplier", IsNew = true };
            GlobalCache.Suppliers.Add(supplier);
            selectedsupplierid.Attributes["value"] = supplier.ID.ToString();
            Bind();
        }

        protected void SupplierSelected(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(selectedsupplierid.Attributes["value"]);
            Supplier supplier = GlobalCache.Suppliers.FirstOrDefault(s => s.ID == id);
            if (supplier == null)
                return;
            ltlPageTitle.Text = "Supplier / Add";
            txtAddress.Text = supplier.Address;
            txtEmail.Text = supplier.Email;
            txtContact.Text = supplier.Contact;
            txtCountry.Text = supplier.Country;
            txtMobilePhone.Text = supplier.Mobile;
            txtName.Text = supplier.Name;
            txtNote.Text = supplier.Note;
            txtFax.Text = supplier.Fax;
            txtPhone.Text =supplier.Phone;
            txtPostcode.Text = supplier.Postcode;
            txtState.Text = supplier.State;
            txtSuburb.Text = supplier.Suburb;
            if (supplier.IsNew)
            {
                dropPaymentMethod.SelectedIndex = 0;
            }
            else
            {
                dropPaymentMethod.SelectedValue = supplier.PaymentMethod.ToString();
            }
            lblId.Text = id.ToString();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            SupplierSelected(null, null);
        }

        protected void BackToList(object sender, EventArgs e)
        {
            ltlPageTitle.Text = "Supplier List";
            GlobalCache.RefreshSuppliers();
            Bind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            Supplier supplier = new Data.Supplier()
            {
                ID = Convert.ToInt32(lblId.Text),
                Name = txtName.Text,
                State = txtState.Text,
                Country = txtCountry.Text,
                Contact = txtContact.Text,
                Suburb = txtSuburb.Text,
                Address = txtAddress.Text,
                ClientID = this.UserClientID,
                Email = txtEmail.Text,
                Fax = txtFax.Text,
                Mobile = txtMobilePhone.Text,
                Phone = txtPhone.Text,
                Postcode = txtPostcode.Text,
                Note = txtNote.Text,
                PaymentMethod = Convert.ToInt32(dropPaymentMethod.SelectedValue),
                UserID = GlobalCache.CurrentUser.usr_ID
            };

            Supplier exs = GlobalCache.Suppliers.FirstOrDefault(s => s.ID == supplier.ID && s.IsNew);
            if (exs != null)
            {
                supplier.ID = 0;
            }
            if (supplier.ID > 0)
            {
                supplier.ServerID = Suppliers.FirstOrDefault(s => s.ID == supplier.ID).ServerID;
            }
            supplier.LastUpdatedTime = DateTime.Now;
            new POSService.POSService().UpdateSupplier(supplier);

            BackToList(null, null);
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = Suppliers.OrderByDescending(s=>s.ID) ;
            rptList.DataBind();
            if (GlobalCache.Suppliers != null && GlobalCache.Suppliers.Any())
            {
                if (selectedsupplierid.Attributes["value"] == "0" || !GlobalCache.Suppliers.Any(p => p.ID.ToString() == selectedsupplierid.Attributes["value"]))
                {
                    selectedsupplierid.Attributes["value"] = GlobalCache.Suppliers.OrderByDescending(c => c.ID).FirstOrDefault().ID.ToString();
                }
            }
            SupplierSelected(null, null);
        }
    }
}