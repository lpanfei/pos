﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using POS.Data;

namespace POS.TouchControls
{
    public partial class WUCTask : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                timerclientid.Attributes["value"] = TaskTimer.ClientID;
            }
        }

        public override void Init()
        {
            //throw new NotImplementedException();
            //timer.Tick += RefreshTask;
            RefreshTask(null,null);
        }

        public override void UnInit()
        {
           // timer.Tick -= RefreshTask;
        }

        protected void RefreshTask(object sender, EventArgs args)
        {
            GlobalCache.RefreshInvoices(0, null, null, null, false);
            List<Invoice> invoices = new List<Invoice>();
            SetDefaults();
            if (GlobalCache.Invoices.Any(i => i.Products.Any(p => !p.IsComplete)))
            {
                var inservicelist = GlobalCache.Invoices.Where(i => i.Products.Any(p => !p.IsComplete)).OrderBy(i2 => i2.inv_CreateDate);
                int count = inservicelist.Count();
                var first = inservicelist.FirstOrDefault();
                rptList.DataSource = first.Products.ToList();
                rptList.DataBind();
                TableNo1.Text = first.inv_Customer;
                table1status.Attributes["value"] = first.OrderStatus;
                table1date.Text = first.inv_CreateDate.Value.ToString("G"); 
                if (count>1)
                {
                    var second = inservicelist.ElementAt(1);
                    Repeater1.DataSource = second.Products.ToList();
                    Repeater1.DataBind();
                    Literal1.Text = second.inv_Customer;
                    table2status.Attributes["value"] = second.OrderStatus;
                    table2date.Text = second.inv_CreateDate.Value.ToString("G"); 
                    if (count > 2)
                    {
                        var third = inservicelist.ElementAt(2);
                        Repeater2.DataSource = third.Products.ToList();
                        Repeater2.DataBind();
                        Literal2.Text = third.inv_Customer;
                        table3status.Attributes["value"] = third.OrderStatus;
                        table3date.Text = third.inv_CreateDate.Value.ToString("G"); 
                    }
                }
            }

           nextrefreshtime.Text = "( Next refresh at "+DateTime.Now.AddMilliseconds(TaskTimer.Interval).ToString("G") + " )";
        
        }

        void SetDefaults()
        {
            rptList.DataSource = null;
            rptList.DataBind();
            TableNo1.Text = "";
            table1status.Attributes["value"] = "normal-order";
            table1date.Text = "";
            Repeater1.DataSource = null;
            Repeater1.DataBind();
            Literal1.Text = "";
            table2status.Attributes["value"] = "normal-order";
            table2date.Text = "";
            Repeater2.DataSource = null;
            Repeater2.DataBind();
            Literal2.Text = "";
            table3status.Attributes["value"] = "normal-order";
            table3date.Text = "";
        }

        protected void CompleteInvoiceProduct(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "Complete")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                Invoice inv = GlobalCache.Invoices.FirstOrDefault(i => i.Products.Any(p => p.ip_ID == id));
                InvoiceProduct ip = inv.Products.FirstOrDefault(p => p.ip_ID == id);
                ip.IsComplete = true;
                inv.Status = InvoiceStatus.UnPaid;
                new POSService.POSService().DoInvoice(inv);
                RefreshTask(null, null);
            }
        }
    }
}