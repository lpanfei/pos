﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS.TouchControls
{
    public partial class WUCCustomer : ControlBase
    {
        public List<Customer> Customers
        {
            get
            {
                return GlobalCache.Customers;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Bind();
            }
        }
        public override void Init()
        {
                Bind();
        }
        protected void Delete(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delcustomerid.Attributes["value"]);
            POSService.POSService service = new POSService.POSService();
            Customer customer = Customers.FirstOrDefault(c => c.ID == id);
            if (customer.IsNew)
            {
                GlobalCache.Customers.Remove(customer);
            }
            else
            {
                customer.IsDeleted = true;
                customer.UserID = GlobalCache.CurrentUser.usr_ID;
                customer.LastUpdatedTime = DateTime.Now;
                service.UpdateCustomer(customer);
                //service.DeleteCustomer(new Customer() { ID = id });
                GlobalCache.RefreshCustomers();
            }
            Bind();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                delcustomerid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "deleteCustomer");
            }
            else if (e.CommandName == "ItemEdit")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                ltlPageTitle.Text = "Customer / Edit";
                lblId.Text = id.ToString();
                RepeaterItem item = e.Item;
                txtName.Text = (item.FindControl("lblName") as Label).Text;
                txtAddress.Text = (item.FindControl("lblAddress") as Label).Text;
                txtPhone.Text = (item.FindControl("lblPhone") as Label).Text;
                txtCountry.Text = (item.FindControl("lblCountry") as Label).Text;
                txtMobilePhone.Text = (item.FindControl("lblMobile") as Label).Text;
                txtEmail.Text = (item.FindControl("lblEmail") as Label).Text;
                txtWalletID.Text = (item.FindControl("lblWalletID") as Label).Text;
            }
        }


        protected void AddCustomer(object sender, EventArgs e)
        {
            ltlPageTitle.Text = "Customer / Add";
            lblId.Text = "0";
            txtAddress.Text = "";
            txtCountry.Text = "";
            txtEmail.Text = "";
            txtMobilePhone.Text = "";
            txtName.Text = "";
            txtNameMessage.Text = "*";
            txtPhone.Text = "";
            txtWalletID.Text = "";
            Customer customer = new Customer() { ID=GlobalCache.Customers.Max(c=>c.ID)+1,Name="New Customer",IsNew=true};
            GlobalCache.Customers.Add(customer);
            selectedcustomerid.Attributes["value"] = customer.ID.ToString();
            Bind();
        }

        protected void CustomerSelected(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(selectedcustomerid.Attributes["value"]);
            ltlPageTitle.Text = "Customer / Edit";
            lblId.Text = id.ToString();
            Customer customer = Customers.FirstOrDefault(C => C.ID == id);
            if (customer == null)
                return;
            txtName.Text = customer.Name;
            txtAddress.Text = customer.Address;
            txtPhone.Text = customer.Phone;
            txtCountry.Text = customer.Country;
            txtMobilePhone.Text = customer.Mobile;
            txtEmail.Text =customer.Email;
            txtWalletID.Text = customer.WalletID;
        }

        protected void BackToList(object sender, EventArgs e)
        {
            ltlPageTitle.Text = "Customers";
            GlobalCache.RefreshCustomers();
            Bind();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            CustomerSelected(null, null);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                ShowError("Customer Name is required!");
                return;
            }

            Customer customer = new Customer() { Name = txtName.Text.Trim(), Phone = txtPhone.Text.Trim(), Mobile = txtMobilePhone.Text.Trim(), Email = txtEmail.Text.Trim(), Address = txtAddress.Text.Trim(), Country = txtCountry.Text.Trim(), ClientID = this.UserClientID, ID = Convert.ToInt32(lblId.Text), WalletID = txtWalletID.Text, UserID = GlobalCache.CurrentUser.usr_ID };
            Customer excus = GlobalCache.Customers.FirstOrDefault(c => c.ID ==customer.ID);
            if (excus != null&&excus.IsNew)
            {
                customer.ID = 0;
            }
            if (customer.ID > 0)
            {
                customer.ServerID = Customers.FirstOrDefault(c => c.ID == customer.ID).ServerID;
            }
            customer.LastUpdatedTime = DateTime.Now;
            new POSService.POSService().UpdateCustomer(customer);

            BackToList(null, null);
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = GlobalCache.Customers.OrderByDescending(c => c.ID);
            rptList.DataBind();
            if (GlobalCache.Customers != null && GlobalCache.Customers.Any())
            {
                if (selectedcustomerid.Attributes["value"] == "0" || !GlobalCache.Customers.Any(p => p.ID.ToString() == selectedcustomerid.Attributes["value"]))
                {
                    selectedcustomerid.Attributes["value"] = GlobalCache.Customers.OrderByDescending(c=>c.ID).FirstOrDefault().ID.ToString();
                }
            }
            CustomerSelected(null, null);
        }
    }
}