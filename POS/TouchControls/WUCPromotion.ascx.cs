﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS.TouchControls
{
    public partial class WUCPromotion : ControlBase
    {
        List<Promotion> Promotions
        {
            get
            {
                return GlobalCache.Promotions;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //InitializeDropdowns();
                //Bind();
                //txtStartDate.Attributes.Add("onFocus", "WdatePicker({lang:'en',dateFmt:'dd-MM-yyyy'})");
                //txtEndDate.Attributes.Add("onFocus", "WdatePicker({lang:'en',dateFmt:'dd-MM-yyyy'})");
            }
        }

        public override void Init()
        {
            InitializeDropdowns();
            Bind();
        }

        void InitializeDropdowns()
        {
            GlobalCache.RefreshProducts(0, "", "");
            dropPromotionProduct.DataSource = GlobalCache.Products;
            dropPromotionProduct.DataBind();
            if (GlobalCache.Products.Any())
            {
                dropPromotionProduct.SelectedIndex = 0;
            }
        }

        void Bind()
        {
            rptPromotionList.DataSource = null;
            rptPromotionList.DataSource = Promotions.OrderByDescending(p => p.ID);
            rptPromotionList.DataBind();
            if (GlobalCache.Promotions != null && GlobalCache.Promotions.Any())
            {
                if (selectedpromotionid.Attributes["value"] == "0" || !GlobalCache.Promotions.Any(p => p.ID.ToString() == selectedpromotionid.Attributes["value"]))
                {
                    selectedpromotionid.Attributes["value"] = GlobalCache.Promotions.OrderByDescending(c => c.ID).FirstOrDefault().ID.ToString();
                }
            }
            PromotionSelected(null, null);
        }

        protected void AddWarranty(object sender, EventArgs e)
        {
            ltlPageTitle.Text = "Promotion / Add";
            if (dropPromotionProduct.Items.Count > 0)
            {
                dropPromotionProduct.SelectedIndex = 0;
            }
            
            txtDiscount.Text = "";
            txtEndDate.SelectedDate = null;
            txtStartDate.SelectedDate = null ;
            txtPromotionDescription.Text = "";
            lblId.Text = "0";
            inpPromotionProductID.Attributes["value"] = "-1";

            Promotion promotion = new Promotion() { ID=GlobalCache.Promotions.Max(p=>p.ID)+1,ProductName="New Promotion",Discount=100,IsNew=true};
            GlobalCache.Promotions.Add(promotion);
            Bind();
            selectedpromotionid.Attributes["value"] = promotion.ID.ToString();
            PromotionSelected(null, null);
        }

        protected void PromotionSelected(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(selectedpromotionid.Attributes["value"]);
            ltlPageTitle.Text = "Warranty / Edit";
            Promotion promotion = GlobalCache.Promotions.FirstOrDefault(p => p.ID == id);
            if (promotion == null)
                return;
            if (!promotion.IsNew)
            {
                dropPromotionProduct.SelectedValue = promotion.ProductID.ToString();
            }
            else if(dropPromotionProduct.Items.Count>0)
            {
                dropPromotionProduct.SelectedIndex = 0;
            }
            txtDiscount.Text = promotion.Discount.ToString();
            try
            {
                txtStartDate.SelectedDate = promotion.StartTime;
            }
            catch
            {
                txtStartDate.SelectedDate = null;
            }
            try
            {
                txtEndDate.SelectedDate = promotion.EndTime;
            }
            catch
            {
                txtEndDate.SelectedDate = null;
            }
            this.txtPromotionDescription.Text = promotion.Description;
            lblId.Text = selectedpromotionid.Attributes["value"];
            inpPromotionProductID.Attributes["value"] = promotion.ProductID.ToString();
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            PromotionSelected(null, null);
        }

        protected void BackToList(object sender, EventArgs e)
        {
            ltlPageTitle.Text = "Promotion List";
            inpPromotionProductID.Attributes["value"] = "-1";
            Bind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDiscount.Text.Trim()))
            {
                ShowError("Discount is required!");
                return;
            }
            if (txtStartDate.SelectedDate==null)
            {
                ShowError("Start date is required!");
                return;
            }
            if (txtEndDate.SelectedDate==null)
            {
                ShowError("End date is required!");
                return;
            }
            try
            {
                decimal discount = Convert.ToDecimal(txtDiscount.Text.Trim());
            }
            catch
            {
                ShowError("Discount inputted is not in a correct format!");
                return;
            }
            POSService.POSService service = new POSService.POSService();
            Promotion w = new Promotion()
            {
                ProductID = Convert.ToInt32(this.dropPromotionProduct.SelectedValue),
                ID = Convert.ToInt32(lblId.Text),
                ClientID = this.UserClientID,
                UserID = GlobalCache.CurrentUser.usr_ID,
                StoreID = this.UserStoreID,
                IsDeleted=false,
                Description=txtPromotionDescription.Text.Trim(),
                Discount=Convert.ToDecimal(txtDiscount.Text.Trim()),
            };

            w.Product = GlobalCache.Products.FirstOrDefault(pp => pp.pro_Id == w.ProductID);
            Promotion exp = GlobalCache.Promotions.FirstOrDefault(p => p.ID == w.ID);
            if (exp != null && exp.IsNew)
            {
                w.ID = 0;
            }
            if (w.ID > 0)
            {
                w.ServerID = Promotions.FirstOrDefault(wa => wa.ID == w.ID).ServerID;
            }
            w.StartTime = txtStartDate.SelectedDate;
            //if (string.IsNullOrEmpty(txtStartDate.Text))
            //{
            //    w.StartTime = null;
            //}
            //else
            //{
            //    try
            //    {
            //        w.StartTime = DateTime.Parse(txtStartDate.Text);
            //    }
            //    catch
            //    {
            //        w.StartTime = GetDateTime(txtStartDate.Text);
            //    }
            //}
            if (w.StartTime == null)
            {
                ShowError("Start time inputted is not in a correct format!");
                return;
            }
            w.EndTime = txtEndDate.SelectedDate;
            //if (string.IsNullOrEmpty(txtEndDate.Text))
            //{
            //    w.EndTime = null;
            //}
            //else
            //{
            //    try
            //    {
            //        w.EndTime = DateTime.Parse(txtEndDate.Text);
            //    }
            //    catch
            //    {
            //        w.EndTime = GetDateTime(txtEndDate.Text);
            //    }
            //}
            if (w.EndTime == null)
            {
                ShowError("End time inputted is not in a correct format!");
                return;
            }

            //need validation
            if (GlobalCache.Promotions != null && GlobalCache.Promotions.Any(p => p.ProductID == w.ProductID&&((p.StartTime>w.StartTime&&p.StartTime<w.EndTime)||(w.StartTime>p.StartTime&&w.StartTime<p.EndTime))))
            {
                ShowError("There are already promotions for the product during the period! Please check it!");
                return;
            }

            w.LastUpdatedTime = DateTime.Now;
            
            service.AddPromotion(w);
            GlobalCache.RefreshPromotions();
            BackToList(null, null);
        }

        DateTime? GetDateTime(string s)
        {
            try
            {
                string[] str = s.Split('-');
                int year = Convert.ToInt32(str[2]);
                int month = Convert.ToInt32(str[1]);
                int day = Convert.ToInt32(str[0]);
                return new DateTime(year, month, day, 0, 0, 0);
            }
            catch
            {
                return null;
            }
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemEdit")
            {
                RepeaterItem item = e.Item;
                formbuttons.Visible = true;
                listbuttons.Visible = false;
                formpanel.Visible = true;
                listpanel.Visible = false;
                ltlPageTitle.Text = "Warranty / Edit";
                dropPromotionProduct.SelectedValue = (item.FindControl("lblProductID") as Label).Text;
                txtDiscount.Text = (item.FindControl("lblDiscount") as Label).Text;
                //txtStartDate.Text = (item.FindControl("lblFromDate") as Label).Text;
                //txtEndDate.Text = (item.FindControl("lblEndDate") as Label).Text;
                this.txtPromotionDescription.Text = (item.FindControl("lblDescription") as Label).Text;
                lblId.Text = (item.FindControl("lblId") as Label).Text;
                inpPromotionProductID.Attributes["value"] = (item.FindControl("lblProductID") as Label).Text;
            }
            else if (e.CommandName == "ItemDel")
            {
                delpromotionid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "deletePromotion");
            }
        }

        protected void Delete(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delpromotionid.Attributes["value"]);
            POSService.POSService service = new POSService.POSService();
            Promotion w = Promotions.FirstOrDefault(wa => wa.ID == id);
            if (w.IsNew)
            {
                GlobalCache.Promotions.Remove(w);
            }
            else
            {
                w.IsDeleted = true;
                w.UserID = GlobalCache.CurrentUser.usr_ID;
                w.LastUpdatedTime = DateTime.Now;
                service.UpdatePromotion(w);
                GlobalCache.RefreshPromotions();
            }
            Bind();
        }
    }
}