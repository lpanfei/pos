﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCInvoice2.ascx.cs"
    Inherits="POS.TouchControls.WUCInvoice2" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<style>
    .total-pay
    {
        font-size: 25px;
        padding-left: 5px;
        line-height: 35px;
        height: 35px;
        margin: 0 auto;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        text-align: center;
        font-family: 'Segoe UI' , Arial, sans-serif;
        cursor: pointer;
        margin-top: 10px;
        margin-bottom: 10px;
        font-weight: bold;
    }
    .total-amount
    {
        font-size: 25px;
        padding-left: 5px;
        line-height: 35px;
        height: 35px;
        margin: 0 auto;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        text-align: center;
        font-family: 'Segoe UI' , Arial, sans-serif;
        cursor: pointer;
        margin-top: 10px;
        margin-bottom: 10px;
        font-weight: bold;
    }
    .sub-title
    {
        font-size: 18px;
        padding-left: 5px;
        line-height: 20px;
        height: 20px;
        margin: 0 auto;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        text-align: center;
        font-family: 'Segoe UI' , Arial, sans-serif;
        cursor: pointer;
        margin-top: 10px;
        margin-bottom: 10px;
    }
    .swiper-container
    {
        width: 720px;
        height: 710px;
        color: #fff;
        text-align: center;
    }
    .product-container
    {
        width: 720px;
        text-align: center;
    }
    .payment-container
    {
        width: 720px;
        text-align: center;
        height: 765px;
    }
    .payment-container
    {
        border: 1px solid #e4e4e4;
        border-radius: 15px;
    }
    
    .black-slide
    {
        background: black;
    }
    .red-slide
    {
        background: #ca4040;
    }
    .blue-slide
    {
        background: rgb(41, 128, 185);
    }
    .orange-slide
    {
        background: #ff8604;
    }
    .green-slide
    {
        background: #49a430;
    }
    .pink-slide
    {
        background: #973e76;
    }
    .swiper-slide .title
    {
        font-style: italic;
        font-size: 12px;
        margin-top: 5px;
        margin-left: 5px;
        line-height: 15px;
        width: 170px;
        height: 25px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        text-align: left;
    }
    .swiper-slide .price
    {
        font-style: italic;
        font-size: 12px;
        margin-top: 10px;
        margin-right: 5px;
        line-height: 15px;
        width: 165px;
        height: 25px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        text-align: right;
    }
    .swiper-slide .productimage
    {
        width: 170px;
        height: 110px;
        overflow: hidden;
    }
    
    .swiper-slide p
    {
        font-style: italic;
        font-size: 25px;
    }
    .pagination
    {
        margin: 0 auto;
        width: auto;
        padding-left: 5px;
    }
    .swiper-pagination-switch
    {
        display: inline-block;
        width: 10px;
        height: 10px;
        border-radius: 8px;
        background: #555;
        margin-right: 5px;
        opacity: 0.8;
        border: 1px solid #fff;
        cursor: pointer;
    }
    .swiper-active-switch
    {
        background: #49a430;
    }
    .swiper-dynamic-links
    {
        text-align: center;
    }
    .swiper-dynamic-links a
    {
        display: inline-block;
        padding: 5px;
        border-radius: 3px;
        border: 1px solid #ccc;
        margin: 5px;
        font-size: 12px;
        text-decoration: none;
        color: #333;
        background: #eee;
    }
    .pitem
    {
        width: 170px;
        height: 170px;
        cursor: pointer;
        border: 1px solid #F7F8FA;
    }
    .swiper-slide td
    {
        width: 180px;
        height: 175px;
    }
    .displaynone
    {
        display: none;
    }
    
    .cat-container
    {
        width: 300px;
        z-index: 100;
        position: absolute;
    }
    .cat-container .list tr
    {
        background-color: #F7F8FA;
    }
    
    .cart-container
    {
        width: 300px;
    }
    
    .cartitem-container
    {
        height: 576px;
        border-bottom: 1px solid #e4e4e4;
        overflow-y: auto;
    }
    
    .cart-container .list .product_name
    {
        font-family: 'Segoe UI' , Arial, sans-serif;
        cursor: pointer;
        text-overflow: ellipsis;
        white-space: nowrap;
        text-align: left;
        width: 150px;
        overflow: hidden;
    }
    .number-row, .discount-row
    {
        display: none;
        background: #EFF3F4;
    }
    
    .number-row:hover, .discount-row:hover
    {
        background: #EFF3F4;
    }
    
    
    .payment-method
    {
        width: 500px;
        margin: 0 auto;
        padding-top: 10px;
        height: 600px;
        border: 1px solid #e4e4e4;
        background: #EFF3F4;
        font-family: 'Segoe UI' , Arial, sans-serif;
        font-size: 20px;
    }
    
    .payment-details
    {
        width: 500px;
        margin: 0 auto;
        padding-top: 10px;
        height: 602px;
        font-family: 'Segoe UI' , Arial, sans-serif;
        font-size: 20px;
        display: none;
    }
    .payment-paid
    {
        display: none;
    }
    .paid-status
    {
        width: 301px;
        margin: 0 auto;
        margin-top: 10px;
    }
    .ui-keyboard-button
    {
        height: 3em;
        width: 9em;
    }
    .RadSearchBox_MetroTouch .rsbInner
    {
        height: 25px;
    }
    .RadSearchBox_MetroTouch .rsbInput
    {
        height: 25px;
    }
    .RadSearchBox_MetroTouch .rsbButtonSearch
    {
        width: 48px;
        height: 28px;
    }
    
    .customerfinder .RadSearchBox_MetroTouch .rsbInner
    {
        height: 36px;
    }
    .customerfinder .RadSearchBox_MetroTouch .rsbInput
    {
        height: 36px;
    }
    .customerfinder .RadSearchBox_MetroTouch .rsbButtonSearch
    {
        width: 48px;
        height: 39px;
    }
    
    .graybutton
    {
        background-color: rgb(232,232,232);
    }
    
    .cancel_box:hover
    {
        background-color: #e4e4e4;
        border: 1px solid #cdcdcd;
    }
    
    .invoicefilterarea input[type=radio]
    {
        width: 30px;
        height: 30px;
        margin-top: -5px;
        cursor: pointer;
    }
</style>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <table style="border-spacing: 0px" class="orderarea">
            <tr>
                <td>
                    <table style="border-spacing: 0px">
                        <tr>
                            <td>
                                <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                                    width: 400px">
                                    <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                        <table style="width: 100%">
                                            <tr>
                                                <td style="width: 32px">
                                                    <img src="images/icon_list.png" style="cursor: pointer" class="shownav" />
                                                </td>
                                                <td align="center">
                                                    <span style="color: White; font-size: 15px; vertical-align: middle">
                                                        <asp:Literal ID="invoiceHeaderLtl" runat="server" Text="Invoice (All)"></asp:Literal></span>
                                                </td>
                                                <td style="width: 32px">
                                                    <img src="images/add.png" style="width: 20px; height: 20px; cursor: pointer" class="addinvoiceicon" />
                                                </td>
                                                <td style="width: 10px">
                                                </td>
                                                <td style="width: 32px">
                                                    <img src="images/more.png" style="width: 24px; height: 24px; cursor: pointer" class="moreactionicon" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="width: 100%; height: 50px; background-color: #e4e4e4; display: none;"
                                        class="invoicefilterarea">
                                        <table style="width: 100%; border-spacing: 0px; height: 50px;">
                                            <tr>
                                                <td valign="middle" style="width: 80px; font-size: 15px; font-weight: bold" align="center">
                                                    Status:
                                                </td>
                                                <td valign="middle">
                                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"
                                                        Width="300" Height="30" Font-Size="12" OnSelectedIndexChanged="PaidStatusChanged">
                                                        <asp:ListItem Text="All" Value="All" Selected="True"></asp:ListItem>
                                                        <asp:ListItem Text="Paid" Value="Paid"></asp:ListItem>
                                                        <asp:ListItem Text="Unpaid" Value="Unpaid"></asp:ListItem>
                                                    </asp:RadioButtonList>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="width: 100%; height: 730px; overflow-y: auto" class="invoicelistarea">
                                        <asp:Repeater ID="rptList" runat="server">
                                            <HeaderTemplate>
                                                <table class="list invoicelist" style="table-layout: fixed">
                                                    <tbody>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td align="center" style="width: 100px;">
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("inv_CreateDate")%>'></asp:Label><input
                                                            type="hidden" value='<%# Eval("inv_Id") %>' class="invoiceid" />
                                                    </td>
                                                    <td align="center" style="width: 100px;">
                                                        $<asp:Label ID="lblTotalIncGST" runat="server" Text='<%# Decimal.Round(Convert.ToDecimal(Eval("Total")),2)%>'></asp:Label>
                                                    </td>
                                                    <td style="display: none" class="invoicepaidstatus">
                                                        <%# Eval("Status")%>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                </tbody> </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div style="height: 765px; overflow-x: hidden; width: 620px;">
                                    <table style="width: 1240px; height: 100%; border-spacing: 0px" class="invoicedetailcontainer">
                                        <tr>
                                            <td>
                                                <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                                                    border-left: 0px solid black; width: 620px;">
                                                    <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                                        <table style="width:100%;border-spacing:0px;height:35px;">
                                                            <tr>
                                                                <td valign="middle" align="center" style="font-size:15px;font-weight:bold;color:White">
                                                                <asp:Literal ID="InvoiceCurrentSatus" runat="server"></asp:Literal>
                                                                </td>
                                                                <td style="padding: 0px; width: 32px; padding-top: 3px;">
                                                                    <img src="images/trash.png" style="cursor: pointer" class="cancelinvoice" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="height: 690px; overflow: auto">
                                                        <div style="width: 100%; border-bottom: 1px solid #e4e4e4;position:relative">
                                                            <div class="total-amount">
                                                                <asp:Label ID="lblTotal" runat="server"></asp:Label></div>
                                                            <div class="sub-title">
                                                                <asp:Label ID="lblNo" runat="server"></asp:Label>
                                                                -
                                                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                            </div>
                                                            
                                                        </div>
                                                        <div style="width: 100%; border-bottom: 1px solid #e4e4e4">
                                                            <div class="sub-title">
                                                                Sold By
                                                                <asp:Label ID="lblUser" runat="server"></asp:Label></div>
                                                            <div class="sub-title">
                                                                Sold to
                                                                <asp:Label ID="lblCustomer" runat="server" CssClass="invoicecustomername"></asp:Label></div>
                                                        </div>
                                                        <div style="height: 520px; width: 100%; overflow-y: scroll; border-bottom: 1px solid #e4e4e4;
                                                            font-weight: bold">
                                                            <asp:Repeater ID="invoiceitems" runat="server">
                                                                <HeaderTemplate>
                                                                    <table class="list invoiceitemslist" style="table-layout: fixed; padding-bottom: 0px;
                                                                        margin-bottom: 0px;">
                                                                        <tbody>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <asp:Label ID="lblDate" runat="server" Text='<%# Eval("ip_Description")%>'></asp:Label>
                                                                        </td>
                                                                        <td align="center">
                                                                            <asp:Label ID="lblTotalIncGST" runat="server" Text='<%# Eval("ip_Quantity")%>'></asp:Label>
                                                                        </td>
                                                                        <td align="center">
                                                                            <asp:Label ID="Label1" runat="server" Text='<%# Decimal.Round(Convert.ToDecimal(Eval("ip_Discount")),2)%>'></asp:Label>%
                                                                        </td>
                                                                        <td align="center">
                                                                            $<asp:Label ID="Label2" runat="server" Text='<%# Decimal.Round(Convert.ToDecimal(Eval("ip_SubTotal")),2)%>'></asp:Label>
                                                                            <input type="hidden" class="invoiceproductid" value='<%# Eval("ip_ProductID")%>' />
                                                                            <input type="hidden" class="invoiceproductprice" value='<%# Eval("ip_Price")%>' />
                                                                            <input type="hidden" class="invoiceproductdiscount" value='<%# Eval("ip_Discount")%>' />
                                                                            <input type="hidden" class="invoiceproductname" value='<%# Eval("ip_Description")%>' />
                                                                            <input type="hidden" class="invoiceproductid" value='<%# Eval("ip_ProductID")%>' />
                                                                            <input type="hidden" class="invoiceproductprice" value='<%# Eval("ip_Price")%>' />
                                                                            <input type="hidden" class="invoiceproductdiscount" value='<%# Eval("ip_Discount")%>' />
                                                                            <input type="hidden" class="invoiceproductquantity" value='<%# Eval("ip_Quantity")%>' />
                                                                        </td>
                                                                    </tr>
                                                                </ItemTemplate>
                                                                <FooterTemplate>
                                                                    </tbody> </table>
                                                                </FooterTemplate>
                                                            </asp:Repeater>
                                                        </div>
                                                    </div>
                                                    <div style="height: 40px; width: 250px; margin: 0 auto;">
                                                        <table style="margin: 0 auto">
                                                            <tr>
                                                                <td>
<%--                                                                    <asp:Button ID="btnReceipt" runat="server" Text="Receipt"
                                                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center btn-receipt"
                                                                        Width="75" Height="30" />--%>
                                                                        <input id="btnReceipt" runat="server" clientidmode="Static" type="button" value="Receipt"
                                                                        style="width: 75px; height: 30px;" class="btn btn-large btn-block btn-info btn-center btn-receipt"
                                                                        visible="false" />
                                                                </td>
                                                                <td>
                                                                    <input id="editBtn" runat="server" clientidmode="Static" type="button" value="Edit"
                                                                        style="width: 75px; height: 30px; margin-left: 10px;" class="btn btn-large btn-block btn-info btn-center btnswitch"
                                                                        visible="false" />
                                                                </td>
                                                                <td>
                                                                    <input id="payBtn" runat="server" clientidmode="Static" type="button" value="Pay it"
                                                                        style="width: 75px; height: 30px; margin-left: 10px;" class="btn btn-large btn-block btn-info btn-center btnpay"
                                                                        visible="false" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                                                    border-left: 0px solid black; width: 620px;">
                                                    <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                                        <table>
                                                            <tr>
                                                                <td style="width: 32px; margin-top: -3px;">
                                                                    <div style="height: 26px; width: 26px; font-size: 26px; color: White; font-weight: bold;
                                                                        line-height: 26px; text-align: center; cursor: pointer" class="back-todetail">
                                                                        <</div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="height: 730px; overflow: auto">
                                                        <div style="border-top: 1px solid rgb(204, 204, 204); width: 100%; height: 290px;">
                                                            <div style="width: 500px; margin: 0 auto; margin-top: 20px; position: relative">
                                                                <%--<input type="text" style="width:500px;height:45px;display:block;margin-top:8px;background:#EFF3F4;font-size:25px;color:rgb(204, 204, 204)" value="SMS" id="inpSMS" />--%>
                                                                <asp:TextBox ID="inpSMS2" runat="server" Text="SMS" ClientIDMode="Static" Style="width: 500px;
                                                                    height: 45px; display: block; margin-top: 8px; background: #EFF3F4; font-size: 25px;
                                                                    color: rgb(204, 204, 204)"></asp:TextBox>
                                                                <%--<input type="button" value="Send" class="btn btn-info btn-block" id="btnSendSMS" style="width:100px;position:absolute;top:4px;right:0px;display:none;" />--%>
                                                                <asp:Button Text="Send" CssClass="btn btn-info btn-block" ID="btnSendSMS2" runat="server"
                                                                    ClientIDMode="Static" OnClick="SendSMS" Style="width: 100px; position: absolute;
                                                                    top: 4px; right: 0px; display: none;" />
                                                            </div>
                                                            <div style="width: 500px; margin: 0 auto; margin-top: 20px; position: relative">
                                                                <asp:TextBox ID="inpWalletId2" runat="server" Text="Wallet ID" ClientIDMode="Static"
                                                                    Style="width: 500px; height: 45px; display: block; margin-top: 8px; background: #EFF3F4;
                                                                    font-size: 25px; color: rgb(204, 204, 204)"></asp:TextBox>
                                                                <%-- <input type="text" style="width:500px;height:45px;display:block;margin-top:8px;background:#EFF3F4;font-size:25px;color:rgb(204, 204, 204)" value="Wallet ID" id="inpWalletId" />--%>
                                                                <%--<input type="button" value="Send" class="btn btn-info btn-block" id="btnSendWallet" style="width:100px;position:absolute;top:4px;right:0px;display:none;" />--%>
                                                                <asp:Button Text="Send" CssClass="btn btn-info btn-block" ID="btnSendWallet2" runat="server"
                                                                    ClientIDMode="Static" OnClick="SendWallet" Style="width: 100px; position: absolute;
                                                                    top: 4px; right: 0px; display: none;" />
                                                            </div>
                                                            <div style="width: 500px; margin: 0 auto; margin-top: 20px; position: relative">
                                                                <asp:TextBox ID="inpEmail2" runat="server" Text="Email" ClientIDMode="Static" Style="width: 500px;
                                                                    height: 45px; display: block; margin-top: 8px; background: #EFF3F4; font-size: 25px;
                                                                    color: rgb(204, 204, 204)"></asp:TextBox>
                                                                <%-- <input type="text" style="width:500px;height:45px;display:block;margin-top:8px;background:#EFF3F4;font-size:25px;color:rgb(204, 204, 204)" value="Wallet ID" id="inpWalletId" />--%>
                                                                <%--<input type="button" value="Send" class="btn btn-info btn-block" id="btnSendWallet" style="width:100px;position:absolute;top:4px;right:0px;display:none;" />--%>
                                                                <asp:Button Text="Send" CssClass="btn btn-info btn-block" ID="btnSendEmail2" runat="server"
                                                                    ClientIDMode="Static" OnClick="SendEmail" Style="width: 100px; position: absolute;
                                                                    top: 4px; right: 0px; display: none;" />
                                                            </div>
                                                            <div style="width: 500px; margin: 0 auto; margin-top: 20px;">
                                                                <input type="button" class="btn btn-block" style="width: 500px; height: 45px; display: block;
                                                                    margin-top: 8px; color: white; background-color: rgb(204,204,204); border-radius: 0px;
                                                                    font-size: 25px;" value="Print Receipt" id="btnPrint2" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table style="border-spacing: 0px">
                        <tr>
                            <td>
                                <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                                    width: 720px">
                                    <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                        <table style="width: 100%;">
                                            <tr class="productsheader">
                                                <td style="width: 32px; margin-top: -3px;">
                                                    <div style="height: 26px; width: 26px; font-size: 26px; color: White; font-weight: bold;
                                                        line-height: 26px; text-align: center; cursor: pointer" class="back-command">
                                                        <</div>
                                                </td>
                                                <td align="center">
                                                    <span style="color: White; font-size: 15px; vertical-align: middle; cursor: pointer;"
                                                        class="showcategory"><span class="cattitle">All Products</span><img src="images/arrow-down.png"
                                                            style="vertical-align: middle" /></span>
                                                    <div class="cat-container" style="display: none">
                                                        <table class="list">
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                                <td style="width: 32px">
                                                    <img src="images/search.png" style="width: 26px; height: 26px; cursor: pointer; margin-top: 3px;"
                                                        class="searchproducticon" />
                                                </td>
                                            </tr>
                                            <tr class="searchrow" style="display: none">
                                                <td colspan="3">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="width: 100px; height: 26px;" align="center" valign="middle">
                                                                <input type="button" value="Cancel" class="cancelsearch btn btn-block" style="width: 80px;
                                                                    height: 27px; display: block; color: black; padding: 0px; background-color: rgb(232,232,232);
                                                                    border-radius: 0px;" />
                                                            </td>
                                                            <td>
                                                                <eid:RadSearchBox ID="searchProductBox" Width="550" Height="25" runat="server" Skin="MetroTouch"
                                                                    EmptyMessage="Product Name" DataTextField="pro_Name" OnClientSearch="productSearch">
                                                                    <DropDownSettings Height="150" Width="550" />
                                                                </eid:RadSearchBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="swiper-container">
                                        <div class="swiper-wrapper">
                                        </div>
                                    </div>
                                    <div class="pagination">
                                    </div>
                                </div>
                                <td>
                                    <div style="height: 765px; overflow-y: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                                        border-left: 0px solid black; width: 300px; overflow-x: hidden">
                                        <table style="width: 100%; border-spacing: 0px;" class="itscart">
                                            <tr>
                                                <td>
                                                    <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da"
                                                        class="cart-header">
                                                        <table style="width: 100%; height: 35px;">
                                                            <tr>
                                                                <td style="padding: 0px; width: 32px;">
                                                                    <div style="height: 30px; width: 30px; font-size: 30px; color: White; font-weight: bold;
                                                                        line-height: 30px; text-align: center; cursor: pointer" class="cart-command">
                                                                    </div>
                                                                </td>
                                                                <td align="center">
                                                                    <span style="color: White; font-size: 15px; vertical-align: middle; height: 35px;"
                                                                        class="title">Cart</span>
                                                                </td>
                                                                <td style="padding: 0px; width: 32px; padding-top: 3px;">
                                                                    <img id="btnDeleteCartItem" src="images/trash.png" style="cursor: pointer; display: none;" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="height: 50px; width: 100%; border-bottom: 1px solid #e4e4e4; cursor: pointer">
                                                        <table style="width: 100%; height: 100%" class="choosecontainer">
                                                            <tr>
                                                                <td style="font-weight: bold" class="selectedcustomer">
                                                                    Choose Customer
                                                                </td>
                                                                <td style="width: 32px">
                                                                    <img src="images/right.png" style="width: 26px; height: 26px; cursor: pointer" class="choosecustomericon" />
                                                                    <input type="hidden" value='0' class="selectedcustomerid" />
                                                                    <input type="hidden" value='' class="selectedcustomername" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="cart-container">
                                                        <div class="cartitem-container">
                                                            <table class="list cartlist" style="margin-bottom: 0px; border-spacing: 0px; border: 0px solid #e4e4e4">
                                                                <tbody>
                                                                    <tr class="number-row">
                                                                        <td colspan="4">
                                                                            <table style="width: 290px; border-spacing: 0px;">
                                                                                <tr>
                                                                                    <td style="width: 87px" align="center">
                                                                                        <input type="button" value="-" style="width: 80px; font-size: 30px; margin-left: 13px;"
                                                                                            class="btn btn-block btn-info btn-minus" />
                                                                                    </td>
                                                                                    <td style="width: 90px" align="center">
                                                                                        <input type="text" value="" style="width: 80px; font-size: 20px; height: 35px;" id="currentquantity" />
                                                                                    </td>
                                                                                    <td style="width: 100px" align="center">
                                                                                        <input type="button" value="+" style="width: 80px; font-size: 30px" class="btn btn-block btn-info btn-add" />
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr class="discount-row">
                                                                        <td colspan="4">
                                                                            <table>
                                                                                <tr>
                                                                                    <td style="width: 95px">
                                                                                        Discount:
                                                                                    </td>
                                                                                    <td align="left" colspan="2">
                                                                                        <input type="text" style="width: 140px; height: 35px" id="currentdiscount" />
                                                                                        &nbsp; %
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="total-container">
                                                            <div style="float: right; margin-bottom: 7px; margin-top: 7px; margin-right: 10px;">
                                                                <input type="checkbox" id="gstcheckbox" style="width: 30px; height: 30px" />
                                                                GST Amount: <span class="gstamount">0.00</span>
                                                            </div>
                                                            <table style="width: 100%; border-spacing: 0px;">
                                                                <tr>
                                                                    <td class="savewithoutpay" style="padding-right: 10px;">
                                                                        <input type="button" id="Button1" value="Save 
Without Pay" class="btn btn-block btn-info btnsaveunpaid" style="height: 55px; margin-bottom: 0px; padding: 0px;" />
                                                                        <%--<asp:Button runat="server" ID="btnSaveWithoutPay"  Text="" CssClass="btn btn-block btn-info btnsaveunpaid" style="height: 55px; margin-bottom: 0px;padding:0px;"/>--%>
                                                                    </td>
                                                                    <td>
                                                                        <input type="button" id="btntotal" value="Pay it" class="btn btn-block btn-info"
                                                                            style="height: 55px; margin-bottom: 0px; padding: 0px;" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                </td>
                                                <td valign="top">
                                                    <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da;
                                                        cursor: pointer">
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td style="padding: 0px; width: 32px;" class="back-tocart">
                                                                    <div style="height: 26px; width: 26px; font-size: 26px; color: White; font-weight: bold;
                                                                        line-height: 26px; text-align: center;">
                                                                        <</div>
                                                                </td>
                                                                <td align="left" class="back-tocart">
                                                                    <span style="color: White; font-size: 15px; vertical-align: middle" class="title">Back
                                                                        to Cart</span>
                                                                </td>
                                                                <td style="padding: 0px; width: 32px; padding-top: 3px;">
                                                                    <img src="images/add.png" style="width: 20px; height: 20px; cursor: pointer" class="addcustomerfrominvoiceicon" />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div style="border: 1px solid #cdcdcd; width: 300px; height: 40px;" class="customerfinder">
                                                        <table style="border-spacing: 0px; width: 100%">
                                                            <tr>
                                                                <td>
                                                                    <eid:RadSearchBox ID="customerSearchBox1" Width="190" Height="38" runat="server"
                                                                        Skin="MetroTouch" EmptyMessage="Customer Name" DataTextField="Name" OnClientSearch="customerSearch">
                                                                        <DropDownSettings Height="150" Width="190" />
                                                                    </eid:RadSearchBox>
                                                                </td>
                                                                <td style="width: 50px">
                                                                    <div style="width: 45px; height: 39px" class="cancel_box">
                                                                        <img src="../Images/cancel_box.png" style="margin-top: -2px; margin-left: -2px" />
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <asp:Repeater ID="customerItems" runat="server">
                                                        <HeaderTemplate>
                                                            <table class="list choosecustomerlist" style="width: 300px">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="left" class="choosecustomername" style="padding-left: 20px">
                                                                            <asp:Label ID="lblName" runat="server" Text='None'></asp:Label>
                                                                            <input type="hidden" value='0' class="choosecustomerid" />
                                                                            <input type="hidden" value='' class="choosecustomersname" />
                                                                        </td>
                                                                        <td style="width: 50px">
                                                                            <img src="images/check.png" class="customerchecked" style="width: 32px; height: 32px" />
                                                                        </td>
                                                                    </tr>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr>
                                                                <td align="left" class="choosecustomername" style="padding-left: 20px">
                                                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                                                    <input type="hidden" value='<%# Eval("ID") %>' class="choosecustomerid" />
                                                                    <input type="hidden" value='<%# Eval("Name") %>' class="choosecustomersname" />
                                                                    <input type="hidden" value='<%# Eval("Email") %>' class="choosecustomeremail" />
                                                                    <input type="hidden" value='<%# Eval("Phone") %>' class="choosecustomerphone" />
                                                                    <input type="hidden" value='<%# Eval("WalletID") %>' class="choosecustomerwallet" />
                                                                </td>
                                                                <td style="width: 50px">
                                                                    <img src="images/check.png" class="customerchecked" style="width: 32px; height: 32px" />
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </tbody> </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                                <td>
                                    <div class="payment-container">
                                        <div class="total-pay">
                                            0.00</div>
                                        <div class="payment-method">
                                            How would you like to pay?
                                            <div style="border: 1px solid #e4e4e4; width: 200px; height: 60px; margin: 0 auto;
                                                margin-top: 20px; font-weight: bold; cursor: pointer; background: rgb(204, 204, 204)">
                                                <table style="width: 100%; margin-top: 10px;" class="method-item cash-pay">
                                                    <tr>
                                                        <td style="width: 40px">
                                                            <img src="images/cash.png" />
                                                        </td>
                                                        <td align="left" style="margin-left: 10px">
                                                            Cash
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="border: 1px solid #e4e4e4; width: 200px; height: 60px; margin: 0 auto;
                                                margin-top: 20px; font-weight: bold; cursor: pointer; background: rgb(204, 204, 204)">
                                                <table style="width: 100%; margin-top: 10px;" class="method-item credit-pay">
                                                    <tr>
                                                        <td style="width: 40px">
                                                            <img src="images/credit-card.png" />
                                                        </td>
                                                        <td align="left" style="margin-left: 10px">
                                                            Credit Card
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="border: 1px solid #e4e4e4; width: 200px; height: 60px; margin: 0 auto;
                                                margin-top: 20px; font-weight: bold; cursor: pointer; background: rgb(204, 204, 204)">
                                                <table style="width: 100%; margin-top: 10px;" class="method-item net-pay">
                                                    <tr>
                                                        <td style="width: 40px">
                                                            <img src="images/net.png" />
                                                        </td>
                                                        <td align="left" style="margin-left: 10px">
                                                            Net
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="border: 1px solid #e4e4e4; width: 200px; height: 60px; margin: 0 auto;
                                                margin-top: 20px; font-weight: bold; cursor: pointer; background: rgb(204, 204, 204)">
                                                <table style="width: 100%; margin-top: 10px;" class="method-item none-pay">
                                                    <tr>
                                                        <td style="width: 40px">
                                                            <img src="images/none.png" />
                                                        </td>
                                                        <td align="left" style="margin-left: 10px">
                                                            None
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="payment-details">
                                            <table style="width: 100%" class="payment-list">
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="payment-action">
                                            <table style="width: 100%">
                                                <tr style="height: 25px">
                                                    <td colspan="3">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right">
                                                        <input type="button" value="Add Payment" style="width: 150px; height: 35px" class="btn"
                                                            disabled="disabled" id="addpayment" />
                                                    </td>
                                                    <td style="width: 50px">
                                                    </td>
                                                    <td align="left">
                                                        <input type="button" value="Mark as Paid" id="markaspaid" style="width: 150px; height: 35px"
                                                            class="btn btn-block btn-info" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div class="payment-paid">
                                            <div class="paid-status">
                                                <img src="images/paid_part.png" /></div>
                                            <div style="margin: 0 auto; width: 300px; margin-top: 10px;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <div style="font-size: 15px; line-height: 30px; width: 130px;">
                                                                TOTAL PAID</div>
                                                            <div style="font-size: 30px; line-height: 30px; font-weight: bold; color: #359a00"
                                                                class="paidamount">
                                                                20.00</div>
                                                        </td>
                                                        <td style="border-right: 1px solid rgb(204, 204, 204); width: 1px;">
                                                        </td>
                                                        <td>
                                                            <div style="font-size: 15px; line-height: 30px; width: 150px;" class="ownedtext">
                                                                OUTSTANDING</div>
                                                            <div style="font-size: 30px; line-height: 30px; font-weight: bold; color: #9b5b05"
                                                                class="ownedamount">
                                                                20.00</div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div style="border-top: 1px solid rgb(204, 204, 204); width: 100%; height: 290px;
                                                margin-top: 20px;">
                                                <div style="width: 500px; margin: 0 auto; margin-top: 20px; position: relative">
                                                    <%--<input type="text" style="width:500px;height:45px;display:block;margin-top:8px;background:#EFF3F4;font-size:25px;color:rgb(204, 204, 204)" value="SMS" id="inpSMS" />--%>
                                                    <asp:TextBox ID="inpSMS" runat="server" Text="SMS" ClientIDMode="Static" Style="width: 500px;
                                                        height: 45px; display: block; margin-top: 8px; background: #EFF3F4; font-size: 25px;
                                                        color: rgb(204, 204, 204)"></asp:TextBox>
                                                    <%--<input type="button" value="Send" class="btn btn-info btn-block" id="btnSendSMS" style="width:100px;position:absolute;top:4px;right:0px;display:none;" />--%>
                                                    <asp:Button Text="Send" CssClass="btn btn-info btn-block" ID="btnSendSMS" runat="server"
                                                        ClientIDMode="Static" OnClick="SendSMS" Style="width: 100px; position: absolute;
                                                        top: 4px; right: 0px; display: none;" />
                                                </div>
                                                <div style="width: 500px; margin: 0 auto; margin-top: 20px; position: relative">
                                                    <asp:TextBox ID="inpWalletId" runat="server" Text="Wallet ID" ClientIDMode="Static"
                                                        Style="width: 500px; height: 45px; display: block; margin-top: 8px; background: #EFF3F4;
                                                        font-size: 25px; color: rgb(204, 204, 204)"></asp:TextBox>
                                                    <%-- <input type="text" style="width:500px;height:45px;display:block;margin-top:8px;background:#EFF3F4;font-size:25px;color:rgb(204, 204, 204)" value="Wallet ID" id="inpWalletId" />--%>
                                                    <%--<input type="button" value="Send" class="btn btn-info btn-block" id="btnSendWallet" style="width:100px;position:absolute;top:4px;right:0px;display:none;" />--%>
                                                    <asp:Button Text="Send" CssClass="btn btn-info btn-block" ID="btnSendWallet" runat="server"
                                                        ClientIDMode="Static" OnClick="SendWallet" Style="width: 100px; position: absolute;
                                                        top: 4px; right: 0px; display: none;" />
                                                </div>
                                                <div style="width: 500px; margin: 0 auto; margin-top: 20px; position: relative">
                                                    <asp:TextBox ID="inpEmail" runat="server" Text="Email" ClientIDMode="Static" Style="width: 500px;
                                                        height: 45px; display: block; margin-top: 8px; background: #EFF3F4; font-size: 25px;
                                                        color: rgb(204, 204, 204)"></asp:TextBox>
                                                    <%-- <input type="text" style="width:500px;height:45px;display:block;margin-top:8px;background:#EFF3F4;font-size:25px;color:rgb(204, 204, 204)" value="Wallet ID" id="inpWalletId" />--%>
                                                    <%--<input type="button" value="Send" class="btn btn-info btn-block" id="btnSendWallet" style="width:100px;position:absolute;top:4px;right:0px;display:none;" />--%>
                                                    <asp:Button Text="Send" CssClass="btn btn-info btn-block" ID="btnSendEmail" runat="server"
                                                        ClientIDMode="Static" OnClick="SendEmail" Style="width: 100px; position: absolute;
                                                        top: 4px; right: 0px; display: none;" />
                                                </div>
                                                <div style="width: 500px; margin: 0 auto; margin-top: 20px;">
                                                    <input type="button" class="btn btn-block" style="width: 500px; height: 45px; display: block;
                                                        margin-top: 8px; color: white; background-color: rgb(204,204,204); border-radius: 0px;
                                                        font-size: 25px;" value="Print Receipt" id="btnPrint" />
                                                </div>
                                            </div>
                                            <div style="width: 420px; margin: 0 auto; margin-top: 10px;">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <input type="button" id="btnStartNew" class="btn btn-info btn-block" style="width: 200px;
                                                                height: 50px" value="Start New Order" />
                                                        </td>
                                                        <td style="width: 20px">
                                                        </td>
                                                        <td>
                                                            <input type="button" id="btnBackToList" class="btn btn-info btn-block" style="width: 200px;
                                                                height: 50px" value="Back to List" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Button ID="btnInvoiceSelected" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="InvoiceSelected" ClientIDMode="Static"></asp:Button>
        <input type="hidden" id="selectedinvoiceid" runat="server" clientidmode="Static"
            value="0" />
        <div class="displaynone">
            <asp:Label ID="lblGSTSetting" runat="server" CssClass="gstvalue"></asp:Label>
            <asp:Repeater ID="productList" runat="server">
                <HeaderTemplate>
                    <table class="pro_list">
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            <span class="product_id">
                                <%# Eval("pro_Id") %></span>
                        </td>
                        <td align="center">
                            <span class="product_catname">
                                <%# Eval("cat_Name")%></span>
                        </td>
                        <td align="center">
                            <span class="product_name">
                                <%# Eval("pro_Name") %></span>
                        </td>
                        <td align="center">
                            <span class="product_price">
                                <%# Eval("pro_Price")%></span>
                        </td>
                        <td align="center">
                            <span class="product_discount">
                                <%# Eval("Discount")%></span>
                        </td>
                        <td align="center">
                            <span class="product_image">
                                <%# HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + Eval("ImageUrl")) %></span>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <input type="hidden" runat="server" id="currentinvoiceid" value="0" clientidmode="Static" />
        <input type="hidden" runat="server" id="currentinvoiceno" value="0" clientidmode="Static" />
        <input type="hidden" runat="server" id="invoicedetails" value="" clientidmode="Static" />
        <input type="hidden" runat="server" id="invoicepaidamount" value="0" clientidmode="Static" />
        <input type="hidden" runat="server" id="invoiceownedamount" value="0" clientidmode="Static" />
        <input type="hidden" runat="server" id="isdoinvoice" value="0" clientidmode="Static" />
        <input type="hidden" runat="server" id="isaddcustomer" value="0" clientidmode="Static" />
        <input type="hidden" runat="server" id="isgstchecked" value="false" clientidmode="Static" />
        <asp:Button ID="resubmitBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="DoInvoice" ClientIDMode="Static"></asp:Button>
        <asp:Button ID="btnSaveUnpaidStatus" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="SaveUnpaid" ClientIDMode="Static"></asp:Button>
        <asp:Button ID="btnPrintReceipt" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="PrintReceipt" ClientIDMode="Static"></asp:Button>
        <asp:Button ID="btnCancelInvoice" OnClick="Cancel" runat="server" CssClass="displaynone"
            ClientIDMode="Static" Text="Cancel" Width="0" Height="0"></asp:Button>
        <asp:Button ID="btnAddCustomer" OnClick="AddCustomer" runat="server" CssClass="displaynone"
            ClientIDMode="Static" Text="Cancel" Width="0" Height="0"></asp:Button>
        <eid:RadWindow runat="server" ID="InvoiceCancelRadWindow1" Skin="Metro" Overlay="true"
            Modal="true" Title="Cancel Invoice" VisibleStatusbar="false" AutoSize="true"
            Width="600" Height="400">
            <ContentTemplate>
                <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table class="form-table">
                            <tr>
                                <td class="form-label">
                                    Invoice No.:
                                </td>
                                <td>
                                    <div style="border: 1px solid #D1E4F6; height: 30px; padding-top: 9px; padding-left: 10px;">
                                        <asp:Label ID="lblCancelNo" runat="server" ClientIDMode="Static"></asp:Label></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Reason:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtReason" runat="server" TextMode="MultiLine" Rows="6" Width="400"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <input type="hidden" id="lblCancelID" name="cancelid" runat="server" clientidmode="Static"
                                        value="" />
                                    <input type="button" id="submitCancelInvoice" value="Submit" class="btn btn-large btn-block btn-info" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </eid:RadWindow>
        <eid:RadWindow runat="server" ID="AddCustomerWindow" Skin="Metro" Overlay="true"
            Modal="true" Title="Add Customer" VisibleStatusbar="false" AutoSize="true" Width="600"
            Height="400">
            <ContentTemplate>
                <asp:UpdatePanel ID="UpdatePanel12" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table class="form-table" style="height: 400px">
                            <tr>
                                <td class="form-label">
                                    Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerName" runat="server" CssClass="customername" Width="200"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="Label2" runat="server" ForeColor="Red">*</asp:Label>
                                </td>
                            </tr>
                            <tr style="display: none" class="customertr">
                                <td class="form-label">
                                </td>
                                <td>
                                    <asp:Label ID="txtNameMessage" runat="server" ForeColor="Red" CssClass="custommessage"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Country:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerCountry" runat="server" Width="200" CssClass="customercountry"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Phone:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerPhone" runat="server" Width="200" CssClass="customerphone"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    MobilePhone:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerMobilePhone" runat="server" Width="200" CssClass="customermobilephone"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Email:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerEmail" runat="server" Width="200" CssClass="customeremail"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Address:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerAddress" runat="server" Width="200" CssClass="customeraddress"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Wallet ID:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCustomerWalletID" runat="server" Width="200" CssClass="customerwalletid"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <input type="button" id="btnAddCustomerInfo" value="Submit" class="btn btn-large btn-block btn-info" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </ContentTemplate>
        </eid:RadWindow>
    </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript">
    Sys.Application.add_load(invoice2Load);
    var mySwiper;
    function randomColor() {
        var colors = ('blue red green orange pink').split(' ');
        return colors[Math.floor(Math.random() * colors.length)];
    }

    function customerSearch(sender, args) {
        $(".choosecustomerlist tr").each(function () {
            $(this).css("display", "table-row");
        });
        $(".choosecustomerlist tr").each(function () {
            if ($(this).find(".choosecustomersname").eq(0).val() != "" && $(this).find(".choosecustomersname").eq(0).val().toLowerCase().indexOf(args.get_text().toLowerCase()) < 0) {
                $(this).css("display", "none");
            }
        });
    }

    function productSearch(sender, args) {
        var lengt = $(".swiper-slide").length;
        for (var i = lengt; i > 0; i--) {
            mySwiper.removeSlide(i - 1);
        }
        buildGrid(args.get_text());
    }

    function buildCategory() {
        $(".cat-container .list>tbody>tr").each(function () { $(this).remove(); });
        var tr = $("<tr><td style='padding-left:20px'>All Products</td></tr>");
        $(".cat-container .list>tbody").append(tr);
        tr.click(function () {
            $(".cattitle").text($(this).find("td").eq(0).text());
            $(".cat-container").toggle("fade");
            var lengt = $(".swiper-slide").length;
            for (var i = lengt; i > 0; i--) {
                mySwiper.removeSlide(i - 1);
            }
            buildGrid("");
        });
        $(".pro_list").find("tr").each(function () {
            var catname = $(this).find(".product_catname").eq(0).text();
            var catexists = false;
            $(".cat-container .list tbody>tr").find("td").each(function () {
                if ($(this).text() == catname) {
                    catexists = true;
                }
            });

            if (!catexists) {
                var tr = $("<tr><td style='padding-left:20px'>" + catname + "</td></tr>");
                $(".cat-container .list>tbody").append(tr);

                tr.click(function () {
                    $(".cattitle").text($(this).find("td").eq(0).text());
                    $(".cat-container").toggle("fade");
                    var lengt = $(".swiper-slide").length;
                    for (var i = lengt; i > 0; i--) {
                        mySwiper.removeSlide(i - 1);
                    }
                    buildGridCategory($(this).find("td").eq(0).text());
                });
            }
        });
    }

    function buildGrid(filter) {
        var total = $(".pro_list").find("tr").length;
        var total2 = 0;
        for (var i = 0; i < total; i++) {
            var productname = $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text();
            if (productname.indexOf(filter) < 0) {
                continue;
            }
            total2 = total2 + 1;
        }
        for (var i = 0; i < Math.ceil(total2 / 16); i++) {
            mySwiper.appendSlide('<table table-index="' + i.toString() + '"></table>', 'swiper-slide')
        }
        var j = 0;
        for (var i = 0; i < total; i++) {
            var productname = $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text();
            if (productname.indexOf(filter) < 0) {
                continue;
            }
            var container = $(".swiper-container").find("table[table-index='" + Math.floor(j / 16) + "']");
            if (container.length > 0) {
                var row = container.find("tr[row-index='" + Math.floor(j / 4) + "']");
                if (row.length > 0) {
                    var column = row.find("td[column-index='" + Math.floor(j % 4) + "']");
                    if (column.length > 0) {
                        column.eq(0).attr("product-id", $(".pro_list").find("tr").eq(i).find(".product_id").eq(0).text());
                        column.eq(0).attr("product-discount", $(".pro_list").find("tr").eq(i).find(".product_discount").eq(0).text());
                        column.eq(0).append($('<div class="pitem ' + randomColor() + '-slide"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="productimage"><img style="min-width:170px;" src="' + $.trim($(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text()) + '"/></div><div class="price">' + parseFloat($.trim($(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text())).toFixed(2) + '</div></div>'));
                    } else {
                        //row.eq(0).append($('<td column-index="' + Math.floor(i % 4) + '" align="center"><div class="pitem ' + randomColor() + '-slide" style="background-image:url(' + $(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text() + ');"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="price">' + $(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text() + '</div></div></td>'));
                        var newcol = $('<td column-index="' + Math.floor(j % 4) + '" align="center"><div class="pitem ' + randomColor() + '-slide"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="productimage"><img style="min-width:170px;" src="' + $.trim($(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text()) + '"/></div><div class="price">' + parseFloat($.trim($(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text())).toFixed(2) + '</div></div></td>');
                        newcol.eq(0).attr("product-id", $(".pro_list").find("tr").eq(i).find(".product_id").eq(0).text());
                        newcol.eq(0).attr("product-discount", $(".pro_list").find("tr").eq(i).find(".product_discount").eq(0).text());
                        row.eq(0).append(newcol);
                    }
                } else {
                    //container.eq(0).append($("<tr row-index='" + Math.floor(i / 4) + "'><td column-index='" + Math.floor(i % 4) + "' align='center'><div class='pitem " + randomColor() + "-slide' style='background-image:url(" + $(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text() + ");'><div class='title'>" + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + "</div><div class='price'>" + $(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text() + "</div></div></td></tr>"));
                    var newrow = $("<tr row-index='" + Math.floor(j / 4) + "'></tr>");
                    var newcol = $('<td column-index="' + Math.floor(j % 4) + '" align="center"><div class="pitem ' + randomColor() + '-slide"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="productimage"><img style="min-width:170px;" src="' + $.trim($(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text()) + '"/></div><div class="price">' + parseFloat($.trim($(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text())).toFixed(2) + '</div></div></td>');
                    newcol.eq(0).attr("product-id", $(".pro_list").find("tr").eq(i).find(".product_id").eq(0).text());
                    newcol.eq(0).attr("product-discount", $(".pro_list").find("tr").eq(i).find(".product_discount").eq(0).text());
                    newrow.append(newcol);
                    container.eq(0).append(newrow);
                }
            }
            j++;
        }
        bindProductClick();
    }

    function buildGridCategory(category) {

        var total = $(".pro_list").find("tr").length;
        var total2 = 0;
        for (var i = 0; i < total; i++) {
            var productcategory = $(".pro_list").find("tr").eq(i).find(".product_catname").eq(0).text();
            if (productcategory != category) {
                continue;
            }
            total2 = total2 + 1;
        }
        for (var i = 0; i < Math.ceil(total2 / 16); i++) {
            mySwiper.appendSlide('<table table-index="' + i.toString() + '"></table>', 'swiper-slide')
        }
        var j = 0;
        for (var i = 0; i < total; i++) {
            var productcategory = $(".pro_list").find("tr").eq(i).find(".product_catname").eq(0).text();
            if (productcategory != category) {
                continue;
            }
            var container = $(".swiper-container").find("table[table-index='" + Math.floor(j / 16) + "']");
            if (container.length > 0) {
                var row = container.find("tr[row-index='" + Math.floor(j / 4) + "']");
                if (row.length > 0) {
                    var column = row.find("td[column-index='" + Math.floor(j % 4) + "']");
                    if (column.length > 0) {
                        column.eq(0).attr("product-id", $(".pro_list").find("tr").eq(i).find(".product_id").eq(0).text());
                        column.eq(0).attr("product-discount", $(".pro_list").find("tr").eq(i).find(".product_discount").eq(0).text());
                        column.eq(0).append($('<div class="pitem ' + randomColor() + '-slide"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="productimage"><img style="min-width:170px;" src="' + $.trim($(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text()) + '"/></div><div class="price">' + parseFloat($.trim($(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text())).toFixed(2) + '</div></div>'));
                    } else {
                        //row.eq(0).append($('<td column-index="' + Math.floor(i % 4) + '" align="center"><div class="pitem ' + randomColor() + '-slide" style="background-image:url(' + $(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text() + ');"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="price">' + $(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text() + '</div></div></td>'));
                        var newcol = $('<td column-index="' + Math.floor(j % 4) + '" align="center"><div class="pitem ' + randomColor() + '-slide"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="productimage"><img style="min-width:170px;" src="' + $.trim($(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text()) + '"/></div><div class="price">' + parseFloat($.trim($(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text())).toFixed(2) + '</div></div></td>');
                        newcol.eq(0).attr("product-id", $(".pro_list").find("tr").eq(i).find(".product_id").eq(0).text());
                        newcol.eq(0).attr("product-discount", $(".pro_list").find("tr").eq(i).find(".product_discount").eq(0).text());
                        row.eq(0).append(newcol);
                    }
                } else {
                    //container.eq(0).append($("<tr row-index='" + Math.floor(i / 4) + "'><td column-index='" + Math.floor(i % 4) + "' align='center'><div class='pitem " + randomColor() + "-slide' style='background-image:url(" + $(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text() + ");'><div class='title'>" + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + "</div><div class='price'>" + $(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text() + "</div></div></td></tr>"));
                    var newrow = $("<tr row-index='" + Math.floor(j / 4) + "'></tr>");
                    var newcol = $('<td column-index="' + Math.floor(j % 4) + '" align="center"><div class="pitem ' + randomColor() + '-slide"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="productimage"><img style="min-width:170px;" src="' + $.trim($(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text()) + '"/></div><div class="price">' + parseFloat($.trim($(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text())).toFixed(2) + '</div></div></td>');
                    newcol.eq(0).attr("product-id", $(".pro_list").find("tr").eq(i).find(".product_id").eq(0).text());
                    newcol.eq(0).attr("product-discount", $(".pro_list").find("tr").eq(i).find(".product_discount").eq(0).text());
                    newrow.append(newcol);
                    container.eq(0).append(newrow);
                }
            }
            j++;
        }
        bindProductClick();
    }

    function bindProductClick() {
        $(".pitem").unbind("click");
        $(".pitem").click(function () {
            //$(this).effect("highlight", {}, 500, function () { });
            var pname = $(this).find(".title").eq(0).text();
            var pprice = $(this).find(".price").eq(0).text();
            var pid = $.trim($(this).parent().attr("product-id"));
            var pdiscount = $.trim($(this).parent().attr("product-discount"));
            var pexists = false;
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    if ($(this).attr("product-id") == pid) {
                        pexists = true;
                        var quantity = parseInt($(this).find(".product_quantity").text());
                        quantity = quantity + 1;
                        $(this).find(".product_quantity").text(quantity);
                        $(this).find(".product_price").text(parseFloat(pprice) * quantity);
                        if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                            if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                                $("#currentquantity").val($(this).find(".product_quantity").text());
                            }
                        }
                        //updateCartTotal();
                        scrollIntoView($(this), ".cartitem-container");
                    }
                }
            });
            if (!pexists) {
                var tr = $("<tr></tr>");

                var ntd = $("<td style='width:160px;padding-left:10px'><div class='product_name'>" + pname + "</div></td>");
                var qtd = $("<td style='width:30px' class='product_quantity'></td>");
                var ptd = $("<td style='width:60px' class='product_price'></td>");
                var dtd = $("<td style='width:30px'><img class='cart_delete' style='width:20px;height:20px;display:none' src='images/remove.png'/></td>");
                ptd.text(pprice);
                qtd.text("1");
                tr.append(ntd);
                tr.append(qtd);
                tr.append(ptd);
                tr.append(dtd);

                dtd.click(function () {
                    $(this).closest("tr").remove();
                    $(".number-row").css("display", "none");
                    $(".discount-row").css("display", "none");
                    $(".number-row").attr("for-id", "0");
                    $(".discount-row").attr("for-id", "0");
                    updateCartTotal();
                });

                $(".cart-container .cartlist>tbody").append(tr);
                tr.attr("product-id", pid);
                tr.attr("product-discount", pdiscount);
                tr.attr("product-price", pprice);
                //scrollToBottom(tr, ".cartitem-container");
                $(".cartitem-container").scrollTop($('.cartitem-container')[0].scrollHeight);
                tr.effect("highlight", {}, 500, function () { });
                tr.click(function () {
                    $(".cart-container .cartlist>tbody>tr").each(function () { $(this).find(".cart_delete").css("display", "none"); });
                    $(this).find(".cart_delete").toggle();
                    if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                        $(".number-row").toggle();
                        $(".discount-row").toggle();
                    } else {
                        $(".number-row").css("display", "table-row");
                        $(".discount-row").css("display", "table-row");
                    }

                    $(".number-row").attr("for-id", "0");
                    $(".discount-row").attr("for-id", "0");

                    if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                        $(".number-row").attr("for-id", $(this).attr("product-id"));
                        $(".discount-row").attr("for-id", $(this).attr("product-id"));
                        $(".discount-row").insertAfter($(this));
                        $(".number-row").insertAfter($(this));
                        $("#currentquantity").val($(this).find(".product_quantity").text());
                        $("#currentdiscount").val($(this).attr("product-discount"));
                        scrollIntoView($(".discount-row"), ".cartitem-container");
                    }

                    $(".cart-container .cartlist>tbody>tr").each(function () {
                        if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                            if ($.trim($(this).find(".product_quantity").text()) == "0" && $(".number-row").attr("for-id") != $(this).attr("product-id")) {
                                $(this).remove();
                            }
                        }
                    });
                });
            }
            updateCartTotal();
        });
    }
    var isshowcategoryclicked = false;
    function invoice2Load(sender, args) {
        mySwiper = new Swiper('.swiper-container', {
            pagination: '.pagination',
            paginationClickable: true
        });
        buildGrid("");
        buildCategory();

        var isiPad = navigator.userAgent.match(/iPad/i) != null;
        if (isiPad) {
            $("#btnSendSMS").css("top", "10px");
            $("#btnSendWallet").css("top", "10px");
            $("#btnSendSMS").css("right", "-20px");
            $("#btnSendWallet").css("right", "-20px");
            $("#btnPrint").css("width", "525px");
        }

        $(".back-todetail").unbind("click");
        $(".back-todetail").click(function () {
            $(".invoicedetailcontainer").animate({
                marginLeft: "+=620px"
            }, 500, function () {
                //$(".cancel_box").trigger("click");
            });
        });

        $(".btn-receipt").unbind("click");
        $(".btn-receipt").click(function () {
//            $(".invoicedetailcontainer").animate({
//                marginLeft: "-=620px"
//            }, 500, function () {
//                //$(".cancel_box").trigger("click");
//            });
        });

        $(".moreactionicon").unbind("click");
        $(".moreactionicon").click(function () {
            $(".invoicefilterarea").toggle("slide", function () {
                if ($(".invoicefilterarea").css("display").toLowerCase() == "none") {
                    $(".invoicelistarea").height(730);
                } else {
                    $(".invoicelistarea").height(680);
                }
            });
        });

        $(".cancel_box").unbind("click");
        $(".cancel_box").click(function () {
            $(".choosecustomerlist tr").each(function () {
                $(this).css("display", "table-row");
            });
            $(".customerfinder .rsbInput").val("");
            var radsearch = $find('<%=customerSearchBox1.ClientID %>');
            radsearch.clear();
        });

        $("#inpSMS").unbind("focus");
        $("#inpSMS").focus(function () {
            if ($(this).val() == "SMS") {
                $(this).val("");
                $(this).css("color", "black");

            }
            $("#btnSendSMS").css("display", "block");
        });

        $("#inpWalletId").unbind("focus");
        $("#inpWalletId").focus(function () {
            if ($(this).val() == "Wallet ID") {
                $(this).val("");
                $(this).css("color", "black");

            }
            $("#btnSendWallet").css("display", "block");
        });

        $("#inpEmail").unbind("focus");
        $("#inpEmail").focus(function () {
            if ($(this).val() == "Email") {
                $(this).val("");
                $(this).css("color", "black");

            }
            $("#btnSendEmail").css("display", "block");
        });

        $("#inpSMS").unbind("blur");
        $("#inpSMS").blur(function () {
            if ($(this).val() == "") {
                $(this).val("SMS");
                $(this).css("color", "rgb(204, 204, 204)");
                $("#btnSendSMS").css("display", "none");
            }

        });

        $("#inpWalletId").unbind("blur");
        $("#inpWalletId").blur(function () {
            if ($(this).val() == "") {
                $(this).val("Wallet ID");
                $(this).css("color", "rgb(204, 204, 204)");
                $("#btnSendWallet").css("display", "none");
            }
        });

        $("#inpEmail").unbind("blur");
        $("#inpEmail").blur(function () {
            if ($(this).val() == "") {
                $(this).val("Email");
                $(this).css("color", "rgb(204, 204, 204)");
                $("#btnSendEmail").css("display", "none");
            }
        });

        $("#inpSMS2").unbind("focus");
        $("#inpSMS2").focus(function () {
            if ($(this).val() == "SMS") {
                $(this).val("");
                $(this).css("color", "black");

            }
            $("#btnSendSMS2").css("display", "block");
        });

        $("#inpWalletId2").unbind("focus");
        $("#inpWalletId2").focus(function () {
            if ($(this).val() == "Wallet ID") {
                $(this).val("");
                $(this).css("color", "black");

            }
            $("#btnSendWallet2").css("display", "block");
        });

        $("#inpEmail2").unbind("focus");
        $("#inpEmail2").focus(function () {
            if ($(this).val() == "Email") {
                $(this).val("");
                $(this).css("color", "black");

            }
            $("#btnSendEmail2").css("display", "block");
        });

        $("#inpSMS2").unbind("blur");
        $("#inpSMS2").blur(function () {
            if ($(this).val() == "") {
                $(this).val("SMS");
                $(this).css("color", "rgb(204, 204, 204)");
                $("#btnSendSMS2").css("display", "none");
            }

        });

        $("#inpWalletId2").unbind("blur");
        $("#inpWalletId2").blur(function () {
            if ($(this).val() == "") {
                $(this).val("Wallet ID");
                $(this).css("color", "rgb(204, 204, 204)");
                $("#btnSendWallet2").css("display", "none");
            }
        });

        $("#inpEmail2").unbind("blur");
        $("#inpEmail2").blur(function () {
            if ($(this).val() == "") {
                $(this).val("Email");
                $(this).css("color", "rgb(204, 204, 204)");
                $("#btnSendEmail2").css("display", "none");
            }
        });

        $("#btnPrint").unbind("click");
        $("#btnPrint").click(function () {
            $("#btnPrintReceipt").trigger("click");
        });

        $("#btnPrint2").unbind("click");
        $("#btnPrint2").click(function () {
            $("#btnPrintReceipt").trigger("click");
        });  

        $(".orderarea").unbind("click");
        $(".orderarea").click(function () {
            if (!isshowcategoryclicked) {
                $(".cat-container").css("display", "none");
            }
            isshowcategoryclicked = false;
        });
        $(".showcategory").unbind("click");
        $(".showcategory").click(function () {
            $(".cat-container").css("left", $(this).position().left + ($(this).width() - 300) / 2 + "px");
            $(".cat-container").toggle("fade");
            isshowcategoryclicked = true;
        });
        $(".cancelsearch").unbind("click");
        $(".cancelsearch").click(function () {
            $(".productsheader").css("display", "table-row");
            $(".searchrow").css("display", "none");
            $(sender.get_inputElement()).val("");
        });

        $(".searchproducticon").unbind("click");
        $(".searchproducticon").click(function () {
            $(".productsheader").css("display", "none");
            $(".searchrow").css("display", "table-row");
        });

        $(".addcustomerfrominvoiceicon").unbind("click");
        $(".addcustomerfrominvoiceicon").click(function () {
            $(".customertr").css("display", "none");
            $(".custommessage").text("");
            $(".customername").val("");
            $(".customercountry").val("");
            $(".customeremail").val("");
            $(".customerphone").val("");
            $(".customermobilephone").val("");
            $(".customerwalletid").val("");
            $(".customeraddress").val("");
            var radwindow = $find('<%=AddCustomerWindow.ClientID %>');
            radwindow.show();
        });

        $(".customername").unbind("focusout");
        $(".customername").focusout(function () {
            if ($(".customername").val() == "") {
                $(".customertr").css("display", "");
                $(".custommessage").text("Customer name is required");
                return false;
            } else {
                $(".customertr").css("display", "none");
                $(".custommessage").text("");
                return false;
            }
        });

        $(".cancelinvoice").unbind("click");
        $(".cancelinvoice").click(function () {
            var radwindow = $find('<%=InvoiceCancelRadWindow1.ClientID %>');
            radwindow.show();
            $("#lblCancelID").val($("#currentinvoiceid").val());
            $("#lblCancelNo").text($("#currentinvoiceno").val());
        });

        $("#btnAddCustomerInfo").unbind("click");
        $("#btnAddCustomerInfo").click(function () {
            if ($(".customername").val() == "") {
                $(".customertr").css("display", "");
                $(".custommessage").text("Customer name is required");
                return false;
            }
            var radwindow = $find('<%=AddCustomerWindow.ClientID %>');
            radwindow.close();
            //$("#btnAddCustomer").trigger("click");
            $("#isaddcustomer").val("1");
            //
            var customertr = $("<tr></tr>");
            var nametd = $('<td align="left" class="choosecustomername" style="padding-left:20px"><span id="InvoiceControl_customerItems_lblName_1">' + $(".customername").val() + '</span><input type="hidden" value="-1" class="choosecustomerid"><input type="hidden" value="' + $(".customername").val() + '" class="choosecustomersname"></td>');
            var checktd = $('<td style="width:50px"><img src="images/check.png" class="customerchecked" style="display: none;"></td>');
            customertr.append(nametd);
            customertr.append(checktd);
            $(".choosecustomerlist").append(customertr);
            customertr.click(function () {
                $(".choosecustomerlist tr").each(function () {
                    $(this).find(".customerchecked").eq(0).css("display", "none");
                });
                $(this).find(".customerchecked").eq(0).css("display", "table-cell");
                $(".selectedcustomer").text("Customer: " + $.trim($(this).find(".choosecustomersname").eq(0).val()));
                $(".selectedcustomername").val($.trim($(this).find(".choosecustomersname").eq(0).val()));
                $(".selectedcustomerid").val($(this).find(".choosecustomerid").eq(0).val());
                //                $("#inpSMS").val($(this).find(".choosecustomerphone").eq(0).val());
                //                $("#inpWalletId").val($(this).find(".choosecustomerwallet").eq(0).val());
                //                $("#inpEmail").val($(this).find(".choosecustomeremail").eq(0).val());
                $(".itscart").animate({
                    marginLeft: "+=300px"
                }, 500, function () {
                    $(".cancel_box").trigger("click");
                });
            });
            customertr.trigger("click");
        });
        $("#submitCancelInvoice").unbind("click");
        $("#submitCancelInvoice").click(function () {

            var radwindow = $find('<%=InvoiceCancelRadWindow1.ClientID %>');
            radwindow.close();
            $("#btnCancelInvoice").trigger("click");
        });

        $(".invoicelist tr").unbind("click");
        $(".invoicelist tr").click(function () {

            $("#selectedinvoiceid").val($(this).find(".invoiceid").eq(0).val());

            $("#btnInvoiceSelected").trigger("click");
        }
        );

        $(".invoicelist tr").each(function () {
            if ($(this).find(".invoiceid").eq(0).val() == $("#selectedinvoiceid").val()) {
                $(this).css("background-color", "#EFF3F4");
            }
        });

        $(".btnswitch").unbind("click");
        $(".btnswitch").click(function () {
            $(".orderarea").animate({
                marginLeft: "-=1024px"
            }, 500, function () {
                //animation = false;
            });

            $(".choosecustomerlist tr").each(function () {
                $(this).find(".customerchecked").eq(0).css("display", "none");
                if ($(this).find(".choosecustomersname").eq(0).val() == $(".selectedcustomername").val()) {
                    $(this).find(".customerchecked").eq(0).css("display", "table-cell");
                    $(".selectedcustomerid").val($(this).find(".choosecustomerid").eq(0).val());
                    //                    $("#inpSMS").val($(this).find(".choosecustomerphone").eq(0).val());
                    //                    $("#inpWalletId").val($(this).find(".choosecustomerwallet").eq(0).val());
                    //                    $("#inpEmail").val($(this).find(".choosecustomeremail").eq(0).val());
                }
            });
            $(".cattitle").text("All Products");
            var lengt = $(".swiper-slide").length;
            for (var i = lengt; i > 0; i--) {
                mySwiper.removeSlide(i - 1);
            }
            buildGrid("");

            $(".savewithoutpay").css("display", "table-cell");
            $("#btntotal").val($("#btntotal").val().replace("Total", "Pay it"));
        });

        $(".back-command").unbind("click");
        $(".back-command").click(function () {
            if (parseInt($(".itscart").css("marginLeft").replace("px", "")) < 0) {
                $(".itscart").animate({
                    marginLeft: "+=300px"
                }, 500, function () {
                    $(".cancel_box").trigger("click");
                });
            }
            $(".orderarea").animate({
                marginLeft: "+=1024px"
            }, 500, function () {
                //animation = false;
            });
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    $(this).remove();
                } else {
                    $(this).css("display", "none");
                }
            });

            $(".invoiceitemslist tbody>tr").each(function () {
                $(".selectedcustomer").text("Customer: " + $(".invoicecustomername").text());
                $(".selectedcustomername").val($(".invoicecustomername").text());
                var invoicepid = $(this).find(".invoiceproductid").eq(0).val();
                var invoicepdiscount = $(this).find(".invoiceproductdiscount").eq(0).val();
                var invoicepprice = $(this).find(".invoiceproductprice").eq(0).val();
                var invoicepname = $(this).find(".invoiceproductname").eq(0).val();
                var invoicepquantity = $(this).find(".invoiceproductquantity").eq(0).val();
                var pexists = false;
                $(".cart-container .cartlist>tbody>tr").each(function () {
                    if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                        if ($(this).attr("product-id") == invoicepid) {
                            pexists = true;
                            var quantity = parseInt($(this).find(".product_quantity").text());
                            quantity = quantity + parseInt(invoicepquantity);
                            $(this).find(".product_quantity").text(quantity);
                            $(this).find(".product_price").text(parseFloat(invoicepprice) * quantity);
                            //updateCartTotal();
                            // scrollIntoView($(this), ".cartitem-container");
                        }
                    }
                });
                if (!pexists) {
                    var tr = $("<tr></tr>");

                    var ntd = $("<td style='width:160px;padding-left:10px'><div class='product_name'>" + invoicepname + "</div></td>");
                    var qtd = $("<td style='width:50px' class='product_quantity'></td>");
                    var ptd = $("<td style='width:70px' class='product_price'></td>");
                    var dtd = $("<td style='width:30px'><img class='cart_delete' style='width:20px;height:20px;display:none' src='images/remove.png'/></td>");
                    ptd.text(invoicepprice);
                    qtd.text(invoicepquantity);
                    tr.append(ntd);
                    tr.append(qtd);
                    tr.append(ptd);
                    tr.append(dtd);

                    dtd.click(function () {
                        $(this).closest("tr").remove();
                        $(".number-row").css("display", "none");
                        $(".discount-row").css("display", "none");
                        $(".number-row").attr("for-id", "0");
                        $(".discount-row").attr("for-id", "0");
                        updateCartTotal();
                    });

                    $(".cart-container .cartlist>tbody").append(tr);
                    tr.attr("product-id", invoicepid);
                    tr.attr("product-discount", invoicepdiscount);
                    tr.attr("product-price", invoicepprice);
                    //scrollToBottom(tr, ".cartitem-container");
                    $(".cartitem-container").scrollTop($('.cartitem-container')[0].scrollHeight);
                    //tr.effect("highlight", {}, 500, function () { });
                    tr.click(function () {
                        $(".cart-container .cartlist>tbody>tr").each(function () { $(this).find(".cart_delete").css("display", "none"); });
                        $(this).find(".cart_delete").toggle();
                        if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                            $(".number-row").toggle();
                            $(".discount-row").toggle();
                        } else {
                            $(".number-row").css("display", "table-row");
                            $(".discount-row").css("display", "table-row");
                        }

                        $(".number-row").attr("for-id", "0");
                        $(".discount-row").attr("for-id", "0");

                        if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                            $(".number-row").attr("for-id", $(this).attr("product-id"));
                            $(".discount-row").attr("for-id", $(this).attr("product-id"));
                            $(".discount-row").insertAfter($(this));
                            $(".number-row").insertAfter($(this));
                            $("#currentquantity").val($(this).find(".product_quantity").text());
                            $("#currentdiscount").val($(this).attr("product-discount"));
                            scrollIntoView($(".discount-row"), ".cartitem-container");
                        }

                        $(".cart-container .cartlist>tbody>tr").each(function () {
                            if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                                if ($.trim($(this).find(".product_quantity").text()) == "0" && $(".number-row").attr("for-id") != $(this).attr("product-id")) {
                                    $(this).remove();
                                }
                            }
                        });
                    });
                }
                updateCartTotal();
            });
        });
        $(".cart-container .cartlist>tbody>tr").each(function () {
            if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                $(this).remove();
            } else {
                $(this).css("display", "none");
            }
        });
        $(".invoiceitemslist tbody>tr").each(function () {
            $(".selectedcustomer").text("Customer: " + $(".invoicecustomername").text());
            $(".selectedcustomername").val($(".invoicecustomername").text());
            var invoicepid = $(this).find(".invoiceproductid").eq(0).val();
            var invoicepdiscount = $(this).find(".invoiceproductdiscount").eq(0).val();
            var invoicepprice = $(this).find(".invoiceproductprice").eq(0).val();
            var invoicepname = $(this).find(".invoiceproductname").eq(0).val();
            var invoicepquantity = $(this).find(".invoiceproductquantity").eq(0).val();
            var pexists = false;
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    if ($(this).attr("product-id") == invoicepid) {
                        pexists = true;
                        var quantity = parseInt($(this).find(".product_quantity").text());
                        quantity = quantity + parseInt(invoicepquantity);
                        $(this).find(".product_quantity").text(quantity);
                        $(this).find(".product_price").text(parseFloat(invoicepprice) * quantity);
                        //updateCartTotal();
                        // scrollIntoView($(this), ".cartitem-container");
                    }
                }
            });
            if (!pexists) {
                var tr = $("<tr></tr>");

                var ntd = $("<td style='width:160px;padding-left:10px'><div class='product_name'>" + invoicepname + "</div></td>");
                var qtd = $("<td style='width:50px' class='product_quantity'></td>");
                var ptd = $("<td style='width:70px' class='product_price'></td>");
                var dtd = $("<td style='width:30px'><img class='cart_delete' style='width:20px;height:20px;display:none' src='images/remove.png'/></td>");

                ptd.text(invoicepprice);
                qtd.text(invoicepquantity);
                tr.append(ntd);
                tr.append(qtd);
                tr.append(ptd);
                tr.append(dtd);

                dtd.click(function () {
                    $(this).closest("tr").remove();
                    $(".number-row").css("display", "none");
                    $(".discount-row").css("display", "none");
                    $(".number-row").attr("for-id", "0");
                    $(".discount-row").attr("for-id", "0");
                    updateCartTotal();
                });

                $(".cart-container .cartlist>tbody").append(tr);
                tr.attr("product-id", invoicepid);
                tr.attr("product-discount", invoicepdiscount);
                tr.attr("product-price", invoicepprice);
                //scrollToBottom(tr, ".cartitem-container");
                $(".cartitem-container").scrollTop($('.cartitem-container')[0].scrollHeight);
                //tr.effect("highlight", {}, 500, function () { });
                tr.click(function () {
                    $(".cart-container .cartlist>tbody>tr").each(function () { $(this).find(".cart_delete").css("display", "none"); });
                    $(this).find(".cart_delete").toggle();
                    if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                        $(".number-row").toggle();
                        $(".discount-row").toggle();
                    } else {
                        $(".number-row").css("display", "table-row");
                        $(".discount-row").css("display", "table-row");
                    }

                    $(".number-row").attr("for-id", "0");
                    $(".discount-row").attr("for-id", "0");

                    if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                        $(".number-row").attr("for-id", $(this).attr("product-id"));
                        $(".discount-row").attr("for-id", $(this).attr("product-id"));
                        $(".discount-row").insertAfter($(this));
                        $(".number-row").insertAfter($(this));
                        $("#currentquantity").val($(this).find(".product_quantity").text());
                        $("#currentdiscount").val($(this).attr("product-discount"));
                        scrollIntoView($(".discount-row"), ".cartitem-container");
                    }

                    $(".cart-container .cartlist>tbody>tr").each(function () {
                        if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                            if ($.trim($(this).find(".product_quantity").text()) == "0" && $(".number-row").attr("for-id") != $(this).attr("product-id")) {
                                $(this).remove();
                            }
                        }
                    });
                });
            }
            updateCartTotal();
        });



        $(".btn-minus").unbind("click");
        $(".btn-minus").click(function () {
            var quantity = parseInt($("#currentquantity").val());
            if (quantity > 0) {
                quantity = quantity - 1;
            }
            $("#currentquantity").val(quantity);
            $(".cart-container .cartlist>tbody>tr[product-id='" + $(".number-row").attr("for-id") + "']").find(".product_quantity").text(quantity);
            updateCartTotal();
        });


        $(".btn-add").unbind("click");
        $(".btn-add").click(function () {
            var quantity = parseInt($("#currentquantity").val());
            quantity = quantity + 1;
            $("#currentquantity").val(quantity);
            $(".cart-container .cartlist>tbody>tr[product-id='" + $(".number-row").attr("for-id") + "']").find(".product_quantity").text(quantity);
            updateCartTotal();
        });

        $("#currentdiscount").unbind("change");
        $("#currentdiscount").change(function () {
            $(".cart-container .cartlist>tbody>tr[product-id='" + $(".discount-row").attr("for-id") + "']").attr("product-discount", $("#currentdiscount").val());
            updateCartTotal();
        });

        $("#currentquantity").unbind("change");
        $("#currentquantity").change(function () {
            $(".cart-container .cartlist>tbody>tr[product-id='" + $(".number-row").attr("for-id") + "']").find(".product_quantity").text($("#currentquantity").val());
            updateCartTotal();
        });

        $("#gstcheckbox").change(function () {
            updateCartTotal();
        });

        $(".choosecontainer").unbind("click");
        $(".choosecontainer").click(function () {
            $(".itscart").animate({
                marginLeft: "-=300px"
            }, 500, function () {
                //animation = false;
            });

            $(".choosecustomerlist tr").each(function () {
                $(this).find(".customerchecked").eq(0).css("display", "none");
                if ($(this).find(".choosecustomerid").eq(0).val() == $(".selectedcustomerid").val()) {
                    $(this).find(".customerchecked").eq(0).css("display", "table-cell");
                    //                    $("#inpSMS").val($(this).find(".choosecustomerphone").eq(0).val());
                    //                    $("#inpWalletId").val($(this).find(".choosecustomerwallet").eq(0).val());
                    //                    $("#inpEmail").val($(this).find(".choosecustomeremail").eq(0).val());
                }
            });

        });

        $(".back-tocart").unbind("click");
        $(".back-tocart").click(function () {
            $(".itscart").animate({
                marginLeft: "+=300px"
            }, 500, function () {
                $(".cancel_box").trigger("click");
            });
        });

        $(".choosecustomerlist tr").each(function () { $(this).unbind("click"); $(this).find(".customerchecked").eq(0).css("display", "none"); });
        $(".choosecustomerlist tr").each(function () {
            $(this).click(function () {
                $(".choosecustomerlist tr").each(function () {
                    $(this).find(".customerchecked").eq(0).css("display", "none");
                });
                $(this).find(".customerchecked").eq(0).css("display", "table-cell");
                $(".selectedcustomer").text("Customer: " + $.trim($(this).find(".choosecustomersname").eq(0).val()));
                $(".selectedcustomername").val($.trim($(this).find(".choosecustomersname").eq(0).val()));
                $(".selectedcustomerid").val($(this).find(".choosecustomerid").eq(0).val());
                //                $("#inpSMS").val($(this).find(".choosecustomerphone").eq(0).val());
                //                alert($("#inpSMS").val());
                //                $("#inpWalletId").val($(this).find(".choosecustomerwallet").eq(0).val());
                //                $("#inpEmail").val($(this).find(".choosecustomeremail").eq(0).val());
                $(".itscart").animate({
                    marginLeft: "+=300px"
                }, 500, function () {
                    $(".cancel_box").trigger("click");
                });
            });
        });

        $(".btnpay").unbind("click");
        $(".btnpay").click(function () {

            $(".choosecustomerlist tr").each(function () {
                $(this).find(".customerchecked").eq(0).css("display", "none");
                if ($(this).find(".choosecustomersname").eq(0).val() == $(".selectedcustomername").val()) {
                    $(this).find(".customerchecked").eq(0).css("display", "table-cell");
                    $(".selectedcustomerid").val($(this).find(".choosecustomerid").eq(0).val());
                    //                    $("#inpSMS").val($(this).find(".choosecustomerphone").eq(0).val());
                    //                    $("#inpWalletId").val($(this).find(".choosecustomerwallet").eq(0).val());
                    //                    $("#inpEmail").val($(this).find(".choosecustomeremail").eq(0).val());
                }
            });

            var selectedrows = false;
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    selectedrows = true;
                }
            });
            $(".cart-container .cartlist>tbody>tr").each(function () { $(this).find(".cart_delete").css("display", "none"); });
            if (!selectedrows) {
                radalert("No order items!", 400, 100, "ERROR");
                return;
            }

            $(".orderarea").animate({
                marginLeft: "-=1745px"
            }, 500, function () {
                $(".cart-command").text("<<");
            });

            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    $(this).unbind("click");
                }
            });

            $(".number-row").css("display", "none");
            $(".discount-row").css("display", "none");
            $(".number-row").attr("product-id", "none");
            $(".discount-row").attr("product-id", "none");

            $("#gstcheckbox").prop("disabled", "disabled");

            $("#btntotal").css("display", "none");
            $(".savewithoutpay").css("display", "none");

            $(".cartitem-container").height($(".cartitem-container").height() + $("#btntotal").height());
            $(".cartitem-container").scrollTop(0);

            //$("#btnDeleteCartItem").css("display", "none");
            $(".choosecontainer").unbind("click");
            $(".choosecustomericon").css("display", "none");

        });

        $("#btntotal").unbind("click");
        $("#btntotal").click(function () {
            var selectedrows = false;
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    selectedrows = true;
                    //invoicedetails = invoicedetails + $(this).attr("product-id") + "," + $(this).attr("product-discount") + "," + $(this).attr("product-price") + ";";
                }
            });
            $(".cart-container .cartlist>tbody>tr").each(function () { $(this).find(".cart_delete").css("display", "none"); });
            if (!selectedrows) {
                radalert("No order items!", 400, 100, "ERROR");
                return;
            }

            $(".orderarea").animate({
                marginLeft: "-=721px"
            }, 500, function () {
                $(".cart-command").text("<");
            });

            //$(".payment-container").css("margin-left", "-3px");

            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    $(this).unbind("click");
                }
            });

            $(".number-row").css("display", "none");
            $(".discount-row").css("display", "none");
            $(".number-row").attr("product-id", "none");
            $(".discount-row").attr("product-id", "none");

            $("#gstcheckbox").prop("disabled", "disabled");

            $(this).css("display", "none");
            $(".savewithoutpay").css("display", "none");

            $(".cartitem-container").height($(".cartitem-container").height() + $(this).height());
            $(".cartitem-container").scrollTop(0);

            //$("#btnDeleteCartItem").css("display", "none");
            $(".choosecontainer").unbind("click");
            $(".choosecustomericon").css("display", "none");

        });

        $(".cart-command").unbind("click");
        $(".cart-command").click(function () {
            if ($(".cart-command").text() == "x") {
                $(".cart-container .cartlist>tbody>tr").each(function () {
                    if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                        $(this).remove();
                    }
                });
                updateCartTotal();
            } else if ($(".cart-command").text() == "<<") {

                $("#btntotal").css("display", "block");

                if (parseInt($("#currentinvoiceid").val()) > 0) {
                    $(".savewithoutpay").css("display", "block");
                } else {
                    $(".savewithoutpay").css("display", "none");
                }
                $(".cartitem-container").height($(".cartitem-container").height() - 56);
                $(".cartitem-container").scrollTop(0);
                $("#gstcheckbox").prop("disabled", "");
                $(".payment-details .payment-list>tbody>tr").each(function () {
                    $(this).remove();
                });
                $(".payment-method").css("display", "block");
                $(".payment-details").css("display", "none");
                //$("#btnDeleteCartItem").css("display", "");
                $(".choosecontainer").unbind("click");
                $(".choosecustomericon").css("display", "");
                $(".choosecontainer").click(function () {
                    $(".itscart").animate({
                        marginLeft: "-=300px"
                    }, 500, function () {
                        //animation = false;
                    });

                    $(".choosecustomerlist tr").each(function () {
                        $(this).find(".customerchecked").eq(0).css("display", "none");
                        if ($(this).find(".choosecustomerid").eq(0).val() == $(".selectedcustomerid").val()) {
                            $(this).find(".customerchecked").eq(0).css("display", "table-cell");
                            //                            $("#inpSMS").val($(this).find(".choosecustomerphone").eq(0).val());
                            //                            $("#inpWalletId").val($(this).find(".choosecustomerwallet").eq(0).val());
                            //                            $("#inpEmail").val($(this).find(".choosecustomeremail").eq(0).val());
                        }
                    });

                });
                $(".cart-container .cartlist>tbody>tr").each(function () {
                    if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                        $(this).unbind("click");
                        $(this).click(function () {
                            $(".cart-container .cartlist>tbody>tr").each(function () { $(this).find(".cart_delete").css("display", "none"); });
                            $(this).find(".cart_delete").toggle();
                            if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                                $(".number-row").toggle();
                                $(".discount-row").toggle();
                            } else {
                                $(".number-row").css("display", "table-row");
                                $(".discount-row").css("display", "table-row");
                            }

                            $(".number-row").attr("for-id", "0");
                            $(".discount-row").attr("for-id", "0");

                            if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                                $(".number-row").attr("for-id", $(this).attr("product-id"));
                                $(".discount-row").attr("for-id", $(this).attr("product-id"));
                                $(".discount-row").insertAfter($(this));
                                $(".number-row").insertAfter($(this));
                                $("#currentquantity").val($(this).find(".product_quantity").text());
                                $("#currentdiscount").val($(this).attr("product-discount"));
                                scrollIntoView($(".discount-row"), ".cartitem-container");
                            }

                            $(".cart-container .cartlist>tbody>tr").each(function () {
                                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                                    if ($.trim($(this).find(".product_quantity").text()) == "0" && $(".number-row").attr("for-id") != $(this).attr("product-id")) {
                                        $(this).remove();
                                    }
                                }
                            });
                        });
                    }
                });

                if (parseInt($(".itscart").css("marginLeft").replace("px", "")) < 0) {
                    $(".itscart").animate({
                        marginLeft: "+=300px"
                    }, 500, function () {
                        $(".cancel_box").trigger("click");
                    });
                }

                $(".cart-container .cartlist>tbody>tr").each(function () {
                    if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                        $(this).remove();
                    } else {
                        $(this).css("display", "none");
                    }
                });

                $(".invoiceitemslist tbody>tr").each(function () {
                    $(".selectedcustomer").text("Customer: " + $(".invoicecustomername").text());
                    $(".selectedcustomername").val($(".invoicecustomername").text());
                    var invoicepid = $(this).find(".invoiceproductid").eq(0).val();
                    var invoicepdiscount = $(this).find(".invoiceproductdiscount").eq(0).val();
                    var invoicepprice = $(this).find(".invoiceproductprice").eq(0).val();
                    var invoicepname = $(this).find(".invoiceproductname").eq(0).val();
                    var invoicepquantity = $(this).find(".invoiceproductquantity").eq(0).val();
                    var pexists = false;
                    $(".cart-container .cartlist>tbody>tr").each(function () {
                        if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                            if ($(this).attr("product-id") == invoicepid) {
                                pexists = true;
                                var quantity = parseInt($(this).find(".product_quantity").text());
                                quantity = quantity + parseInt(invoicepquantity);
                                $(this).find(".product_quantity").text(quantity);
                                $(this).find(".product_price").text(parseFloat(invoicepprice) * quantity);
                                //updateCartTotal();
                                // scrollIntoView($(this), ".cartitem-container");
                            }
                        }
                    });
                    if (!pexists) {
                        var tr = $("<tr></tr>");

                        var ntd = $("<td style='width:160px;padding-left:10px'><div class='product_name'>" + invoicepname + "</div></td>");
                        var qtd = $("<td style='width:50px' class='product_quantity'></td>");
                        var ptd = $("<td style='width:70px' class='product_price'></td>");
                        var dtd = $("<td style='width:30px'><img class='cart_delete' style='width:20px;height:20px;display:none' src='images/remove.png'/></td>");
                        ptd.text(invoicepprice);
                        qtd.text(invoicepquantity);
                        tr.append(ntd);
                        tr.append(qtd);
                        tr.append(ptd);
                        tr.append(dtd);

                        dtd.click(function () {
                            $(this).closest("tr").remove();
                            $(".number-row").css("display", "none");
                            $(".discount-row").css("display", "none");
                            $(".number-row").attr("for-id", "0");
                            $(".discount-row").attr("for-id", "0");
                            updateCartTotal();
                        });

                        $(".cart-container .cartlist>tbody").append(tr);
                        tr.attr("product-id", invoicepid);
                        tr.attr("product-discount", invoicepdiscount);
                        tr.attr("product-price", invoicepprice);
                        //scrollToBottom(tr, ".cartitem-container");
                        $(".cartitem-container").scrollTop($('.cartitem-container')[0].scrollHeight);
                        //tr.effect("highlight", {}, 500, function () { });
                        tr.click(function () {
                            $(".cart-container .cartlist>tbody>tr").each(function () { $(this).find(".cart_delete").css("display", "none"); });
                            $(this).find(".cart_delete").toggle();
                            if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                                $(".number-row").toggle();
                                $(".discount-row").toggle();
                            } else {
                                $(".number-row").css("display", "table-row");
                                $(".discount-row").css("display", "table-row");
                            }

                            $(".number-row").attr("for-id", "0");
                            $(".discount-row").attr("for-id", "0");

                            if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                                $(".number-row").attr("for-id", $(this).attr("product-id"));
                                $(".discount-row").attr("for-id", $(this).attr("product-id"));
                                $(".discount-row").insertAfter($(this));
                                $(".number-row").insertAfter($(this));
                                $("#currentquantity").val($(this).find(".product_quantity").text());
                                $("#currentdiscount").val($(this).attr("product-discount"));
                                scrollIntoView($(".discount-row"), ".cartitem-container");
                            }

                            $(".cart-container .cartlist>tbody>tr").each(function () {
                                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                                    if ($.trim($(this).find(".product_quantity").text()) == "0" && $(".number-row").attr("for-id") != $(this).attr("product-id")) {
                                        $(this).remove();
                                    }
                                }
                            });
                        });
                    }
                    updateCartTotal();
                });
                $(".orderarea").animate({
                    marginLeft: "+=1745px"
                }, 500, function () {
                    $(".cart-command").text("");
                });
            }
            else if ($(".cart-command").text() == "<") {
                $(".orderarea").animate({
                    marginLeft: "+=721px"
                }, 500, function () {
                    $(".cart-command").text("");
                });

                $("#btntotal").css("display", "block");
                if (parseInt($("#currentinvoiceid").val()) > 0) {
                    $(".savewithoutpay").css("display", "block");
                } else {
                    $(".savewithoutpay").css("display", "none");
                }
                $(".cartitem-container").height($(".cartitem-container").height() - 56);
                $(".cartitem-container").scrollTop(0);
                $("#gstcheckbox").prop("disabled", "");
                $(".payment-details .payment-list>tbody>tr").each(function () {
                    $(this).remove();
                });
                $(".payment-method").css("display", "block");
                $(".payment-details").css("display", "none");
                //$("#btnDeleteCartItem").css("display", "");
                $(".choosecontainer").unbind("click");
                $(".choosecustomericon").css("display", "");
                $(".choosecontainer").click(function () {
                    $(".itscart").animate({
                        marginLeft: "-=300px"
                    }, 500, function () {
                        //animation = false;
                    });

                    $(".choosecustomerlist tr").each(function () {
                        $(this).find(".customerchecked").eq(0).css("display", "none");
                        if ($(this).find(".choosecustomerid").eq(0).val() == $(".selectedcustomerid").val()) {
                            $(this).find(".customerchecked").eq(0).css("display", "table-cell");
                            //                            $("#inpSMS").val($(this).find(".choosecustomerphone").eq(0).val());
                            //                            $("#inpWalletId").val($(this).find(".choosecustomerwallet").eq(0).val());
                            //                            $("#inpEmail").val($(this).find(".choosecustomeremail").eq(0).val());
                        }
                    });

                });
                $(".cart-container .cartlist>tbody>tr").each(function () {
                    if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                        $(this).unbind("click");
                        $(this).click(function () {
                            $(".cart-container .cartlist>tbody>tr").each(function () { $(this).find(".cart_delete").css("display", "none"); });
                            $(this).find(".cart_delete").toggle();
                            if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                                $(".number-row").toggle();
                                $(".discount-row").toggle();
                            } else {
                                $(".number-row").css("display", "table-row");
                                $(".discount-row").css("display", "table-row");
                            }

                            $(".number-row").attr("for-id", "0");
                            $(".discount-row").attr("for-id", "0");

                            if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                                $(".number-row").attr("for-id", $(this).attr("product-id"));
                                $(".discount-row").attr("for-id", $(this).attr("product-id"));
                                $(".discount-row").insertAfter($(this));
                                $(".number-row").insertAfter($(this));
                                $("#currentquantity").val($(this).find(".product_quantity").text());
                                $("#currentdiscount").val($(this).attr("product-discount"));
                                scrollIntoView($(".discount-row"), ".cartitem-container");
                            }

                            $(".cart-container .cartlist>tbody>tr").each(function () {
                                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                                    if ($.trim($(this).find(".product_quantity").text()) == "0" && $(".number-row").attr("for-id") != $(this).attr("product-id")) {
                                        $(this).remove();
                                    }
                                }
                            });
                        });
                    }
                });
            }
        });

        $(".method-item").unbind("click");
        $(".method-item").click(function () {
            var image = "";
            var text = "";
            if ($(this).hasClass("cash-pay")) {
                image = "images/cash.png";
                text = "Cash";
            } else if ($(this).hasClass("credit-pay")) {
                image = "images/credit-card.png";
                text = "Credit";
            } else if ($(this).hasClass("net-pay")) {
                image = "images/net.png";
                text = "Net"
            } else if ($(this).hasClass("none-pay")) {
                $(".payment-method").css("display", "none");
                $(".payment-details").css("display", "block");
            }

            if (image != "") {
                var tr = $('<tr style="height:50px"></tr>');
                var tdremove = $('<td style="width:50px"><img src="images/remove.png" style="cursor:pointer;" /></td>');
                var input = $('<input class="payamount ui-keyboard-lockedinput" type="text" style="width:430px;height:40px;margin-top:0px;" readonly="true" value="0.00" />');
                var isiPad = navigator.userAgent.match(/iPad/i) != null;
                var span = "";
                if (isiPad) {
                    span = $('<span  style="position:absolute;right:2px;top:7px;vertical-align:middle;font-size:20px;line-height:30px;"><img src="' + image + '" style="vertical-align:middle" /> <span style="opacity:0.8;color:Gray">' + text + '</span>');
                } else {
                    span = $('<span  style="position:absolute;right:2px;top:3px;vertical-align:middle;font-size:20px;line-height:30px;"><img src="' + image + '" style="vertical-align:middle" /> <span style="opacity:0.8;color:Gray">' + text + '</span>');
                }
                var div = $('<div style="position:relative;width:430px;margin-left:10px;"></div>');
                var tdinput = $('<td></td>');
                div.append(input);
                div.append(span);
                tdinput.append(div);
                tr.append(tdremove);
                tr.append(tdinput);
                input.keyboard({
                    layout: 'customer',
                    customLayout: {
                        'default': [
				            '{clear} . {b} ' + $.trim($(".total-pay").text()),
				            '0 1 2 3',
				            '4 5 6 7',
				            '8 9 {a} {c}',
			            ]
                    },
                    restrictInput: true, // Prevent keys not in the displayed keyboard from being typed in
                    preventPaste: true,  // prevent ctrl-v and right click
                    autoAccept: false,
                    visible: function (e, keyboard, el) {
                        var isiPad = navigator.userAgent.match(/iPad/i) != null;
                        if (isiPad) {
                            keyboard.$preview[0].setSelectionRange(0, 9999);
                        } else {
                            keyboard.$preview[0].select();
                        }
                    }
                });

                $(".payment-details .payment-list>tbody").append(tr);
                $(".payment-method").css("display", "none");
                $(".payment-details").css("display", "block");
                tdremove.click(function () {
                    $(this).closest("tr").remove();
                    if ($(".payment-details .payment-list>tbody").find("tr").length == 0) {
                        $(".payment-method").css("display", "block");
                        $(".payment-details").css("display", "none");
                        $("#addpayment").prop("disabled", "disabled");
                        $("#addpayment").removeClass("btn-info");
                        $("#addpayment").removeClass("btn-block");
                    } else {
                        updatePayment()
                    }
                });
                input.change(function () {
                    if ($(this).val() == "") {
                        $(this).val("0.00");
                    }
                    $(this).val(parseFloat($(this).val()).toFixed(2));
                    updatePayment();
                });
            }
            updatePayment();
        });

        $("#addpayment").unbind("click");
        $("#addpayment").click(function () {
            $(".payment-method").css("display", "block");
            $(".payment-details").css("display", "none");
        });

        $(".btnsaveunpaid").unbind("click");
        $(".btnsaveunpaid").click(function () {
            var invoicedetails = $("#currentinvoiceid").val();
            invoicedetails = invoicedetails + "|" + $(".selectedcustomername").val();
            invoicedetails = invoicedetails + "|" + $("#gstcheckbox").prop("checked") + "|";
            var selectedrows = false;
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    selectedrows = true;
                    invoicedetails = invoicedetails + $(this).attr("product-id") + "," + $(this).attr("product-discount") + "," + $(this).attr("product-price") + "," + $(this).find(".product_quantity").eq(0).text() + ";";
                }
            });
            if (!selectedrows) {
                radalert("No order item! Please try to cancel the order if it's what you need.", 400, 100, "ERROR");
                return;
            }
            $("#invoicedetails").val(invoicedetails);
            $("#btnSaveUnpaidStatus").trigger("click");

        });

        $("#markaspaid").unbind("click");
        $("#markaspaid").click(function () {
            var invoicedetails = $("#currentinvoiceid").val();
            invoicedetails = invoicedetails + "|" + $(".selectedcustomername").val();
            invoicedetails = invoicedetails + "|" + $("#gstcheckbox").prop("checked") + "|";
            var selectedrows = false;
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    selectedrows = true;
                    invoicedetails = invoicedetails + $(this).attr("product-id") + "," + $(this).attr("product-discount") + "," + $(this).attr("product-price") + "," + $(this).find(".product_quantity").eq(0).text() + ";";
                }
            });
            if (!selectedrows) {
                radalert("No order items!", 400, 100, "ERROR");
                return;
            }
            $("#invoicedetails").val(invoicedetails);
            var paidamount = 0;
            var ownedamount = 0;
            $(".payment-list tbody>tr").each(function () {
                paidamount = paidamount + parseFloat($(this).find("input").eq(0).val());
            });
            $(".paid-status").find("img").eq(0).attr("src", "images/paid.png");
            var totalamount = parseFloat($(".total-pay").text());
            if (totalamount > paidamount) {
                $(".paid-status").find("img").eq(0).attr("src", "images/paid_part.png");
            }
            ownedamount = totalamount - paidamount;
            $("#invoicepaidamount").val(paidamount);
            $("#invoiceownedamount").val(ownedamount);
            $("#resubmitBtn").trigger("click");

        });

        $(".addinvoiceicon").unbind("click");
        $(".addinvoiceicon").click(function () {
            $("#currentinvoiceid").val("0");
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    $(this).remove();
                } else {
                    $(this).css("display", "none");
                }
            });
            $(".selectedcustomerid").val("0");
            //            $("#inpSMS").val("");
            //            $("#inpWalletId").val("");
            //            $("#inpEmail").val("");
            $(".selectedcustomer").text("Choose Customer");
            $(".selectedcustomername").val("");
            updateCartTotal();
            $(".orderarea").animate({
                marginLeft: "-=1024px"
            }, 500, function () {
                //animation = false;
            });
            $(".cattitle").text("All Products");
            var lengt = $(".swiper-slide").length;
            for (var i = lengt; i > 0; i--) {
                mySwiper.removeSlide(i - 1);
            }
            buildGrid("");
            $(".savewithoutpay").css("display", "none");
            $("#btntotal").val($("#btntotal").val().replace("Pay it", "Total"));
        });

        $("#btnStartNew").unbind("click");
        $("#btnStartNew").click(function () {
            $("#currentinvoiceid").val("0");
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    $(this).remove();
                } else {
                    $(this).css("display", "none");
                }
            });
            $(".cattitle").text("All Products");
            var lengt = $(".swiper-slide").length;
            // alert(mySwiper);
            for (var i = lengt; i > 0; i--) {
                mySwiper.removeSlide(i - 1);
            }
            buildGrid("");
            $(".selectedcustomer").text("Choose Customer");
            $(".selectedcustomername").val("");
            $(".selectedcustomerid").val("0");
            //            $("#inpSMS").val("");
            //            $("#inpWalletId").val("");
            //            $("#inpEmail").val("");
            updateCartTotal();
            $(".orderarea").animate({
                marginLeft: "+=721px"
            }, 500, function () {
                //animation = false;
            });
            $("#btntotal").css("display", "block");
            $(".cartitem-container").height($(".cartitem-container").height() - 36);
            $(".cartitem-container").scrollTop(0);
            $("#gstcheckbox").prop("disabled", "");
            $(".payment-details .payment-list>tbody>tr").each(function () {
                $(this).remove();
            });
            $(".payment-method").css("display", "block");
            $(".payment-details").css("display", "none");

            $(".total-pay").css("display", "block");
            $(".payment-action").css("display", "block");
            $(".payment-paid").css("display", "none");
            $(".cart-command").css("display", "block");
            //$("#btnDeleteCartItem").css("display", "");
            $(".choosecontainer").unbind("click");
            $(".choosecustomericon").css("display", "");
            $("#inpWalletId").val("Wallet ID");
            $("#inpSMS").val("SMS");
            $("#gstcheckbox").prop("checked", false);
            $(".choosecontainer").click(function () {
                $(".itscart").animate({
                    marginLeft: "-=300px"
                }, 500, function () {
                    $(".cancel_box").trigger("click");
                });

                $(".choosecustomerlist tr").each(function () {
                    $(this).find(".customerchecked").eq(0).css("display", "none");
                    if ($(this).find(".choosecustomerid").eq(0).val() == $(".selectedcustomerid").val()) {
                        $(this).find(".customerchecked").eq(0).css("display", "table-cell");
                        //                        $("#inpSMS").val($(this).find(".choosecustomerphone").eq(0).val());
                        //                        $("#inpWalletId").val($(this).find(".choosecustomerwallet").eq(0).val());
                        //                        $("#inpEmail").val($(this).find(".choosecustomeremail").eq(0).val());
                    }
                });

            });
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    $(this).unbind("click");
                    $(this).click(function () {
                        if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                            $(".number-row").toggle();
                            $(".discount-row").toggle();
                        } else {
                            $(".number-row").css("display", "table-row");
                            $(".discount-row").css("display", "table-row");
                        }

                        $(".number-row").attr("for-id", "0");
                        $(".discount-row").attr("for-id", "0");

                        if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                            $(".number-row").attr("for-id", $(this).attr("product-id"));
                            $(".discount-row").attr("for-id", $(this).attr("product-id"));
                            $(".discount-row").insertAfter($(this));
                            $(".number-row").insertAfter($(this));
                            $("#currentquantity").val($(this).find(".product_quantity").text());
                            $("#currentdiscount").val($(this).attr("product-discount"));
                            scrollIntoView($(".discount-row"), ".cartitem-container");
                        }

                        $(".cart-container .cartlist>tbody>tr").each(function () {
                            if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                                if ($.trim($(this).find(".product_quantity").text()) == "0" && $(".number-row").attr("for-id") != $(this).attr("product-id")) {
                                    $(this).remove();
                                }
                            }
                        });
                    });
                }
            });
            $(".savewithoutpay").css("display", "none");
            $("#btntotal").val($("#btntotal").val().replace("Pay it", "Total"));
        });

        $("#btnBackToList").unbind("click");
        $("#btnBackToList").click(function () {
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    $(this).remove();
                } else {
                    $(this).css("display", "none");
                }
            });
            $(".selectedcustomer").text("Choose Customer");
            $(".selectedcustomername").val("");
            $(".selectedcustomerid").val("0");
            //             $("#inpSMS").val("");
            //             $("#inpWalletId").val("");
            //             $("#inpEmail").val("");
            updateCartTotal();
            $(".orderarea").animate({
                marginLeft: "+=1745px"
            }, 500, function () {
                //animation = false;
            });
            $("#btntotal").css("display", "block");
            $(".cartitem-container").height($(".cartitem-container").height() - 36);
            $(".cartitem-container").scrollTop(0);
            $("#gstcheckbox").prop("disabled", "");
            $(".payment-details .payment-list>tbody>tr").each(function () {
                $(this).remove();
            });
            $(".payment-method").css("display", "block");
            $(".payment-details").css("display", "none");

            $(".total-pay").css("display", "block");
            $(".payment-action").css("display", "block");
            $(".payment-paid").css("display", "none");
            $(".cart-command").css("display", "block");
            //$("#btnDeleteCartItem").css("display", "");
            $(".choosecontainer").unbind("click");
            $(".choosecustomericon").css("display", "");
            $("#inpWalletId").val("Wallet ID");
            $("#inpSMS").val("SMS");
            $("#gstcheckbox").prop("checked", false);
            $(".choosecontainer").click(function () {
                $(".itscart").animate({
                    marginLeft: "-=300px"
                }, 500, function () {
                    //animation = false;
                });

                $(".choosecustomerlist tr").each(function () {
                    $(this).find(".customerchecked").eq(0).css("display", "none");
                    if ($(this).find(".choosecustomerid").eq(0).val() == $(".selectedcustomerid").val()) {
                        $(this).find(".customerchecked").eq(0).css("display", "table-cell");
                        //                         $("#inpSMS").val($(this).find(".choosecustomerphone").eq(0).val());
                        //                         $("#inpWalletId").val($(this).find(".choosecustomerwallet").eq(0).val());
                        //                         $("#inpEmail").val($(this).find(".choosecustomeremail").eq(0).val());
                    }
                });

            });

            $(".invoiceitemslist tbody>tr").each(function () {
                $(".selectedcustomer").text("Customer: " + $(".invoicecustomername").text());
                $(".selectedcustomername").val($(".invoicecustomername").text());
                var invoicepid = $(this).find(".invoiceproductid").eq(0).val();
                var invoicepdiscount = $(this).find(".invoiceproductdiscount").eq(0).val();
                var invoicepprice = $(this).find(".invoiceproductprice").eq(0).val();
                var invoicepname = $(this).find(".invoiceproductname").eq(0).val();
                var invoicepquantity = $(this).find(".invoiceproductquantity").eq(0).val();
                var pexists = false;
                $(".cart-container .cartlist>tbody>tr").each(function () {
                    if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                        if ($(this).attr("product-id") == invoicepid) {
                            pexists = true;
                            var quantity = parseInt($(this).find(".product_quantity").text());
                            quantity = quantity + parseInt(invoicepquantity);
                            $(this).find(".product_quantity").text(quantity);
                            $(this).find(".product_price").text(parseFloat(invoicepprice) * quantity);
                            //updateCartTotal();
                            // scrollIntoView($(this), ".cartitem-container");
                        }
                    }
                });
                if (!pexists) {
                    var tr = $("<tr></tr>");

                    var ntd = $("<td style='width:160px;padding-left:10px'><div class='product_name'>" + invoicepname + "</div></td>");
                    var qtd = $("<td style='width:50px' class='product_quantity'></td>");
                    var ptd = $("<td style='width:70px' class='product_price'></td>");
                    var dtd = $("<td style='width:30px'><img class='cart_delete' style='width:20px;height:20px;display:none' src='images/remove.png'/></td>");
                    ptd.text(invoicepprice);
                    qtd.text(invoicepquantity);
                    tr.append(ntd);
                    tr.append(qtd);
                    tr.append(ptd);
                    tr.append(dtd);

                    dtd.click(function () {
                        $(this).closest("tr").remove();
                        $(".number-row").css("display", "none");
                        $(".discount-row").css("display", "none");
                        $(".number-row").attr("for-id", "0");
                        $(".discount-row").attr("for-id", "0");
                        updateCartTotal();
                    });

                    $(".cart-container .cartlist>tbody").append(tr);
                    tr.attr("product-id", invoicepid);
                    tr.attr("product-discount", invoicepdiscount);
                    tr.attr("product-price", invoicepprice);
                    //scrollToBottom(tr, ".cartitem-container");
                    $(".cartitem-container").scrollTop($('.cartitem-container')[0].scrollHeight);
                    //tr.effect("highlight", {}, 500, function () { });
                    tr.click(function () {
                        $(".cart-container .cartlist>tbody>tr").each(function () { $(this).find(".cart_delete").css("display", "none"); });
                        $(this).find(".cart_delete").toggle();
                        if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                            $(".number-row").toggle();
                            $(".discount-row").toggle();
                        } else {
                            $(".number-row").css("display", "table-row");
                            $(".discount-row").css("display", "table-row");
                        }

                        $(".number-row").attr("for-id", "0");
                        $(".discount-row").attr("for-id", "0");

                        if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                            $(".number-row").attr("for-id", $(this).attr("product-id"));
                            $(".discount-row").attr("for-id", $(this).attr("product-id"));
                            $(".discount-row").insertAfter($(this));
                            $(".number-row").insertAfter($(this));
                            $("#currentquantity").val($(this).find(".product_quantity").text());
                            $("#currentdiscount").val($(this).attr("product-discount"));
                            scrollIntoView($(".discount-row"), ".cartitem-container");
                        }

                        $(".cart-container .cartlist>tbody>tr").each(function () {
                            if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                                if ($.trim($(this).find(".product_quantity").text()) == "0" && $(".number-row").attr("for-id") != $(this).attr("product-id")) {
                                    $(this).remove();
                                }
                            }
                        });
                    });
                }
                updateCartTotal();
            });

            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    $(this).unbind("click");
                    $(this).click(function () {
                        if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                            $(".number-row").toggle();
                            $(".discount-row").toggle();
                        } else {
                            $(".number-row").css("display", "table-row");
                            $(".discount-row").css("display", "table-row");
                        }

                        $(".number-row").attr("for-id", "0");
                        $(".discount-row").attr("for-id", "0");

                        if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                            $(".number-row").attr("for-id", $(this).attr("product-id"));
                            $(".discount-row").attr("for-id", $(this).attr("product-id"));
                            $(".discount-row").insertAfter($(this));
                            $(".number-row").insertAfter($(this));
                            $("#currentquantity").val($(this).find(".product_quantity").text());
                            $("#currentdiscount").val($(this).attr("product-discount"));
                            scrollIntoView($(".discount-row"), ".cartitem-container");
                        }

                        $(".cart-container .cartlist>tbody>tr").each(function () {
                            if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                                if ($.trim($(this).find(".product_quantity").text()) == "0" && $(".number-row").attr("for-id") != $(this).attr("product-id")) {
                                    $(this).remove();
                                }
                            }
                        });
                    });
                }
            });
        });

        if ($("#isdoinvoice").val() == "1") {
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    $(this).unbind("click");
                }
            });

            $(".number-row").css("display", "none");
            $(".discount-row").css("display", "none");
            $(".number-row").attr("product-id", "none");
            $(".discount-row").attr("product-id", "none");

            $("#gstcheckbox").prop("disabled", "disabled");

            $("#btntotal").css("display", "none");
            $(".savewithoutpay").css("display", "none");

            $(".cartitem-container").height($(".cartitem-container").height() + $("#btntotal").height());
            $(".cartitem-container").scrollTop(0);

            //$("#btnDeleteCartItem").css("display", "none");
            $(".choosecontainer").unbind("click");
            $(".choosecustomericon").css("display", "none");
            $(".paid-status").find("img").eq(0).attr("src", "images/paid.png");
            var owned = parseFloat($("#invoiceownedamount").val());
            //alert(owned);
            if (owned > 0) {
                $(".paid-status").find("img").eq(0).attr("src", "images/paid_part.png");
            }
            if ($("#isgstchecked").val() == "true") {
                $("#gstcheckbox").prop("checked", true);
            } else {
                $("#gstcheckbox").prop("checked", false);
            }
            $("#isdoinvoice").val("0");
        }

        $(".invoicelist tr").each(function () {
            if ($(this).find(".invoiceid").val() == $("#currentinvoiceid").val()) {
                //scrollIntoView($(this), ".invoicelistarea");
                $(".invoicelistarea").scrollTop($(this).offset().top - 36);
            }
        });
    }

    function saveUnpaid() {
    }

    function paidSuccess() {
        $(".orderarea").animate({
            marginLeft: "-=1745px"
        }, 100, function () {

        });
        $(".total-pay").css("display", "none");
        $(".payment-method").css("display", "none");
        $(".payment-details").css("display", "none");
        $(".payment-action").css("display", "none");
        $(".payment-paid").css("display", "block");
        $(".cart-command").css("display", "none");


        $(".paidamount").text(parseFloat($("#invoicepaidamount").val()).toFixed(2));
        if (parseFloat($("#invoiceownedamount").val()) < 0) {
            $(".ownedamount").text((0 - parseFloat($("#invoiceownedamount").val())).toFixed(2));
            $(".ownedamount").css("color", "#25a0da");
            $(".ownedtext").text("CHANGE DUE");
        } else {
            $(".ownedamount").text(parseFloat($("#invoiceownedamount").val()).toFixed(2));
            $(".ownedtext").text("OUTSTANDING");
            $(".ownedamount").css("color", "#9b5b05");
        }


    }

    function updatePayment() {
        var totalpaid = 0;
        $(".payment-details .payment-list>tbody>tr").each(function () {
            totalpaid += parseFloat($(this).find(".payamount").eq(0).val());
        });

        //alert(totalpaid);
        //alert(parseFloat($(".total-pay").text()));
        if (totalpaid < parseFloat($(".total-pay").text())) {
            $("#addpayment").prop("disabled", "");
            $("#addpayment").addClass("btn-info");
            $("#addpayment").addClass("btn-block");
        } else {

            $("#addpayment").prop("disabled", "disabled");
            $("#addpayment").removeClass("btn-info");
            $("#addpayment").removeClass("btn-block");
        }
    }

    function scrollIntoView(element, container) {
        var containerTop = $(container).scrollTop();

        var containerBottom = containerTop + $(container).height();

        var elemTop = element.offset().top;
        var elemBottom = elemTop + element.height();
        if (elemTop < containerTop) {
            $(container).scrollTop(elemTop);
        } else if (elemBottom > containerBottom) {
            $(container).scrollTop(elemBottom - $(container).height());
        }
        if (!element.hasClass("discount-row") && !element.hasClass("number-row")) {
            element.effect("highlight", {}, 500, function () { });
        }
    }

    function updateCartTotal() {
        var quantity = 0;
        var total = 0.0;
        $(".cart-container .cartlist>tbody>tr").each(function () {
            if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                quantity += parseInt($(this).find(".product_quantity").text());
                var subtotal = parseFloat(($(this).attr("product-price")) * parseFloat($(this).attr("product-discount")) * parseInt($(this).find(".product_quantity").text()) / 100).toFixed(2);
                $(this).find(".product_price").text(subtotal);
                total += parseFloat(subtotal);
            }
        });

        var gst = total * parseFloat($(".gstvalue").text()) / 100;
        $(".gstamount").text(parseFloat(gst).toFixed(2));

        if ($("#gstcheckbox").prop("checked")) {
            total += gst;
        }

        $(".cart-header .title").text("Cart (" + quantity + ")");
        if (parseInt($("#currentinvoiceid").val()) > 0) {
            $("#btntotal").val("Pay it (" + parseFloat(total).toFixed(2) + ")");
        } else {
            $("#btntotal").val("Total (" + parseFloat(total).toFixed(2) + ")");
        }
        $(".total-pay").text(parseFloat(total).toFixed(2));
    }

</script>
