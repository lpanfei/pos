﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;
using System.IO.Ports;
using Twilio;
using System.Text;
using System.Net;
using POS.Helper;
using System.Drawing;
using System.Drawing.Printing;

namespace POS.TouchControls
{
    public partial class WUCInvoice2 : ControlBase
    {
        public string InvoiceNumberPrefix
        {
            get
            {
                if (GlobalCache.Setting == null)
                    return "";
                return GlobalCache.Setting.Prefix;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //Init();
                //StringBuilder sb = new StringBuilder();
                //sb.AppendLine("Save");
                //sb.Append("Withou Pay");
                //btnSaveWithoutPay.Text = sb.ToString();
            }
            searchProductBox.DataSource = GlobalCache.Products;
            customerSearchBox1.DataSource = GlobalCache.Customers;
            
        }

        public override void Init()
        {
            InitialInvoices(0, null, null, null, false);
            productList.DataSource = GetProductsWithDiscount(GlobalCache.Products);
            productList.DataBind();
            //ScriptManager.RegisterStartupScript(this, GetType(), "buildGrid", "buildGrid();", true);
            GlobalCache.RefreshSetting();
            if (GlobalCache.Setting != null)
            {
                lblGSTSetting.Text = GlobalCache.Setting.GST.ToString();
            }
            else
            {
                lblGSTSetting.Text = "0";
            }
            customerItems.DataSource = GlobalCache.Customers;
            customerItems.DataBind();
            
        }

        protected void btnReceipt_Click(object sender, EventArgs e)
        {
            
        }

        protected void PaidStatusChanged(object sender, EventArgs e)
        {
            FilterInvoices(0, null, null, null, false, RadioButtonList1.SelectedValue);
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
        }

        public List<Product> GetProductsWithDiscount(List<Product> products)
        {
            if (products == null)
                return null;
            products.ForEach(p => { p.Discount = 100; });
            if (GlobalCache.Promotions != null)
            {
                foreach (Promotion promotion in GlobalCache.Promotions)
                {
                    if (DateTime.Now >= promotion.StartTime && DateTime.Now <= promotion.EndTime)
                    {
                        products.FirstOrDefault(p => p.pro_Id == promotion.ProductID).Discount = promotion.Discount;
                    }
                }
            }
            return products;
        }

        protected void InvoiceSelected(object sender, EventArgs e)
        {
            InvoiceCurrentSatus.Text = "";
            Invoice invoice = GlobalCache.Invoices.FirstOrDefault(i => i.inv_Id == Convert.ToInt32(selectedinvoiceid.Attributes["value"]));
            if (invoice != null)
            {
                InvoiceCurrentSatus.Text=invoice.Products.Any(p=>!p.IsComplete)?"Delivering In Process":"Delivered";
                invoiceitems.DataSource = null;
                invoiceitems.DataSource = invoice.Products;
                invoiceitems.DataBind();
                lblTotal.Text = invoice.Total.ToString("C");
                lblNo.Text = InvoiceNumberPrefix + invoice.inv_No.ToString();
                lblStatus.Text = invoice.Status.ToString();
                lblCustomer.Text = invoice.inv_Customer;
                lblUser.Text = invoice.inv_Staff;
                currentinvoiceid.Attributes["value"] = invoice.inv_Id.ToString();
                currentinvoiceno.Attributes["value"] = InvoiceNumberPrefix + invoice.inv_No.ToString();
                if (invoice.Status == InvoiceStatus.Paid)
                {
                    editBtn.Visible = false;
                    payBtn.Visible = false;
                    btnReceipt.Visible = true;
                }
                else
                {
                    editBtn.Visible = true;
                    payBtn.Visible = true;
                    btnReceipt.Visible = false;
                }
            }
            //ScriptManager.RegisterStartupScript(this, GetType(), "invoiceselected", "InvoiceSelected();", true);
        }

        protected void SendSMS(object sender, EventArgs e)
        {
            

            string url = Request.Url.AbsoluteUri.Replace(Request.Url.AbsolutePath, "") + "/Receipt.aspx?";
            
            //this.ShowSuccess(BitlyHelper.Shorten(url));
            Invoice invoice = GlobalCache.Invoices.FirstOrDefault(i => i.inv_Id == Convert.ToInt32(currentinvoiceid.Attributes["value"]));
            if (invoice != null)
            {
                if (invoice.SMS != null && invoice.SMS.Any(s => s.MobileNo.Equals(inpSMS.Text.Trim(), StringComparison.InvariantCultureIgnoreCase)))
                {
                    url = url + "mn=" + inpSMS.Text.Trim() + "&vc=" + invoice.SMS.FirstOrDefault(s => s.MobileNo.Equals(inpSMS.Text.Trim(), StringComparison.InvariantCultureIgnoreCase)).ValidationCode;
                }   
                else
                {
                    StringBuilder builder = new StringBuilder();
                    builder.Append(RandomNumber(1000, 9999));
                    builder.Append(RandomString(2, false));
                    url = url + "mn=" + inpSMS.Text.Trim() + "&vc=" + builder.ToString();
                    if (invoice.SMS == null)
                        invoice.SMS = new List<InvoiceSMS>();
                    invoice.SMS.Add(new InvoiceSMS() { InvoiceID = invoice.inv_Id, MobileNo = inpSMS.Text.Trim(), ValidationCode = builder.ToString() });
                    invoice.Update();
                    InitialInvoices(0, null, null, null, false);
                }

                url = BitlyHelper.Shorten(url);
            //    invoice.inv_WalletID = inpWalletId.Text.Trim();
            //    //new POSService.POSService().DoInvoice(invoice);
            //    int totalcount = 0;
            //    invoice.Products.ForEach(p => { totalcount = totalcount + p.ip_Quantity; });
            //    var total = invoice.inv_Total;
            //    if (invoice.IsGST)
            //    {
            //        total += invoice.inv_GST;
            //    }
                
            //    StringBuilder sb = new StringBuilder();
            //    sb.Append("Receipt No. " + InvoiceNumberPrefix + invoice.inv_No + " from ");
            //    sb.Append(GlobalCache.Stores.FirstOrDefault(s => s.sto_Id == this.UserStoreID).sto_Name);
            //    sb.Append(". Total " +totalcount.ToString()+" items, cost $"+Decimal.Round(total,2).ToString()+". Including:" );
            //    foreach(InvoiceProduct ip in invoice.Products)
            //    {
            //        sb.Append(" "+ip.ip_Description);
            //        sb.Append(" x "+ip.ip_Quantity.ToString());
            //        sb.Append(" - $"+Decimal.Round(ip.ip_SubTotal,2).ToString());
            //        sb.Append(";");
            //    }
            //    sb.Remove(sb.Length - 1, 1);
            //    if (invoice.IsGST)
            //    {
            //        sb.Append("; GST Amount: $" + Decimal.Round(invoice.inv_GST, 2).ToString());
            //    }
            //    sb.Append(".");
                

            //    //this.ShowSuccess(sb.ToString());
                var twilio = new TwilioRestClient(System.Web.Configuration.WebConfigurationManager.AppSettings["accountsid"], System.Web.Configuration.WebConfigurationManager.AppSettings["authtoken"]);
                var message = twilio.SendMessage("+14702997838", inpSMS.Text.Trim(), "Dear customer, your receipt from " + GlobalCache.Stores.FirstOrDefault(s => s.sto_Id == this.UserStoreID).sto_Name+" can be viewed here: "+url);
                if (message.RestException != null)
                {
                    this.ShowError(message.RestException.Message);
                }
                else
                {
                    this.ShowSuccess("Receipt has been sent to your mobile:)");
                }
                isdoinvoice.Attributes["value"] = "1";
                ScriptManager.RegisterStartupScript(resubmitBtn, GetType(), "paidSuccess", "paidSuccess();", true);
            }
        }

        private string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        private int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        protected void SendWallet(object sender, EventArgs e)
        {
            Invoice invoice = GlobalCache.Invoices.FirstOrDefault(i => i.inv_Id == Convert.ToInt32(currentinvoiceid.Attributes["value"]));
            if (invoice != null)
            {
                invoice.inv_WalletID = inpWalletId.Text.Trim();
                new POSService.POSService().DoInvoice(invoice);
                this.ShowSuccess("Receipt has been sent to your Wallet account:)");
                isdoinvoice.Attributes["value"] = "1";
                ScriptManager.RegisterStartupScript(resubmitBtn, GetType(), "paidSuccess", "paidSuccess();", true);
            }
        }

        protected void SendEmail(object sender, EventArgs e)
        {
            Invoice invoice = GlobalCache.Invoices.FirstOrDefault(i => i.inv_Id == Convert.ToInt32(currentinvoiceid.Attributes["value"]));
            if (invoice != null)
            {
                StringBuilder body = new StringBuilder();
                body.Append("<body><div>Dear Customer,</div><p>You have made an invoice at ");
                body.Append(GlobalCache.Stores.FirstOrDefault(s => s.sto_Id == this.UserStoreID).sto_Name);
                body.Append(" on ");
                body.Append(invoice.inv_CreateDate.Value.ToLongDateString());
                body.Append(". Here is your receipt:</p>");
                body.Append("<table style='border: 1px solid #e4e4e4;  border-collapse: collapse; width: 100%; margin-bottom: 4px;   -webkit-border-radius: 6px;-moz-border-radius: 6px;border-radius: 6px;'><thead><tr style='border-bottom:1px solid #e4e4e4;cursor:pointer'><th style='background-color:#F7F8FA; padding: 4px; white-space:nowrap;height:35px;'>Product</th><th style='background-color:#F7F8FA; padding: 4px; white-space:nowrap;height:35px;'>Quantity</th><th style='background-color:#F7F8FA; padding: 4px; white-space:nowrap;height:35px;'>Sub Total</th></tr></thead><tbody>");
                foreach (InvoiceProduct ip in invoice.Products)
                {
                    body.Append("<tr style='border-bottom:1px solid #e4e4e4;cursor:pointer'><td style='vertical-align: middle; padding-right:10px; height:50px; ' align='center'>");
                    body.Append(ip.ip_Description);
                    body.Append("</td><td style='vertical-align: middle; padding-right:10px; height:50px; ' align='center'>");
                    body.Append(ip.ip_Quantity.ToString());
                    body.Append("</td><td style='vertical-align: middle; padding-right:10px; height:50px; ' align='center'>$");
                    body.Append(Decimal.Round(ip.ip_SubTotal, 2).ToString());
                    body.Append("</td></tr>");
                }
                body.Append("</tbody></table><div style='float:right;font-weight:bold;margin-right:5px;'>");
                if (invoice.IsGST)
                {
                    body.Append("<div><span  style='font-weight:bold'>GST Amount:</span>&nbsp;&nbsp;&nbsp;&nbsp;");
                    body.Append(Decimal.Round(invoice.inv_GST, 2).ToString());
                    body.Append("</div>");
                    body.Append("<span style='font-weight:bold'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total:</span>&nbsp;&nbsp;&nbsp;&nbsp;");
                    body.Append(Decimal.Round(invoice.inv_TotalIncGST, 2).ToString());
                    body.Append("</div>");
                }
                else
                {
                    body.Append("<span style='font-weight:bold'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total:</span>&nbsp;&nbsp;&nbsp;&nbsp;");
                    body.Append(Decimal.Round(invoice.inv_TotalIncGST, 2).ToString());
                    body.Append("</div>");
                }
                body.Append("<br/><p>Best regards,<br/> Infini POS <br/> Note: This is a system generated email. Please don't reply to it</p>");
                MailManager.Send("noreply@myinfinitechsg.com", inpEmail.Text.Trim(), "Receipt from "+GlobalCache.Stores.FirstOrDefault(s => s.sto_Id == this.UserStoreID).sto_Name, body.ToString(), true);
                this.ShowSuccess("Receipt has been sent to your email:)");
                isdoinvoice.Attributes["value"] = "1";
                ScriptManager.RegisterStartupScript(resubmitBtn, GetType(), "paidSuccess", "paidSuccess();", true);
            }
        }

        protected void PrintReceipt(object sender, EventArgs e)
        {
            var doc = new PrintDocument();
            doc.DefaultPageSettings.Margins = new Margins(0, 0, 0, 0);
            doc.PrintPage += new PrintPageEventHandler(ProvideContent);
            doc.Print();
            isdoinvoice.Attributes["value"] = "1";
            ScriptManager.RegisterStartupScript(resubmitBtn, GetType(), "paidSuccess", "paidSuccess();", true);
        }

        protected void ProvideContent(object sender, PrintPageEventArgs e)
        {
            string invoiceheader = string.Empty;
            string invoicefooter = string.Empty;
            Invoice invoice = GlobalCache.Invoices.FirstOrDefault(i => i.inv_Id == Convert.ToInt32(currentinvoiceid.Attributes["value"]));
            GlobalCache.RefreshSetting();
            if (GlobalCache.Setting != null)
            {
                invoiceheader = GlobalCache.Setting.InvoiceHeader;
                invoicefooter = GlobalCache.Setting.InvoiceFooter;
            }
            if (invoice != null)
            {
                PointF point = new PointF(0f, 0f);
                if (!string.IsNullOrEmpty(invoiceheader))
                {
                    point = AddItem(e.Graphics, invoiceheader, point.X, point.Y);
                    point = AddItem(e.Graphics, "---------------------------------------------------", point.X, point.Y+5);
                }
                foreach (InvoiceProduct ip in invoice.Products)
                {
                    point = AddItem(e.Graphics, ip, point.X, point.Y + 5);
                }
                point = AddItem(e.Graphics, "---------------------------------------------------", point.X, point.Y + 5);

                if (invoice.IsGST)
                {
                    point = AddItem(e.Graphics, "GST Amount:    $" + Decimal.Round(invoice.inv_GST, 2).ToString(), point.X, point.Y + 5);
                    point = AddItem(e.Graphics, "Total:    $" + Decimal.Round(invoice.inv_TotalIncGST, 2).ToString(), point.X, point.Y + 5);
                }
                else
                {
                    point = AddItem(e.Graphics, "Total:    $" + Decimal.Round(invoice.inv_Total, 2).ToString(), point.X, point.Y + 5);
                }

                if (!string.IsNullOrEmpty(invoicefooter))
                {
                    point = AddItem(e.Graphics, invoicefooter, point.X, point.Y+5);
                }
            }
        }

        System.Drawing.PointF AddItem(System.Drawing.Graphics g, InvoiceProduct ip, float x, float y)
        {
            float _rx = 0f;
            float _ry = 0f;
            SizeF size = g.MeasureString(ip.ip_Description, new System.Drawing.Font("Arail", 8), 130);
            g.DrawString(ip.ip_Description, new System.Drawing.Font("Arail", 8), Brushes.Black, new RectangleF(x, y, size.Width, size.Height));
            g.DrawString(ip.ip_Quantity.ToString(), new System.Drawing.Font("Arail", 8), Brushes.Black, new RectangleF(130f, y + (size.Height - 10) / 2, 20, size.Height));
            g.DrawString("$" + Decimal.Round(ip.ip_SubTotal, 2).ToString(), new System.Drawing.Font("Arail", 8), Brushes.Black, new RectangleF(150f, y + (size.Height - 10) / 2, 50, size.Height));
            _rx = x;
            _ry = y + size.Height;
            return new PointF(_rx, _ry);
        }

        PointF AddItem(Graphics g, string text, float x, float y)
        {
            float _rx = 0f;
            float _ry = 0f;
            SizeF size = g.MeasureString(text, new System.Drawing.Font("Arail", 8), 200);
            g.DrawString(text, new System.Drawing.Font("Arail", 8), Brushes.Black, new RectangleF(x, y, size.Width, size.Height));
            _rx = x;
            _ry = y + size.Height;
            return new PointF(_rx, _ry);
        }

        void InitialInvoices(int invno, string customer, DateTime? datefrom, DateTime? dateto, bool isCancel)
        {
            FilterInvoices(invno, customer, datefrom, dateto, isCancel, RadioButtonList1.SelectedValue);
            //GlobalCache.RefreshInvoices(invno, customer, datefrom, dateto, isCancel);
            //rptList.DataSource = null;
            //rptList.DataSource = GlobalCache.Invoices.OrderByDescending(i => i.inv_No);
            //rptList.DataBind();
            //if (GlobalCache.Invoices != null && GlobalCache.Invoices.Any())
            //{
            //    if (selectedinvoiceid.Attributes["value"] == "0" || !GlobalCache.Invoices.Any(p => p.inv_Id.ToString() == selectedinvoiceid.Attributes["value"]))
            //    {
            //        selectedinvoiceid.Attributes["value"] = GlobalCache.Invoices.OrderByDescending(c => c.inv_Id).FirstOrDefault().inv_Id.ToString();
            //    }
            //}
            //InvoiceSelected(null, null);
        }

        void FilterInvoices(int invno, string customer, DateTime? datefrom, DateTime? dateto, bool isCancel,string status)
        {
            GlobalCache.RefreshInvoices(invno, customer, datefrom, dateto, isCancel);
            List<Invoice> invoices = new List<Invoice>();
            if (status == "All")
            {
                foreach (Invoice invoice in GlobalCache.Invoices)
                {
                    invoices.Add(invoice);
                }
            }
            else if (status == "Paid")
            {
                foreach (Invoice invoice in GlobalCache.Invoices)
                {
                    if (invoice.Status == InvoiceStatus.Paid)
                    {
                        invoices.Add(invoice);
                    }
                }
            }
            else if (status == "Unpaid")
            {
                foreach (Invoice invoice in GlobalCache.Invoices)
                {
                    if (invoice.Status == InvoiceStatus.UnPaid)
                    {
                        invoices.Add(invoice);
                    }
                }
            }
            invoiceHeaderLtl.Text = "Invoice (" + status + ")";
            rptList.DataSource = null;
            rptList.DataSource = invoices.OrderByDescending(i => i.inv_No);
            rptList.DataBind();
            if (invoices != null && invoices.Any())
            {
                if (selectedinvoiceid.Attributes["value"] == "0" || !invoices.Any(p => p.inv_Id.ToString() == selectedinvoiceid.Attributes["value"]))
                {
                    selectedinvoiceid.Attributes["value"] = invoices.OrderByDescending(c => c.inv_Id).FirstOrDefault().inv_Id.ToString();
                }
            }
            InvoiceSelected(null, null);
        }

        protected void SaveUnpaid(object sender, EventArgs e)
        {
            if (isaddcustomer.Attributes["value"] == "1")
            {
                AddCustomer(null, null);
                isaddcustomer.Attributes["value"] = "0";
            }
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            decimal total = 0;
            string orderdetails = invoicedetails.Attributes["value"];
            string[] detailArray = orderdetails.Split('|');
            int id = Convert.ToInt32(detailArray[0]);
            string customer = detailArray[1];
            bool isGST = detailArray[2] == "true" ? true : false;
            string[] items = detailArray[3].Substring(0, detailArray[3].Length - 1).Split(';');
            for (int i = 0; i < items.Count(); i++)
            {
                string[] item = items[i].Split(','); ;
                int pid = Convert.ToInt32(item[0]);
                decimal pdiscount = Convert.ToDecimal(item[1]);
                decimal pprice = Convert.ToDecimal(item[2]);
                int pquantity = Convert.ToInt32(item[3]);
                Product product = GlobalCache.Products.FirstOrDefault(p => p.pro_Id == pid);
                InvoiceProduct ipp = new InvoiceProduct() { ip_Description = product.pro_Name, ip_Discount = pdiscount, ip_Note = "", ip_Price = pprice, ip_Quantity = pquantity, ip_SubTotal = pprice * pquantity * pdiscount / 100, ip_ProductID = pid };
                ipp.Product = GlobalCache.Products.FirstOrDefault(p => p.pro_Id == ipp.ip_ProductID);
                ipp.Status = InvoiceProductStatus.Complete;
                ips.Add(ipp);
                total += ipp.ip_SubTotal;
            }
            int invoiceno = Convert.ToInt32(this.lblNo.Text.Replace(InvoiceNumberPrefix, ""));
            if (id == 0)
            {
                if (GlobalCache.Invoices.Any())
                {
                    invoiceno = GlobalCache.Invoices.First().inv_No + 1;
                }
                else
                {
                    invoiceno = 100000;
                }
            }
            Invoice invoice = new Invoice()
            {
                inv_Id = id,
                inv_Staff = GlobalCache.CurrentUser.usr_Name,
                inv_ClientID = this.UserClientID,
                inv_Customer = customer,
                inv_GST = total * Convert.ToDecimal(lblGSTSetting.Text) / 100,
                inv_Total = total,
                inv_TotalIncGST = total + total * Convert.ToDecimal(lblGSTSetting.Text) / 100,
                inv_No = invoiceno,
                inv_StoreID = this.UserStoreID,
                IsGST = isGST,
            };

            invoice.Products = ips;
            invoice.UserID = GlobalCache.CurrentUser.usr_ID;
            invoice.LastUpdatedTime = DateTime.Now;
            invoice.Status = InvoiceStatus.UnPaid;

            new POSService.POSService().DoInvoice(invoice);
            //rptList.DataSource = null;
            //rptList.DataSource = invoices.OrderByDescending(i => i.inv_No);
            //rptList.DataBind();
            selectedinvoiceid.Attributes["value"] = "0";
            InitialInvoices(0, null, null, null, false);
            //ScriptManager.RegisterStartupScript(btnSaveUnpaidStatus, GetType(), "saveUnpaid", "saveUnpaid();", true);
        }

        protected void DoInvoice(object sender, EventArgs e)
        {
            
            if (isaddcustomer.Attributes["value"] == "1")
            {
                AddCustomer(null, null);
                isaddcustomer.Attributes["value"] = "0";
            }
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            decimal total = 0;
            string orderdetails = invoicedetails.Attributes["value"];
            string[] detailArray = orderdetails.Split('|');
            int id = Convert.ToInt32(detailArray[0]);
            string customer = detailArray[1];
            bool isGST = detailArray[2] == "true" ? true : false;
            string[] items = detailArray[3].Substring(0, detailArray[3].Length - 1).Split(';');
            for (int i = 0; i < items.Count(); i++)
            {
                string[] item = items[i].Split(','); ;
                int pid = Convert.ToInt32(item[0]);
                decimal pdiscount = Convert.ToDecimal(item[1]);
                decimal pprice = Convert.ToDecimal(item[2]);
                int pquantity = Convert.ToInt32(item[3]);
                Product product = GlobalCache.Products.FirstOrDefault(p => p.pro_Id == pid);
                InvoiceProduct ipp = new InvoiceProduct() { ip_Description = product.pro_Name, ip_Discount = pdiscount, ip_Note = "", ip_Price = pprice, ip_Quantity = pquantity, ip_SubTotal = pprice*pquantity*pdiscount/100, ip_ProductID = pid };
                ipp.Product = GlobalCache.Products.FirstOrDefault(p => p.pro_Id == ipp.ip_ProductID);
                ipp.Status = InvoiceProductStatus.Complete;
                ips.Add(ipp);
                total += ipp.ip_SubTotal;
            }
            int invoiceno = Convert.ToInt32(this.lblNo.Text.Replace(InvoiceNumberPrefix,""));
            if(id==0)
            {
                if (GlobalCache.Invoices.Any())
                {
                    invoiceno = GlobalCache.Invoices.First().inv_No + 1;
                }
                else
                {
                    invoiceno = 100000;
                }
            }
            Invoice invoice = new Invoice()
            {
                inv_Id = id,
                inv_Staff = GlobalCache.CurrentUser.usr_Name,
                inv_ClientID = this.UserClientID,
                inv_Customer =customer,
                inv_GST = total*Convert.ToDecimal(lblGSTSetting.Text)/100,
                inv_Total = total,
                inv_TotalIncGST = total + total * Convert.ToDecimal(lblGSTSetting.Text) / 100,
                inv_No = invoiceno,
                inv_StoreID = this.UserStoreID,
                IsGST = isGST,
            };

            invoice.Products = ips;
            invoice.UserID = GlobalCache.CurrentUser.usr_ID;
            invoice.LastUpdatedTime = DateTime.Now;
            invoice.Status = InvoiceStatus.Paid;
            
            new POSService.POSService().DoInvoice(invoice);
            
            selectedinvoiceid.Attributes["value"] = "0";
            InitialInvoices(0, null, null, null, false);
            Customer customerObj = GlobalCache.Customers.FirstOrDefault(c => c.Name.ToLower() == customer.ToLower());
            if (customerObj != null)
            {
                inpSMS.Text = customerObj.Phone;
                inpEmail.Text = customerObj.Email;
                inpWalletId.Text = customerObj.WalletID;
            }
            else
            {
                inpSMS.Text = "SMS";
                inpEmail.Text = "Email";
                inpWalletId.Text = "Wallet ID";
            }
            isdoinvoice.Attributes["value"] = "1";
            if (invoice.IsGST)
            {
                isgstchecked.Attributes["value"] = "true";
            }
            else
            {
                isgstchecked.Attributes["value"] = "false";
            }
            SerialPort _serialPort = null;
            try
            {
                _serialPort = new SerialPort("COM8", 9600, Parity.None, 8, StopBits.One);
                if (!(_serialPort.IsOpen))
                    _serialPort.Open();
                _serialPort.Write("1\r\n");
            }
            catch { }
            finally
            {
                if (_serialPort != null)
                {
                    _serialPort.Close();
                }
            }
           
            ScriptManager.RegisterStartupScript(resubmitBtn, GetType(), "paidSuccess", "paidSuccess();", true);
            
            //if (invoice.inv_PrintTo == "Wallet(App)" || invoice.inv_PrintTo == "Print & Wallet (App)")
            //{
            //    int clientid = this.UserClientID;
            //    int storeid = this.UserStoreID;
            //    int invoiceno = invoice.inv_No;
            //    //notify the wallet app to get receipt by client id, store id, invoice no
            //}

        }

        protected void Cancel(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(lblCancelID.Value);
            List<Invoice> list = GlobalCache.Invoices;
            if (list != null)
            {
                Invoice invoice = list.FirstOrDefault(i => i.inv_Id == id);
                if (invoice != null)
                {
                    invoice.IsCancel = true;
                    invoice.Reason = txtReason.Text;
                    invoice.UserID = GlobalCache.CurrentUser.usr_ID;
                    invoice.LastUpdatedTime = DateTime.Now;
                    new POSService.POSService().DoInvoice(invoice);
                }
            }
            selectedinvoiceid.Attributes["value"] = "0";
            InitialInvoices(0, null, null, null, false);
            isdoinvoice.Attributes["value"] = "0";
        }

        protected void AddCustomer(object sender, EventArgs e)
        {
            Customer customer = new Customer();
            customer.Name = txtCustomerName.Text.Trim();
            customer.Address = txtCustomerAddress.Text.Trim();
            customer.ClientID = this.UserClientID;
            customer.Country = txtCustomerCountry.Text.Trim();
            customer.Email = txtCustomerEmail.Text.Trim();
            customer.ID = 0;
            customer.LastUpdatedTime = DateTime.Now;
            customer.Mobile = txtCustomerMobilePhone.Text.Trim();
            customer.Phone = txtCustomerPhone.Text.Trim();
            customer.WalletID = txtCustomerWalletID.Text.Trim();
            customer.UserID = GlobalCache.CurrentUser.usr_ID;
            Customer newcus = new POSService.POSService().AddCustomer(customer);
            GlobalCache.RefreshCustomers();
            customerItems.DataSource = null;
            customerItems.DataSource = GlobalCache.Customers;
            customerItems.DataBind();
            txtCustomerName.Text = string.Empty;
            txtCustomerAddress.Text = string.Empty;
            txtCustomerCountry.Text = string.Empty;
            txtCustomerEmail.Text = string.Empty;
            txtCustomerMobilePhone.Text = string.Empty;
            txtCustomerPhone.Text = string.Empty;
            txtCustomerWalletID.Text = string.Empty;
        }

    }
}