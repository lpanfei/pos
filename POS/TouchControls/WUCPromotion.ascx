﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCPromotion.ascx.cs"
    Inherits="POS.TouchControls.WUCPromotion" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    var previouswarranty = null;
    Sys.Application.add_load(warrantyLoad);
    function warrantyLoad() {
        $(".promotionlist tr").unbind("click");
        $(".promotionlist tr").click(function () {
            $("#selectedpromotionid").val($(this).find(".promotionid").eq(0).val());

            $("#btnPromotionSelected").trigger("click");
            }
        );

        if ($("#inpPromotionProductID").val() != "-1") {
            $("#dropPromotionProduct").val($("#inpPromotionProductID").val());
        }

        $(".promotionlist tr").each(function () {
            if ($(this).find(".promotionid").eq(0).val() == $("#selectedpromotionid").val()) {
                $(this).css("background-color", "#EFF3F4");
            }
        });

        $(".addpromotionicon").unbind("click");
        $(".addpromotionicon").click(function () {
            $(".btnAddPromotion").trigger("click");
            $("#btnDeletePromotion").css("display", "none");
        });

        $("#btnDeletePromotion").unbind("click");
        $("#btnDeletePromotion").click(function () {
            $("#delpromotionid").val($("#selectedpromotionid").val());
            radconfirm("Are you sure to delete the item?", deletePromotion, 300, 100, null, "Delete Promotion", "Images/Confirm.png");
        });
    }

    function deletePromotion(sender) {
        if (sender) {
            $("#delPromotionBtn").trigger("click");
        } else {
            $("#delpromotionid").val("-1");
        }
    }
</script>
<style type="text/css">
    input[type=submit]
    {
        cursor: pointer;
    }
    .displaynone
    {
        display: none;
    }
</style>
<asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="mainheader">
            <div class="contextual">
                <asp:Panel ID="listbuttons" runat="server">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add btnAddPromotion" ID="addLink"
                            Text="Add" OnClick="AddWarranty" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="formbuttons" runat="server" Visible="false">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                            Text="Back" OnClick="BackToList" />
                    </div>
                </asp:Panel>
            </div>
            <div class="subheader">
                <h2 class="icon-credit pagetitle">
                    <asp:Literal ID="ltlPageTitle" Text="Promotion List" runat="server"></asp:Literal></h2>
            </div>
        </div>
        <table style="border-spacing:0px">
            <tr>
                <td>
        <asp:Panel ID="listpanel" runat="server">
        <div style="height:765px;overflow:auto;border:1px solid #e4e4e4;border-radius:15px;width:400px">
                        <div style="height:35px;width:100%;border-bottom:1px solid #e4e4e4;background-color:#25a0da">
                        <table style="width:100%">
                        <tr>
                        <td style="width:32px"><img src="images/icon_list.png" style="cursor:pointer" class="shownav" />
                        </td>
                        <td align="center"><span style="color:White;font-size:15px;vertical-align:middle">Promotions</span>
                        </td>
                        <td style="width:32px">
                        <img src="images/add.png" style="width:20px;height:20px;cursor:pointer" class="addpromotionicon" />
                        </td>
                        </tr>
                        </table>
                        
                        
                        </div>
                         <div style="width: 100%; height: 730px; overflow-y: auto">
            <asp:Repeater ID="rptPromotionList" runat="server">
                <HeaderTemplate>
                    <table class="list promotionlist">
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            <%# Eval("ProductName")%>
                            <input type="hidden" value='<%# Eval("ID") %>' class="promotionid" />
                        </td>
                         <td align="center">
                             <asp:Label ID="lblDiscount" runat="server" Text='<%# Eval("Discount")%>'></asp:Label>%
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
            </div>
            </div>
            <%--<webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged"
                PageSize="10" CssClass="aspnetpager">
            </webdiyer:AspNetPager>--%>
        </asp:Panel>
        </td>
        <td>
        <asp:Panel ID="formpanel" runat="server">
            <div  style="height:765px;overflow:auto;border:1px solid #e4e4e4;border-radius:15px;border-left:0px solid black;width:620px;">
                        <div style="height:35px;width:100%;border-bottom:1px solid #e4e4e4;background-color:#25a0da">
                        <table style="float:right">
                            <tr>
                            <%--<td  style="padding:0px">
                                                            <asp:Button ID="addfiled" runat="server" Text="Add New Fields"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center displaynone"
                                        Height="30" Width="120" ClientIDMode="Static" />
                                                            </td>--%>
                                                            <td  style="padding:0px;width:32px;padding-top:3px;"><img id="btnDeletePromotion" src="images/trash.png" style="cursor:pointer" /></td>
                                    
                            </tr>
                            </table>
                        </div>
                            <div style="padding-left:20px;height:690px;overflow:auto">
                        <table class="form-table">
                            <tr>
                                <td class="form-label">
                                   Product:
                                </td>
                                <td>
                                    <asp:DropDownList ID="dropPromotionProduct" runat="server" DataTextField="pro_Name" DataValueField="pro_Id">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Discount:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDiscount" runat="server" ></asp:TextBox>%
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Description:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPromotionDescription" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Start Date:
                                </td>
                                <td>
                                    <%--<asp:TextBox ID="txtStartDate" runat="server" CssClass="Wdate"></asp:TextBox>--%>
                                    <eid:RadDatePicker runat="server" ID="txtStartDate" Width="295px" Height="35px"
                                        Skin="MetroTouch" ShowPopupOnFocus="true" DateInput-Enabled="false" >
                                        <Calendar ID="Calendar1" runat="server" EnableKeyboardNavigation="true">
                                        </Calendar>
                                    </eid:RadDatePicker>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    End Date:
                                </td>
                                <td>
                                    <%--<asp:TextBox ID="txtEndDate" runat="server" CssClass="Wdate"></asp:TextBox>--%>
                                    <eid:RadDatePicker runat="server" ID="txtEndDate" Width="295px" Height="35px"
                                        Skin="MetroTouch" ShowPopupOnFocus="true" DateInput-Enabled="false" >
                                        <Calendar ID="Calendar2" runat="server" EnableKeyboardNavigation="true">
                                        </Calendar>
                                    </eid:RadDatePicker>
                                </td>
                            </tr>
                            
                            <tr style="display: none;">
                                <td colspan="2" align="center">
                                    <input type="hidden" id="inpPromotionProductID" runat="server" clientidmode="Static"
                                        value="-1" />
                                    <asp:Label ID="lblProductID" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="height:40px;width:160px;margin:0 auto;">
                            <table>
                            <tr>
                            <td>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                            UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                            Width="75" Height="30" /></td>
                                        <td style="width:10px"></td>
                            <td><asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                        Height="30" Width="75" /></td>
                            </tr>
                            </table></div>
                </div>
        </asp:Panel>
        </td>
        </tr></table>
        <input type="hidden" id="delpromotionid" runat="server" clientidmode="Static" value="" />
        <asp:Button ID="delPromotionBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="Delete" ClientIDMode="Static"></asp:Button>
             <asp:Button ID="btnPromotionSelected" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="PromotionSelected" ClientIDMode="Static"></asp:Button>
            <input type="hidden" id="selectedpromotionid" runat="server" clientidmode="Static" value="0" />
    </ContentTemplate>
</asp:UpdatePanel>
