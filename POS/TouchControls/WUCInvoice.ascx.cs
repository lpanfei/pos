﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;
using System.Text.RegularExpressions;
using Telerik.Web.UI;

namespace POS.TouchControls
{
    public partial class WUCInvoice : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialize();
                txtDateFrom.Attributes.Add("onFocus", "WdatePicker({lang:'en',dateFmt:'dd-MM-yyyy'})");
                txtDateTo.Attributes.Add("onFocus", "WdatePicker({lang:'en',dateFmt:'dd-MM-yyyy'})");
            }
        }

        public override void Init()
        {
            if (pageform.Visible)
            {
                BackToList(null, null);
            }

            Initialize();
        }

        public string InvoiceNumberPrefix
        {
            get
            {
                if (GlobalCache.Setting == null)
                    return "";
                return GlobalCache.Setting.Prefix;
            }
        }

        void Initialize()
        {
            pagelist.Visible = true;
            pageform.Visible = false;
            addLink.Visible = true;
            searcharea.Visible = true;
            IntialSetting();
            InitialInvoices(0, null, null, null, false);
            if (GlobalCache.Invoices.Any())
            {
                lblNo.Text = (GlobalCache.Invoices.First().inv_No + 1).ToString();
            }
            else
            {
                lblNo.Text = "100000";
            }
            List<Category> cats = GlobalCache.Categories;
            //dropCategory.DataSource = cats;
            //dropCategory.DataBind();
            //if (cats.Any())
            //{
            //    dropCategory.SelectedIndex = 0;
            //    dropCategory_SelectedIndexChanged(null, null);
            //}
            catPanelBar.DataSource = cats;
            catPanelBar.DataBind();
            catPanelBar.Items[0].Expanded = true;
            int id = Convert.ToInt32(catPanelBar.Items[0].Value);
            productsListView.DataSource = GlobalCache.Inventories.Where(p => p.Product.Category != null && p.Product.Category.cat_Id == id).ToList();
            productsListView.DataBind();


            dropCustomer.DataSource = GlobalCache.Customers;
            dropCustomer.DataBind();
            if (GlobalCache.Customers.Any())
            {
                dropCustomer.SelectedIndex = 0;
                this.dropCustomer_SelectedIndexChanged(null, null);
            }

            lblDate.Text = DateTime.Now.ToShortDateString();
        }

        void InitialInvoices(int invno, string customer, DateTime? datefrom, DateTime? dateto, bool isCancel)
        {
            GlobalCache.RefreshInvoices(invno, customer, datefrom, dateto, isCancel);
            //this.AspNetPager1.RecordCount = GlobalCache.Invoices.Count();
            rptList.DataSource = null;
            rptList.DataSource = GlobalCache.Invoices;//.TakeFrom(0, this.AspNetPager1.PageSize);
            //this.AspNetPager1.CurrentPageIndex = 1;
            //this.AspNetPager1.GoToPage(this.AspNetPager1.CurrentPageIndex);
            rptList.DataBind();
        }

        void IntialSetting()
        {
            if (GlobalCache.Setting != null)
            {
                lblGSTSetting.Text = GlobalCache.Setting.GST.ToString();
                dropType.Items.Add(new ListItem() { Text = "Cash", Value = "100" });
                dropType.Items.Add(new ListItem() { Text = "Retailer", Value = GlobalCache.Setting.RetailerDiscount.ToString("0.00") });
                dropType.Items.Add(new ListItem() { Text = "Wholesaler", Value = GlobalCache.Setting.WholesalerDiscount.ToString("0.00") });
                dropType.Items.Add(new ListItem() { Text = "Export", Value = GlobalCache.Setting.ExportDiscount.ToString("0.00") });
                lblPrefix.Text = GlobalCache.Setting.Prefix;
            }
            else
            {
                lblGSTSetting.Text = "0";
                dropType.Items.Add(new ListItem() { Text = "Cash", Value = "100" });
                dropType.Items.Add(new ListItem() { Text = "Retailer", Value = "100" });
                dropType.Items.Add(new ListItem() { Text = "Wholesaler", Value = "100" });
                dropType.Items.Add(new ListItem() { Text = "Export", Value = "100" });
            }
        }

        protected void CategorySelected(object sender, Telerik.Web.UI.RadPanelBarEventArgs e)
        {
            int id = Convert.ToInt32(e.Item.Value);
            productsListView.DataSource = GlobalCache.Inventories.Where(p => p.Product.Category != null && p.Product.Category.cat_Id == id).ToList();
            productsListView.DataBind();
        }

        protected void FilterByTime(object sender, EventArgs e)
        {
            IEnumerable<Invoice> list = null;
           
            DateTime? startDate = RadDatePicker1.SelectedDate;
            DateTime? endDate = RadDatePicker2.SelectedDate;
            if (startDate == null && endDate == null)
            {
                list=GlobalCache.Invoices;
            }
            else if (startDate == null && endDate != null)
            {
                list = GlobalCache.Invoices.Where(i => i.inv_CreateDate.Value <= endDate.Value.AddDays(1));
            }
            else if (startDate != null && endDate == null)
            {
                list = GlobalCache.Invoices.Where(i => i.inv_CreateDate.Value >= startDate.Value);
            }
            else
            {
                if (startDate.Value > endDate.Value)
                {
                    ShowError("Start date should be less than end date");
                    return;
                }
                list = GlobalCache.Invoices.Where(i => i.inv_CreateDate.Value >= startDate.Value && i.inv_CreateDate.Value <= endDate.Value.AddDays(1));
            }

            if (list != null)
            {
                switch (dropPaidStatus.SelectedValue)
                {
                    case "All":
                        break;
                    case "Paid":
                        list = list.Where(i => i.Status == InvoiceStatus.Paid).ToList();
                        break;
                    case "Unpaid":
                        list =list.Where(i => i.Status == InvoiceStatus.UnPaid).ToList();
                        break;
                }
                rptList.DataSource = list;
                rptList.DataBind();
            }
        }

        protected void PaidStatusChanged(object sender, EventArgs e)
        {
            List<Invoice> list = new List<Invoice>();
            try
            {
                switch (dropPaidStatus.SelectedValue)
                {
                    case "All":
                        list = GlobalCache.Invoices.ToList();
                        break;
                    case "Paid":
                        list = GlobalCache.Invoices.Where(i => i.Status == InvoiceStatus.Paid).ToList();
                        break;
                    case "Unpaid":
                        list = GlobalCache.Invoices.Where(i => i.Status == InvoiceStatus.UnPaid).ToList();
                        break;
                }
            }
            catch { }

            rptList.DataSource = null;
            rptList.DataSource = list;
            rptList.DataBind();
            RadDatePicker1.SelectedDate = null;
            RadDatePicker2.SelectedDate = null;
            //this.AspNetPager1.RecordCount = list.Count();
            //this.AspNetPager1.CurrentPageIndex = 1;
            //this.AspNetPager1.GoToPage(this.AspNetPager1.CurrentPageIndex);
        }

        protected void DeleteInvoiceItem(object sender, EventArgs e)
        {
            RepeaterItem currentri = (sender as LinkButton).Parent as RepeaterItem;

            int id = Convert.ToInt32(lblId.Text);
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            int i = 0;
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                if (ri == currentri)
                    continue;
                string desc = (ri.FindControl("lblName") as Label).Text;
                decimal price = Convert.ToDecimal((ri.FindControl("txtPrice") as TextBox).Text);
                decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = price * quantity;
                string note = (ri.FindControl("txtNote") as TextBox).Text;
                ips.Add(new InvoiceProduct() { ip_Description = desc, ip_Discount = discount, ip_InvoiceID = id, ip_Note = note, ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = cost, ip_ProductID = pid, ip_ID = i });
                i++;
                total += subtotal;
            }
            lblTotal.Text = total.ToString();
            lblGST.Text = (total * Convert.ToInt32(lblGSTSetting.Text) / 100).ToString();
            if (chkIsGST.Checked)
            {
                lblTotalGST.Text = (total + total * Convert.ToInt32(lblGSTSetting.Text) / 100).ToString();
            }
            else
            {
                lblTotalGST.Text = lblTotal.Text;
            }
            rptProductList.DataSource = ips;
            rptProductList.DataBind();

            if (ips.Any())
            {
                totaltable.Visible = true;
            }
            else
            {
                totaltable.Visible = false;
            }
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemEdit")
            {
                if (GlobalCache.Invoices != null)
                {
                    List<Invoice> invoices = GlobalCache.Invoices;
                    if (invoices != null)
                    {
                        Invoice invoice = invoices.FirstOrDefault(i => i.inv_Id == Convert.ToInt32(e.CommandArgument));
                        txtAddress.Text = invoice.inv_Address;
                        txtCountry.Text = invoice.Country;
                        txtEmail.Text = invoice.Email;
                        txtMobile.Text = invoice.inv_Mobile;
                        txtPhone.Text = invoice.inv_Phone;
                        txtWalletID.Text = invoice.inv_WalletID;
                        bool dropCustomerSelected = false;
                        if (dropCustomer.Items != null)
                        {
                            foreach (ListItem item in dropCustomer.Items)
                            {
                                if (item.Text == invoice.inv_Customer)
                                {
                                    item.Selected = true;
                                    dropCustomerSelected = true;
                                }
                                else
                                {
                                    item.Selected = false;
                                }
                            }
                        }
                        dropCustomer.Visible = dropCustomerSelected;
                        txtName.Visible = !dropCustomerSelected;
                        if (!dropCustomerSelected)
                        {
                            editImageBtn.ImageUrl = "~/Images/test.png";
                            txtName.Text = invoice.inv_Customer;
                        }
                        else
                        {
                            editImageBtn.ImageUrl = "~/Images/edit.png";
                        }

                        foreach (ListItem item in dropType.Items)
                        {
                            if (item.Text == invoice.CustomerType)
                            {
                                item.Selected = true;
                            }
                            else
                            {
                                item.Selected = false;
                            }
                        }
                        foreach (ListItem item in dropPayMethod.Items)
                        {
                            if (item.Text == invoice.inv_PaymentMethod)
                            {
                                item.Selected = true;
                            }
                            else
                            {
                                item.Selected = false;
                            }
                        }
                        foreach (ListItem item in dropPrintTo.Items)
                        {
                            if (item.Text == invoice.inv_PrintTo)
                            {
                                item.Selected = true;
                            }
                            else
                            {
                                item.Selected = false;
                            }
                        }
                        lblGST.Text = invoice.inv_GST.ToString("0.00");
                        lblTotal.Text = invoice.inv_Total.ToString("0.00");
                        if (invoice.IsGST)
                        {
                            chkIsGST.Checked = true;
                            lblTotalGST.Text = invoice.inv_TotalIncGST.ToString("0.00");
                        }
                        else
                        {
                            chkIsGST.Checked = false;
                            lblTotalGST.Text = invoice.inv_Total.ToString("0.00");
                        }
                        lblNo.Text = invoice.inv_No.ToString();
                        lblDate.Text = invoice.inv_CreateDate.Value.ToShortDateString();
                        rptProductList.DataSource = invoice.Products;
                        rptProductList.DataBind();
                        if (invoice.Products.Any())
                        {
                            totaltable.Visible = true;
                        }
                        else
                        {
                            totaltable.Visible = false;
                        }
                        pageform.Visible = true;
                        pagelist.Visible = false;
                        formbuttons.Visible = true;
                        listbuttons.Visible = false;
                        lblId.Text = invoice.inv_Id.ToString();
                        ltlPageTitle.Text = "Invoices / Edit";
                    }
                }
            }
            else if (e.CommandName == "ItemCancel")
            {
                //RadWindow1.Visible = true;
                // RadWindow1.VisibleOnPageLoad = true;
            }
            else if (e.CommandName == "ItemPrint")
            {
            }
            else if (e.CommandName == "ItemPay")
            {
                Invoice invoice = GlobalCache.Invoices.FirstOrDefault(i => i.inv_Id == Convert.ToInt32(e.CommandArgument));
                if (invoice.Products.Any(p => !p.IsComplete))
                {
                    ShowError("There are products in this order which have not been delivered! Please verify in task mode.");
                    return;
                }
                invoice.Status = InvoiceStatus.Paid;
                new POSService.POSService().DoInvoice(invoice);
                InitialInvoices(0, null, null, null, false);
            }
        }

        //protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        //{
        //    List<Invoice> invoices = GlobalCache.Invoices;
        //    int pageSize = AspNetPager1.PageSize;
        //    int currentPage = AspNetPager1.CurrentPageIndex;

        //    if (this.ltlPageTitle.Text == "Cancelled List")
        //    {
        //        rptList.DataSource = null;
        //        rptList.DataSource = invoices.TakeFrom((currentPage - 1) * pageSize, pageSize);
        //        rptList.DataBind();
        //        if (rptList.Controls.Count > 0)
        //        {
        //            var obj = rptList.Controls[0].FindControl("reasonColumn");
        //            if (obj != null)
        //            {
        //                Label lbl = obj as Label;
        //                if (lbl != null)
        //                {
        //                    lbl.Text = "Reason";
        //                }
        //            }
        //        }
        //    }
        //    else
        //    {
        //        List<Invoice> list = new List<Invoice>();
        //        try
        //        {
        //            switch (dropPaidStatus.SelectedValue)
        //            {
        //                case "All":
        //                    list = GlobalCache.Invoices.ToList();
        //                    break;
        //                case "Paid":
        //                    list = GlobalCache.Invoices.Where(i => i.Status == InvoiceStatus.Paid).ToList();
        //                    break;
        //                case "Unpaid":
        //                    list = GlobalCache.Invoices.Where(i => i.Status == InvoiceStatus.UnPaid).ToList();
        //                    break;
        //            }
        //        }
        //        catch { }
        //        rptList.DataSource = null;
        //        rptList.DataSource = list.TakeFrom((currentPage - 1) * pageSize, pageSize);
        //        rptList.DataBind();
        //        if (rptList.Controls.Count > 0)
        //        {
        //            var obj = rptList.Controls[0].FindControl("reasonColumn");
        //            if (obj != null)
        //            {
        //                Label lbl = obj as Label;
        //                if (lbl != null)
        //                {
        //                    lbl.Text = "";
        //                }
        //            }
        //        }
        //    }
        //}

        protected void AddCustomer(object sender, EventArgs e)
        {
            Customer customer = new Customer();
            customer.Name = txtCustomerName.Text.Trim();
            customer.Address = txtCustomerAddress.Text.Trim();
            customer.ClientID = this.UserClientID;
            customer.Country = txtCustomerCountry.Text.Trim();
            customer.Email = txtCustomerEmail.Text.Trim();
            customer.ID = 0;
            customer.LastUpdatedTime = DateTime.Now;
            customer.Mobile = txtCustomerMobilePhone.Text.Trim();
            customer.Phone = txtCustomerPhone.Text.Trim();
            customer.WalletID = txtCustomerWalletID.Text.Trim();
            customer.UserID = GlobalCache.CurrentUser.usr_ID;
            Customer newcus=new POSService.POSService().AddCustomer(customer);
            GlobalCache.RefreshCustomers();
            dropCustomer.DataSource = GlobalCache.Customers;
            dropCustomer.DataBind();
            if (GlobalCache.Customers.Any())
            {
                dropCustomer.SelectedValue = newcus.ID.ToString();
                this.dropCustomer_SelectedIndexChanged(null, null);
            }
            txtCustomerName.Text = string.Empty;
            txtCustomerAddress.Text = string.Empty;
            txtCustomerCountry.Text = string.Empty;
            txtCustomerEmail.Text = string.Empty;
            txtCustomerMobilePhone.Text = string.Empty;
            txtCustomerPhone.Text = string.Empty;
            txtCustomerWalletID.Text = string.Empty;
        }

        protected void Cancel(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(lblCancelID.Value);
            List<Invoice> list = GlobalCache.Invoices;
            if (list != null)
            {
                Invoice invoice = list.FirstOrDefault(i => i.inv_Id == id);
                if (invoice != null)
                {
                    invoice.IsCancel = true;
                    invoice.Reason = txtReason.Text;
                    invoice.UserID = GlobalCache.CurrentUser.usr_ID;
                    invoice.LastUpdatedTime = DateTime.Now;
                    new POSService.POSService().DoInvoice(invoice);
                }
            }
            Initialize();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {

            InitialInvoices(string.IsNullOrEmpty(this.txtNo.Text) ? 0 : Convert.ToInt32(this.txtNo.Text.Replace(InvoiceNumberPrefix, "")), this.txtCustomer.Text.Trim(),
                string.IsNullOrEmpty(this.txtDateFrom.Text) ? new DateTime?() : GetDateTime(this.txtDateFrom.Text), string.IsNullOrEmpty(this.txtDateTo.Text) ? new DateTime?() : GetDateTime(this.txtDateTo.Text), false);
        }

        protected void CancelledList(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (btn.Text == "Cancelled List")
            {
                btn.Text = "Invoice List";
                InitialInvoices(0, null, null, null, true);
                this.ltlPageTitle.Text = "Cancelled List";
                addLink.Visible = false;
                if (rptList.Controls.Count > 0)
                {
                    var obj = rptList.Controls[0].FindControl("reasonColumn");
                    if (obj != null)
                    {
                        Label lbl = obj as Label;
                        if (lbl != null)
                        {
                            lbl.Text = "Reason";
                        }
                    }
                }
                searcharea.Visible = false;
            }
            else
            {
                btn.Text = "Cancelled List";
                searcharea.Visible = true;
                addLink.Visible = true;
                InitialInvoices(0, null, null, null, false);
                this.ltlPageTitle.Text = "Invoice List";
                if (rptList.Controls.Count > 0)
                {
                    var obj = rptList.Controls[0].FindControl("reasonColumn");
                    if (obj != null)
                    {
                        Label lbl = obj as Label;
                        if (lbl != null)
                        {
                            lbl.Text = "";
                        }
                    }
                }
            }
        }

        DateTime GetDateTime(string s)
        {
            string[] str = s.Split('-');
            int year = Convert.ToInt32(str[2]);
            int month = Convert.ToInt32(str[1]);
            int day = Convert.ToInt32(str[0]);
            return new DateTime(year, month, day, 0, 0, 0);
        }

        protected void AddInvoice(object sender, EventArgs e)
        {
            pageform.Visible = true;
            pagelist.Visible = false;
            formbuttons.Visible = true;
            listbuttons.Visible = false;
            dropCustomer.Visible = true;
            txtName.Visible = false;
            lblId.Text = "0";
            ltlPageTitle.Text = "Invoices / Add";
            //if (dropCategory.Items.Count > 0)
            //{
            //    dropCategory.SelectedIndex = 0;
            //    dropCategory_SelectedIndexChanged(null, null);
            //}
            if (dropCustomer.Items.Count > 0)
            {
                dropCustomer.SelectedIndex = 0;
                this.dropCustomer_SelectedIndexChanged(null, null);
            }
            totaltable.Visible = false;
        }

        protected void BackToList(object sender, EventArgs e)
        {
            pageform.Visible = false;
            pagelist.Visible = true;
            formbuttons.Visible = false;
            listbuttons.Visible = true;
            lblId.Text = "0";

            txtAddress.Text = txtCountry.Text = txtName.Text = txtEmail.Text = txtMobile.Text = txtPhone.Text = txtWalletID.Text = "";
            //dropCategory.SelectedIndex = 0;
            if (dropCustomer.Items != null && dropCustomer.Items.Count > 0)
            {
                dropCustomer.SelectedIndex = 0;
            }
            dropPayMethod.SelectedIndex = 0;
            dropPrintTo.SelectedIndex = 0;
            dropType.SelectedIndex = 0;
            rptProductList.DataSource = null;
            rptProductList.DataBind();
            lblGST.Text = "";
            lblTotal.Text = "";
            lblTotalGST.Text = "";
            txtCustomer.Text = "";
            txtDateFrom.Text = "";
            txtDateTo.Text = "";
            txtNo.Text = "";
           
            if (Button1.Text == "Invoice List")
            {
                ltlPageTitle.Text = "Cancelled List";
                //Button1.Text = "Cancelled List";
                searcharea.Visible = false;
                addLink.Visible = false;
                InitialInvoices(0, null, null, null, true);
            }
            else
            {
                ltlPageTitle.Text = "Invoices";
                //Button1.Text = "Cancelled List";
                searcharea.Visible = true;
                addLink.Visible = true;
                InitialInvoices(0, null, null, null, false);
            }
        }

        protected void dropCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            //productsListView.DataSource = GlobalCache.Inventories.Where(p => p.Product.Category != null && p.Product.Category.cat_Id == Convert.ToInt32(dropCategory.SelectedValue)).ToList();
           // productsListView.DataBind();
        }

        protected void dropCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(dropCustomer.SelectedValue))
                return;
            int id = Convert.ToInt32(dropCustomer.SelectedValue);
            Customer customer = new POSService.POSService().GetCustomerByID(id);
            txtAddress.Text = customer.Address;
            txtCountry.Text = customer.Country;
            txtPhone.Text = customer.Phone;
            txtMobile.Text = customer.Mobile;
            txtEmail.Text = customer.Email;
            txtWalletID.Text = customer.WalletID;
        }

        protected void OnEdit(object sender, EventArgs e)
        {
            txtName.Visible = !txtName.Visible;
            dropCustomer.Visible = !dropCustomer.Visible;
            if (txtName.Visible)
            {
                editImageBtn.ImageUrl = "~/Images/test.png";
                txtName.Text = dropCustomer.SelectedItem != null ? dropCustomer.SelectedItem.Text : "";
            }
            else
            {
                editImageBtn.ImageUrl = "~/Images/edit.png";
                if (dropCustomer.Items.Count > 0)
                {
                    dropCustomer.SelectedIndex = 0;
                    this.dropCustomer_SelectedIndexChanged(null, null);
                }
            }
        }

        protected void dropProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        protected void DoInvoice(object sender, EventArgs e)
        {
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                string desc = (ri.FindControl("lblName") as Label).Text;
                decimal price = Convert.ToDecimal((ri.FindControl("txtPrice") as TextBox).Text);
                decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = price * quantity;
                string note = (ri.FindControl("txtNote") as TextBox).Text;
                InvoiceProduct ipp = new InvoiceProduct() { ip_Description = desc, ip_Discount = discount, ip_Note = note, ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = cost, ip_ProductID = pid };
                ipp.Product = GlobalCache.Products.FirstOrDefault(p => p.pro_Id == ipp.ip_ProductID);
                ipp.Status = InvoiceProductStatus.Complete;
                ips.Add(ipp);
                total += cost;
            }

            Invoice invoice = new Invoice()
            {
                inv_Id = Convert.ToInt32(lblId.Text),
                inv_Staff = GlobalCache.CurrentUser.usr_Name,
                inv_Address = txtAddress.Text.Trim(),
                inv_ClientID = this.UserClientID,
                inv_Customer = txtName.Visible ? this.txtName.Text.Trim() : (dropCustomer.SelectedItem == null ? "" : dropCustomer.SelectedItem.Text),
                inv_GST = Convert.ToDecimal(this.lblGST.Text),
                inv_Total = Convert.ToDecimal(this.lblTotal.Text),
                inv_TotalIncGST = Convert.ToDecimal(this.lblGST.Text) + Convert.ToDecimal(this.lblTotal.Text),
                inv_Mobile = this.txtMobile.Text.Trim(),
                inv_No = Convert.ToInt32(this.lblNo.Text),
                inv_PaymentMethod = dropPayMethod.Text,
                inv_PrintTo = dropPrintTo.Text,
                CustomerType = dropType.SelectedItem.Text,
                inv_WalletID = txtWalletID.Text.Trim(),
                inv_Phone = txtPhone.Text.Trim(),
                inv_StoreID = this.UserStoreID,
                Email = txtEmail.Text,
                Country = txtCountry.Text,
                IsGST = chkIsGST.Checked,
            };

            invoice.cost = total;
            invoice.Products = ips;
            invoice.UserID = GlobalCache.CurrentUser.usr_ID;
            invoice.LastUpdatedTime = DateTime.Now;
            invoice.Status = InvoiceStatus.Paid;
            //invoice.WalletConnection = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["WalletDBConnection"].ConnectionString;
            new POSService.POSService().DoInvoice(invoice);
            if (invoice.inv_PrintTo == "Wallet(App)" || invoice.inv_PrintTo == "Print & Wallet (App)")
            {
                int clientid = this.UserClientID;
                int storeid = this.UserStoreID;
                int invoiceno = invoice.inv_No;
                //notify the wallet app to get receipt by client id, store id, invoice no
            }
            BackToList(null, null);


        }

        protected void btnAddProduct_Click(object sender, EventArgs e)
        {

            //var obj=productsListView.DataSourceObject;
            int id = Convert.ToInt32(lblId.Text);
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            int i = 0;
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                string desc = (ri.FindControl("lblName") as Label).Text;
                //decimal price = Convert.ToDecimal((ri.FindControl("txtPrice") as TextBox).Text);
                decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                Product p = GlobalCache.Products.FirstOrDefault(p2 => p2.pro_Id == pid);
                decimal price = p.pro_price;
                decimal subtotal = price * quantity * discount / 100;
                string note = (ri.FindControl("txtNote") as TextBox).Text;
                ips.Add(new InvoiceProduct() { ip_Description = desc, ip_Discount = discount, ip_InvoiceID = id, ip_Note = note, ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = cost, ip_ProductID = pid, ip_ID = i });
                i++;
                total += subtotal;
            }

            RadListViewDataItem item = (sender as ImageButton).Parent as RadListViewDataItem;
            if (item != null)
            {
                int pid = Convert.ToInt32((item.FindControl("lblPId") as Label).Text);
                decimal price = GlobalCache.Products.FirstOrDefault(p2 => p2.pro_Id == pid).pro_price;
                if (ips.Any(p => p.ip_ProductID == pid))
                {
                    ips.FirstOrDefault(p => p.ip_ProductID == pid).ip_Quantity += 1;
                    ips.FirstOrDefault(p => p.ip_ProductID == pid).ip_SubTotal = ips.FirstOrDefault(p => p.ip_ProductID == pid).ip_Price * ips.FirstOrDefault(p => p.ip_ProductID == pid).ip_Quantity * ips.FirstOrDefault(p => p.ip_ProductID == pid).ip_Discount / 100;
                    total += price * ips.FirstOrDefault(p => p.ip_ProductID == pid).ip_Discount/100;
                }
                else
                {
                    decimal discount = 100;
                    if (dropType.SelectedIndex > -1)
                    {
                        discount = Convert.ToDecimal(dropType.SelectedValue);
                    }
                    if (GlobalCache.Promotions != null && GlobalCache.Promotions.Any(pro => pro.ProductID == pid && DateTime.Now >= pro.StartTime && DateTime.Now <= pro.EndTime))
                    {
                        discount = GlobalCache.Promotions.FirstOrDefault(pro => pro.ProductID == pid && DateTime.Now >= pro.StartTime && DateTime.Now <= pro.EndTime).Discount;
                    }
                    string desc = (item.FindControl("lblPName") as Label).Text;
                   
                    int quantity = 1;
                    decimal cost = Convert.ToDecimal(Regex.Replace((item.FindControl("lblPCost") as Label).Text, @"[^\d.]", ""));
                    total += price*discount/100;

                    ips.Add(new InvoiceProduct() { ip_ID = i, ip_SubTotal = price*discount/100, ip_Quantity = quantity, ip_ProductID = pid, ip_Price = price, ip_Description = desc, ip_InvoiceID = id, ip_Note = "", cost = cost, ip_Discount = discount });
                }
            }

            lblTotal.Text = total.ToString();
            lblGST.Text = (total * Convert.ToInt32(lblGSTSetting.Text) / 100).ToString();
            if (chkIsGST.Checked)
            {
                lblTotalGST.Text = (total + total * Convert.ToInt32(lblGSTSetting.Text) / 100).ToString();
            }
            else
            {
                lblTotalGST.Text = lblTotal.Text;
            }


            rptProductList.DataSource = ips;
            rptProductList.DataBind();

            if (ips.Any())
            {
                totaltable.Visible = true;
            }
            else
            {
                totaltable.Visible = false;
            }
        }

        protected void chkIsGST_CheckedChanged(object sender, EventArgs e)
        {
            decimal total = Convert.ToDecimal(lblTotal.Text);

            if (chkIsGST.Checked)
            {
                lblTotalGST.Text = (total + total * Convert.ToInt32(lblGSTSetting.Text) / 100).ToString();
            }
            else
            {
                lblTotalGST.Text = lblTotal.Text;
            }
        }

        protected void dropType_SelectedIndexChanged(object sender, EventArgs e)
        {
        }
        //protected void txtQuantity_TextChanged(object sender, EventArgs e)
        //{
        //    UpdatePendingList();
        //}

        //protected void txtPrice_TextChanged(object sender, EventArgs e)
        //{
        //    UpdatePendingList();
        //}

        //protected void txtDiscount_TextChanged(object sender, EventArgs e)
        //{
        //    UpdatePendingList();
        //}

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if ((dropPrintTo.Text == "Wallet(App)" || dropPrintTo.Text == "Print & Wallet (App)") && string.IsNullOrEmpty(txtWalletID.Text))
            {
                ShowError("Please input Wallet ID if you want to receive receipt by Wallet App!");
                return;
            }

            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                string desc = (ri.FindControl("lblName") as Label).Text;
                
                decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                decimal price = GlobalCache.Products.FirstOrDefault(p2 => p2.pro_Id == pid).pro_price;
                decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = price * quantity*discount/100;
                string note = (ri.FindControl("txtNote") as TextBox).Text;
                InvoiceProduct ipp = new InvoiceProduct() { ip_Description = desc, ip_Discount = discount, ip_Note = note, ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = cost, ip_ProductID = pid };
                ipp.Product = GlobalCache.Products.FirstOrDefault(p => p.pro_Id == ipp.ip_ProductID);
                ips.Add(ipp);
                total += cost;
            }

            if (!ips.Any())
            {
                ShowError("Please select products!");
                return;
            }

            bool quantityValidation = true;
            string productname = string.Empty;
            foreach (var ipp in ips.GroupBy(ip2 => ip2.ip_ProductID))
            {
                Inventory inventory = GlobalCache.Inventories.FirstOrDefault(i => i.ProductID == ipp.Key);
                int quantity = inventory.Quantity;
                int inv_quantity = 0;
                foreach (var ippp in ipp)
                {
                    inv_quantity += ippp.ip_Quantity;
                }
                if (inv_quantity > quantity)
                {
                    quantityValidation = false;
                    if (!string.IsNullOrEmpty(productname))
                    {
                        productname = productname + "," + inventory.Product.pro_Name;
                    }
                }
            }

            if (!quantityValidation)
            {
                ShowConfirm("The quantity you inputted for products(" + productname + ") is larger than what we have in inventory. Do you want to continue anyway?", "ReDoInvoice");
                return;
            }

            foreach (InvoiceProduct ipppp in ips)
            {
                ipppp.Status = InvoiceProductStatus.Complete;
            }

            List<Inventory> inventories = new List<Inventory>();
            foreach (var ipp in ips.GroupBy(ip2 => ip2.ip_ProductID))
            {
                Inventory inventory = GlobalCache.Inventories.FirstOrDefault(i => i.ProductID == ipp.Key);
                int inv_quantity = 0;
                foreach (var ippp in ipp)
                {
                    inv_quantity += ippp.ip_Quantity;
                }
                inventory.Quantity = inventory.Quantity - inv_quantity;
                if (inventory.Quantity < 0)
                {
                    inventory.Quantity = 0;
                }
                inventories.Add(inventory);
            }

            Invoice invoice = new Invoice()
            {
                inv_Id = Convert.ToInt32(lblId.Text),
                inv_Staff = GlobalCache.CurrentUser.usr_Name,
                inv_Address = txtAddress.Text.Trim(),
                inv_ClientID = this.UserClientID,
                inv_Customer = txtName.Visible ? this.txtName.Text.Trim() : (dropCustomer.SelectedItem == null ? "" : dropCustomer.SelectedItem.Text),
                inv_GST = Convert.ToDecimal(this.lblGST.Text),
                inv_Total = Convert.ToDecimal(this.lblTotal.Text),
                inv_TotalIncGST = Convert.ToDecimal(this.lblGST.Text) + Convert.ToDecimal(this.lblTotal.Text),
                inv_Mobile = this.txtMobile.Text.Trim(),
                inv_No = Convert.ToInt32(this.lblNo.Text),
                inv_PaymentMethod = dropPayMethod.Text,
                inv_PrintTo = dropPrintTo.Text,
                CustomerType = dropType.SelectedItem.Text,
                inv_WalletID = txtWalletID.Text.Trim(),
                inv_Phone = txtPhone.Text.Trim(),
                inv_StoreID = this.UserStoreID,
                Email = txtEmail.Text,
                Country = txtCountry.Text,
                IsGST = chkIsGST.Checked,
            };
            invoice.Inventories = inventories;
            invoice.cost = total;
            invoice.Products = ips;
            invoice.UserID = GlobalCache.CurrentUser.usr_ID;
            invoice.LastUpdatedTime = DateTime.Now;
            invoice.Status = InvoiceStatus.Paid;
            //invoice.WalletConnection = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["WalletDBConnection"].ConnectionString;
            new POSService.POSService().DoInvoice(invoice);
            if (invoice.inv_PrintTo == "Wallet(App)" || invoice.inv_PrintTo == "Print & Wallet (App)")
            {
                int clientid = this.UserClientID;
                int storeid = this.UserStoreID;
                int invoiceno = invoice.inv_No;
                //notify the wallet app to get receipt by client id, store id, invoice no
            }
            BackToList(null, null);
        }

        protected void btnPrint_Click(object sender, EventArgs e)
        {
        }

        protected void UpdateInvoiceProduct(object sender, EventArgs e)
        {
            UpdatePendingList();
        }

        void UpdatePendingList()
        {
            int id = Convert.ToInt32(lblId.Text);
            List<InvoiceProduct> ips = new List<InvoiceProduct>();
            int i = 0;
            decimal total = 0;
            foreach (RepeaterItem ri in rptProductList.Items)
            {
                string desc = (ri.FindControl("lblName") as Label).Text;
                //decimal price = Convert.ToDecimal((ri.FindControl("txtPrice") as TextBox).Text);
                decimal discount = Convert.ToDecimal((ri.FindControl("txtDiscount") as TextBox).Text);
                int quantity = Convert.ToInt32((ri.FindControl("txtQuantity") as TextBox).Text);
                int pid = Convert.ToInt32((ri.FindControl("lblProductId") as Label).Text);
                decimal price = GlobalCache.Products.FirstOrDefault(p2 => p2.pro_Id == pid).pro_price;
                decimal cost = Convert.ToDecimal((ri.FindControl("lblCost") as Label).Text);
                decimal subtotal = price * quantity*discount/100;
                string note = (ri.FindControl("txtNote") as TextBox).Text;
                ips.Add(new InvoiceProduct() { ip_Description = desc, ip_Discount = discount, ip_InvoiceID = id, ip_Note = note, ip_Price = price, ip_Quantity = quantity, ip_SubTotal = subtotal, cost = cost, ip_ProductID = pid, ip_ID = i });
                i++;
                total += subtotal;
            }
            lblTotal.Text = total.ToString();
            lblGST.Text = (total * Convert.ToInt32(lblGSTSetting.Text) / 100).ToString();
            if (chkIsGST.Checked)
            {
                lblTotalGST.Text = (total + total * Convert.ToInt32(lblGSTSetting.Text) / 100).ToString();
            }
            else
            {
                lblTotalGST.Text = lblTotal.Text;
            }
            rptProductList.DataSource = ips;
            rptProductList.DataBind();

        }
    }
}