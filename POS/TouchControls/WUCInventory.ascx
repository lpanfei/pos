﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCInventory.ascx.cs"
    Inherits="POS.TouchControls.WUCInventory" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    Sys.Application.add_load(inventoryLoad);
    var previousinventory = null;
    function inventoryLoad(sender, args) {

        $(".inventorylist tr").unbind("click");
        $(".inventorylist tr").click(function () {

            $("#selectedinventoryid").val($(this).find(".inventoryid").eq(0).val());

            $("#btnInventorySelected").trigger("click");
        }
            );

        $(".inventorylist tr").each(function () {
            if ($(this).find(".inventoryid").eq(0).val() == $("#selectedinventoryid").val()) {
                $(this).css("background-color", "#EFF3F4");
            }
        });
    }

    function stock(sender, obj) {
        if (sender != null) {
            $("#inventoryquantity").val(sender);
            $("#stoBtn").trigger("click");
        }
    }
</script>
<style type="text/css">
    .displaynone
    {
        display: none;
    }
    
    input[type=button]
    {
        cursor: pointer;
    }
</style>
<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="mainheader">
            <div class="contextual">
            </div>
            <div class="subheader">
                <h3 class="icon-products pagetitle" id="pagetitle">
                    <asp:Literal ID="ltlPageTitle" Text="Products" runat="server"></asp:Literal>
                </h3>
            </div>
        </div>
        <table style="border-spacing: 0px">
            <tr>
                <td>
        <asp:Panel CssClass="pagelist" runat="server" ID="inventorypagelist" ClientIDMode="Static">
            <div style="display:none">
                <div style="float: left">
                    <asp:DropDownList ID="dropCategory" runat="server" Height="30" DataTextField="cat_Name"
                        DataValueField="cat_Id">
                    </asp:DropDownList>
                </div>
                <div style="float: left; margin-left: 2px;">
                    <asp:DropDownList ID="dropField" runat="server" Height="30">
                        <asp:ListItem Text="Name of Product" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Serial No" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div style="float: left; margin-left: 2px; margin-top: -1px;">
                    <asp:TextBox ID="txtKey" runat="server" CssClass="searchbox" Height="28"></asp:TextBox></div>
                <div style="float: left; margin-left: 5px; margin-right: 15px;">
                    <asp:DropDownList ID="dropQuantity" runat="server" Height="30">
                        <asp:ListItem Text="-- All Quantity --" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Greater than 1000" Value="1000"></asp:ListItem>
                        <asp:ListItem Text="Greater than 500" Value="500"></asp:ListItem>
                        <asp:ListItem Text="Greater than 200" Value="200"></asp:ListItem>
                        <asp:ListItem Text="Less than 100" Value="100"></asp:ListItem>
                        <asp:ListItem Text="Less than 50" Value="50"></asp:ListItem>
                        <asp:ListItem Text="Less than 10" Value="10"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search"
                    UseSubmitBehavior="false" class="btn btn-large btn-block btn-info" Width="75"
                    Height="30" Style="margin-left: 15px;" />
            </div>
            <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                            width: 400px">
                            <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 32px">
                                            <img src="images/icon_list.png" style="cursor: pointer" class="shownav" />
                                        </td>
                                        <td align="center">
                                            <span style="color: White; font-size: 15px; vertical-align: middle">Inventory</span>
                                        </td>
                                        <td style="width: 32px">
                                            <%--<img src="images/add.png" style="width: 20px; height: 20px; cursor: pointer" class="addsuppliericon" />--%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                             <div style="width: 100%; height: 730px; overflow-y: auto">
            <asp:Repeater ID="rptList" runat="server">
                <HeaderTemplate>
                    <table class="list inventorylist">
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                         <div  style="width: 300px; overflow: hidden; text-overflow: ellipsis;
                                        white-space: nowrap;padding-left:15px;font-size: 12px;">
                            <%# Eval("Product.pro_Name") %>
                            </div>
                            <input type="hidden" value='<%# Eval("Product.pro_Id") %>' class="inventoryid" />
                        </td>
                        <td align="center">
                            <%# Eval("Quantity") %>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
            </div>
           <%-- <webdiyer:AspNetPager ID="inventoryAspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged"
                 PageSize="10" CssClass="aspnetpager">
            </webdiyer:AspNetPager>--%>
            </div>
        </asp:Panel>
        </td>
        <td>
        <div style="height: 765px; overflow: auto; border: 1px solid #e4e4e4; border-radius: 15px;
                            border-left: 0px solid black; width: 620px;">
                            <div style="height: 35px; width: 100%; border-bottom: 1px solid #e4e4e4; background-color: #25a0da">
                               
                            </div>
                            <div style="padding-left: 20px; height: 690px; overflow: auto">
                                <table class="form-table">
                                    <tr>
                                        <td class="form-label">
                                            Product:
                                        </td>
                                        <td>
                                            <asp:Label ID="txtName" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="form-label">
                                            Quantity:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtQuantity" runat="server"></asp:TextBox>
                                        </td>
                                    </tr> 
                                </table>
                            </div>
                            <div style="height: 40px; width: 160px; margin: 0 auto;">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                                UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                                Width="75" Height="30" />
                                        </td>
                                        <td style="width: 10px">
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click"
                                                UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                                Height="30" Width="75" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
        </td>
        </tr>
        </table>
        <input type="hidden" id="inventoryinvid" runat="server" clientidmode="Static" value="0" />
        <input type="hidden" id="inventorypid" runat="server" clientidmode="Static" value="0" />
        <input type="hidden" id="inventoryquantity" runat="server" clientidmode="Static"
            value="0" />
        <asp:Button ID="stoBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="Stock" ClientIDMode="Static"></asp:Button>
            <asp:Button ID="btnInventorySelected" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="InventorySelected" ClientIDMode="Static"></asp:Button>
            <input type="hidden" id="selectedinventoryid" runat="server" clientidmode="Static" value="0" />
        <div style="clear: both;">
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
