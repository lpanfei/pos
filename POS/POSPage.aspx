﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="POSPage.aspx.cs" Inherits="POS.POSPage" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<%@ Register TagPrefix="wuc" TagName="Invoice" Src="~\Controls\WUCInvoice.ascx" %>
<%@ Register TagPrefix="wuc" TagName="Product" Src="~\Controls\WUCProduct.ascx" %>
<%@ Register TagPrefix="wuc" TagName="Promotion" Src="~\Controls\WUCPromotion.ascx" %>
<%@ Register TagPrefix="wuc" TagName="Category" Src="~\Controls\WUCCategory.ascx" %>
<%@ Register TagPrefix="wuc" TagName="Warranty" Src="~\Controls\WUCWarranty.ascx" %>
<%@ Register TagPrefix="wuc" TagName="User" Src="~\Controls\WUCUser.ascx" %>
<%@ Register TagPrefix="wuc" TagName="Setting" Src="~\Controls\WUCSetting.ascx" %>
<%@ Register TagPrefix="wuc" TagName="Customer" Src="~\Controls\WUCCustomer.ascx" %>
<%@ Register TagPrefix="wuc" TagName="Supplier" Src="~\Controls\WUCSupplier.ascx" %>
<%@ Register TagPrefix="wuc" TagName="Log" Src="~\Controls\WUCLog.ascx" %>
<%@ Register TagPrefix="wuc" TagName="Task" Src="~\Controls\WUCTask.ascx" %>
<%@ Register TagPrefix="wuc" TagName="Inventory" Src="~\Controls\WUCInventory.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head id="Head1" runat="server">
    <title>Distributed POS System - Client 2011</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.2)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.2)">
    <link href="themes/cupertino/jquery-ui.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pos.css" rel="stylesheet" type="text/css" media="screen" />
    <link rel="stylesheet" type="text/css" href="window/css/component.css" />
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script src="Scripts/jquery-ui.js"></script>
    <script src="Scripts/jquery.ui.touch-punch.min.js"></script>
    <script src="Scripts/jquery.keyboard.js"></script>
    <link rel="stylesheet" type="text/css" href="css/keyboard.css" />
    <script src="window/js/modernizr.custom.js"></script>
    <script language="javascript" type="text/javascript" src="My97DatePicker/WdatePicker.js"></script>
    <style type="text/css">
        .loginhover
        {
            background-color: #1B67AF;
        }
    </style>
    <script type="text/javascript">

        function relogin() {
            window.parent.location.href = "index.htm";
        }
        
    </script>
</head>
<body class="controller-admin action-projects">
    <div id="fb-root">
    </div>
    <eid:RadSkinManager ID="QsfSkinManager" runat="server" Skin="Metro" />
    <eid:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="Scrollbars"
        EnableRoundedCorners="false" Skin="Silk" />
    <div id="wrapper">
        <div id="wrapper2">
            <div id="wrapper3">
                <div id="header">
                    <table width="99%" style="margin: 0 auto; border-spacing: 0px; border-width: 0px;
                        border-collapse: collapse;" cellspacing="0">
                        <tr>
                            <td valign="middle" style="color: white">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Image ID="Image1" runat="server" ImageAlign="Middle" ImageUrl="~/Images/LOGO_w.png" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td align="center" valign="middle" width="80px" id="logintd1">
                                <div id="loginarea">
                                    <div id="sublogin" class="account">
                                        <table id="logintd2">
                                            <tr id="logintd3">
                                                <td id="logintd4">
                                                    <i class="icon-account"></i>
                                                </td>
                                                <td id="logintd5">
                                                    <asp:Literal ID="ltlUser" runat="server" ClientIDMode="Static"></asp:Literal>
                                                </td>
                                                <td id="logintd6" style="padding-left: 5px">
                                                    <i class="icon-down"></i>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <%--<img id="tn" src="images/icon_list.png" width="30px" height="30px" style="cursor: pointer;
                        position: absolute; top: 30px; left: 160px; cursor: pointer; z-index: 2000" />--%>
                </div>
                <div id="main">
                    <table width="100%" height="100%" style="margin: 0 auto; border-spacing: 0px; border-width: 0px;
                        border-collapse: collapse;" cellspacing="0">
                        <tr>
                            <td valign="top" style="border-width: 0px;width:160px;">
                                <div id="sidebar">
                                
                                <div style="width:160px;height:32px;margin-top:4px;margin-bottom:4px;">
                                <div id="showheader" style="width:32px;height:32px;overflow: hidden;background: transparent url(images/hpc12.png) no-repeat;background-position: -96px -53px;cursor:pointer;float:left;"></div>
                                <div id="tn" style="width:32px;height:32px;overflow: hidden;background: transparent url(images/hpc12.png) no-repeat;background-position: -192px -53px;cursor:pointer;float:right;"></div>
                                    </div>
                                    <div id="admin-menu" style="width:160px">
                                    
                                        <ul class="menulist">
                                            <li>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='settings'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Settings</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='order'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Order Mode</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='task'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Task Mode</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li class='current'>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='invoicesel'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Invoices</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='credit'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Inventory</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='products'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Products</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='promotions'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Promotions</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='categories'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Categories</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='users'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Users</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li>
                                                <table>
                                                   <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='suppliers'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Suppliers</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='warranty'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Warranties</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='groups'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Customers</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                            <li>
                                                <table>
                                                    <tr style="height:37px">
                                                        <td style="width:37px">
                                                            <i class='logs'></i>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(0)">Logs</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </td>

                            <td style="border-width: 0px; vertical-align: top;">
                                <div id="content">
                                    <form id="form1" runat="server" enctype="multipart/form-data">
                                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"
                                        EnablePageMethods="true">
                                        <Scripts>
                                            <asp:ScriptReference Path="common.js" />
                                        </Scripts>
                                    </asp:ScriptManager>
                                   
                                    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
                                        <ContentTemplate>
                                        <asp:Panel ID="ContainerPanel1" runat="server" >
                                            <div class="contentcontainer" id="settings" style="min-height: 700px; display: none">
                                                <wuc:Setting runat="server" ID="SettingControl" />
                                            </div>
                                            <div class="contentcontainer" id="categories" style="min-height: 700px; display: none">
                                                <wuc:Category runat="server" ID="CategoryControl" />
                                            </div>
                                            <div class="contentcontainer" id="invoice" style="min-height: 700px; display: none;">
                                                <wuc:Invoice runat="server" ID="InvoiceControl" />
                                            </div>
                                            <div class="contentcontainer" id="credit" style="min-height: 700px; display: none">
                                                <wuc:Inventory runat="server" ID="InventoryControl" />
                                            </div>
                                            <div class="contentcontainer" id="products" style="min-height: 700px; display: none">
                                                <wuc:Product runat="server" ID="ProductControl" />
                                            </div>
                                            <div class="contentcontainer" id="promotions" style="min-height: 700px; display: none">
                                                <wuc:Promotion runat="server" ID="PromotionControl" />
                                            </div>
                                            <div class="contentcontainer" id="users" style="min-height: 700px; display: none">
                                                <wuc:User runat="server" ID="UsersControl" />
                                            </div>
                                            <div class="contentcontainer" id="suppliers" style="min-height: 700px; display: none">
                                                <wuc:Supplier runat="server" ID="SupplierControl" />
                                            </div>
                                            <div class="contentcontainer" id="warranty" style="min-height: 700px; display: none">
                                                <wuc:Warranty runat="server" ID="WarrantyControl" />
                                            </div>
                                            <div class="contentcontainer" id="groups" style="min-height: 700px; display: none">
                                                <wuc:Customer runat="server" ID="CustomerControl" />
                                            </div>
                                            <div class="contentcontainer" id="logs" style="min-height: 700px; display: none">
                                                <wuc:Log runat="server" ID="LogControl" />
                                            </div>
                                            <div class="contentcontainer" id="order" style="min-height: 700px; display: none">
                                            </div>
                                            <div class="contentcontainer" id="task" style="min-height: 700px; display: none">
                                                <wuc:Task runat="server" ID="TaskControl" />
                                            </div>
                                            
                                            <input type="hidden" id="previouscontrolid" runat="server" clientidmode="Static"
                                                value="" />
                                            <input type="hidden" id="controlid" runat="server" clientidmode="Static" value="invoice" />
                                            <asp:Button ID="btnInitControls" runat="server" Width="0" Height="0" OnClick="InitControls"
                                                ClientIDMode="Static" Style="display: none"></asp:Button>
                                                </asp:Panel>
                                                <img id="busyprocessing" src="Images/busy.gif" alt="processing" style="position:absolute;top:361px;display:none;" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                    <eid:RadWindow runat="server" ID="RadWindow1" Skin="Metro" Overlay="true" Modal="true"
                                        Title="Modify Password" VisibleStatusbar="false" AutoSize="true" Width="600"
                                        Height="400">
                                        <ContentTemplate>
                                            <asp:UpdatePanel ID="Updatepanel11" runat="server" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <table class="form-table">
                                                        <tr>
                                                            <td class="form-label">
                                                                Old Password:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtOld" runat="server" Width="200" TextMode="Password"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="form-label">
                                                                New Password:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtNew" runat="server" Width="200" TextMode="Password"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="form-label">
                                                                Confirm Password:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtConfirm" runat="server" Width="200" TextMode="Password"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" align="center">
                                                                <asp:Button ID="Button1" Width="100" runat="server" CssClass="btn btn-large btn-block btn-info"
                                                                    Text="Submit" OnClick="Click" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </ContentTemplate>
                                    </eid:RadWindow>
                                    <eid:RadWindowManager runat="server" ID="windowManager" Skin="Metro" KeepInScreenBounds="true"
                                        AutoSize="true">
                                        <AlertTemplate>
                                            <div class="rwDialogPopup radalert">
                                                <div class="rwDialogText">
                                                    {1}
                                                </div>
                                                <div>
                                                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                                                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </AlertTemplate>
                                    </eid:RadWindowManager>
                                    <eid:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Metro" IsSticky="true">
                                    </eid:RadAjaxLoadingPanel>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <div id="shownav" style="width:40px;height:100px;position:absolute;top:300px;left:0px;z-index:2000;background-color:Black;text-align:center;color:White;cursor:pointer;display:none">
                        <div style="margin-left:4px;width:32px;height:32px;overflow: hidden;background: transparent url(images/hpc12.png) no-repeat;background-position: -160px -53px;cursor:pointer;"></div>
                        M<br />E<br />N<br />U
                    </div>
                </div>
            </div>
            <div id="footer">
                <div class="bgl">
                    <div class="bgr">
                    <i class='settingssel'></i>
<i class='ordersel'></i>
<i class='tasksel'></i>
<i class='invoicesel'></i>
<i class='creditsel'></i>
<i class='productssel'></i>
<i class='categoriessel'></i>
<i class='userssel'></i>
<i class='supplierssel'></i>
<i class='warrantysel'></i>
<i class='groupssel'></i>
<i class='logssel'></i>
                    </div>
                </div>
            </div>
            <div id="ls" style="display: none; width: 160px;">
                <ul>
                    <li><a href="javascript:void(0)" class="modify_password">Modify Password</a></li>
                    <li><a href="javascript:void(0)" class="logout">Logout</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class='md-overlay'>
    </div>
    <script>
        var cancelClick = false;
        function pageLoad() {
            if ($("#controlid").val().length > 0) {
                if ($("#" + $("#controlid").val()).css("display") == "none") {
                    $("#" + $("#controlid").val()).toggle("slide", function () {
                        if ($("#controlid").val() == "task" || $("#controlid").val() == "invoice") {
                            $("#sidebar").toggle("fade");
                            
                            $("#sidebar").parent().animate({
                                width: 0
                            }, 1000, function () {
                                $("#shownav").toggle("drop");
                            });
                        } else {
                            if ($("#sidebar").width() == 0) {
                                $("#shownav").toggle("drop");
                                $("#sidebar").animate({
                                    width: 160
                                }, 1000, function () {
                                    $("#sidebar").toggle("drop");
                                });
                                $("#sidebar").parent().animate({
                                    width: 160
                                }, 1000, function () {
                                    // Animation complete.
                                });
                            }
                        }
                    });
                }
            }

            $("#busyprocessing").css("display", "none");

            $("#shownav").draggable({ containment: "#main", scroll: false, stop: function (event, ui) {
                cancelClick = true;
                $("#shownav").animate({
                    left: 0
                }, 1000, function () {
                    // Animation complete.
                });
            }
            });
            $(window).unbind("resize");
            $(window).resize(function () {
                var left = 0;
                if ($("#sidebar").parent().css("display") == "none") {

                    left = ($(window).width() - 32) / 2 - 160;
                } else {
                    left = ($(window).width() - 160 - 32) / 2 + 160;
                }

                $("#busyprocessing").css("left", left);
            });
            $(".menulist li").each(function () {
                $(this).unbind("click");
                $(this).click(function () {
                    var previouscontainer = null;
                    var previouscontrol = "";
                    $(".menulist li").each(function () {
                        var class2 = $(this).find("i").attr("class");

                        if ($(this).hasClass("current")) {
                            previouscontrol = class2.toString().replace("sel", "");
                            previouscontainer = $("#" + class2.toString().replace("sel", ""));
                            $(this).removeClass("current");
                        }

                        $(this).find("i").removeClass(class2.toString());
                        $(this).find("i").addClass(class2.toString().replace("sel", ""));

                    });
                    $(this).addClass("current");
                    var class1 = $(this).find("i").attr("class");
                    $(this).find("i").removeClass(class1.toString());
                    $(this).find("i").addClass(class1.toString() + "sel");
                    if (class1 == "order") {
                        $("body").toggle("fade", function () { window.parent.location.href = "index.htm?page=order"; });
                    } else {
                        if (previouscontainer != null) {
                            // previouscontainer.toggle("fade", function () {
                            //                                $("#" + class1).toggle("fade", function () {
                            //                                    if (class1 == "task" || class1 == "invoice") {
                            //                                        $("#sidebar").parent().css("display", "none");
                            //                                    } else {
                            //                                        $("#sidebar").parent().css("display", "");
                            //                                    }
                            var left = ($(window).width() - 160 - 32) / 2 + 160;
                            $("#busyprocessing").css("left", left);
                            $("#busyprocessing").css("display", "");
                            $("#previouscontrolid").val(previouscontrol);
                            $("#controlid").val(class1);
                            $("#btnInitControls").trigger("click");
                            // });
                            //});
                        }
                    }

                });
            });
            $("#sublogin").unbind("mouseover");
            $("#sublogin").unbind("mouseout");
            $("#sublogin").unbind("mouseup");

            $("#sublogin").mouseover(function (login) { $("#sublogin").toggleClass("loginhover"); });
            $("#sublogin").mouseout(function (login) { $("#sublogin").toggleClass("loginhover"); });
            $("#sublogin").mouseup(function (login) {
                $("#ls").css({ right: "0px", top: '50px', position: 'absolute' }).toggle();
            });

            $("#ls").unbind("mouseup");
            $("#ls").mouseup(function () {
                return false;
            });
            $(this).unbind("mouseup");
            $(this).mouseup(function (login) {
                if (!($(login.target).parent('#loginarea').length > 0 || $(login.target).parent('#logintd1').length > 0
                                || $(login.target).parent('#logintd2').length > 0 || $(login.target).parent('#logintd4').length > 0 || $(login.target).parent('#logintd6').length > 0
                                || $(login.target).parent('#logintd3').length > 0 || $(login.target).parent('#logintd5').length > 0 || $(login.target).parent('#sublogin').length > 0
                                )) {
                    $("#ls").hide();
                }
            });

            $(".modify_password").unbind("click");
            $(".modify_password").click(function () {
                var radwindow = $find('<%=RadWindow1.ClientID %>');
                radwindow.show();
            });

            $("#tn").unbind("click");
            $("#tn").click(function () {
                $("#sidebar").toggle("fade");
                
                $("#sidebar").parent().animate({
                    width: 0
                }, 1000, function () {
                    $("#shownav").css("left", "0px");
                    $("#shownav").toggle("drop");
                });
            });

            $("#shownav").unbind("click");
            $("#shownav").click(function (e) {
                var isiPad = navigator.userAgent.match(/iPad/i) != null;
                if (isiPad) {
                    cancelClick = false;
                }
                if (cancelClick) {
                    cancelClick = false;
                    return;
                }
                $("#shownav").toggle("drop");
                $("#sidebar").animate({
                    width: 160
                }, 1000, function () {
                    $("#sidebar").toggle("drop");
                });
                $("#sidebar").parent().animate({
                    width: 160
                }, 1000, function () {
                    // Animation complete.
                });
            });
            $(".logout").unbind("click");
            $(".logout").click(function () {
                window.parent.location.href = "index.htm";
            });

            $("#showheader").unbind("click");
            $("#showheader").click(function () {
                if ($("#header").css("display") == "none") {
                    $("#header").toggle("slow");
                    $(this).css("background-position", "-128px -53px");
                   
                    $("#productContainer").css("height", window.screen.availHeight - 455);
                    $("#categoryContainer").css("height", window.screen.availHeight - 455);

                    $("#invoiceProductContainer").css("height", window.screen.availHeight - 713);
                } else {
                    $("#header").toggle("slow");
                    $(this).css("background-position", "-96px -53px");
                    $("#productContainer").css("height", window.screen.availHeight - 400);
                    $("#categoryContainer").css("height", window.screen.availHeight - 400);

                    $("#invoiceProductContainer").css("height", window.screen.availHeight - 658);
                }
            });
        }
        $(function () {
            
        });

        
    </script>
</body>
</html>
