﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS
{
    public partial class Receipt : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string mobileno = Request.QueryString["mn"];
                
                string validationcode = Request.QueryString["vc"];
                if (Request.Url.AbsoluteUri.IndexOf("mn=+") > 0)
                {
                    mobileno = "+" + mobileno.Trim();
                }
                if (string.IsNullOrEmpty(mobileno) || string.IsNullOrEmpty(validationcode))
                {
                    errormessage.Text = "Invalid input!";
                    errorpanel.Visible = true;
                    receiptarea.Visible = false;
                    return;
                }

                Invoice invoice = new POSService.POSService().GetSMSInvoice(mobileno, validationcode);
                if (invoice == null)
                {
                    errormessage.Text = "No receipt Found!";
                    errorpanel.Visible = true;
                    receiptarea.Visible = false;
                    return;
                }
                errorpanel.Visible = false;
                receiptarea.Visible = true;
                invoiceno.Text = invoice.inv_No.ToString();
                createdate.Text = invoice.inv_CreateDate.Value.ToLongDateString();
                if (invoice.IsGST)
                {
                    totalcost.Text = Decimal.Round(Convert.ToDecimal(invoice.inv_TotalIncGST), 2).ToString();
                }
                else
                {
                    totalcost.Text = Decimal.Round(Convert.ToDecimal(invoice.inv_Total), 2).ToString();
                }
                gstamount.Text = Decimal.Round(Convert.ToDecimal(invoice.inv_GST), 2).ToString();
                gstPanel.Visible = invoice.IsGST;
                rptList.DataSource = invoice.Products;
                rptList.DataBind();
            }
        }
    }
}