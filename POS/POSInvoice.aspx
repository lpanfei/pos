﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="POSInvoice.aspx.cs" Inherits="POS.POSInvoice" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Distributed POS System - Client 2011</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
    <link href="themes/cupertino/jquery-ui.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pos2.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="Scripts/jquery-1.9.1.js"></script>
    <script src="Scripts/jquery-ui.js"></script>
    <script src="Scripts/jquery.ui.touch-punch.min.js"></script>
    <script src="window/js/modernizr.custom.js"></script>
    <link rel="stylesheet" href="css/idangerous.swiper.css">
    <script src="Scripts/idangerous.swiper-2.4.1.js"></script>
    <style>
        /* Demo Styles */body
        {
            margin: 0;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13px;
            line-height: 1.5;
        }
        .swiper-container
        {
            width: 640px;
            height: 500px;
            color: #fff;
            text-align: center;
        }
        .product-container, .payment-container
        {
            width: 640px;
            text-align: center;
        }
        .payment-container
        {
            border-right: 1px solid #25a0da;
            border-top: 1px solid #25a0da;
            border-bottom: 1px solid #25a0da;
            height: 545px;
        }
        
        .main-container
        {
            width: 950px;
            text-align: center;
            margin: 0 auto;
            overflow: hidden;
        }
        .black-slide
        {
            background: black;
        }
        .red-slide
        {
            background: #ca4040;
        }
        .blue-slide
        {
            background: rgb(41, 128, 185);
        }
        .orange-slide
        {
            background: #ff8604;
        }
        .green-slide
        {
            background: #49a430;
        }
        .pink-slide
        {
            background: #973e76;
        }
        .swiper-slide .title
        {
            font-style: italic;
            font-size: 12px;
            margin-top: 5px;
            margin-left: 5px;
            line-height: 15px;
            width: 150px;
            height: 25px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            text-align: left;
        }
        .swiper-slide .price
        {
            font-style: italic;
            font-size: 12px;
            margin-top: 65px;
            margin-right: 5px;
            line-height: 15px;
            width: 145px;
            height: 25px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            text-align: right;
        }
        .swiper-slide p
        {
            font-style: italic;
            font-size: 25px;
        }
        .pagination
        {
            margin: 0 auto;
        }
        .swiper-pagination-switch
        {
            display: inline-block;
            width: 10px;
            height: 10px;
            border-radius: 8px;
            background: #555;
            margin-right: 5px;
            opacity: 0.8;
            border: 1px solid #fff;
            cursor: pointer;
        }
        .swiper-active-switch
        {
            background: #49a430;
        }
        .swiper-dynamic-links
        {
            text-align: center;
        }
        .swiper-dynamic-links a
        {
            display: inline-block;
            padding: 5px;
            border-radius: 3px;
            border: 1px solid #ccc;
            margin: 5px;
            font-size: 12px;
            text-decoration: none;
            color: #333;
            background: #eee;
        }
        .pitem
        {
            width: 150px;
            height: 115px;
            cursor: pointer;
            border: 1px solid #F7F8FA;
        }
        .swiper-slide td
        {
            width: 160px;
            height: 120px;
        }
        .displaynone
        {
            display: none;
        }
        .header-bar
        {
            height: 35px;
            background: #25a0da;
        }
        
        .RadSearchBox_MetroTouch .rsbInner
        {
            border-color: #25a0da;
        }
        
        .header-bar .title
        {
            font-size: 18px;
            padding-left: 5px;
            line-height: 35px;
            height: 35px;
            width: 100px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            text-align: left;
            color: White;
            font-family: 'Segoe UI' , Arial, sans-serif;
            cursor: pointer;
        }
        .cat-container
        {
            width: 100px;
            z-index: 100;
            position: absolute;
        }
        .cat-container .list tr
        {
            background-color: #F7F8FA;
        }
        
        .cart-container
        {
            width: 300px;
        }
        
        .cartitem-container
        {
            height: 409px;
            border-bottom: 1px solid #e4e4e4;
            overflow-y: auto;
        }
        
        .cart-container .list .product_name
        {
            font-family: 'Segoe UI' , Arial, sans-serif;
            cursor: pointer;
            text-overflow: ellipsis;
            white-space: nowrap;
            text-align: left;
            width: 150px;
            overflow: hidden;
        }
        .number-row, .discount-row
        {
            display: none;
            background: #EFF3F4;
        }
        
        .number-row:hover, .discount-row:hover
        {
            background: #EFF3F4;
        }
        
        .total-pay
        {
            font-size: 25px;
            padding-left: 5px;
            line-height: 35px;
            height: 35px;
            margin: 0 auto;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
            text-align: center;
            font-family: 'Segoe UI' , Arial, sans-serif;
            cursor: pointer;
            margin-top: 10px;
            margin-bottom: 10px;
            font-weight: bold;
        }
        
        .payment-method
        {
            width: 500px;
            margin: 0 auto;
            padding-top: 10px;
            height: 400px;
            border: 1px solid #e4e4e4;
            background: #EFF3F4;
            font-family: 'Segoe UI' , Arial, sans-serif;
            font-size: 20px;
           
        }
        
        .payment-details
        {
            width: 500px;
            margin: 0 auto;
            padding-top: 10px;
            height: 402px;
            font-family: 'Segoe UI' , Arial, sans-serif;
            font-size: 20px;
             display:none;
        }
        .payment-paid
        {
            display:none;
        }
        .paid-status
        {
            width:301px;
            margin:0 auto;
            margin-top:10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true">
        <Scripts>
            <asp:ScriptReference Path="common.js" />
        </Scripts>
    </asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="main-container">
                <table style="width: 100%; overflow: hidden" class="main-table">
                    <tr>
                        <td>
                            <div class="product-container">
                                <div class="header-bar">
                                    <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <div class="title">
                                                    All Products
                                                </div>
                                                <div class="cat-container" style="display: none">
                                                    <table class="list">
                                                        <tbody>
                                                            <tr>
                                                                <td>
                                                                    All Products
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                            <td align="right">
                                                <telerik:RadSearchBox ID="SearchBox" runat="server" Width="150" Height="32" Skin="MetroTouch"
                                                    EmptyMessage="Product Name" Style="margin-top: -8px; margin-right: -4px;">
                                                </telerik:RadSearchBox>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="swiper-container">
                                    <div class="swiper-wrapper">
                                    </div>
                                </div>
                                <div class="pagination">
                                </div>
                            </div>
                        </td>
                        <td valign="top" style="border: 1px solid #25a0da; border-spacing: 0px; width: 300px;
                            overflow: hidden;">
                            <div class="cart-container">
                                <div class="header-bar">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 30px; border-spacing: 0px;" align="left" valign="middle">
                                                <div style="height: 30px; width: 30px; font-size: 30px; color: White; font-weight: bold;
                                                    line-height: 30px; text-align: center; cursor: pointer" class="cart-command">
                                                    x</div>
                                            </td>
                                            <td align="center">
                                                <div class="title" style="text-align: center; width: 100%; float: right">
                                                    Cart</div>
                                            </td>
                                            <td style="width: 30px">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="cartitem-container">
                                    <table class="list cartlist" style="margin-bottom: 0px; border-spacing: 0px; border: 0px solid #e4e4e4">
                                        <tbody>
                                            <tr class="number-row">
                                                <td colspan="3">
                                                    <table style="width: 290px; border-spacing: 0px;">
                                                        <tr>
                                                            <td style="width: 87px" align="center">
                                                                <input type="button" value="-" style="width: 80px; font-size: 30px; margin-left: 13px;"
                                                                    class="btn btn-block btn-info btn-minus" />
                                                            </td>
                                                            <td style="width: 90px" align="center">
                                                                <input type="text" value="" style="width: 80px; font-size: 20px; height: 35px;" id="currentquantity" />
                                                            </td>
                                                            <td style="width: 100px" align="center">
                                                                <input type="button" value="+" style="width: 80px; font-size: 30px" class="btn btn-block btn-info btn-add" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr class="discount-row">
                                                <td colspan="3">
                                                    <table>
                                                        <tr>
                                                            <td style="width: 95px">
                                                                Discount:
                                                            </td>
                                                            <td align="left" colspan="2">
                                                                <input type="text" style="width: 140px; height: 35px" id="currentdiscount" />
                                                                &nbsp; %
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="total-container">
                                    <div style="float: right; margin-bottom: 7px; margin-top: 7px; margin-right: 10px;">
                                        <input type="checkbox" id="gstcheckbox" style="width: 30px; height: 30px" />
                                        GST Amount: <span class="gstamount">0.00</span>
                                    </div>
                                    <input type="button" id="btntotal" value="Total" class="btn btn-block btn-info" style="height: 55px;
                                        margin-bottom: 0px;" />
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="payment-container">
                                <div class="total-pay">0.00</div>
                                <div class="payment-method">
                                    How would you like to pay?
                                    <div style="border: 1px solid #e4e4e4; width: 200px; height: 60px; margin: 0 auto;
                                        margin-top: 20px; font-weight: bold; cursor: pointer; background: rgb(204, 204, 204)">
                                        <table style="width: 100%; margin-top: 10px;" class="method-item cash-pay">
                                            <tr>
                                                <td style="width: 40px">
                                                    <img src="images/cash.png" />
                                                </td>
                                                <td align="left" style="margin-left: 10px">
                                                    Cash
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="border: 1px solid #e4e4e4; width: 200px; height: 60px; margin: 0 auto;
                                        margin-top: 20px; font-weight: bold; cursor: pointer; background: rgb(204, 204, 204)">
                                        <table style="width: 100%; margin-top: 10px;"  class="method-item credit-pay">
                                            <tr>
                                                <td style="width: 40px">
                                                    <img src="images/credit-card.png" />
                                                </td>
                                                <td align="left" style="margin-left: 10px">
                                                    Credit Card
                                                </td>
                                            </tr>
                                    </table>
                                </div>
                                    <div style="border: 1px solid #e4e4e4; width: 200px; height: 60px; margin: 0 auto;margin-top: 20px; font-weight: bold; cursor: pointer; background: rgb(204, 204, 204)">
                                        <table style="width: 100%; margin-top: 10px;" class="method-item net-pay">
                                            <tr>
                                                <td style="width: 40px">
                                                    <img src="images/net.png" />
                                                </td>
                                                <td align="left" style="margin-left: 10px">
                                                    Net
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div style="border: 1px solid #e4e4e4; width: 200px; height: 60px; margin: 0 auto;margin-top: 20px; font-weight: bold; cursor: pointer; background: rgb(204, 204, 204)">
                                        <table style="width: 100%; margin-top: 10px;" class="method-item none-pay">
                                            <tr>
                                                <td style="width: 40px">
                                                    <img src="images/none.png" />
                                                </td>
                                                <td align="left" style="margin-left: 10px">
                                                    None
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <div class="payment-details">
                                
                                    <table style="width:100%" class="payment-list">
                                    <tbody>
                                    </tbody>
                                    </table>
                                </div>
                                <div class="payment-action">
                                    <table style="width: 100%">
                                        <tr style="height: 25px">
                                            <td colspan="3">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right">
                                                <input type="button" value="Add Payment" style="width: 150px; height: 35px" class="btn"
                                                    disabled="disabled" id="addpayment" />
                                            </td>
                                            <td style="width: 50px">
                                            </td>
                                            <td align="left">
                                                <input type="button" value="Mark as Paid" id="markaspaid" style="width: 150px; height: 35px" class="btn btn-block btn-info" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                 <div class="payment-paid">
                                    <div class="paid-status"><img src="images/paid_part.png" /></div>
                                    <div style="margin:0 auto;width:300px;margin-top:10px;">
                                        <table>
                                        <tr>
                                        <td>
                                        <div style="font-size:15px;line-height:30px;width:130px;">TOTAL PAID</div>
                                        <div style="font-size:30px;line-height:30px;font-weight:bold;color:#359a00">20.00</div>
                                        </td>
                                        <td style="border-right:1px solid rgb(204, 204, 204);width:1px;"></td>
                                        <td><div style="font-size:15px;line-height:30px;width:150px;">OUTSTANDING</div>
                                        <div style="font-size:30px;line-height:30px;font-weight:bold;color:#9b5b05">20.00</div></td>
                                        </tr>
                                        </table>
                                    </div>
                                    <div style="border-top:1px solid rgb(204, 204, 204);width:100%;height:190px;margin-top:10px;">
                                        <div style="width:500px; margin:0 auto;margin-top:20px;">
                                            <input type="text" style="width:500px;height:45px;display:block;margin-top:8px;background:#EFF3F4;font-size:25px;color:rgb(204, 204, 204)" value="SMS" />
                                            <input type="text" style="width:500px;height:45px;display:block;margin-top:8px;background:#EFF3F4;font-size:25px;color:rgb(204, 204, 204)" value="Wallet ID" />
                                            <input type="text" style="width:500px;height:45px;display:block;margin-top:8px;background:#EFF3F4;border:0px solid black;font-size:25px;color:rgb(204, 204, 204)" value="Print Receipt" />
                                        </div>
                                        
                                    </div>
                                    <div style="width:200px;margin:0 auto;margin-top:10px;">
                                    <input type="button" class="btn btn-info btn-block" style="width:200px; height:50px" value="Start New Order"/>
                                    </div>
                                 </div>
                            </div>
                           
                        </td>
                    </tr>
                </table>
            </div>
            <div class="displaynone">
                <asp:Label ID="lblGSTSetting" runat="server" CssClass="gstvalue"></asp:Label>
                <asp:Repeater ID="rptList" runat="server">
                    <HeaderTemplate>
                        <table class="pro_list">
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <span class="product_id">
                                    <%# Eval("pro_Id") %></span>
                            </td>
                            <td align="center">
                                <span class="product_catname">
                                    <%# Eval("cat_Name")%></span>
                            </td>
                            <td align="center">
                                <span class="product_name">
                                    <%# Eval("pro_Name") %></span>
                            </td>
                            <td align="center">
                                <span class="product_price">
                                    <%# Eval("pro_Price")%></span>
                            </td>
                            <td align="center">
                                <span class="product_discount">
                                    <%# Eval("Discount")%></span>
                            </td>
                            <td align="center">
                                <span class="product_image">
                                    <%# HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + Eval("ImageUrl")) %></span>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script>

        var mySwiper = new Swiper('.swiper-container', {
            pagination: '.pagination',
            paginationClickable: true
        })

        function pageLoad() {
            buildCategory();

            $(".product-container .header-bar .title").unbind("click");
            $(".product-container .header-bar .title").click(function () {
                $(".cat-container").toggle("slide");
            });

            $(" .RadSearchBox_MetroTouch .rsbInner").css("border-color", "#25a0da");

            $(".pitem").unbind("click");
            $(".pitem").click(function () {
                //$(this).effect("highlight", {}, 500, function () { });
                var pname = $(this).find(".title").eq(0).text();
                var pprice = $(this).find(".price").eq(0).text();
                var pid = $.trim($(this).parent().attr("product-id"));
                var pdiscount = $.trim($(this).parent().attr("product-discount"));
                //alert(pid);
                var pexists = false;
                $(".cart-container .cartlist>tbody>tr").each(function () {
                    if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                        if ($(this).attr("product-id") == pid) {
                            pexists = true;
                            var quantity = parseInt($(this).find(".product_quantity").text());
                            quantity = quantity + 1;
                            $(this).find(".product_quantity").text(quantity);
                            $(this).find(".product_price").text(parseFloat(pprice) * quantity);
                            //updateCartTotal();
                            scrollIntoView($(this), ".cartitem-container");
                        }
                    }
                });
                if (!pexists) {
                    var tr = $("<tr></tr>");

                    var ntd = $("<td style='width:150'><div class='product_name'>" + pname + "</div></td>");
                    var qtd = $("<td style='width:50px' class='product_quantity'></td>");
                    var ptd = $("<td style='width:70px' class='product_price'></td>");
                    ptd.text(pprice);
                    qtd.text("1");
                    tr.append(ntd);
                    tr.append(qtd);
                    tr.append(ptd);

                    $(".cart-container .cartlist>tbody").append(tr);
                    tr.attr("product-id", pid);
                    tr.attr("product-discount", pdiscount);
                    tr.attr("product-price", pprice);
                    //scrollToBottom(tr, ".cartitem-container");
                    $(".cartitem-container").scrollTop($('.cartitem-container')[0].scrollHeight);
                    tr.effect("highlight", {}, 500, function () { });
                    tr.click(function () {
                        if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                            $(".number-row").toggle();
                            $(".discount-row").toggle();
                        } else {
                            $(".number-row").css("display", "table-row");
                            $(".discount-row").css("display", "table-row");
                        }

                        $(".number-row").attr("for-id", "0");
                        $(".discount-row").attr("for-id", "0");

                        if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                            $(".number-row").attr("for-id", $(this).attr("product-id"));
                            $(".discount-row").attr("for-id", $(this).attr("product-id"));
                            $(".discount-row").insertAfter($(this));
                            $(".number-row").insertAfter($(this));
                            $("#currentquantity").val($(this).find(".product_quantity").text());
                            $("#currentdiscount").val($(this).attr("product-discount"));
                            scrollIntoView($(".discount-row"), ".cartitem-container");
                        }

                        $(".cart-container .cartlist>tbody>tr").each(function () {
                            if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                                if ($.trim($(this).find(".product_quantity").text()) == "0" && $(".number-row").attr("for-id") != $(this).attr("product-id")) {
                                    $(this).remove();
                                }
                            }
                        });
                    });
                }
                updateCartTotal();
            });

            $(".btn-minus").unbind("click");
            $(".btn-minus").click(function () {
                var quantity = parseInt($("#currentquantity").val());
                if (quantity > 0) {
                    quantity = quantity - 1;
                }
                $("#currentquantity").val(quantity);
                $(".cart-container .cartlist>tbody>tr[product-id='" + $(".number-row").attr("for-id") + "']").find(".product_quantity").text(quantity);
                updateCartTotal();
            });


            $(".btn-add").unbind("click");
            $(".btn-add").click(function () {
                var quantity = parseInt($("#currentquantity").val());
                quantity = quantity + 1;
                $("#currentquantity").val(quantity);
                $(".cart-container .cartlist>tbody>tr[product-id='" + $(".number-row").attr("for-id") + "']").find(".product_quantity").text(quantity);
                updateCartTotal();
            });

            $("#currentdiscount").unbind("change");
            $("#currentdiscount").change(function () {
                $(".cart-container .cartlist>tbody>tr[product-id='" + $(".discount-row").attr("for-id") + "']").attr("product-discount", $("#currentdiscount").val());
                updateCartTotal();
            });

            $("#currentquantity").unbind("change");
            $("#currentquantity").change(function () {
                $(".cart-container .cartlist>tbody>tr[product-id='" + $(".number-row").attr("for-id") + "']").find(".product_quantity").text($("#currentquantity").val());
                updateCartTotal();
            });

            $("#gstcheckbox").change(function () {
                updateCartTotal();
            });

            $("#btntotal").unbind("click");
            $("#btntotal").click(function () {
                $(".main-table").animate({
                    marginLeft: "-=644px"
                }, 500, function () {
                    $(".cart-command").text("<");
                });

                $(".payment-container").css("margin-left", "-2px");

                $(".cart-container .cartlist>tbody>tr").each(function () {
                    if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                        $(this).unbind("click");
                    }
                });

                $(".number-row").css("display", "none");
                $(".discount-row").css("display", "none");
                $(".number-row").attr("product-id", "none");
                $(".discount-row").attr("product-id", "none");

                $("#gstcheckbox").prop("disabled", "disabled");

                $(this).css("display", "none");

                $(".cartitem-container").height($(".cartitem-container").height() + $(this).height());
                $(".cartitem-container").scrollTop(0);
            });

            $(".cart-command").unbind("click");
            $(".cart-command").click(function () {
                if ($(".cart-command").text() == "x") {
                    $(".cart-container .cartlist>tbody>tr").each(function () {
                        if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                            $(this).remove();
                        }
                    });
                    updateCartTotal();
                } else {
                    $(".main-table").animate({
                        marginLeft: "+=644px"
                    }, 500, function () {
                        $(".cart-command").text("x");
                    });

                    $(".payment-container").css("margin-left", "2px");

                    $("#btntotal").css("display", "block");
                    $(".cartitem-container").height($(".cartitem-container").height() - 36);
                    $(".cartitem-container").scrollTop(0);
                    $("#gstcheckbox").prop("disabled", "");
                    $(".payment-details .payment-list>tbody>tr").each(function () {
                        $(this).remove();
                    });
                    $(".payment-method").css("display", "block");
                    $(".payment-details").css("display", "none");


                    $(".cart-container .cartlist>tbody>tr").each(function () {
                        if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                            $(this).unbind("click");
                            $(this).click(function () {
                                if ($(".number-row").attr("for-id") == $(this).attr("product-id")) {
                                    $(".number-row").toggle();
                                    $(".discount-row").toggle();
                                } else {
                                    $(".number-row").css("display", "table-row");
                                    $(".discount-row").css("display", "table-row");
                                }

                                $(".number-row").attr("for-id", "0");
                                $(".discount-row").attr("for-id", "0");

                                if ($(".discount-row").css("display") != "none" && $(".number-row").css("display") != "none") {
                                    $(".number-row").attr("for-id", $(this).attr("product-id"));
                                    $(".discount-row").attr("for-id", $(this).attr("product-id"));
                                    $(".discount-row").insertAfter($(this));
                                    $(".number-row").insertAfter($(this));
                                    $("#currentquantity").val($(this).find(".product_quantity").text());
                                    $("#currentdiscount").val($(this).attr("product-discount"));
                                    scrollIntoView($(".discount-row"), ".cartitem-container");
                                }

                                $(".cart-container .cartlist>tbody>tr").each(function () {
                                    if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                                        if ($.trim($(this).find(".product_quantity").text()) == "0" && $(".number-row").attr("for-id") != $(this).attr("product-id")) {
                                            $(this).remove();
                                        }
                                    }
                                });
                            });
                        }
                    });
                }
            });

            $(".method-item").unbind("click");
            $(".method-item").click(function () {
                var image = "";
                var text = "";
                if ($(this).hasClass("cash-pay")) {
                    image = "images/cash.png";
                    text = "Cash";
                } else if ($(this).hasClass("credit-pay")) {
                    image = "images/credit-card.png";
                    text = "Credit";
                } else if ($(this).hasClass("net-pay")) {
                    image = "images/net.png";
                    text = "Net"
                } else if ($(this).hasClass("none-pay")) {
                    $(".payment-method").css("display", "none");
                    $(".payment-details").css("display", "block");
                }

                if (image != "") {
                    var tr = $('<tr style="height:50px"></tr>');
                    var tdremove = $('<td style="width:50px"><img src="images/remove.png" style="cursor:pointer" /></td>');
                    var input = $('<input class="payamount" type="text" style="width:430px;height:40px;direction:RTL" value="0.00"/>');
                    var span = $('<span  style="position:absolute;left:2px;top:4px;vertical-align:middle;font-size:20px;line-height:30px;"><img src="' + image + '" style="vertical-align:middle" /> <span style="opacity:0.8;color:Gray">' + text + '</span>');
                    var div = $('<div style="position:relative;width:430px;margin-left:10px;"></div>');
                    var tdinput = $('<td></td>');
                    div.append(input);
                    div.append(span);
                    tdinput.append(div);
                    tr.append(tdremove);
                    tr.append(tdinput);
                    $(".payment-details .payment-list>tbody").append(tr);
                    $(".payment-method").css("display", "none");
                    $(".payment-details").css("display", "block");
                    tdremove.click(function () {
                        $(this).closest("tr").remove();
                        if ($(".payment-details .payment-list>tbody").find("tr").length == 0) {
                            $(".payment-method").css("display", "block");
                            $(".payment-details").css("display", "none");
                            $("#addpayment").prop("disabled", "disabled");
                            $("#addpayment").removeClass("btn-info");
                            $("#addpayment").removeClass("btn-block");
                        } else {
                            updatePayment()
                        }
                    });
                    input.change(function () {
                        if ($(this).val() == "") {
                            $(this).val("0.00");
                        }
                        $(this).val(parseFloat($(this).val()).toFixed(2));
                        updatePayment();
                    });
                }
                updatePayment();
            });

            $("#addpayment").unbind("click");
            $("#addpayment").click(function () {
                $(".payment-method").css("display", "block");
                $(".payment-details").css("display", "none");
            });

            $("#markaspaid").unbind("click");
            $("#markaspaid").click(function () {
                $(".total-pay").css("display", "none");
                $(".payment-method").css("display", "none");
                $(".payment-details").css("display", "none");
                $(".payment-action").css("display", "none");
                $(".payment-paid").css("display", "block");
                $(".cart-command").css("display", "none");
            });
        }

        function updatePayment() {
            var totalpaid = 0;
            $(".payment-details .payment-list>tbody>tr").each(function () {
                totalpaid += parseFloat($(this).find(".payamount").eq(0).val());
            });

            //alert(totalpaid);
            //alert(parseFloat($(".total-pay").text()));
            if (totalpaid < parseFloat($(".total-pay").text())) {
                $("#addpayment").prop("disabled", "");
                $("#addpayment").addClass("btn-info");
                $("#addpayment").addClass("btn-block");
            } else {
           
                $("#addpayment").prop("disabled", "disabled");
                $("#addpayment").removeClass("btn-info");
                $("#addpayment").removeClass("btn-block");
            }
        }

        function scrollIntoView(element, container) {
            var containerTop = $(container).scrollTop();

            var containerBottom = containerTop + $(container).height();

            var elemTop = element.offset().top;
            var elemBottom = elemTop + element.height();
            if (elemTop < containerTop) {
                $(container).scrollTop(elemTop);
            } else if (elemBottom > containerBottom) {
                $(container).scrollTop(elemBottom - $(container).height());
            }
            if (!element.hasClass("discount-row") && !element.hasClass("number-row")) {
                element.effect("highlight", {}, 500, function () { });
            }
        }

        function updateCartTotal() {
            var quantity = 0;
            var total = 0.0;
            $(".cart-container .cartlist>tbody>tr").each(function () {
                if (!$(this).hasClass("number-row") && !$(this).hasClass("discount-row")) {
                    quantity += parseInt($(this).find(".product_quantity").text());
                    var subtotal = parseFloat(($(this).attr("product-price")) * parseFloat($(this).attr("product-discount")) * parseInt($(this).find(".product_quantity").text()) / 100).toFixed(2);
                    $(this).find(".product_price").text(subtotal);
                    total += parseFloat(subtotal);
                }
            });

            var gst = total * parseFloat($(".gstvalue").text()) / 100;
            $(".gstamount").text(parseFloat(gst).toFixed(2));

            if ($("#gstcheckbox").prop("checked")) {
                total += gst;
            }

            $(".cart-container .title").text("Cart (" + quantity + ")");
            $("#btntotal").val("Total (" + parseFloat(total).toFixed(2) + ")");
            $(".total-pay").text(parseFloat(total).toFixed(2));
        }

        function buildCategory() {
            $(".pro_list").find("tr").each(function () {
                var catname = $(this).find(".product_catname").eq(0).text();
                var catexists = false;
                $(".cat-container .list tbody>tr").find("td").each(function () {
                    if ($(this).text() == catname) {
                        catexists = true;
                    }
                });

                if (!catexists) {
                    var tr = $("<tr><td>" + catname + "</td></tr>");
                    $(".cat-container .list tbody").append(tr);

                    tr.click(function () {
                        $(".product-container .header-bar .title").text($(this).find("td").eq(0).text());
                        $(".cat-container").toggle("slide");
                    });
                }
            });
        }

        function buildGrid() {

            var total = $(".pro_list").find("tr").length;
            for (var i = 0; i < Math.ceil(total / 16); i++) {
                mySwiper.appendSlide('<table table-index="' + i.toString() + '"></table>', 'swiper-slide')
            }

            for (var i = 0; i < total; i++) {
                var container = $(".swiper-container").find("table[table-index='" + Math.floor(i / 16) + "']");
                if (container.length > 0) {
                    var row = container.find("tr[row-index='" + Math.floor(i / 4) + "']");
                    if (row.length > 0) {
                        var column = row.find("td[column-index='" + Math.floor(i % 4) + "']");
                        if (column.length > 0) {
                            column.eq(0).attr("product-id", $(".pro_list").find("tr").eq(i).find(".product_id").eq(0).text());
                            column.eq(0).attr("product-discount", $(".pro_list").find("tr").eq(i).find(".product_discount").eq(0).text());
                            column.eq(0).append($('<div class="pitem ' + randomColor() + '-slide"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="price">' + parseFloat($.trim($(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text())).toFixed(2) + '</div></div>'));
                        } else {
                            //row.eq(0).append($('<td column-index="' + Math.floor(i % 4) + '" align="center"><div class="pitem ' + randomColor() + '-slide" style="background-image:url(' + $(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text() + ');"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="price">' + $(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text() + '</div></div></td>'));
                            var newcol = $('<td column-index="' + Math.floor(i % 4) + '" align="center"><div class="pitem ' + randomColor() + '-slide"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="price">' + parseFloat($.trim($(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text())).toFixed(2) + '</div></div></td>');
                            newcol.eq(0).attr("product-id", $(".pro_list").find("tr").eq(i).find(".product_id").eq(0).text());
                            newcol.eq(0).attr("product-discount", $(".pro_list").find("tr").eq(i).find(".product_discount").eq(0).text());
                            row.eq(0).append(newcol);
                        }
                    } else {
                        //container.eq(0).append($("<tr row-index='" + Math.floor(i / 4) + "'><td column-index='" + Math.floor(i % 4) + "' align='center'><div class='pitem " + randomColor() + "-slide' style='background-image:url(" + $(".pro_list").find("tr").eq(i).find(".product_image").eq(0).text() + ");'><div class='title'>" + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + "</div><div class='price'>" + $(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text() + "</div></div></td></tr>"));
                        var newrow = $("<tr row-index='" + Math.floor(i / 4) + "'></tr>");
                        var newcol = $('<td column-index="' + Math.floor(i % 4) + '" align="center"><div class="pitem ' + randomColor() + '-slide"><div class="title">' + $(".pro_list").find("tr").eq(i).find(".product_name").eq(0).text() + '</div><div class="price">' + parseFloat($.trim($(".pro_list").find("tr").eq(i).find(".product_price").eq(0).text())).toFixed(2) + '</div></div></td>');
                        newcol.eq(0).attr("product-id", $(".pro_list").find("tr").eq(i).find(".product_id").eq(0).text());
                        newcol.eq(0).attr("product-discount", $(".pro_list").find("tr").eq(i).find(".product_discount").eq(0).text());
                        newrow.append(newcol);
                        container.eq(0).append(newrow);
                    }
                }

            }
        }

        /* Dynamic Swiper Links*/
        function randomColor() {
            var colors = ('blue red green orange pink').split(' ');
            return colors[Math.floor(Math.random() * colors.length)]
        }
    </script>
    </form>
</body>
</html>
