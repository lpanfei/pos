﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Receipt.aspx.cs" Inherits="POS.Receipt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Distributed POS System - Client 2011</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <style type="text/css">
    table.list { border: 1px solid #e4e4e4;  border-collapse: collapse; width: 100%; margin-bottom: 4px;   -webkit-border-radius: 6px;
            -moz-border-radius: 6px;
            border-radius: 6px;}
table.list tr{border-bottom:1px solid #e4e4e4;cursor:pointer}
table.list tr:hover{background-color:#EFF3F4}
table.list th {  background-color:#F7F8FA; padding: 4px; white-space:nowrap;height:35px; }
table.list td { vertical-align: middle; padding-right:10px; height:50px; }
    </style>
</head>
<body style="width:100%;height:100%;margin:0px;padding:0px;">
    <form id="form1" runat="server" style="width:100%;height:100%">
    <asp:Panel runat="server" ID="receiptarea" style="width:100%;height:100%">
        <div style="width:100%;height:100px;">
            <table>
                <tr>
                    <td style="font-size: 20px; font-weight: bold">
                        Invoice No:
                    </td>
                    <td valign="middle" style="font-size: 20px;">
                        <asp:Label ID="invoiceno" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="font-size: 20px; font-weight: bold">
                        Date:
                    </td>
                    <td  valign="middle" style="font-size: 20px;">
                        <asp:Label ID="createdate" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <asp:Repeater ID="rptList" runat="server">
        <HeaderTemplate>
            <table class="list">
            <thead>
                <tr>
                    <th>Product</th>
                    <th>Quantity</th>
                    <th>Sub Total</th>
                </tr>
            </thead>
            <tbody>
        </HeaderTemplate>
        <ItemTemplate>
                <tr>
                    <td align="center"><asp:Label runat="server" ID="lblName" Text='<%# Eval("ip_Description")%>'></asp:Label></td>
                    <td align="center"><asp:Label runat="server" ID="lblContact" Text='<%# Eval("ip_Quantity")%>'></asp:Label></td>
                    <td align="center"><asp:Label runat="server" ID="lblAddress" Text='<%#  Decimal.Round(Convert.ToDecimal(Eval("ip_SubTotal")),2)%>'></asp:Label></td>
                </tr>
        </ItemTemplate>
        <FooterTemplate>
            </tbody>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    
    <div style="float:right;font-weight:bold;margin-right:5px;">
    <div>
    <asp:Panel ID="gstPanel" runat="server" style="font-weight:bold">
    <span  style='font-weight:bold'>GST Amount:</span>&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="gstamount" runat="server"></asp:Label>
    </asp:Panel>
    </div>
     <span style="font-weight:bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Total:</span>&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="totalcost" runat="server"></asp:Label>
    </div>
    </asp:Panel >
    <asp:Panel runat="server" ID="errorpanel">
    <asp:Label runat="server" ID="errormessage" ForeColor="Red"></asp:Label>
    </asp:Panel>
    </form>
</body>
</html>
