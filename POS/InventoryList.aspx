﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="InventoryList.aspx.cs" Inherits="POS.InventoryList" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var previous = null;
        function pageLoad(sender, args) {

            $(".list tr").unbind("click");
            $(".list tr").click(function () {

                var current = $(this).find(".buttons div").eq(0);
                if ($(previous).is(current))
                    return;
                current.toggle("slow");
                if (previous != null) {
                    $(previous).toggle("slow", function () { });
                }
                previous = current;
            }
            );
        }

        function stock(sender,obj) {
            if (sender != null) {
                $("#quantity").val(sender);
                $("#stoBtn").trigger("click");
            }
        }
    </script>
    <style type="text/css">
    .displaynone
    {
        display:none;
    }
   
        input[type=button]
        {
            cursor: pointer;
        }
        
        input[type=file]
        {
            cursor: pointer;
        }
         .tr-header
        {
            
            font-size: 13px;
            font-weight: bold;
        }

        .padding5
        {
            padding-top: 5px;
        }
        
       
        .tr-header td
        {
            padding-top: 25px;
            padding-bottom: 10px;
        }
        #ftable
        {
            border-collapse: collapse;
            margin: 0 auto;
        }
        input[type=text], .form-table input[type=password], .form-table textarea
        {
            height: 30px;
            width: 280px;
            padding-left: 10px;
        }
        
        .pageform
        {
            margin:0 auto;
        }
        
        .btn-add
        {
            cursor:pointer;
        }
        

    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="mainheader">
                <div class="contextual">
                
                </div>
                <div class="subheader">
                    <h3 class="icon-products pagetitle" id="pagetitle">
                        <asp:Literal ID="ltlPageTitle" Text="Products" runat="server"></asp:Literal>
                    </h3>
                </div>
            </div>
            <asp:Panel CssClass="pagelist" runat="server" ID="pagelist" ClientIDMode="Static">
                <div>
                    <div style="float: left">
                        <asp:DropDownList ID="dropCategory" runat="server" Height="30" DataTextField="cat_Name"
                            DataValueField="cat_Id">
                        </asp:DropDownList>
                    </div>
                    <div style="float: left; margin-left: 2px;">
                        <asp:DropDownList ID="dropField" runat="server" Height="30">
                            <asp:ListItem Text="Name of Product" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Serial No" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div style="float: left; margin-left: 2px; margin-top: -1px;">
                        <asp:TextBox ID="txtKey" runat="server" CssClass="searchbox" Height="28"></asp:TextBox></div>
                    <div style="float: left; margin-left: 5px; margin-right: 15px;">
                        <asp:DropDownList ID="dropQuantity" runat="server" Height="30">
                            <asp:ListItem Text="-- All Quantity --" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Greater than 1000" Value="1000"></asp:ListItem>
                            <asp:ListItem Text="Greater than 500" Value="500"></asp:ListItem>
                            <asp:ListItem Text="Greater than 200" Value="200"></asp:ListItem>
                            <asp:ListItem Text="Less than 100" Value="100"></asp:ListItem>
                            <asp:ListItem Text="Less than 50" Value="50"></asp:ListItem>
                            <asp:ListItem Text="Less than 10" Value="10"></asp:ListItem>
                        </asp:DropDownList>
                       
                    </div>
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search"
                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info" Width="75"
                        Height="30" Style="margin-left: 15px;" />
                </div>
                <br />
                <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand">
                    <HeaderTemplate>
                        <table class="list">
                            <thead>
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Name of Product
                                    </th>
                                    <th>
                                        Category
                                    </th>
                                    <th>
                                        Supplier
                                    </th>
                                    <th>
                                        Serial No
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                        Quantity
                                    </th>
                                    <th>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <%# Container.ItemIndex+1 %>
                            </td>
                            <td align="center">
                                <%# Eval("Product.pro_Name") %>
                            </td>
                            <td align="center">
                                <%# Eval("Product.cat_Name")%>
                            </td>
                             <td align="center">
                                <%# Eval("Product.SupplierName")%>
                            </td>
                            <td align="center">
                                <%# Eval("Product.pro_No")%>
                            </td>
                            <td align="center">
                                <%# Convert.ToDecimal(Eval("Product.pro_Price")).ToString("0.00")%>
                            </td>
                            <td align="center">
                                <%# Eval("Quantity") %>
                            </td>
                            <td class="buttons">
                                <div style="display: none;">
                                    <asp:LinkButton ID="lblStock" runat="server" Text="Restock" CommandName="ItemStock" CommandArgument='<%# Eval("ID") %>'
                                        class="icon icon-restock" rel="nofollow"></asp:LinkButton>
                               
                            </td>
                           <td style="display:none"><asp:Label runat="server" ID="lblProductID" Text='<%# Eval("Product.pro_Id")%>'></asp:Label></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
                <webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged"
                    ClientIDMode="Static" PageSize="10">
                </webdiyer:AspNetPager>
            </asp:Panel>
           
            <eid:RadWindowManager runat="server" ID="WindowManager" Skin="Metro">
            </eid:RadWindowManager>
            <input type="hidden" id="invid" runat="server" clientidmode="Static" value="0" />
            <input type="hidden" id="pid" runat="server" clientidmode="Static" value="0" />
            <input type="hidden" id="quantity" runat="server" clientidmode="Static" value="0" />
            <asp:Button ID="stoBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
                OnClick="Stock" ClientIDMode="Static"></asp:Button>
            <div style="clear: both;">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

