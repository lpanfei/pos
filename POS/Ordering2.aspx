﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Ordering2.aspx.cs" Inherits="POS.Ordering2" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#">
<head id="Head1" runat="server">
    <title>Distributed POS System - Client 2011</title>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.2)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.2)">
    <link href="themes/cupertino/jquery-ui.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="pos.css" rel="stylesheet" type="text/css" media="screen" />
    <script src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Scripts/blocksit.js"></script>
    <script src="Scripts/jquery.kinetic.min.js"></script>
    <script src="Scripts/jquery-ui.js"></script>
    <script src="Scripts/jquery.ui.touch-punch.min.js"></script>
    <script src="window/js/modernizr.custom.js"></script>
    <script src="Scripts/jquery.smoothZoom.min.js"></script>
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/idangerous.swiper.css">
    <link rel="stylesheet" href="css/ordering.css">
    <script src="Scripts/idangerous.swiper-2.4.1.js"></script>
    <style>
        #fixedheader
        {
            font-family: Helvetica, Arial, sans-serif;
        }
        .hname, hprice
        {
            display: none;
        }
        .displaynone
        {
            display: none;
        }
        
        .fb_edge_widget_with_comment span.fb_edge_comment_widget iframe.fb_ltr
        {
            display: none !important;
        }
        .rltbDescriptionBox
        {
            display: none;
        }
        #waterfall-loading
        {
            display: none;
        }
        .ui-effects-transfer
        {
            border: 2px dotted gray;
        }
        .drag-cart-item
        {
            z-index: 3000;
        }
        .oproductid
        {
            display: none;
        }
        
        .checkstatus
        {
            display: none;
        }
        div::-webkit-scrollbar, body::-webkit-scrollbar
        {
            width: 0px;
        }
        #listcontainer
        {
            position: relative;
            width: 1024px;
            margin: 0 auto 25px;
            padding-bottom: 10px;
        }
        .grid
        {
            width: 188px;
            min-height: 100px;
            padding: 15px;
            background: #fff;
            margin: 8px;
            font-size: 12px;
            float: left;
            box-shadow: 0 1px 3px rgba(34,25,25,0.4);
            -moz-box-shadow: 0 1px 3px rgba(34,25,25,0.4);
            -webkit-box-shadow: 0 1px 3px rgba(34,25,25,0.4);
            -webkit-transition: top 1s ease, left 1s ease;
            -moz-transition: top 1s ease, left 1s ease;
            -o-transition: top 1s ease, left 1s ease;
            -ms-transition: top 1s ease, left 1s ease;
        }
        
        .grid strong
        {
            border-bottom: 1px solid #ccc;
            margin: 10px 0;
            display: block;
            padding: 0 0 5px;
            font-size: 17px;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }
        .grid .list_pdiscount
        {
            color: red;
        }
        .grid .meta
        {
            text-align: right;
            font-style: italic;
        }
        .grid .imgholder
        {
            position: relative;
        }
        .grid .imgholder img
        {
            width: 100%;
            background: #ccc;
            display: block;
        }
        body
        {
            font-family: 'Times New Roman';
        }
        .fooditem
        {
            box-shadow: 0 1px 3px rgba(34,25,25,0.4);
            -moz-box-shadow: 0 1px 3px rgba(34,25,25,0.4);
            -webkit-box-shadow: 0 1px 3px rgba(34,25,25,0.4);
            -webkit-transition: top 1s ease, left 1s ease;
            -moz-transition: top 1s ease, left 1s ease;
            -o-transition: top 1s ease, left 1s ease;
            -ms-transition: top 1s ease, left 1s ease;
            background: #fff;
            margin: 8px;
            font-size: 12px;
            height: 635px;
        }
        
        .existingrow
        {
            color: Red;
        }
    </style>
</head>
<body>
    <script>
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=226988780808773";
            fjs.parentNode.insertBefore(js, fjs);
        } (document, 'script', 'facebook-jssdk'));
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true"
        EnablePageMethods="true">
        <Scripts>
            <asp:ScriptReference Path="common.js" />
        </Scripts>
    </asp:ScriptManager>
    <div style="width: 1024px; height: 768px; margin: 0 auto; overflow: hidden">
        <telerik:RadSkinManager ID="QsfSkinManager" runat="server" ShowChooser="false" Skin="Metro" />
        <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
            <script type="text/javascript">
           
            </script>
        </telerik:RadCodeBlock>
        <asp:Button ID="submitBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="Order" ClientIDMode="Static" UseSubmitBehavior="false"></asp:Button>
        <asp:Button ID="updateBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="UpdateOrder" ClientIDMode="Static" UseSubmitBehavior="false"></asp:Button>
        <asp:Button ID="updateBoxBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="UpdateBox" ClientIDMode="Static" UseSubmitBehavior="false"></asp:Button>
        <asp:Button ID="receiveBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="ReceiveInvoice" ClientIDMode="Static" UseSubmitBehavior="false"></asp:Button>
        <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
            <ContentTemplate>
                <table style="width: 1024px; height: 76px; table-layout: fixed; top: 0px; z-index: 1000"
                    id="fixedheader">
                    <tr style="height: 36px;">
                        <td style="overflow-x: auto;">
                            <div id="header2" style="width: 100%">
                                <div class="swiper-container cartitem-container" style="background: #25a0da; color: White;
                                    font-size: 15px; height: 36px">
                                    <div class="swiper-wrapper" style="height: 36px">
                                        <asp:Repeater ID="Repeater3" runat="server">
                                            <ItemTemplate>
                                                <div class="swiper-slide" style="height: 36px; cursor: pointer">
                                                    <div class="cat-item" style="height: 36px">
                                                        <table style="width: 100%; border-spacing: 0px; height: 36px;">
                                                            <tr>
                                                                <td valign="middle" align="center">
                                                                    <div style="height: 20px; border-left: 1px solid white;" class="menuitem">
                                                                        <%#Eval("cat_Name")%>
                                                                        <input type="hidden" class="cat_id" value='<%#Eval("cat_Id")%>' />
                                                                     </div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </div>
                                </div>
                                <asp:Button ID="refreshAllBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
                                    OnClick="RefreshAll" ClientIDMode="Static"></asp:Button>
                                    <asp:Button ID="categorySelectBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
                                    OnClick="CategorySelected" ClientIDMode="Static"></asp:Button>
                                    <input type="hidden" id="selectedcategoryid" runat="server" clientidmode="Static" value="-1" />
                            </div>
                        </td>
                        <td style="width: 200px; background: #25a0da; border-left: 1px solid white; color: White"
                            valign="middle">
                            <table style="width: 100%; height: 100%; table-layout: fixed; font-weight: bold;
                                cursor: pointer" id="cartsummary" class="ui-droppable">
                                <tr>
                                    <td style="width: 40px" align="right">
                                        <img src="images/icon_basket.png" width="30px" height="30px" style="cursor: pointer"
                                            alt="Cart" />
                                    </td>
                                    <td align="left" style="padding-left: 10px">
                                        <asp:Label ID="ltlItems" runat="server" CssClass="totalitems"></asp:Label>
                                    </td>
                                    <td style="width: 40px; display: none">
                                        <a href="javascript:void(0)" class="hpcExpand" id="sc_opener" title="Expand" style="display: block;
                                            background: transparent url(images/hpc12.png) no-repeat; overflow: hidden; background-position: -128px -53px;
                                            width: 32px; height: 32px"></a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr style="height: 40px; background-color: #3894E7; color: White; font-weight: bold;
                        vertical-align: middle;">
                        <td colspan="2">
                            <table style="width: 100%">
                                <tr>
                                    <td>
                                        <div style="margin-left: 20px; width: 700px;">
                                            <a href="javascript:void" style="color: White" id="refreshAllLink">ALL</a>
                                            <asp:Literal ID="breadcrumb" runat="server"></asp:Literal>
                                        </div>
                                    </td>
                                    <td valign="middle" style="height: 40px; width: 100%; padding-right: 10px;" align="right">
                                        <%--<a href="#" id="allfolds" style="color: White">ALL FOODS</a>--%>
                                        <a class="listview" style="color: White" href="javascript:void">List View</a> &nbsp;|
                                        &nbsp;<a class="galleryview" style="color: White" href="javascript:void">Gallery View</a>
                                        &nbsp;|&nbsp;<a class="menuview" style="color: White" href="javascript:void">Menu View</a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <div id="productcontainer">
                    <div id="waterfall" style="width: 1024px; height: 680px; overflow-y: auto; overflow-x: hidden;
                        margin: 0 auto; margin-top: 10px;">
                        <div id="listcontainer">
                            <asp:Repeater ID="Repeater2" runat="server">
                                <ItemTemplate>
                                    <div class="grid">
                                        <div class="imgholder">
                                            <div class="checkstatus" style="position: absolute; top: 0px; right: 3px; cursor: pointer;">
                                                <img src="images/check.png" style="background: transparent; width: 32px; height: 32px;">
                                            </div>
                                            <img class="list_pimage" style="cursor: pointer" src="<%# HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + Eval("ImageUrl")) %>" />
                                        </div>
                                        <strong>
                                            <%#Eval("pro_Name") %></strong> <span>$<%#Decimal.Round(Convert.ToDecimal(Eval("pro_price")),2)%>
                                                &nbsp;&nbsp;&nbsp;<span class="list_pdiscount">(<span class="list_discountvalue"><%#Eval("Discount") %></span>%
                                                    Discount)</span></span>
                                        <div class="meta">
                                            <img class="list_addicon" src="images/plus-outline.png" style="cursor: pointer" /></div>
                                        <div class="list_pid" style="display: none">
                                            <%#Eval("pro_Id")%></div>
                                        <span class="list_pprice" style="display: none">
                                            <%#Eval("pro_price")%></span> <span class="list_pname" style="display: none">
                                                <%#Eval("pro_Name") %></span>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                    <div class="slideshow" style="width: 928px;">
                        <a class="arrow-left" href="#"></a><a class="arrow-right" href="#"></a>
                        <div class="swiper-container thumbs-cotnainer pitem-container">
                            <div class="swiper-wrapper">
                                <asp:Repeater ID="Repeater1" runat="server">
                                    <ItemTemplate>
                                        <div class="swiper-slide">
                                            <div class="fooditem">
                                                <div class="foodcheckicon checkstatus" style="position: absolute; top: 28px; margin-left: 264px;
                                                    cursor: pointer; display: none; width: 32px; height: 32px;">
                                                    <img src="images/check.png" style="width: 32px; height: 32px">
                                                </div>
                                                <div style="height: 50px;">
                                                    <div class="app-title" style="padding: 10px; font-size: 17px;">
                                                        <%#Eval("pro_Name") %></div>
                                                    <div class="app-discount" style="color: Red">
                                                        (<span class="discountvalue"><%#Eval("Discount") %></span>% Discount)
                                                    </div>
                                                </div>
                                                <div>
                                                    <table style="width: 100%; border-spacing: 0px;">
                                                        <tr>
                                                            <td valign="top">
                                                                <div style="overflow: hidden; max-width: 295px; max-height: 345px; margin: 0 auto">
                                                                    <asp:Image ID="Image2" ImageUrl='<%# HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + Eval("ImageUrl")) %>'
                                                                        runat="server" CssClass="foodimage" Style="width: 100%" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="pdescription" style="width: 295px; background: white; color: Black; height: 235px;
                                                    box-shadow: 0px 0px 5px #fff; margin-bottom: 45px; border-radius: 10px; margin-left: 0px;
                                                    color: #25a0da; font-size: 22px; font-style: italic; overflow-y: auto">
                                                    <div style="opacity: 1.0; padding: 10px;">
                                                        <%#Eval("pro_Description") %>
                                                    </div>
                                                </div>
                                                <div class="foodaddicon" style="position: absolute; top: 600px; margin-left: 240px;
                                                    cursor: pointer;">
                                                    <img src="images/add_basic_blue.png" style="width: 48px; height: 48px;">
                                                </div>
                                                <div class="facebookicon" style="position: absolute; top: 600px; margin-left: 180px;
                                                    cursor: pointer;">
                                                    <img src="images/facebook.png" style="width: 48px; height: 48px;">
                                                </div>
                                                <div class="app-price" style="display: none">
                                                    <%#Eval("pro_price")%></div>
                                                <div class="app-id" style="display: none">
                                                    <%#Eval("pro_Id")%></div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                    <div id="normalmenu" style="display: none; margin-top: 10px; overflow-y: auto; height: 680px;">
                        <table class="menuviewcontainer list" style="margin: 0 auto; width: auto; border: 0px solid white;
                            width: 945px;">
                        </table>
                    </div>
                    <div style="visibility: hidden">
                        <asp:Repeater ID="foodRepeater" runat="server">
                            <ItemTemplate>
                                <div>
                                    <asp:Image CssClass="himage" ID="Image1" runat="server" ImageUrl='<%# HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + Eval("ImageUrl")) %>'
                                        Width="230" Style="overflow: hidden" />
                                    <asp:Label ID="Label1" CssClass="hname" Text='<%#Eval("pro_Name") %>' runat="server"></asp:Label>
                                    <asp:Label ID="Label4" CssClass="hdiscount" Text='<%#Eval("Discount") %>' runat="server"></asp:Label>
                                    <asp:Label ID="Label2" CssClass="hprice" Text='<%#String.Format("{0:C}",Eval("pro_Price") ) %>'
                                        runat="server"></asp:Label>
                                    <asp:Label ID="Label3" CssClass="hid" Text='<%#Eval("pro_Id")  %>' runat="server"></asp:Label>
                                    <asp:Label ID="Label5" CssClass="hcatname" Text='<%#Eval("Category.cat_Name")  %>'
                                        runat="server"></asp:Label>
                                    <asp:Label ID="Label6" CssClass="hdescription" Text='<%#Eval("pro_Description")  %>'
                                        runat="server"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div id="detail" style="position: absolute; right: 0px; width: 600px; height: 500px;
            background-color: gray; top: 36px; z-index: 2000; display: none; border: 1px solid black">
            <table style="width: 100%">
                <tr style="height: 40px">
                    <td colspan="2">
                        <div style="width: 588px; height: 14px; background-color: black; color: White; vertical-align: middle;
                            padding-top: 13px; padding-bottom: 13px; padding-left: 13px;">
                            <div style="float: left">
                                <label class="dname" style="color: White">
                                </label>
                                &nbsp; / &nbsp;
                                <label class="dprice" style="color: White">
                                </label>
                                <label class="did" style="color: White; display: none">
                                </label>
                            </div>
                            <div style="float: right; margin-right: 10px; margin-top: -8px;">
                                <input type="button" value="Back" id="backbtn" class="btn btn-large btn-block btn-info btn-add"
                                    style="width: 100px" />
                            </div>
                            <div style="float: right; margin-right: 10px; margin-top: -8px;">
                                <input type="button" value="Order" id="orderbtn" class="btn btn-large btn-block btn-info btn-add"
                                    style="width: 100px" />
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td width="50%">
                        <div style="height: 460px; border-right: 1px solid #3894E7; width: 300px; overflow: hidden;
                            display: table-cell; vertical-align: middle; text-align: center">
                            <img src="" alt="" style="width: 300px; height: 460px;" class="dimg" />
                        </div>
                    </td>
                    <td width="50%" style="vertical-align: top">
                        <div style="height: 400px; width: 300px; overflow: auto;" class="commentsarea" id="comments">
                        </div>
                        <div style="height: 60px; width: 300px; border-top: 1px solid white; overflow: hidden;">
                            <div style="height: 20px; text-align: left; overflow: hidden; margin-top: 15px; width: 70px;"
                                class="likearea" id="like">
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <asp:UpdatePanel UpdateMode="Conditional" runat="server" class="cartcontainer" style="display: none">
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="submitBtn" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="updateBtn" EventName="Click" />
                <asp:AsyncPostBackTrigger ControlID="receiveBtn" EventName="Click" />
            </Triggers>
            <ContentTemplate>
                <div class="rltbOverlay" style="z-index: 3000;">
                </div>
                <div id="cartdetails" class="RadLightBox RadLightBox_Metro" style="position: fixed;
                    left: 50%; top: 50%; z-index: 3001; width: 420px; height: 670px; margin-left: -210px;
                    margin-top: -345px; visibility: visible; overflow: visible;">
                    <div class="rltbGlowEffect">
                    </div>
                    <div class="rltbClose orderclose" style="">
                        <button type="button" class="rltbActionButton rltbCloseButton">
                            <span class="rltbIcon rltbCloseIcon"></span><span class="rltbButtonText">Close</span></button>
                    </div>
                    <div class="rltbWrapper">
                        <div class="rltbItemBox">
                            <table style="width: 100%; height: 100%;" id="cart">
                                <tr style="height: 60px;">
                                    <td style="margin-bottom: 10px;">
                                        <table style="width: 100%">
                                            <tr>
                                                <td>
                                                    <table style="width: 100%; height: 100%">
                                                        <tr>
                                                            <td style="width: 70px;">
                                                                Table NO.:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtName" runat="server" CssClass="inputtype" Height="30"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                        <tr style="display: none">
                                                            <td>
                                                                Wallet ID:
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtWalletID" runat="server" CssClass="inputtype"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td align="center" style="text-align: center">
                                                    <asp:Button ID="Button1" Text="Submit" runat="server" OnClick="DoInvoice" CssClass="btn btn-large btn-block btn-info btn-add"
                                                        Width="100" Height="30" Style="margin-left: 10px; float: left;" UseSubmitBehavior="true" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top">
                                        <div id="orderdetail" style="width: 400px; z-index: 1000; height: 560px; overflow: auto">
                                            <table style="width: 100%;" class="list olist">
                                                <thead>
                                                    <tr>
                                                        <th align="center">
                                                        </th>
                                                        <th>
                                                            Name
                                                        </th>
                                                        <th>
                                                            Price
                                                        </th>
                                                        <th>
                                                            Qty
                                                        </th>
                                                        <th>
                                                            Sub Total
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <asp:Repeater ID="rptProductList" runat="server">
                                                        <HeaderTemplate>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr class='<%# Convert.ToBoolean(Eval("IsExisting"))? "existingrow":"newrow"%> '>
                                                                <td align="center">
                                                                    <asp:LinkButton ID="lbtnDel" runat="server" class="icon icon-del" OnClick="DeleteItem"></asp:LinkButton>
                                                                    <asp:Label ID="lblId" CssClass="opid" runat="server" Text='<%# Eval("ip_ID")%>' Visible="false"></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:Label ID="lblName" CssClass="opname" runat="server" Text='<%# Eval("ip_Description")%>'></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    $<asp:Label ID="txtPrice" CssClass="opprice" runat="server" Text='<%# Convert.ToDecimal(Eval("ip_Price")).ToString("0.00")%>'></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    <asp:TextBox ID="txtQuantity" CssClass="opquantity" runat="server" Width="50px" Text='<%# Eval("ip_Quantity")%>'></asp:TextBox>
                                                                    <asp:Label ID="lblProductId" runat="server" Text='<%# Eval("ip_ProductID")%>' CssClass="oproductid"></asp:Label>
                                                                </td>
                                                                <td align="center">
                                                                    $<asp:Label ID="lblSubTotal" CssClass="opsubtotal" runat="server" Text='<%# Convert.ToDecimal(Eval("ip_SubTotal")).ToString("0.00")%>'></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </tbody>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr style="height: 50px">
                                    <td style="height: 50px">
                                        <div style="height: 50px; text-align: right; color: White; padding-top: 10px;">
                                            <asp:Label ID="lblTotal" CssClass="totalcost" runat="server" Style="margin-right: 10px;
                                                color: Black;"></asp:Label>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <input type="hidden" runat="server" id="currentinvoiceid" clientidmode="Static" value="0" />
                <input type="hidden" runat="server" id="isreceiving" clientidmode="Static" value="0" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel runat="server" UpdateMode="Conditional" class="productfacebook"
            style="display: none">
            <ContentTemplate>
                <div class="rltbOverlay" style="z-index: 3000;">
                </div>
                <div id="Div1" class="RadLightBox RadLightBox_Metro" style="position: fixed; left: 50%;
                    top: 25px; z-index: 3001; width: 970px; height: 700px; margin-left: -495px; visibility: visible;
                    overflow: visible;">
                    <div class="rltbGlowEffect">
                    </div>
                    <div class="rltbClose pfclose" style="">
                        <button type="button" class="rltbActionButton rltbCloseButton">
                            <span class="rltbIcon rltbCloseIcon"></span><span class="rltbButtonText">Close</span></button>
                    </div>
                    <div class="rltbWrapper" style="padding: 0px">
                        <div class="rltbItemBox">
                            <table style="width: 100%; height: 680px;" class="pf_item">
                                <tr>
                                    <td style="width: 475px; border-right: 1px solid #25a0da">
                                        <table style="width: 100%; height: 680px;">
                                            <tr style="height: 50px">
                                                <td class="pf_name" colspan="2" valign="middle" align="center" style="font-size: 17px;
                                                    overflow: hidden; text-overflow: ellipsis; white-space: nowrap;">
                                                    <strong>Here you are</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" valign="middle" align="center">
                                                    <div id="zoomimagecontainer" style="max-width: 450px; max-height: 550px; overflow: hidden">
                                                        <img class="pf_image" src="" /></div>
                                                </td>
                                            </tr>
                                            <tr style="height: 50px">
                                                <td class="pf_price" style="padding-left: 10px; font-size: 25px;">
                                                    $10.00
                                                </td>
                                                <td valign="middle" align="center" style="width: 50px">
                                                    <img src="images/plus-outline.png" style="width: 32px; height: 32px; cursor: pointer;"
                                                        class="pf_addicon"><span class="pf_pid" style="display: none"></span><span class="pf_pname"
                                                            style="display: none"></span><span class="pf_pprice" style="display: none"></span>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 475px; border-right: 0px solid #e4e4e4" valign="top">
                                        <div id="fbcomments" style="width: 475px; height: 680px; overflow-y: auto; overflow-x: hidden;
                                            padding-left: 5px;">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
                <telerik:RadLightBox runat="server">
                </telerik:RadLightBox>
            </ContentTemplate>
        </asp:UpdatePanel>
        <script type="text/javascript">
            var heights = {};
            jQuery.fn.outerHTML = function (s) {
                return s
        ? this.before(s).remove()
        : jQuery("<p>").append(this.eq(0).clone()).html();
            };

            var mySwiper = null;
            function pageLoad(sender, args) {
                refreshWaterfall();
                if (mySwiper != null) {
                    mySwiper.destroy();
                    mySwiper = null;
                }

                $('.cartitem-container').swiper({
                    slidesPerView: 6,
                    offsetPxBefore: 0,
                    offsetPxAfter: 0,
                    calculateHeight: true,
                    slidesPerGroup: 6
                });

                $(".menuitem").eq(0).css("borderLeft", "0px solid white");

                $(".cat-item").unbind("click");
                $(".cat-item").click(function () {
                    $("#selectedcategoryid").val($(this).find(".cat_id").val());
                    $("#categorySelectBtn").trigger("click");
                });

                mySwiper = $('.pitem-container').swiper({
                    slidesPerView: 3,
                    offsetPxBefore: 0,
                    offsetPxAfter: 0,
                    calculateHeight: true,
                    slidesPerGroup: 3
                });

                $(".listview").unbind("click");
                $(".listview").click(function () {
                    $(".slideshow").css("display", "none");
                    $('#waterfall').css("display", "");
                    $("#normalmenu").css("display", "none");
                    $("#currentview").val("listview");
                });

                $(".galleryview").unbind("click");
                $(".galleryview").click(function () {
                    $(".slideshow").css("display", "");
                    $('#waterfall').css("display", "none");
                    $("#normalmenu").css("display", "none");
                    $("#currentview").val("galleryview");
                    if (mySwiper != null) {
                        mySwiper.destroy();
                        mySwiper = null;
                    }
                    if (mySwiper == null) {
                        mySwiper = $('.pitem-container').swiper({
                            slidesPerView: 3,
                            offsetPxBefore: 0,
                            offsetPxAfter: 0,
                            calculateHeight: true,
                            slidesPerGroup: 3
                        });
                    }
                });

                $("#refreshAllLink").click(function () {
                    $("#selectedcategoryid").val("-1");
                    $("#refreshAllBtn").trigger("click");
                });

                $(".menuview").unbind("click");
                $(".menuview").click(function () {
                    $(".slideshow").css("display", "none");
                    $('#waterfall').css("display", "none");
                    $("#normalmenu").css("display", "");
                    $("#currentview").val("menuview");
                });

                $("." + $("#currentview").val()).trigger("click");

                $(".foodaddicon").click(function () {
                    var fitem = $(this).closest(".fooditem");

                    $("#pid").val($.trim(fitem.find(".app-id").eq(0).text()));
                    $("#pname").val($.trim(fitem.find(".app-title").eq(0).text()));
                    $("#pprice").val("$" + $.trim(fitem.find(".app-price").eq(0).text()));
                    $("#submitBtn").trigger("click");
                    e.stopPropagation();
                });


                $('.arrow-left').click(function (e) {
                    e.preventDefault();
                    mySwiper.swipePrev();
                });

                $('.arrow-right').click(function (e) {
                    e.preventDefault();
                    mySwiper.swipeNext(true);
                });

                var totalquantity = 0;
                $(".olist tbody > tr").each(function () {
                    totalquantity += parseInt($(this).find(".opquantity").eq(0).val());
                });
                if ($.trim($(".totalcost").text()) != "") {
                    $(".totalitems").text($.trim($(".totalcost").text()) + ", " + totalquantity.toString() + " items");
                } else {
                    $(".totalitems").text("");
                }

                if ($("#cart").parent().width() > 0) {
                    $("#cart").css("display", "");
                    $("#cart").parent().css("display", "");
                    $("#cart").parent().css("width", "400px");
                }

                $(".opquantity").change(function () { $("#updateBtn").trigger("click"); });

                $("#sc_opener").unbind("click");
                $("#sc_opener").click(function (event) {
                    if ($(this).hasClass("hpcExpand")) {
                        $(this).addClass("hpcCollapse");
                        $(this).removeClass("hpcExpand");
                        $(this).css("background-position", "-96px -53px");
                        $(".cartcontainer").css("display", "");
                        $(this).attr("title", "Collapse");
                    }
                    event.stopPropagation();
                });

                $("#cartsummary").unbind("click");
                $("#cartsummary").click(function (event) {
                    if ($("#sc_opener").hasClass("hpcExpand")) {
                        $("#sc_opener").addClass("hpcCollapse");
                        $("#sc_opener").removeClass("hpcExpand");
                        $("#sc_opener").css("background-position", "-96px -53px");
                        $(".cartcontainer").css("display", "");
                        $("#sc_opener").attr("title", "Collapse");
                    }
                    event.stopPropagation();
                });

                $(".orderclose").unbind("click");
                $(".orderclose").click(function () {
                    $("#sc_opener").removeClass("hpcCollapse");
                    $("#sc_opener").addClass("hpcExpand");
                    $("#sc_opener").css("background-position", "-128px -53px");
                    $(".cartcontainer").css("display", "none");
                    $("#sc_opener").attr("title", "Expand");
                });

                $(".grid").each(function () {
                    $(this).find(".checkstatus").each(function () {
                        $(this).css("display", "none");
                    });
                });

                $(".fooditem").each(function () {
                    $(this).find(".checkstatus").each(function () {
                        $(this).css("display", "none");
                    });
                });

                $(".menuviewcontainer tbody>tr").each(function () {
                    $(this).find(".checkstatus").each(function () {
                        $(this).css("display", "none");
                    });
                });

                $(".oproductid").each(function () {
                    var productid = $.trim($(this).text());
                    $(".grid").each(function () {
                        if ($.trim($(this).find(".list_pid").eq(0).text()) == productid) {
                            $(this).find(".checkstatus").toggle("fade");
                        }
                    });
                    $(".fooditem").each(function () {
                        if ($.trim($(this).find(".app-id").eq(0).text()) == productid) {
                            $(this).find(".checkstatus").toggle("fade");
                        }
                    });
                    $(".menuviewcontainer tbody>tr").each(function () {
                        if ($.trim($(this).find(".proid").eq(0).text()) == productid) {
                            $(this).find(".checkstatus").toggle("fade");
                        }
                    });

                });

                $(".list_addicon").unbind("click");
                $(".list_addicon").click(function () {
                    var fitem = $(this).closest(".grid");
                    $("#pid").val($.trim(fitem.find(".list_pid").eq(0).text()));
                    $("#pname").val($.trim(fitem.find(".list_pname").eq(0).text()));
                    $("#pprice").val("$" + $.trim(fitem.find(".list_pprice").eq(0).text()));
                    $("#submitBtn").trigger("click");
                    e.stopPropagation();
                });


                $(".grid").each(function () {
                    var gh = heights[$.trim($(this).find(".list_pid").text())];

                    if (gh != undefined) {
                        $(this).height(gh);
                    }
                });
                $('#listcontainer').BlocksIt({
                    numOfCol: 4,
                    offsetX: 6,
                    offsetY: 6
                });

                //                $('#waterfall').kinetic({
                ////                    filterTarget: function (target, e) {
                ////                        if (!/down|start/.test(e.type)) {
                ////                            return !(/area|img/i.test(target.tagName));
                ////                        }
                ////                    }
                //                });
                //                $('#normalmenu').kinetic({
                ////                    filterTarget: function (target, e) {
                ////                        if (!/down|start/.test(e.type)) {
                ////                            return !(/area|img|tr/i.test(target.tagName));
                ////                        }
                ////                    }
                //                });

                $(".list_pimage").unbind("click");
                $(".list_pimage").click(function () {
                    var grid = $(this).closest(".grid");
                    $(".pf_name").find("strong").text(grid.find(".list_pname").text());
                    $(".pf_price").text("$" + parseFloat(grid.find(".list_pprice").text()).toFixed(2));
                    $(".pf_image").attr("src", grid.find(".list_pimage").attr("src"));
                    $(".pf_pid").text($.trim(grid.find(".list_pid").text()));
                    $(".pf_pname").text($.trim(grid.find(".list_pname").text()));
                    $(".pf_pprice").text($.trim(grid.find(".list_pprice").text()));

                    var src = $(".pf_image").attr("src");
                    $('#fbcomments').html(' <fb:comments href="' + src.replace("localhost", "127.0.0.1") + '" width="475" numposts="5" colorscheme="light"></fb:comments>');
                    if (typeof FB !== 'undefined') {
                        FB.XFBML.parse(document.getElementById('fbcomments'));
                    }
                    $('.pf_image').smoothZoom("destroy");
                    $('.pf_image').attr("style", "");
                    var img = new Image();
                    img.onload = function () {
                        var imgwidth = this.width;
                        var imgheight = this.height;

                        if (imgwidth > 450 || imgheight > 550) {
                            $('.pf_image').smoothZoom("destroy").smoothZoom({
                                width: imgwidth,
                                height: imgheight,
                                responsive: false,
                                responsive_maintain_ratio: true,
                                max_WIDTH: '',
                                max_HEIGHT: '',
                                pan_BUTTONS_SHOW: false,
                                zoom_BUTTONS_SHOW: false,
                                mouse_DRAG: true, 						//Enable/disable the dragability of image (mouse)
                                mouse_WHEEL: true,
                                animation_SPEED_ZOOM: 0.5,
                                pan_LIMIT_BOUNDARY: false
                            });
                        }
                    }
                    img.src = src;

                    $(".productfacebook").css("display", "");
                });

                $(".facebookicon").unbind("click");
                $(".facebookicon").click(function () {
                    var fitem = $(this).closest(".fooditem");
                    $(".pf_name").find("strong").text($.trim(fitem.find(".app-title").eq(0).text()));
                    $(".pf_price").text("$" + parseFloat($.trim(fitem.find(".app-price").eq(0).text())).toFixed(2));
                    $(".pf_image").attr("src", fitem.find(".foodimage").attr("src"));
                    $(".pf_pid").text($.trim(fitem.find(".app-id").eq(0).text()));
                    $(".pf_pname").text($.trim(fitem.find(".app-title").eq(0).text()));
                    $(".pf_pprice").text($.trim(fitem.find(".app-price").eq(0).text()));

                    var src = $(".pf_image").attr("src");
                    $('#fbcomments').html(' <fb:comments href="' + src.replace("localhost", "127.0.0.1") + '" width="475" numposts="5" colorscheme="light"></fb:comments>');
                    if (typeof FB !== 'undefined') {
                        FB.XFBML.parse(document.getElementById('fbcomments'));
                    }
                    $('.pf_image').smoothZoom("destroy");
                    $('.pf_image').attr("style", "");
                    var img = new Image();
                    img.onload = function () {
                        var imgwidth = this.width;
                        var imgheight = this.height;

                        if (imgwidth > 450 || imgheight > 550) {
                            $('.pf_image').smoothZoom("destroy").smoothZoom({
                                width: imgwidth,
                                height: imgheight,
                                responsive: false,
                                responsive_maintain_ratio: true,
                                max_WIDTH: '',
                                max_HEIGHT: '',
                                pan_BUTTONS_SHOW: false,
                                zoom_BUTTONS_SHOW: false,
                                mouse_DRAG: true, 						//Enable/disable the dragability of image (mouse)
                                mouse_WHEEL: true,
                                animation_SPEED_ZOOM: 0.5,
                                pan_LIMIT_BOUNDARY: false
                            });
                        }
                    }
                    img.src = src;
                    $(".productfacebook").css("display", "");
                });

                $(".pfclose").unbind("click");
                $(".pfclose").click(function () {
                    $(".productfacebook").css("display", "none");
                });

                $(".pf_addicon").unbind("click");
                $(".pf_addicon").click(function () {
                    var fitem = $(this).closest(".pf_item");
                    $("#pid").val($.trim(fitem.find(".pf_pid").eq(0).text()));
                    $("#pname").val($.trim(fitem.find(".pf_pname").eq(0).text()));
                    $("#pprice").val("$" + $.trim(fitem.find(".pf_pprice").eq(0).text()));
                    $("#submitBtn").trigger("click");
                    $(".productfacebook").css("display", "none");
                    e.stopPropagation();
                });
                $('#txtName').unbind("keyup");
                $('#txtName').keyup(function (e) {
                    //alert(e.keyCode);
                    if (e.keyCode == 13) {
                        $("#receiveBtn").trigger("click");
                    }
                });

                if ($("#isreceiving").val() == "1") {
                    $("#isreceiving").val("0");
                    $("#sc_opener").addClass("hpcCollapse");
                    $("#sc_opener").removeClass("hpcExpand");
                    $("#sc_opener").css("background-position", "-96px -53px");
                    $(".cartcontainer").css("display", "");
                    $("#sc_opener").attr("title", "Collapse");
                }
            }

            function closeOrder() {
                $("#sc_opener").removeClass("hpcCollapse");
                $("#sc_opener").addClass("hpcExpand");
                $("#sc_opener").css("background-position", "-128px -53px");
                $(".cartcontainer").css("display", "none");
                $("#sc_opener").attr("title", "Expand");
                //                FB.logout(function (response) {
                //                    window.parent.location.href = "index.htm?page=order";
                //                });
            }

            function refreshWaterfall() {
                $(".menuviewcontainer tr").remove();
                var menus = new Array();
                var menuProducts = new Array();
                $(".himage").each(function () {
                    var psrc = $(this).attr("src");
                    var name = $.trim($(this).parent().find(".hname").eq(0).text());
                    var price = $.trim($(this).parent().find(".hprice").eq(0).text());
                    var pid = $.trim($(this).parent().find(".hid").eq(0).text());
                    var discount = $.trim($(this).parent().find(".hdiscount").eq(0).text());
                    var catname = $.trim($(this).parent().find(".hcatname").eq(0).text());
                    var pdescription = $.trim($(this).parent().find(".hdescription").eq(0).text());
                    if (menus.indexOf(catname) >= 0) {
                        for (var k = 0; k < menuProducts.length; k++) {
                            if (menuProducts[k].cat == catname) {
                                menuProducts[k].productList.push({ id: pid, productPrice: price, productDiscount: discount, productName: name, description: pdescription, src: psrc });
                                break;
                            }
                        }
                    } else {
                        var products = new Array();
                        products.push({ id: pid, productPrice: price, productDiscount: discount, productName: name, description: pdescription, src: psrc });
                        menuProducts.push({ cat: catname, productList: products });
                        menus.push(catname);
                    }

                });

                for (var j = 0; j < menuProducts.length; j++) {
                    var catrow = $("<tr style='height:50px;border:0px solid white'><td colspan='7' align='center' style='font-weight:bold;font-size:25px'>" + menuProducts[j].cat + "</td></tr>");
                    $(".menuviewcontainer").append(catrow);
                    for (var m = 0; m < menuProducts[j].productList.length; m++) {
                        var prow = $("<tr style='height:48px;border:0px solid white;font-weight:bold;font-size:15px;'></tr>");
                        var ntd = $("<td class='proname' style='overflow: hidden;text-overflow: ellipsis;white-space: nowrap;font-size:20px;'>" + menuProducts[j].productList[m].productName + "</td>");
                        var ptd = $("<td class='proprice' style='width:100px'>" + menuProducts[j].productList[m].productPrice + "</td>");
                        var pidtd = $("<td class='proid' style='display:none'>" + menuProducts[j].productList[m].id + "</td>");
                        var atd = $('<td style="width:40px;padding:0px;height:40px;" align="center"><img src="images/plus-outline.png" style="width:32px;height:32px;"/></td>');
                        var ctd = $('<td><img class="checkstatus" src="images/check.png" style="width:32px;height:32px" /></td>');
                        prow.append(ntd);
                        prow.append($("<td style='width:50px'><span class='proimagesrc' style='display:none'>" + menuProducts[j].productList[m].src + "</span></td>"));
                        prow.append(ptd);
                        prow.append($("<td style='width:20px'></td>"));
                        prow.append(atd);
                        prow.append(ctd);
                        prow.append(pidtd);

                        atd.click(function (e) {
                            var protr = $(this).closest("tr");
                            var proname = $.trim($(this).closest("tr").find(".proname").text());
                            var proprice = $.trim($(this).closest("tr").find(".proprice").text());
                            var proid = $.trim($(this).closest("tr").find(".proid").text());
                            $("#pid").val(proid);
                            $("#pname").val(proname);
                            $("#pprice").val(proprice);
                            $(this).closest("tr").effect("highlight", {}, 500, function () { });
                            $("#submitBtn").trigger("click");
                            e.stopPropagation();
                        });

                        $(".menuviewcontainer").append(prow);
                        prow.click(function () {
                            $(".pf_name").find("strong").text($(this).find(".proname").text());
                            $(".pf_price").text($(this).find(".proprice").text());
                            $(".pf_image").attr("src", $(this).find(".proimagesrc").text());
                            $(".pf_pid").text($(this).find(".proid").text());
                            $(".pf_pname").text($(this).find(".proname").text());
                            $(".pf_pprice").text($(this).find(".proprice").text().replace("$", ""));

                            var src = $(".pf_image").attr("src");
                            $('#fbcomments').html(' <fb:comments href="' + src.replace("localhost", "127.0.0.1") + '" width="475" numposts="5" colorscheme="light"></fb:comments>');
                            if (typeof FB !== 'undefined') {
                                FB.XFBML.parse(document.getElementById('fbcomments'));
                            }
                            $(".productfacebook").css("display", "");
                        });

                        if (menuProducts[j].productList[m].description != "") {
                            var descriptionrow = $("<tr style='height:12px;border:0px solid white;font-size:12px;font-style: italic;'></tr>");
                            var descriptiontd = $("<td colspan='7' style='height:12px;margin-bottom:10px;color:#25a0da'>" + menuProducts[j].productList[m].description + "</td>");
                            descriptionrow.append(descriptiontd);
                            $(".menuviewcontainer").append(descriptionrow);
                        }
                    }
                }

                var i = 0;
                if ($(".fooditem").length == 0) {
                    $(".arrow-left").css("display", "none");
                    $(".arrow-right").css("display", "none");
                } else {
                    $(".arrow-left").css("display", "");
                    $(".arrow-right").css("display", "");
                }

                $(".app-discount").each(function () {
                    var discountvalue = $.trim($(this).find(".discountvalue").eq(0).text());
                    if (discountvalue == "100") {
                        $(this).css("display", "none");
                    } else {
                        $(this).css("display", "");
                    }
                });

                $(".list_pdiscount").each(function () {
                    var discountvalue = $.trim($(this).find(".list_discountvalue").eq(0).text());
                    if (discountvalue == "100") {
                        $(this).css("display", "none");
                    } else {
                        $(this).css("display", "");
                    }
                });
            }

            $(document).ready(function () {
                //blocksit define
                $(window).load(function () {
                    $('#listcontainer').BlocksIt({
                        numOfCol: 4,
                        offsetX: 6,
                        offsetY: 6
                    });
                    $(".grid").each(function () {
                        heights[$.trim($(this).find(".list_pid").text())] = $(this).height();
                    });
                });
            });
        </script>
        <input type="hidden" runat="server" id="pid" clientidmode="Static" />
        <input type="hidden" runat="server" id="pname" clientidmode="Static" />
        <input type="hidden" runat="server" id="pprice" clientidmode="Static" />
        <input type="hidden" runat="server" id="pwidth" clientidmode="Static" />
        <input type="hidden" runat="server" id="currentview" clientidmode="Static" value="listview" />
        <asp:Label ID="lblNo" runat="server" Visible="false"></asp:Label>
    </div>
    <telerik:RadWindowManager runat="server" ID="windowManager" Skin="Metro" KeepInScreenBounds="true"
        AutoSize="true">
        <AlertTemplate>
            <div class="rwDialogPopup radalert">
                <div class="rwDialogText">
                    {1}
                </div>
                <div>
                    <a onclick="$find('{0}').close();" class="rwPopupButton" href="javascript:void(0);">
                        <span class="rwOuterSpan"><span class="rwInnerSpan">##LOC[OK]##</span> </span>
                    </a>
                </div>
            </div>
        </AlertTemplate>
    </telerik:RadWindowManager>
    </form>
</body>
</html>
