﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Telerik.Web.UI;
using POS.Data;

namespace POS
{
    public partial class TaskMode : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            }
        }

        protected void RefreshTask(object sender, EventArgs args)
        {
            GlobalCache.RefreshInvoices(0, null, null, null, false);
            List<Invoice> invoices = new List<Invoice>();
            int count = 0;
            if (GlobalCache.Invoices.Any(i => i.Products.Any(p => !p.IsComplete)))
            {
                var aa = GlobalCache.Invoices.Where(i => i.Products.Any(p => !p.IsComplete)).OrderBy(i2 => i2.inv_CreateDate);
                foreach (Invoice inv in GlobalCache.Invoices.Where(i => i.Products.Any(p => !p.IsComplete)).OrderBy(i2 => i2.inv_CreateDate))
                {
                    int pc = inv.Products.Count(p => !p.IsComplete);
                    if (pc + count <= 5)
                    {
                        inv.Take = pc;
                        invoices.Add(inv);
                        count = pc + count;
                        if (count == 5)
                            break;

                    }
                    else
                    {
                        inv.Take = 5 - count;
                        invoices.Add(inv);
                        break;
                    }
                }
            }
            RadGrid1.DataSource = null;
            RadGrid1.DataSource = invoices;
            RadGrid1.DataBind();
        }

        protected void RadGrid1_ItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName == "Complete")
            {
                GridDataItem dataItem = (GridDataItem)e.Item;
                int id = Convert.ToInt32(dataItem.GetDataKeyValue("ip_ID").ToString());
                Invoice inv = GlobalCache.Invoices.FirstOrDefault(i => i.Products.Any(p => p.ip_ID == id));
                InvoiceProduct ip = inv.Products.FirstOrDefault(p => p.ip_ID == id);
                ip.IsComplete = true;
                inv.Status = InvoiceStatus.UnPaid;
                new POSService.POSService().DoInvoice(inv);
                RefreshTask(null,null);

            }
            else if (e.CommandName == "RebindGrid")
            {
                RefreshTask(null, null);
            }
        }

        protected void RadGrid1_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            List<Invoice> invoices = new List<Invoice>();
            int count = 0;
            if (GlobalCache.Invoices.Any(i => i.Products.Any(p => !p.IsComplete)))
            {
                var aa = GlobalCache.Invoices.Where(i => i.Products.Any(p => !p.IsComplete)).OrderBy(i2 => i2.inv_CreateDate);
                foreach (Invoice inv in GlobalCache.Invoices.Where(i => i.Products.Any(p => !p.IsComplete)).OrderBy(i2 => i2.inv_CreateDate))
                {
                    int pc = inv.Products.Count(p => !p.IsComplete);
                    if (pc + count <= 5)
                    {
                        inv.Take = pc;
                        invoices.Add(inv);
                        count = pc + count;
                        if (count == 5)
                            break;
                    }
                    else
                    {
                        inv.Take = 5 - count;
                        invoices.Add(inv);
                        break;
                    }
                }
            }
            RadGrid1.DataSource = invoices;
        }

        protected void RadGrid1_DetailTableDataBind(object sender, Telerik.Web.UI.GridDetailTableDataBindEventArgs e)
        {
            GridDataItem dataItem = (GridDataItem)e.DetailTableView.ParentItem;
            switch (e.DetailTableView.Name)
            {
                case "Orders":
                    {
                        int invId = Convert.ToInt32(dataItem.GetDataKeyValue("inv_Id").ToString());
                        Invoice inv = GlobalCache.Invoices.FirstOrDefault(i => i.inv_Id == invId);
                        List<InvoiceProduct> products= GlobalCache.Invoices.FirstOrDefault(i => i.inv_Id == invId).Products;
                        e.DetailTableView.DataSource = products.Where(p => !p.IsComplete).Take(inv.Take);
                        break;
                    }
            }
        }
    }
}