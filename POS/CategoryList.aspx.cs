﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS
{
    public partial class CategoryList : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //GlobalCache.RefreshCategories();
                Initialize();
            }
        }
        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                delid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "confirmclosed");
            }
            else if (e.CommandName == "ItemEdit")
            {
                 RepeaterItem item = e.Item;
                 formbuttons.Visible = true;
                 listbuttons.Visible = false;
                 formpanel.Visible = true;
                 listpanel.Visible = false;
                 ltlPageTitle.Text = "Category / Edit";
                catid.Attributes["value"] = e.CommandArgument.ToString();
                serverid.Attributes["value"] = (item.FindControl("lblServerID") as Label).Text;
                txtName.Text = (item.FindControl("lblName") as Label).Text.Replace("├", "").Replace("─", "");
                BindCategories(Convert.ToInt32(e.CommandArgument.ToString()));
                dropParent.SelectedValue = (item.FindControl("lblParentID") as Label).Text;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                ShowError("Category name is mandatory!");
                return;
            }
            POSService.POSService pos = new POSService.POSService();
            Category Parent = new Category();
            if (Convert.ToInt32(dropParent.SelectedValue)!=0)
            {
                Parent = GlobalCache.Categories.FirstOrDefault(c => c.cat_Id == Convert.ToInt32(dropParent.SelectedValue));
            }
            pos.AddCategory(new Category() { cat_Id = Convert.ToInt32(catid.Value), cat_Name = txtName.Text.Trim(), ParentID = Convert.ToInt32(dropParent.SelectedValue), cat_ClientID = this.UserClientID, UserID = GlobalCache.CurrentUser.usr_ID, ServerID = Convert.ToInt32(serverid.Value),LastUpdatedTime=DateTime.Now,Parent=Parent });
            GlobalCache.RefreshCategories();
            BackToList(null,null);
        }

        void Initialize()
        {
            catid.Value = "0";
            serverid.Value = "0";
            delid.Attributes["value"] = "-1";
            txtName.Text = "";
            BindCategories(0);
            Bind();
        }

        void BindCategories(int catid)
        {
            List<Category> cats2 = new List<Category>();
            cats2.Add(new Category() { cat_Id = 0, cat_Name = "--No Parent--" });
            GlobalCache.Categories.GetHiarachy().ForEach(c => {
                if (c.cat_Id != catid)
                {
                    cats2.Add(c);
                }
            });
            dropParent.DataSource = cats2;
            dropParent.DataBind();
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = GlobalCache.Categories.GetHiarachy() ;
            rptList.DataBind();
        }

        protected void AddCategory(object sender, EventArgs args)
        {
            formbuttons.Visible = true;
            listbuttons.Visible = false;
            formpanel.Visible = true;
            listpanel.Visible = false;
            ltlPageTitle.Text = "Category / Add";
            dropParent.SelectedIndex = 0;
            txtName.Text = "";
            catid.Value = "0";
            serverid.Value = "0";
        }

        protected void BackToList(object sender, EventArgs e)
        {
            listbuttons.Visible = true;
            formbuttons.Visible = false;
            formpanel.Visible = false;
            listpanel.Visible = true;
            ltlPageTitle.Text = "Category List";
            Initialize();
        }

        protected void DeleteCategory(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delid.Attributes["value"]);
           
            Category category = GlobalCache.Categories.FirstOrDefault(c => c.cat_Id == id);
            POSService.POSService service = new POSService.POSService();
            category.IsDeleted = true;
            category.UserID= GlobalCache.CurrentUser.usr_ID;
            category.LastUpdatedTime = DateTime.Now;
            service.UpdateCategory(category);
            GlobalCache.RefreshCategories();
            Initialize();
        }
    }
}