﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="True"
    CodeBehind="OrderingMode.aspx.cs" Inherits="POS.OrderingMode" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/waterfall.css">
    <style>
        .hname, hprice
        {
            display: none;
        }
        .displaynone
        {
            display: none;
        }
        
        .fb_edge_widget_with_comment span.fb_edge_comment_widget iframe.fb_ltr
        {
            display: none !important;
        }
        .rltbDescriptionBox
        {
            display: none;
        }
        #waterfall-loading
        {
            display:none;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <telerik:RadSkinManager ID="QsfSkinManager" runat="server" ShowChooser="false" Skin="Metro" />
    <telerik:RadFormDecorator ID="QsfFromDecorator" runat="server" DecoratedControls="All"
        EnableRoundedCorners="false" Skin="Silk" />
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
           
        </script>
    </telerik:RadCodeBlock>
    <asp:Button ID="submitBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
        OnClick="Order" ClientIDMode="Static" UseSubmitBehavior="false"></asp:Button>
    <asp:Button ID="updateBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
        OnClick="UpdateOrder" ClientIDMode="Static" UseSubmitBehavior="false"></asp:Button>
    <asp:Button ID="updateBoxBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
        OnClick="UpdateBox" ClientIDMode="Static" UseSubmitBehavior="false"></asp:Button>
    <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
        <ContentTemplate>
            <table style="width: 100%; height: 76px; table-layout: fixed; position: fixed; top: 0px;
                z-index: 1000">
                <tr style="height: 36px;">
                    <td>
                        <div id="header2" style="width: 100%">
                            <telerik:RadMenu ID="Menu" runat="server" Skin="MetroTouch" DataTextField="cat_Name"
                                DataFieldID="cat_Id" DataFieldParentID="BindableParentID" Width="100%" OnItemClick="CategorySelected"
                                DataValueField="cat_Id">
                            </telerik:RadMenu>
                        </div>
                    </td>
                    <td style="width: 300px; background: #25a0da; border-left: 1px solid white; color: White"
                        valign="middle">
                        <table style="width: 100%; height: 100%; table-layout: fixed; font-weight: bold;
                            cursor: pointer" id="cartsummary">
                            <tr>
                                <td style="width: 40px" align="right">
                                    <img src="images/icon_basket.png" width="30px" height="30px" style="cursor: pointer"
                                        alt="Cart" />
                                </td>
                                <td align="left" style="padding-left: 10px">
                                    <asp:Label ID="ltlItems" runat="server" CssClass="totalitems"></asp:Label>
                                </td>
                                <td style="width: 40px">
                                    <a href="javascript:void(0)" class="hpcExpand" id="sc_opener" title="Expand" style="display: block;
                                        background: transparent url(images/hpc12.png) no-repeat; overflow: hidden; background-position: -128px -53px;
                                        width: 32px; height: 32px"></a>
                                    <%--<img src="images/badge_cancel.png" width="30px" height="30px" style="cursor: pointer"
                                        alt="Close cart" />--%>
                                    <%--  <input type="button" value="Close" id="closecart" class="btn btn-large btn-block btn-info btn-add" style="width:100px;margin-right:10px" />--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr style="height: 40px;">
                    <td valign="middle"  colspan="2" style="height: 40px; background-color: #3894E7; color: White;
                        font-weight: bold; vertical-align: middle; width: 100%" >
                        <a href="#" id="allfolds" style="color: White">ALL FOODS</a>
                        <asp:Literal ID="breadcrumb" runat="server"></asp:Literal>
                    </td>
                </tr>
            </table>
            <table style="width: 100%; table-layout: fixed;margin-top:91px;" id="layout">
                <tr>
                    <td colspan="3" valign="top">
                        <table style="width: 100%; height: 100%">
                            <tr>
                                <td valign="top">
                                    <div id="waterfall" >
                                    </div>
                                </td>
                                <td width="0px" valign="top" style="border-left: 1px solid #3894E7; display: none;
                                    border-bottom: 1px solid #3894E7;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <div style="visibility: hidden">
                <asp:Repeater ID="foodRepeater" runat="server">
                    <ItemTemplate>
                        <div>
                            <asp:Image CssClass="himage" ID="Image1" runat="server" ImageUrl='<%# HttpContext.Current.Request.Url.ToString().Replace(HttpContext.Current.Request.RawUrl, "/Upload/" + Eval("ImageUrl")) %>'
                                Width="250" Style="overflow: hidden" />
                            <asp:Label CssClass="hname" Text='<%#Eval("pro_Name") %>' runat="server"></asp:Label>
                            <asp:Label CssClass="hprice" Text='<%#String.Format("{0:C}",Eval("pro_Price") ) %>'
                                runat="server"></asp:Label>
                            <asp:Label CssClass="hid" Text='<%#Eval("pro_Id")  %>' runat="server"></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div id="detail" style="position: absolute; right: 0px; width: 600px; height: 500px;
        background-color: gray; top: 36px; z-index: 2000; display: none; border: 1px solid black">
        <table style="width: 100%">
            <tr style="height: 40px">
                <td colspan="2">
                    <div style="width: 588px; height: 14px; background-color: black; color: White; vertical-align: middle;
                        padding-top: 13px; padding-bottom: 13px; padding-left: 13px;">
                        <div style="float: left">
                            <label class="dname" style="color: White">
                            </label>
                            &nbsp; / &nbsp;
                            <label class="dprice" style="color: White">
                            </label>
                            <label class="did" style="color: White; display: none">
                            </label>
                        </div>
                        <div style="float: right; margin-right: 10px; margin-top: -8px;">
                            <input type="button" value="Back" id="backbtn" class="btn btn-large btn-block btn-info btn-add"
                                style="width: 100px" />
                        </div>
                        <div style="float: right; margin-right: 10px; margin-top: -8px;">
                            <input type="button" value="Order" id="orderbtn" class="btn btn-large btn-block btn-info btn-add"
                                style="width: 100px" />
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td width="50%">
                    <div style="height: 460px; border-right: 1px solid #3894E7; width: 300px; overflow: hidden;
                        display: table-cell; vertical-align: middle; text-align: center">
                        <img src="" alt="" style="width: 300px; height: 460px;" class="dimg" />
                    </div>
                </td>
                <td width="50%" style="vertical-align: top">
                    <div style="height: 400px; width: 300px; overflow: auto;" class="commentsarea" id="comments">
                    </div>
                    <div style="height: 60px; width: 300px; border-top: 1px solid white; overflow: hidden;">
                        <div style="height: 20px; text-align: left; overflow: hidden; margin-top: 15px; width: 70px;"
                            class="likearea" id="like">
                        </div>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server" class="cartcontainer"
        style="position: fixed; right: 0px; top: 40px; z-index: 4000; border: 1px solid #3894E7;
        background: white;">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Button1" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="submitBtn" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="updateBtn" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <table style="width: 100%; height: 100%; display: none;" id="cart">
                <tr style="height: 60px;">
                    <td style="margin-bottom: 10px;">
                        <table>
                            <tr>
                                <td>
                                    <table style="width: 100%; height: 100%">
                                        <tr>
                                            <td style="width: 120px;">
                                                Table NO.:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtName" runat="server" CssClass="inputtype"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Wallet ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtWalletID" runat="server" CssClass="inputtype"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td align="center" style="text-align: center">
                                    <asp:Button ID="Button1" Text="Submit" runat="server" OnClick="DoInvoice" CssClass="btn btn-large btn-block btn-info btn-add"
                                        Width="100" Height="30" Style="margin-left: 10px; float: left;" UseSubmitBehavior="true" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <div id="orderdetail" style="width: 400px; z-index: 1000;">
                            <table style="width: 100%;" class="list">
                                <thead>
                                    <tr>
                                        <th align="center">
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Price
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Sub Total
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptProductList" runat="server">
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td align="center">
                                                    <asp:LinkButton ID="lbtnDel" runat="server" class="icon icon-del" OnClick="DeleteItem"></asp:LinkButton>
                                                    <asp:Label ID="lblId" CssClass="opid" runat="server" Text='<%# Eval("ip_ID")%>' Visible="false"></asp:Label>
                                                </td>
                                                <td align="center">
                                                    <asp:Label ID="lblName" CssClass="opname" runat="server" Text='<%# Eval("ip_Description")%>'></asp:Label>
                                                </td>
                                                <td align="center">
                                                    $<asp:Label ID="txtPrice" CssClass="opprice" runat="server" Text='<%# Convert.ToDecimal(Eval("ip_Price")).ToString("0.00")%>'></asp:Label>
                                                </td>
                                                <td align="center">
                                                    <asp:TextBox ID="txtQuantity" CssClass="opquantity" runat="server" Width="50px" Text='<%# Eval("ip_Quantity")%>'></asp:TextBox>
                                                    <asp:Label ID="lblProductId" runat="server" Text='<%# Eval("ip_ProductID")%>' Visible="false"></asp:Label>
                                                </td>
                                                <td align="center">
                                                    $<asp:Label ID="lblSubTotal" CssClass="opsubtotal" runat="server" Text='<%# Eval("ip_SubTotal")%>'></asp:Label>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
                <tr style="height: 50px">
                    <td style="height: 50px">
                        <div style="height: 50px; text-align: right; color: White; padding-top: 10px;">
                            <asp:Label ID="lblTotal" CssClass="totalcost" runat="server" Style="margin-right: 10px;
                                color: Black;"></asp:Label>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <telerik:RadLightBox ID="OpenImageLightBox" runat="server" Modal="true" Skin="Metro"
                ShowCloseButton="true">
                <ClientSettings>
                    <AnimationSettings HideAnimation="Resize" NextAnimation="Fade" PrevAnimation="Fade"
                        ShowAnimation="Resize" />
                </ClientSettings>
            </telerik:RadLightBox>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="updateBoxBtn" EventName="Click" />
        </Triggers>
    </asp:UpdatePanel>
    <script src="js/libs/handlebars/handlebars.js"></script>
    <script src="js/libs/jquery.easing.js"></script>
    <script src="js/waterfall.js"></script>
    <script type="text/javascript">
        var isrefresh = false;
        var isshowlb = false;
        var fbwidth = "";
        function OpenImageBox(fwidth) {
            isshowlb = true;
            fbwidth = fwidth;
        }

        jQuery.fn.outerHTML = function (s) {
            return s
        ? this.before(s).remove()
        : jQuery("<p>").append(this.eq(0).clone()).html();
        };

        $(function () {
            

            
        });

        function InCartContainer(ele) {
            var parent = ele.parent();
            if (parent.is("body")) {
                return false;
            } else if (parent.attr("class") == "cartcontainer") {
                return true;
            } else {
                return InCartContainer(parent);
            }
        }

        function pageLoad(sender, args) {
            $("#header").css("display", "none");
            $("#content").css("padding", "0px");
            $("html").css("height", "100%");
            $("form").css("height", "100%");
            $("#layout").parent().css("height", "100%");
            // $("#cart").parent().css("height", "100%");
           // $("body").css("height", "100%");
            $("#main").css("height", "100%");
            $("#main").css("background", "transparent");
            $("#wrapper").css("height", "100%");
            $("#wrapper2").css("height", "100%");
            $("#wrapper3").css("height", "100%");
            $("#content").css("height", "100%");
            $("#content").parent().css("height", "100%");
            $("#sidebar").parent().css("display", "none");
            $("#footer").css("display", "none");

            if ($.trim($(".totalcost").text()) != "") {
                $(".totalitems").text($.trim($(".totalcost").text()) + ", " + $(".list tbody > tr").length.toString() + " items");
            } else {
                $(".totalitems").text("");
            }

            if ($("#cart").parent().width() > 0) {
                $("#cart").css("display", "");
                $("#cart").parent().css("display", "");
                $("#cart").parent().css("width", "400px");
                $('#waterfall').waterfall('resize');
            }

            $(".opquantity").change(function () { $("#updateBtn").trigger("click"); });
            if (isshowlb) {
                var lightBox = $find('<%= OpenImageLightBox.ClientID %>');
                lightBox.show();
                var src = $("#detail").find(".dimg").eq(0).attr("src");
                $('#fbcomments').html(' <fb:comments href="' + src.replace("localhost", "127.0.0.1") + '" width="' + fbwidth + '" numposts="5" colorscheme="light"></fb:comments>');
                if (typeof FB !== 'undefined') {
                    FB.XFBML.parse(document.getElementById('fbcomments'));
                }

                $('#fblike').html('<fb:like href="' + src.replace("localhost", "127.0.0.1") + '" layout="button_count" show_faces="false" width="65" action="like" font="segoe ui" colorscheme="light" />');
                if (typeof FB !== 'undefined') {
                    FB.XFBML.parse(document.getElementById('fblike'));
                }

                $("#addtocart").click(function () {
                    var pname = $.trim($("#detail").find(".dname").eq(0).text());
                    var pprice = $.trim($("#detail").find(".dprice").eq(0).text());
                    var pid = $.trim($("#detail").find(".did").eq(0).text());
                    $("#pid").val(pid);
                    $("#pname").val(pname);
                    $("#pprice").val(pprice);
                    $("#submitBtn").trigger("click");
                    lightBox.hide();
                });
            }
            isshowlb = false;
            fbwidth = "";
            $("#sc_opener").unbind("click");
            $("#sc_opener").click(function (event) {
                if ($(this).hasClass("hpcExpand")) {
                    $(this).addClass("hpcCollapse");
                    $(this).removeClass("hpcExpand");
                    $(this).css("background-position", "-96px -53px");
                    $("#cart").css("display", "");
                    $("#cart").parent().css("display", "");
                    $("#cart").parent().css("width", "400px");
                    $(this).attr("title", "Collapse");
                } else {

                    $(this).removeClass("hpcCollapse");
                    $(this).addClass("hpcExpand");
                    $(this).css("background-position", "-128px -53px");
                    $("#cart").css("display", "none");
                    $("#cart").parent().css("display", "none");
                    $("#cart").parent().css("width", "0px");
                    $(this).attr("title", "Expand");
                }
                event.stopPropagation();
            });
            $("#sc_opener").unbind("mouseup");
            $("#sc_opener").mouseup(function () {
                return false;
            });
            $("#cartsummary").unbind("mouseup");
            $("#cartsummary").mouseup(function () {
                return false;
            });
            $("#backbtn").unbind("click");
            $("#backbtn").click(function () { $("#detail").css("display", "none"); });
            $("#orderbtn").unbind("click");
            $("#orderbtn").click(function () {
                $("#detail").css("display", "none");

                var pname = $.trim($("#detail").find(".dname").eq(0).text());
                var pprice = $.trim($("#detail").find(".dprice").eq(0).text());
                var pid = $.trim($("#detail").find(".did").eq(0).text());
                $("#pid").val(pid);
                $("#pname").val(pname);
                $("#pprice").val(pprice);
                $("#submitBtn").trigger("click");
            });
            $(this).unbind("mouseup");
            $(this).mouseup(function (login) {
                if (!InCartContainer($(login.target))) {
                    $("#sc_opener").removeClass("hpcCollapse");
                    $("#sc_opener").addClass("hpcExpand");
                    $("#sc_opener").css("background-position", "-128px -53px");
                    $("#cart").css("display", "none");
                    $("#cart").parent().css("display", "none");
                    $("#cart").parent().css("width", "0px");
                    $("#sc_opener").attr("title", "Expand");
                }
            });
            $("#cartsummary").unbind("click");
            $("#cartsummary").click(function (event) {
                //alert($("#sc_opener").hasClass("hpcExpand"));
                if ($("#sc_opener").hasClass("hpcExpand")) {
                    $("#sc_opener").addClass("hpcCollapse");
                    $("#sc_opener").removeClass("hpcExpand");
                    $("#sc_opener").css("background-position", "-96px -53px");
                    $("#cart").css("display", "");
                    $("#cart").parent().css("display", "");
                    $("#cart").parent().css("width", "400px");
                    $("#sc_opener").attr("title", "Collapse");
                } else {

                    $("#sc_opener").removeClass("hpcCollapse");
                    $("#sc_opener").addClass("hpcExpand");
                    $("#sc_opener").css("background-position", "-128px -53px");
                    $("#cart").css("display", "none");
                    $("#cart").parent().css("display", "none");
                    $("#cart").parent().css("width", "0px");
                    $("#sc_opener").attr("title", "Expand");
                }
                event.stopPropagation();
            });
        }

        function refreshFaceBook() {
            $(".witem").each(function () {
                var img = $(this).find("img").eq(0);
                var item = $(this);
                $.ajax({
                    type: "GET",
                    url: "https://graph.facebook.com/?ids=" + img.attr("src").replace("localhost", "127.0.0.1"),
                    contentType: "application/json;charset=utf-8",
                    data: null,
                    processData: false,
                    dataType: "json",
                    success: function (result) {
                        for (var prop in result) {
                            if (result[prop].shares != undefined) {
                                item.find(".fblike").eq(0).text(result[prop].shares.toString());
                            } else {
                                item.find(".fblike").eq(0).text("0");
                            }
                            if (result[prop].comments != undefined) {
                                item.find(".fbcomment").eq(0).text(result[prop].comments.toString());
                            } else {
                                item.find(".fbcomment").eq(0).text("0");
                            }
                            break;
                        }

                    },
                    error: function (textStatus, str, err) {

                    }
                });
            });

        }

        function refreshWaterfall() {
            var wf = $('#waterfall').waterfall({
                itemCls: 'witem',
                colWidth: 250,
                gutterWidth: 10,
                gutterHeight: 30,
                checkImagesLoaded: false,
                path: function (page) {
                    return '';
                },
                callbacks: {
                    loadingError: function ($message, xhr) {
                        //$message.html('Data load faild, please try again later.');
                    }
                }
            });
            $('#waterfall').waterfall('removeItems', $('.witem:lt(' + $(".witem").length + ')'), function () { });
            $(".himage").each(
             function () {
                 $(this).css("display", "");
                 $(this).parent().css("display", "");
             });

            $(".himage").each(function () {
                var img = new Image();
                var height = $(this).height();
                var html = $(this).outerHTML();
                var src = $(this).attr("src");
                var name = $.trim($(this).parent().find(".hname").eq(0).text());
                var price = $.trim($(this).parent().find(".hprice").eq(0).text());
                var pid = $.trim($(this).parent().find(".hid").eq(0).text());
                var aa = $(this);
                img.onerror = function () {
                    aa.css("display", "none");
                    aa.parent().css("display", "none");
                }
                img.onload = function () {
                    var fh = Math.ceil(name.length / 33) * 20;
                    var bh = Math.ceil(name.length / 33) * 20 + 30;
                    var item = $('<div class="witem" style="cursor:pointer;height:' + (aa.height() + bh) + 'px;width:250px; background: #2b8af8; color: #fff;"></div>');



                    var bottom = $('<div style="position:absolute;top:' + aa.height() + 'px;width:250px;height:' + bh.toString() + 'px;background-color:gray;vertical-align:middle;overflow:hidden"></div>');
                    var comments = $('<a class="fbcomment" href="#" style="position:absolute;top:' + (fh + 10).toString() + 'px;left:5px;background-repeat: no-repeat;background-position: 0 0;background-image:  url(../images/icon-comments.png);padding-left:15px;color:#fff;aligh:right">0</a>');
                    var hearts = $('<a class="fblike" href="#" style="position:absolute;top:' + (fh + 10).toString() + 'px;left:45px;background-repeat: no-repeat;background-position: 0 0;background-image:  url(../images/icon-hearts.png);padding-left:15px;color:#fff;aligh:right">0</a>');
                    var ename = $('<span style="position:absolute;top:2px;left:2px;width:200px;">' + name + '</span>');
                    var hid = $('<span style="display:none;" class="pid">' + pid + '</span>');

                    var ah = (bh - 32) / 2;
                    var addbtn = $('<img src="images/plus-outline.png" style="width:32px;height:32px;position:absolute;top:' + ah + 'px;left:210px;"/>');
                    var img = $('<img src="' + src + '" style="width:250px;overflow:hidden"></img>');

                    var eprice = $('<div style="position:absolute;width:50px;height:15px;background-color:gray;top:0px;">' + price + '</div>');

                    bottom.append(ename);
                    bottom.append(comments);
                    bottom.append(hearts);
                    bottom.append(addbtn);

                    item.append(img);
                    item.append(bottom);
                    item.append(eprice);

                    comments.mouseover(function () { $(this).css("background-position", "0% 75%"); });
                    comments.mouseout(function () { $(this).css("background-position", "0 0"); });

                    hearts.mouseover(function () { $(this).css("background-position", "0% 120%"); });
                    hearts.mouseout(function () { $(this).css("background-position", "0 0"); });
                    addbtn.click(function (e) {
                        $("#pid").val(pid);
                        $("#pname").val(name);
                        $("#pprice").val(price);
                        $("#submitBtn").trigger("click");
                        e.stopPropagation();
                    });
                    item.click(function () {


                        $("#detail").find(".dname").eq(0).text(name);
                        $("#detail").find(".dprice").eq(0).text(price);
                        $("#detail").find(".did").eq(0).text(pid);
                        $("#detail").find(".dimg").eq(0).attr("src", src);


                        //$(".lbimage").attr("src", src);
                        //alert($(".rltbActiveImage").attr("src"));
                        $("#pid").val(pid);
                        $("#updateBoxBtn").trigger("click");
                        //$("#detail").css("display", "");
                        //$("#detail").toggle("slide");
                    });

                    $('#waterfall').waterfall('append', item, function () {

                    });
                    $(window).trigger("resize");
                    //wf._resize();
                    aa.css("display", "none");
                    aa.parent().css("display", "none");
                    refreshFaceBook();
                }
                img.src = $(this).attr("src");

            });
        }
    </script>
    <input type="hidden" runat="server" id="pid" clientidmode="Static" />
    <input type="hidden" runat="server" id="pname" clientidmode="Static" />
    <input type="hidden" runat="server" id="pprice" clientidmode="Static" />
    <input type="hidden" runat="server" id="pwidth" clientidmode="Static" />
    <asp:Label ID="lblNo" runat="server" Visible="false"></asp:Label>
</asp:Content>
