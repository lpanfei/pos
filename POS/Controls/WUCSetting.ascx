﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCSetting.ascx.cs"
    Inherits="POS.Controls.WUCSetting" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    Sys.Application.add_load(settingLoad);
    function settingLoad() {
        $("#syncbtn").unbind("click");
        $("#syncbtn").click(function () {
            $('#btnUpload').trigger('click');

        });
        $("#testbtn").unbind("click");
        $("#testbtn").click(function () {
            $('#btnTestConnection').trigger('click');
        });
    }

    function openWindow(msg) {
        $("#lblLog").html(msg);
        var radwindow = $find('<%=SettingRadWindow1.ClientID %>');
        radwindow.show();
    }
</script>
<style>
    .settingform .tr-header
    {
        border-bottom: 1px solid #eee;
        font-size: 13px;
        font-weight: bold;
    }
    
    .settingform .tr-header td
    {
        padding-top: 25px;
        padding-bottom: 10px;
    }
    .displaynone
    {
        display: none;
    }
</style>
<div class="mainheader">
    <div class="contextual">
        <div style="height: 10px; width: 85px">
        </div>
    </div>
    <div class="subheader">
        <h2 class="pagetitle icon-settings">
            Settings</h2>
    </div>
</div>
<div style="max-width: 800px; height: auto;" id="modal-form">
    <div class="md-content">
        <div >
            <table>
                <tr>
                    <td align="left" style="width:300px;">
                        <table class="form-table settingform"  style="margin-left:0px;width:310px;">
                            <tr class="tr-header">
                                <td colspan="2" style="padding-top: 5px;">
                                    <label>
                                        Discount Value Settings</label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Retailer:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtRetailer" runat="server" Width="150"></asp:TextBox>
                                    %
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Wholesaler:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtWholesaler" runat="server" Width="150"></asp:TextBox>
                                    %
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Export:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtExport" runat="server" Width="150"></asp:TextBox>
                                    %
                                </td>
                            </tr>
                            <tr class="tr-header">
                                <td colspan="2">
                                    <label>
                                        Server Database Connection Settings</label>
                                    <a href="#" id="testbtn" style="margin-right: 0px; float: right" title="Testing Connection">
                                        <i class="c-icon-test"></i></a><a href="#" id="syncbtn" style="margin-right: 10px;
                                            float: right" title="Immediate Sync"><i class="c-icon-sync"></i></a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Server Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtServerName" runat="server" Width="150"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Database Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDatabaseName" runat="server" Width="150"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Username:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUsername" runat="server" Style="float: left; margin-right: 10px;" Width="150"></asp:TextBox>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Button ID="btnUpload" runat="server" Text="Immediate Sync" Width="0px" Height="0px"
                                                class="btn btn-large btn-block btn-info" OnClick="btnUpload_Click" ClientIDMode="Static" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Password:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPassword" runat="server" Style="float: left; margin-right: 10px;" Width="150"></asp:TextBox>
                                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:Button ID="btnTestConnection" runat="server" Text="Testing Connection" Width="0px"
                                                Height="0px" class="btn btn-large btn-block btn-info displaynone" OnClick="btnTestConnection_Click"
                                                ClientIDMode="Static" />
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                    <td style="width:50px"></td>
                    <td valign="top">
                        <table  class="form-table settingform">
                        <tr class="tr-header">
                                <td style="padding-top:0px">
                                    <label>
                                        Sync Time Settings</label>
                                </td>
                                <td align="right">
                                    <%--<a href="#" id="addfiled"><i class="c-icon-add"></i></a>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Sync Time:
                                </td>
                                <td>
                                    <asp:DropDownList ID="dropUploadTime" runat="server" Width="150">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="tr-header">
                                <td>
                                    <label>
                                        Other Settings</label>
                                </td>
                                <td align="right">
                                    <%--<a href="#" id="addfiled"><i class="c-icon-add"></i></a>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Invoice Number Prefix:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPrefix" runat="server" Width="150"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    GST Amount:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtGST" runat="server" Width="150"></asp:TextBox>%
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Invoice Header:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtInvoiceHeader" runat="server" Rows="6" Height="75" TextMode="MultiLine" Width="150"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Invoice Footer:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtInvoiceFooter" runat="server" Rows="6"  Height="75" TextMode="MultiLine" Width="150"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="md-footer">
            <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Button ID="btnSave" runat="server" Text="Save All" OnClick="btnSave_Click" class="btn btn-large btn-block btn-info btn-center"
                        Height="30" Width="80" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
<eid:RadWindow runat="server" ID="SettingRadWindow1" Skin="Metro" Overlay="true"
    Modal="true" Title="Sync Data" VisibleStatusbar="false" AutoSize="false" KeepInScreenBounds="true"
    Behaviors="Close" MinWidth="600" MinHeight="400">
    <ContentTemplate>
        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Label ID="lblLog" runat="server" ClientIDMode="Static" Text="Start data sync..."></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </ContentTemplate>
</eid:RadWindow>
