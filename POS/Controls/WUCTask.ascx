﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCTask.ascx.cs" Inherits="POS.Controls.WUCTask" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<div class="mainheader">
        <div class="contextual">
            
        </div>
        <div class="subheader">
            <h2 class="icon-categories pagetitle">
                <asp:Literal ID="ltlPageTitle" Text="Tasks" runat="server"></asp:Literal></h2>
        </div>
    </div>
    <div>
 
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <AjaxSettings>
            <telerik:AjaxSetting AjaxControlID="RadGrid1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadGrid1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </AjaxSettings>
    </telerik:RadAjaxManager>
    <asp:UpdatePanel UpdateMode="Conditional" runat="server">
    <ContentTemplate>
    
   
    <telerik:RadGrid ID="RadGrid1" ShowStatusBar="true"
         runat="server" AutoGenerateColumns="False" PageSize="10" Skin="MetroTouch"
        AllowSorting="True" AllowMultiRowSelection="False" AllowPaging="True" GridLines="None" OnItemCommand="RadGrid1_ItemCommand" OnDetailTableDataBind="RadGrid1_DetailTableDataBind" OnNeedDataSource="RadGrid1_NeedDataSource">
        <PagerStyle Mode="NumericPages"></PagerStyle>
        <MasterTableView EnableHierarchyExpandAll="true" DataKeyNames="inv_Id" AllowMultiColumnSorting="True" HierarchyDefaultExpanded="true" CommandItemDisplay="Top">
            <DetailTables>
                <telerik:GridTableView EnableHierarchyExpandAll="true" DataKeyNames="ip_ID" Width="100%"
                    runat="server"  Name="Orders">
                    <ParentTableRelation>
                        <telerik:GridRelationFields DetailKeyField="ip_InvoiceID" MasterKeyField="inv_Id">
                        </telerik:GridRelationFields>
                    </ParentTableRelation>
                    <Columns>
                        <telerik:GridBoundColumn SortExpression="ip_Description" HeaderText="Product Name" HeaderButtonType="TextButton"
                            DataField="ip_Description" UniqueName="ip_Description">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="ip_Price" HeaderText="Price" HeaderButtonType="TextButton"
                            DataField="ip_Price" UniqueName="ip_Price" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn SortExpression="ip_Quantity" HeaderText="Quantity" HeaderButtonType="TextButton"
                            DataField="ip_Quantity" UniqueName="ip_Quantity">
                        </telerik:GridBoundColumn>
                         <telerik:GridBoundColumn SortExpression="ip_SubTotal" HeaderText="Sub Total" HeaderButtonType="TextButton"
                            DataField="ip_SubTotal" UniqueName="ip_SubTotal" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridCheckBoxColumn DataField="IsComplete" UniqueName="IsComplete" HeaderButtonType="None" ReadOnly="false">
                        </telerik:GridCheckBoxColumn>
                        <telerik:GridButtonColumn ButtonType="LinkButton" CommandName="Complete" Text="Complete"></telerik:GridButtonColumn>
                    </Columns>
                    <SortExpressions>
                        <telerik:GridSortExpression FieldName="ip_Description"></telerik:GridSortExpression>
                    </SortExpressions>
                </telerik:GridTableView>
            </DetailTables>
            <Columns>
                <telerik:GridBoundColumn SortExpression="TableNo" HeaderText="Table No" HeaderButtonType="TextButton"
                    DataField="TableNo" UniqueName="TableNo">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="inv_Total" HeaderText="Total Cost" HeaderButtonType="TextButton"
                    DataField="inv_Total" UniqueName="inv_Total" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn SortExpression="inv_CreateDate" HeaderText="Order Time" HeaderButtonType="TextButton"
                    DataField="inv_CreateDate" UniqueName="inv_CreateDate" DataFormatString="{0:dd/MM/yyyy hh:mm:ss}">
                </telerik:GridBoundColumn>
            </Columns>
            <SortExpressions>
                <telerik:GridSortExpression FieldName="inv_CreateDate"></telerik:GridSortExpression>
            </SortExpressions>
        </MasterTableView>
    </telerik:RadGrid>
    <%-- <asp:Timer Interval="3000" runat="server" ID="timer">
      </asp:Timer>--%>
     </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        Sys.Application.add_load(taskLoad);
        function taskLoad() {
            $(".rgCommandTable").find("td[align='left']").eq(0).css("display", "none");
        }
    </script>
    </div>

