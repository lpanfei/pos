﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS.Controls
{
    public partial class WUCWarranty : ControlBase
    {
        List<Warranty> Warranties
        {
            get
            {
                return GlobalCache.Warranties;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeDropdowns();
                Bind();
                txtDate.Attributes.Add("onFocus", "WdatePicker({lang:'en',dateFmt:'dd-MM-yyyy'})");
            }
        }

        public override void Init()
        {
            if (formpanel.Visible)
            {
                BackToList(null, null);
            }
            InitializeDropdowns();
            Bind();
        }

        void InitializeDropdowns()
        {
            //POSService.POSService service = new POSService.POSService();
            GlobalCache.RefreshInvoices(0, "", null, null, false);
            dropInvoice.DataSource = GlobalCache.Invoices;
            dropInvoice.DataBind();
            if (GlobalCache.Invoices.Any())
            {
                dropInvoice.SelectedIndex = 0;
                InvoiceChanged(null, null);
            }
        }

        protected void InvoiceChanged(object sender, EventArgs args)
        {
            POSService.POSService service = new POSService.POSService();
            dropWarrantyProduct.DataSource = service.GetInvoiceProducts(Convert.ToInt32(dropInvoice.SelectedValue));
            dropWarrantyProduct.DataBind();
        }

        void Bind()
        {
            this.AspNetPager1.RecordCount = Warranties.Count;
            rptList.DataSource = null;
            rptList.DataSource = Warranties.TakeFrom(0, this.AspNetPager1.PageSize);
            rptList.DataBind();
            this.AspNetPager1.CurrentPageIndex = 1;
            this.AspNetPager1.GoToPage(this.AspNetPager1.CurrentPageIndex);
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            rptList.DataSource = null;
            rptList.DataSource = Warranties.TakeFrom((currentPage - 1) * pageSize, pageSize);
            rptList.DataBind();
        }

        protected void AddWarranty(object sender, EventArgs e)
        {
            formbuttons.Visible = true;
            listbuttons.Visible = false;
            formpanel.Visible = true;
            listpanel.Visible = false;
            ltlPageTitle.Text = "Warranty / Add";
            dropInvoice.SelectedIndex = 0;
            //InvoiceChanged(null, null);
            txtComment.Text = "";
            txtDate.Text = "";
            txtQty.Text = "";
            txtResolution.Text = "";
            lblId.Text = "0";
            inpWarrantyProductID.Attributes["value"] = "-1";
        }

        protected void BackToList(object sender, EventArgs e)
        {
            listbuttons.Visible = true;
            formbuttons.Visible = false;
            formpanel.Visible = false;
            listpanel.Visible = true;
            ltlPageTitle.Text = "Warranty List";
            inpWarrantyProductID.Attributes["value"] = "-1";
            Bind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            POSService.POSService service = new POSService.POSService();
            Warranty w = new Warranty()
            {
                InvoiceID = Convert.ToInt32(dropInvoice.SelectedValue),
                ID = Convert.ToInt32(lblId.Text),
                ProductID = Convert.ToInt32(dropWarrantyProduct.SelectedValue),
                Comment = txtComment.Text,
                Quantity = string.IsNullOrEmpty(txtQty.Text) ? 0 : Convert.ToInt32(txtQty.Text),
                Resolution = txtResolution.Text,
                ClientID = this.UserClientID,
                UserID = GlobalCache.CurrentUser.usr_ID,
                StoreID = this.UserStoreID
            };
            w.Invoice = GlobalCache.Invoices.FirstOrDefault(i => i.inv_Id == w.InvoiceID);
            w.Product = GlobalCache.Products.FirstOrDefault(p => p.pro_Id == w.ProductID);
            if (w.ID > 0)
            {
                w.ServerID = Warranties.FirstOrDefault(wa => wa.ID == w.ID).ServerID;
            }
            if (string.IsNullOrEmpty(txtDate.Text))
            {
                w.Date = null;
            }
            else
            {
                try
                {
                    w.Date = DateTime.Parse(txtDate.Text);
                }
                catch
                {
                    w.Date = GetDateTime(txtDate.Text);
                }
            }
            w.LastUpdatedTime = DateTime.Now;
            service.AddWarranty(w);
            GlobalCache.RefreshWarranties();
            BackToList(null, null);
        }

        DateTime GetDateTime(string s)
        {
            string[] str = s.Split('-');
            int year = Convert.ToInt32(str[2]);
            int month = Convert.ToInt32(str[1]);
            int day = Convert.ToInt32(str[0]);
            return new DateTime(year, month, day, 0, 0, 0);
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemEdit")
            {
                RepeaterItem item = e.Item;
                formbuttons.Visible = true;
                listbuttons.Visible = false;
                formpanel.Visible = true;
                listpanel.Visible = false;
                ltlPageTitle.Text = "Warranty / Edit";
                dropInvoice.SelectedValue = (item.FindControl("lblInvoiceID") as Label).Text;
                txtComment.Text = (item.FindControl("lblComment") as Label).Text;
                txtDate.Text = (item.FindControl("lblDate") as Label).Text;
                txtQty.Text = (item.FindControl("lblQty") as Label).Text;
                txtResolution.Text = (item.FindControl("lblResolution") as Label).Text;
                lblId.Text = (item.FindControl("lblId") as Label).Text;
                inpWarrantyProductID.Attributes["value"] = (item.FindControl("lblProductID") as Label).Text;
            }
            else if (e.CommandName == "ItemDel")
            {
                delwarrantyid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "deleteWarranty");
            }
        }

        protected void Delete(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delwarrantyid.Attributes["value"]);
            POSService.POSService service = new POSService.POSService();
            Warranty w = Warranties.FirstOrDefault(wa => wa.ID == id);
            w.IsDeleted = true;
            w.UserID = GlobalCache.CurrentUser.usr_ID;
            w.LastUpdatedTime = DateTime.Now;
            service.UpdateWarranty(w);
            GlobalCache.RefreshWarranties();
            //service.DeleteWarranty(id);
            Bind();
        }

    }
}