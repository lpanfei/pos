﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCProduct.ascx.cs" Inherits="POS.Controls.WUCProduct" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
    <script type="text/javascript">
        var previousproduct = null;
        Sys.Application.add_load(productLoad);
        function productLoad(sender, args) {
            $("#addfiled").unbind("click");
            $("#addfiled").click(function () { $("#btnAddField").trigger("click"); });

            $(".productlist tr").unbind("click");
            $(".productlist tr").click(function () {

                var current = $(this).find(".buttons div").eq(0);
                if ($(previousproduct).is(current))
                    return;
                current.toggle("slow");
                if (previousproduct != null) {
                    $(previousproduct).toggle("slow", function () { });
                }
                previousproduct = current;
            }
            );

            $("#addpicture").unbind("click");
            $("#addpicture").click(function () { $('.ruFileInput').trigger('click'); });

        }
        function deleteProduct(sender) {
            if (sender) {
                $("#delProductBtn").trigger("click");
            } else {
                $("#delproductid").val("-1");
            }
        }

        function OnClientValidationFailed(sender, args) {
            var fileExtention = args.get_fileName().substring(args.get_fileName().lastIndexOf('.') + 1, args.get_fileName().length);
            if (args.get_fileName().lastIndexOf('.') != -1) {//this checks if the extension is correct
                if (sender.get_allowedFileExtensions().indexOf(fileExtention) == -1) {
                    radalert("Wrong Extension! You can only upload .jpg/.jpeg/.gif/.png/.bmp file of type.", 400, 100, "ERROR");
                }
                else {
                    radalert("Wrong file size! You can only update picture less than 100kb", 400, 100, "ERROR");
                }
            }
            else {
                radalert("not correct extension!", 300, 100, "ERROR");
            }
            var index = $(args.get_row()).index();
            sender.deleteFileInputAt(index);
        }

        function OnClientFileUploadFailed(sender, args) {
            if (args.get_message() == "error") {
                args.set_handled(true);
                radalert("Upload Failed! " + args.get_message(), 400, 200, "ERROR");
                var index = $(args.get_row()).index();
                sender.deleteFileInputAt(index);
            }
        }

        function FileUploaded(sender, args) {
            $("#refreshImageBtn").trigger("click");
        }

    </script>
    <style type="text/css">
    .displaynone
    {
        display:none;
    }
   
        input[type=button]
        {
            cursor: pointer;
        }
        
        input[type=file]
        {
            cursor: pointer;
        }
          #ftable .tr-header
        {
            
            font-size: 13px;
            font-weight: bold;
        }

        .padding5
        {
            padding-top: 5px;
        }
        
       
         #ftable .tr-header td
        {
            padding-top: 25px;
            padding-bottom: 10px;
        }
        #ftable
        {
            border-collapse: collapse;
            margin: 0 auto;
        }
        .pageform input[type=text]
        {
            height: 30px;
            width: 280px;
            padding-left: 10px;
        }
        
        .pageform
        {
            margin:0 auto;
        }
        
        .btn-add
        {
            cursor:pointer;
        }

     
 
     .RadUpload .ruBrowse {
          color: white;
          background-color: #25a0da;
          background-image: -webkit-linear-gradient( top, #3894E7 0%, #25a0da 100% );
          background-image: -moz-linear-gradient( top, #3894E7 0%, #25a0da 100% );
          background-image: -ms-linear-gradient( top, #3894E7 0%, #25a0da 100% );
          background-image: -o-linear-gradient( top, #3894E7 0%, #25a0da 100% );
          background-image: linear-gradient( top, #3894E7 0%, #25a0da 100% );
          filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3894E7', endColorstr='#25a0da',GradientType=0 );
     }
     
     .ruButtonHover
     {
          color: white;
     }
     
     .ruRemove
     {
         margin-top:10px;
     }   
     
     .cursorpointer
     {
         cursor:pointer;
     }

    </style>

    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="mainheader">
                <div class="contextual">
                    <div style="height: 10px; width: 85px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="addLink"
                            Text="Add" OnClick="AddProduct" />
                    </div>
                </div>
                <div class="subheader">
                    <h3 class="icon-products pagetitle" id="pagetitle">
                        <asp:Literal ID="ltlPageTitle" Text="Products" runat="server"></asp:Literal>
                    </h3>
                </div>
            </div>
            <asp:Panel CssClass="pagelist" runat="server" ID="pagelist">
                <div>
                    <div style="float: left">
                        <asp:DropDownList ID="dropCategory" runat="server" Height="30" DataTextField="cat_Name"
                            DataValueField="cat_Id">
                        </asp:DropDownList>
                    </div>
                    <div style="float: left; margin-left: 2px;">
                        <asp:DropDownList ID="dropField" runat="server" Height="30">
                            <asp:ListItem Text="Name of Product" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Serial No" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div style="float: left; margin-left: 2px; margin-top: -1px;margin-right:15px">
                        <asp:TextBox ID="txtKey" runat="server" CssClass="searchbox" Height="28"></asp:TextBox></div>
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search"
                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info" Width="75"
                        Height="30" Style="margin-left: 15px;" />
                </div>
                <br />
                <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand">
                    <HeaderTemplate>
                        <table class="list productlist">
                            <thead>
                                <tr>
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Category
                                    </th>
                                    <th>
                                        Name of Product
                                    </th>
                                    <th>
                                        Supplier
                                    </th>
                                    <th>
                                        Serial No
                                    </th>
                                    <th>
                                        Price
                                    </th>
                                    <th>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td align="center">
                                <%# Container.ItemIndex+1 %>
                            </td>
                            <td align="center">
                                <%# Eval("cat_Name")%>
                            </td>
                            <td align="center">
                                <%# Eval("pro_Name") %>
                            </td>
                            <td align="center">
                                <%# Eval("SupplierName") %>
                            </td>
                            <td align="center">
                                <%# Eval("pro_No") %>
                            </td>
                            <td align="center">
                                <%# Convert.ToDecimal(Eval("pro_Price")).ToString("0.00") %>
                            </td>
                            <td class="buttons">
                                <div style="display: none;">
                                    <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CommandName="ItemEdit" CommandArgument='<%# Eval("pro_Id") %>'
                                        class="icon icon-edit" rel="nofollow"></asp:LinkButton>
                                    &nbsp;&nbsp;
                                    <asp:LinkButton ID="lblDel" runat="server" Text="Delete" CommandName="ItemDel" CommandArgument='<%# Eval("pro_Id") %>'
                                        class="icon icon-del" rel="nofollow"></asp:LinkButton></div>
                                <input type="hidden" value='<%# Eval("pro_Id") %>' id="proid" />
                            </td>
                           
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody> </table>
                    </FooterTemplate>
                </asp:Repeater>
                <webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged" PageSize="10" CssClass="aspnetpager">
                </webdiyer:AspNetPager>
            </asp:Panel>
            <asp:Panel CssClass="pageform" runat="server" ID="pageform">
                <div>
                    <div>
                        <asp:UpdatePanel runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <table id="ftable">
                                    <tr class="tr-header">
                                        <td colspan="2" class="padding5">
                                            Basic Info.<a href="#" id="addfiled" style="float: right;" title="Add new fields"><i
                                                class="c-icon-add"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 150px;" class="padding5">
                                            Category:
                                        </td>
                                        <td class="padding5">
                                            <asp:DropDownList Height="30" ID="dropCategory2" runat="server" Width="290" DataTextField="cat_Name"
                                                DataValueField="cat_Id">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 150px;" class="padding5">
                                            Supplier:
                                        </td>
                                        <td class="padding5">
                                            <asp:DropDownList Height="30" ID="dropSupplier" runat="server" Width="290" DataTextField="Name"
                                                DataValueField="ID">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding5">
                                            Name of Product:
                                        </td>
                                        <td class="padding5">
                                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding5">
                                            Serial No:
                                        </td>
                                        <td class="padding5">
                                            <asp:TextBox ID="txtNo" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding5">
                                            Price:
                                        </td>
                                        <td class="padding5">
                                            <asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding5">
                                            Cost:
                                        </td>
                                        <td class="padding5">
                                            <asp:TextBox ID="txtCost" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td class="padding5">
                                            Description:
                                        </td>
                                        <td class="padding5">
                                            <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Repeater ID="rptDynamicFields" runat="server" OnItemCommand="rptDynamicFields_ItemCommand">
                                                <ItemTemplate>
                                                    <tr>
                                                        <td class="padding5">
                                                            <asp:TextBox ID="lblName" runat="server" Text='<%# Eval("fie_Name") %>' Width="127"></asp:TextBox>:
                                                        </td>
                                                        <td class="padding5">
                                                            <asp:TextBox ID="txtValue" runat="server" Text='<%# Eval("fie_Value") %>'></asp:TextBox>
                                                            <asp:LinkButton ID="lbtnDel" runat="server" Text="×" CommandName="ItemDel" CommandArgument='<%# Eval("fie_Id") %>'></asp:LinkButton><br />
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="padding5">
                                            Related Products:
                                        </td>
                                        <td class="padding5">
                                            <div style="width: 300px; height: 120px; overflow: auto;">
                                                <asp:Repeater ID="rptRelateProduct" runat="server">
                                                    <HeaderTemplate>
                                                        <table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <tr>
                                                            <td>
                                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                                <asp:Label ID="lblID" runat="server" Text='<%# Eval("pro_ID") %>' Visible="false"></asp:Label>
                                                                <asp:Label ID="lblName" runat="server" Text='<%# Eval("pro_Name") %>'></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        </table>
                                                    </FooterTemplate>
                                                </asp:Repeater>
                                                <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                                                </asp:CheckBoxList>
                                            </div>
                                        </td>
                                    </tr>
                                    
                                    <tr class="tr-header">
                                        <td >
                                            Pictures
                                        </td>
                                        <td>
                                        <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="dlPictureList_ItemCommand">
                                            <HeaderTemplate><div class="RadUpload RadAsyncUpload RadUpload_Metro"><ul class="ruInputs"></HeaderTemplate>
                                            <ItemTemplate>
                                            <li>
                                            <span class="ruFileWrap ruStyled" style="vertical-align:middle"><span class="ruUploadProgress ruUploadSuccess"><%# Eval("Name") %></span></span>
                                            <asp:Image ID="img" ImageUrl='<%# Eval("Url") %>' CssClass="cursorpointer" runat="server" Height="50" ImageAlign="Middle" />
                                            <span style="vertical-align:middle"><asp:Button Text="Remove" CssClass="ruButton ruRemove" runat="server" CommandName="ItemDel" /></span>
                                            <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblTemp" runat="server" Text='<%# Eval("IsTemp") %>' Visible="false"></asp:Label>
                                            </li>
                                            </ItemTemplate>
                                            <FooterTemplate></ul></div></FooterTemplate>
                                         </asp:Repeater>
                                        <eid:RadAsyncUpload runat="server" Skin="Metro" ID="fileupload" OnClientValidationFailed="OnClientValidationFailed" OnClientFileUploadFailed="OnClientFileUploadFailed"
                                              AllowedFileExtensions=".jpeg,.jpg,.png,.gif,.bmp,.JPG,.JPEG,.PNG,.GIF,.BMP" TargetFolder="Upload/Temp" MaxFileSize="102400" PostbackTriggers="refreshImageBtn" OnClientFileUploaded="FileUploaded" OnFileUploaded="ImageUploaded"  >
                                            </eid:RadAsyncUpload>
                                        </td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td colspan="2" align="center">
                                            <asp:Label ID="lblCategoryID" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblUploadFlag" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lblServerID" runat="server" Visible="false"></asp:Label>
                                            <input type="hidden" id="pid" runat="server"/>
                                        </td>
                                    </tr>
                                    <asp:Button ID="btnAddField"  runat="server" Text="Update" ClientIDMode="Static" OnClick="AddDynamicFields"
                                        CssClass="displaynone" />
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    <div>
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                            UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                            Height="30" Width="75" />
                    </div>
                </div>
            </asp:Panel>
            <input type="hidden" id="delproductid" runat="server" clientidmode="Static" value="-1" />
            <asp:Button ID="delProductBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
                OnClick="Delete" ClientIDMode="Static"></asp:Button>
                <asp:Button ID="refreshImageBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
                ClientIDMode="Static"></asp:Button>
            <div style="clear: both;">
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
