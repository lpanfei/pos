﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCCustomer.ascx.cs" Inherits="POS.Controls.WUCCustomer" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
    <script type="text/javascript">
        var previouscustomer = null;
        Sys.Application.add_load(customerLoad);
        function customerLoad(sender, args) {
            $(document).ready(function () {
                $(".customerlist tr").click(function () {

                    var current = $(this).find(".buttons div").eq(0);
                    if ($(previouscustomer).is(current))
                        return;
                    current.toggle("slow");
                    if (previouscustomer != null) {
                        $(previouscustomer).toggle("slow", function () { });
                    }
                    previouscustomer = current;
                }
                );
            });
        }
        function deleteCustomer(sender) {
            if (sender) {
                $("#delCustomerBtn").trigger("click");
            } else {
                $("#delcustomerid").val("-1");
            }
        }
    </script>
    <style type="text/css">
        input[type=submit]
        {
            cursor: pointer;
        }
         .displaynone
        {
            display:none;
        }
    </style>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="mainheader">
                <div class="contextual">
                    <asp:Panel ID="listbuttons" runat="server">
                        <div style="height: 10px; width: 75px">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="addLink"
                                Text="Add" OnClick="AddCustomer" />
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="formbuttons" runat="server" Visible="false">
                        <div style="height: 10px; width: 75px">
                            <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                                Text="Back" OnClick="BackToList" />
                        </div>
                    </asp:Panel>
                </div>
                <div class="subheader">
                    <h2 class="icon-customer pagetitle">
                        <asp:Literal ID="ltlPageTitle" Text="Customers" runat="server"></asp:Literal></h2>
                </div>
            </div>

                <asp:Panel runat="server" ID="listpanel">
                    <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand">
                        <HeaderTemplate>
                            <table class="list customerlist">
                                <thead>
                                    <tr>
                                        <th>
                                            No.
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Address
                                        </th>
                                        <th>
                                            Phone
                                        </th>
                                        <th>
                                            Mobile
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Country
                                        </th>
                                         <th>
                                            Wallet ID
                                        </th>
                                        <th>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr>
                                <td align="center">
                                    <%# Container.ItemIndex+1 %>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblPhone" runat="server" Text='<%# Eval("Phone")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblMobile" runat="server" Text='<%# Eval("Mobile")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email")%>'></asp:Label>
                                </td>
                                <td align="center">
                                    <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Country")%>'></asp:Label>
                                </td>
                                 <td align="center">
                                    <asp:Label ID="lblWalletID" runat="server" Text='<%# Eval("WalletID")%>'></asp:Label>
                                </td>
                                <td class="buttons">
                                    <div style="display: none;">
                                        <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandName="ItemEdit" CommandArgument='<%# Eval("ID") %>'
                                            class="icon icon-edit"></asp:LinkButton>
                                        &nbsp;&nbsp;
                                        <asp:LinkButton ID="lblDel" runat="server" Text="Delete" CommandName="ItemDel" CommandArgument='<%# Eval("ID") %>'
                                            class="icon icon-del"></asp:LinkButton></div>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </tbody> </table>
                        </FooterTemplate>
                    </asp:Repeater>
                    <webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged" PageSize="10"
                        CssClass="aspnetpager">
                    </webdiyer:AspNetPager>
                </asp:Panel>
                <asp:Panel runat="server" ID="formpanel" Visible="false">
                    <div>
                   
                        <div class="md-modal md-effect-1 md-show" id="modal-form">
                            <div class="md-content">
                                <h3>
                                    Customer Form</h3>
                                <div>
                                    <table class="form-table">
                                        <tr>
                                            <td class="form-label">
                                                Name:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                            </td>
                                            <td style="width:200px;">
                                                <asp:Label ID="txtNameMessage" runat="server" ForeColor="Red" >*</asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form-label">
                                                Country:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCountry" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form-label">
                                                Phone:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form-label">
                                                MobilePhone:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMobilePhone" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form-label">
                                                Email:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="form-label">
                                                Address:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                         <tr>
                                            <td class="form-label">
                                                Wallet ID:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtWalletID" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style="display: none;">
                                            <td colspan="2" align="center">
                                                <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="md-footer">
                                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                        UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                                        Width="75" Height="30" /></div>
                            </div>
                        </div>
                      
                    </div>
                </asp:Panel>
    <input type="hidden" id="delcustomerid" runat="server" clientidmode="Static" value="" />
    <asp:Button ID="delCustomerBtn" runat="server" Width="0" Height="0" CssClass="displaynone" OnClick="Delete" ClientIDMode="Static"></asp:Button>
        </ContentTemplate>
    </asp:UpdatePanel>