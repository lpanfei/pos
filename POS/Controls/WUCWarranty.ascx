﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCWarranty.ascx.cs"
    Inherits="POS.Controls.WUCWarranty" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    var previouswarranty = null;
    Sys.Application.add_load(warrantyLoad);
    function warrantyLoad() {
        $(".warrantylist tr").unbind("click");
        $(".warrantylist tr").click(function () {
            var current = $(this).find(".buttons div").eq(0);
            if ($(previouswarranty).is(current))
                return;
            current.toggle("slow");
            if (previouswarranty != null) {
                $(previouswarranty).toggle("slow", function () { });
            }
            previouswarranty = current;
        }
             );

        if ($("#inpWarrantyProductID").val() != "-1") {
            $("#dropWarrantyProduct").val($("#inpWarrantyProductID").val());
        }

    }

    function deleteWarranty(sender) {
        if (sender) {
            $("#delWarrantyBtn").trigger("click");
        } else {
            $("#delwarrantyid").val("-1");
        }
    }
</script>
<style type="text/css">
    input[type=submit]
    {
        cursor: pointer;
    }
    .displaynone
    {
        display: none;
    }
</style>
<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="mainheader">
            <div class="contextual">
                <asp:Panel ID="listbuttons" runat="server">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="addLink"
                            Text="Add" OnClick="AddWarranty" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="formbuttons" runat="server" Visible="false">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                            Text="Back" OnClick="BackToList" />
                    </div>
                </asp:Panel>
            </div>
            <div class="subheader">
                <h2 class="icon-warranty pagetitle">
                    <asp:Literal ID="ltlPageTitle" Text="Warranty List" runat="server"></asp:Literal></h2>
            </div>
        </div>
        <asp:Panel ID="listpanel" runat="server">
            <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand">
                <HeaderTemplate>
                    <table class="list warrantylist">
                        <thead>
                            <tr>
                                <th>
                                    Invoice No.
                                </th>
                                <th>
                                    Item Description
                                </th>
                                <th>
                                    Qty
                                </th>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Comments
                                </th>
                                <th>
                                    Resolution
                                </th>
                                <th>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            <%# Eval("InvoiceNo")%>
                        </td>
                        <td align="center">
                            <%# Eval("ProductName")%>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblQty" runat="server" Text='<%# Eval("Quantity")%>'></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblDate" runat="server" Text='<%# Eval("Date")%>'></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblComment" runat="server" Text='<%# Eval("Comment")%>'></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblResolution" runat="server" Text='<%# Eval("Resolution")%>'></asp:Label>
                        </td>
                        <td class="buttons">
                            <div style="display: none;">
                                <%-- <a href='<%# "WarrantyEdit.aspx?id="+Eval("ID") %>' class="icon icon-edit">Edit</a>--%>
                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandName="ItemEdit"
                                    CommandArgument='<%# Eval("ID") %>' class="icon icon-edit"></asp:LinkButton>
                                &nbsp;&nbsp;
                                <asp:LinkButton ID="lblDel" runat="server" Text="Delete" CommandName="ItemDel" CommandArgument='<%# Eval("ID") %>'
                                    class="icon icon-del"></asp:LinkButton>
                                <asp:Label ID="lblProductID" runat="server" Text='<%# Eval("ProductID")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lblInvoiceID" runat="server" Text='<%# Eval("InvoiceID")%>' Visible="false"></asp:Label>
                                <asp:Label ID="lblId" runat="server" Text='<%# Eval("ID")%>' Visible="false"></asp:Label></div>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged"
                PageSize="10" CssClass="aspnetpager">
            </webdiyer:AspNetPager>
        </asp:Panel>
        <asp:Panel ID="formpanel" runat="server" Visible="false">
            <div class="md-modal md-effect-1 md-show" id="modal-form">
                <div class="md-content">
                    <h3>
                        Warranty Form</h3>
                    <div>
                        <table class="form-table">
                            <tr>
                                <td class="form-label">
                                    Invoice No.:
                                </td>
                                <td>
                                    <asp:DropDownList ID="dropInvoice" runat="server" DataTextField="inv_No" DataValueField="inv_Id"
                                        AutoPostBack="True" OnSelectedIndexChanged="InvoiceChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Item Description:
                                </td>
                                <td>
                                    <asp:DropDownList ID="dropWarrantyProduct" runat="server" DataTextField="ip_Description"
                                        DataValueField="ip_ProductID" ClientIDMode="Static">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Qty:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtQty" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Date:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDate" runat="server" CssClass="Wdate"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Comments:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtComment" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Resolution:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtResolution" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <td colspan="2" align="center">
                                    <input type="hidden" id="inpWarrantyProductID" runat="server" clientidmode="Static"
                                        value="-1" />
                                    <asp:Label ID="lblInvoiceID" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblProductID" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblOldQty" runat="server" Visible="false"></asp:Label>
                                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="md-footer">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                            UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                            Width="75" Height="30" /></div>
                </div>
            </div>
        </asp:Panel>
        <input type="hidden" id="delwarrantyid" runat="server" clientidmode="Static" value="" />
        <asp:Button ID="delWarrantyBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="Delete" ClientIDMode="Static"></asp:Button>
    </ContentTemplate>
</asp:UpdatePanel>
