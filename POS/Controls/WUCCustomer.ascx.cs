﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS.Controls
{
    public partial class WUCCustomer : ControlBase
    {
        public List<Customer> Customers
        {
            get
            {
                return GlobalCache.Customers;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind();
            }
        }
        public override void Init()
        {
            if (formpanel.Visible)
            {
                BackToList(null, null);
            }
            else
            {
                Bind();
            }
        }
        protected void Delete(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delcustomerid.Attributes["value"]);
            POSService.POSService service = new POSService.POSService();
            Customer customer = Customers.FirstOrDefault(c => c.ID == id);
            customer.IsDeleted = true;
            customer.UserID = GlobalCache.CurrentUser.usr_ID;
            customer.LastUpdatedTime = DateTime.Now;
            service.UpdateCustomer(customer);
            //service.DeleteCustomer(new Customer() { ID = id });
            GlobalCache.RefreshCustomers();
            Bind();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemDel")
            {
                delcustomerid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "deleteCustomer");
            }
            else if (e.CommandName == "ItemEdit")
            {
                int id = Convert.ToInt32(e.CommandArgument.ToString());
                formbuttons.Visible = true;
                listbuttons.Visible = false;
                formpanel.Visible = true;
                listpanel.Visible = false;
                ltlPageTitle.Text = "Customer / Edit";
                lblId.Text = id.ToString();
                RepeaterItem item = e.Item;
                txtName.Text = (item.FindControl("lblName") as Label).Text;
                txtAddress.Text = (item.FindControl("lblAddress") as Label).Text;
                txtPhone.Text = (item.FindControl("lblPhone") as Label).Text;
                txtCountry.Text = (item.FindControl("lblCountry") as Label).Text;
                txtMobilePhone.Text = (item.FindControl("lblMobile") as Label).Text;
                txtEmail.Text = (item.FindControl("lblEmail") as Label).Text;
                txtWalletID.Text = (item.FindControl("lblWalletID") as Label).Text;
            }
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            rptList.DataSource = null;
            rptList.DataSource = Customers.TakeFrom((currentPage - 1) * pageSize, pageSize);
            rptList.DataBind();
        }

        protected void AddCustomer(object sender, EventArgs e)
        {
            formbuttons.Visible = true;
            listbuttons.Visible = false;
            formpanel.Visible = true;
            listpanel.Visible = false;
            ltlPageTitle.Text = "Customer / Add";
            lblId.Text = "0";
            txtAddress.Text = "";
            txtCountry.Text = "";
            txtEmail.Text = "";
            txtMobilePhone.Text = "";
            txtName.Text = "";
            txtNameMessage.Text = "*";
            txtPhone.Text = "";
            txtWalletID.Text = "";
        }

        protected void BackToList(object sender, EventArgs e)
        {
            listbuttons.Visible = true;
            formbuttons.Visible = false;
            formpanel.Visible = false;
            listpanel.Visible = true;
            ltlPageTitle.Text = "Customers";
            GlobalCache.RefreshCustomers();
            Bind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtName.Text.Trim()))
            {
                ShowError("Customer Name is required!");
                return;
            }

            Customer customer = new Customer() { Name = txtName.Text.Trim(), Phone = txtPhone.Text.Trim(), Mobile = txtMobilePhone.Text.Trim(), Email = txtEmail.Text.Trim(), Address = txtAddress.Text.Trim(), Country = txtCountry.Text.Trim(), ClientID = this.UserClientID, ID = Convert.ToInt32(lblId.Text), WalletID = txtWalletID.Text, UserID = GlobalCache.CurrentUser.usr_ID };
            if (customer.ID > 0)
            {
                customer.ServerID = Customers.FirstOrDefault(c => c.ID == customer.ID).ServerID;
            }
            customer.LastUpdatedTime = DateTime.Now;
            new POSService.POSService().UpdateCustomer(customer);

            BackToList(null, null);
        }

        void Bind()
        {
            rptList.DataSource = null;
            rptList.DataSource = Customers.TakeFrom(0, this.AspNetPager1.PageSize);
            AspNetPager1.RecordCount = Customers.Count;
            AspNetPager1.CurrentPageIndex = 1;
            AspNetPager1.GoToPage(AspNetPager1.CurrentPageIndex);
            rptList.DataBind();
        }
    }
}