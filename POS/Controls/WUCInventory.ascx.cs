﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS.Controls
{
    public partial class WUCInventory : ControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Initialize();
            }
        }

        public override void Init()
        {
            Initialize();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            int categoryId = Convert.ToInt32(dropCategory.SelectedValue);
            string name = string.Empty;
            string serialNo = string.Empty;
            switch (dropField.SelectedValue)
            {
                case "1":
                    name = txtKey.Text.Trim();
                    break;
                case "2":
                    serialNo = txtKey.Text.Trim();
                    break;
            }
            int quantity = Convert.ToInt32(dropQuantity.SelectedValue);

            GlobalCache.RefreshInventories(categoryId, name, serialNo, quantity);
            Bind();
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {

            int pageSize = inventoryAspNetPager1.PageSize;
            int currentPage = inventoryAspNetPager1.CurrentPageIndex;
            rptList.DataSource = null;
            rptList.DataSource = GlobalCache.Inventories.TakeFrom((currentPage - 1) * pageSize, pageSize);
            rptList.DataBind();
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemStock")
            {
                inventoryinvid.Attributes["value"] = e.CommandArgument.ToString();
                inventorypid.Attributes["value"] = (e.Item.FindControl("lblProductID") as Label).Text;
                RestockWindowManager.RadPrompt("Please input quantity", "stock", 300, 100, null, "Stock", "0");
            }

        }

        protected void Stock(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(inventoryinvid.Attributes["value"]);
            int proid = Convert.ToInt32(inventorypid.Attributes["value"]);
            int quan = Convert.ToInt32(inventoryquantity.Attributes["value"]);
            POSService.POSService service = new POSService.POSService();
            List<Inventory> inventories = GlobalCache.Inventories;
            if (inventories != null)
            {
                Inventory p = inventories.FirstOrDefault(p2 => p2.Product.pro_Id == proid);
                p.Quantity = quan + p.Quantity;
                p.ProductID = proid;
                p.ClientID = this.UserClientID;
                p.StoreID = this.UserStoreID;
                p.UserID = GlobalCache.CurrentUser.usr_ID;
                p.LastUpdatedTime = DateTime.Now;
                service.StockToInventory(p);
            }
            GlobalCache.RefreshInventories(0, "", "", 0);
            Initialize();
        }

        void Bind()
        {
            inventoryAspNetPager1.RecordCount = GlobalCache.Inventories.Count;

            rptList.DataSource = null;
            rptList.DataSource = GlobalCache.Inventories.TakeFrom(0, this.inventoryAspNetPager1.PageSize);
            inventoryAspNetPager1.CurrentPageIndex = 1;
            inventoryAspNetPager1.GoToPage(this.inventoryAspNetPager1.CurrentPageIndex);
            rptList.DataBind();
        }

        void Initialize()
        {
            inventoryinvid.Attributes["value"] = "0";
            inventorypid.Attributes["value"] = "0";
            inventoryquantity.Attributes["value"] = "0";

            Bind();
            BindCategories();
        }

        void BindCategories()
        {
            List<Category> cats = GlobalCache.Categories.GetHiarachy();
            cats.Insert(0, new Category() { cat_Id = 0, cat_Name = "-- All Category --" });
            dropCategory.DataSource = cats;
            dropCategory.DataBind();
        }
    }
}