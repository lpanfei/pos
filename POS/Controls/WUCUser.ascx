﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="WUCUser.ascx.cs" Inherits="POS.Controls.WUCUser" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="eid" %>
<script type="text/javascript">
    var previoususer = null;
    Sys.Application.add_load(usersLoad);
    function usersLoad(sender, args) {
        $(".userslist tr").unbind("click");
        $(".userslist tr").click(function () {
            var current = $(this).find(".buttons div").eq(0);
            if ($(previoususer).is(current))
                return;
            current.toggle("slow");
            if (previoususer != null) {
                $(previous).toggle("slow", function () { });
            }
            previoususer = current;
        }
       );
        $("#userTypedrop").unbind("change");
        $("#userTypedrop").change(function () {
            if ($("#userTypedrop").val() == "1") {
                $(".clientlevel").css("display", "none");
                $(".storelevel").css("display", "none");
                $(".permissionlevel").css("display", "none");
            } else if ($("#userTypedrop").val() == "2") {
                $(".clientlevel").css("display", "");
                $(".storelevel").css("display", "none");
                $(".permissionlevel").css("display", "");

            } else if ($("#userTypedrop").val() == "3") {
                $(".clientlevel").css("display", "none");
                $(".storelevel").css("display", "");
                $(".permissionlevel").css("display", "");
            }
        });

        if ($.trim($(".pagetitle").text()) == "User / Edit") {
            $(".passwordfields").css("display", "none");
        } else {
            $(".passwordfields").css("display", "");
        }

        if ($("#userTypedrop").val() == "1") {
            $(".clientlevel").css("display", "none");
            $(".storelevel").css("display", "none");
            $(".permissionlevel").css("display", "none");
        } else if ($("#userTypedrop").val() == "2") {
            $(".clientlevel").css("display", "");
            $(".storelevel").css("display", "none");
            $(".permissionlevel").css("display", "");

        } else if ($("#userTypedrop").val() == "3") {
            $(".clientlevel").css("display", "none");
            $(".storelevel").css("display", "");
            $(".permissionlevel").css("display", "");
        }

    }

    function deleteUser(sender) {
        if (sender) {
            $("#delUserBtn").trigger("click");
        } else {
            $("#deluserid").val("-1");
        }
    }
</script>
<style type="text/css">
    input[type=submit]
    {
        cursor: pointer;
    }
    
    .displaynone
    {
        display: none;
    }
</style>
<asp:UpdatePanel runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="mainheader">
            <div class="contextual">
                <asp:Panel ID="listbuttons" runat="server">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="addLink"
                            Text="Add" OnClick="AddUser" />
                    </div>
                </asp:Panel>
                <asp:Panel ID="formbuttons" runat="server" Visible="false">
                    <div style="height: 10px; width: 75px">
                        <asp:Button runat="server" CssClass="btn btn-large btn-block btn-info btn-add" ID="Button1"
                            Text="Back" OnClick="BackToList" />
                    </div>
                </asp:Panel>
            </div>
            <div class="subheader">
                <h2 class="pagetitle icon-user">
                    <asp:Literal ID="ltlPageTitle" Text="User List" runat="server"></asp:Literal></h2>
            </div>
        </div>
        <asp:Panel ID="listpanel" runat="server">
            <asp:Repeater ID="rptList" runat="server" OnItemCommand="rptList_ItemCommand">
                <HeaderTemplate>
                    <table class="list userslist">
                        <thead>
                            <tr>
                                <th>
                                    No.
                                </th>
                                <th>
                                    Username
                                </th>
                                <th>
                                    Pemission
                                </th>
                                <th>
                                    First Name
                                </th>
                                <th>
                                    Last Name
                                </th>
                                <th>
                                    Address
                                </th>
                                <th>
                                    Phone
                                </th>
                                <th>
                                    Email
                                </th>
                                <%--<td style="width:100px;">Store</td>--%>
                                <th>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td align="center">
                            <%# Container.ItemIndex+1 %>
                        </td>
                        <td align="center">
                            <%# Eval("usr_Name")%>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblPermission" runat="server" Text='<%# Eval("Permission")%>'></asp:Label>
                        </td>
                        <td align="center">
                            <%# Eval("usr_FirstName")%>
                        </td>
                        <td align="center">
                            <%# Eval("usr_LastName")%>
                        </td>
                        <td align="center">
                            <%# Eval("usr_Address")%>
                        </td>
                        <td align="center">
                            <%# Eval("usr_Phone") %>
                        </td>
                        <td align="center">
                            <%# Eval("usr_Email") %>
                        </td>
                        <%--<td><%# Eval("sto_Name")%></td>--%>
                        <td class="buttons">
                            <div style="display: none;">
                                <%--<a href='<%# "UserEdit.aspx?id="+Eval("usr_Id") %>' class="icon icon-edit">Edit</a>--%>
                                <asp:LinkButton ID="LinkButton1" runat="server" Text="Edit" CommandName="ItemEdit"
                                    CommandArgument='<%# Eval("usr_Id")%>' class="icon icon-edit"></asp:LinkButton>
                                &nbsp;&nbsp;
                                <asp:LinkButton ID="lblDel" runat="server" Text="Delete" CommandName="ItemDel" CommandArgument='<%# Eval("usr_Id") %>'
                                    class="icon icon-del"></asp:LinkButton></div>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </tbody> </table>
                </FooterTemplate>
            </asp:Repeater>
            <webdiyer:AspNetPager ID="AspNetPager1" runat="server" OnPageChanged="AspNetPager1_PageChanged" PageSize="10" CssClass="aspnetpager">
            </webdiyer:AspNetPager>
        </asp:Panel>
        <asp:Panel ID="formpanel" runat="server" Visible="false">
            <div class="md-modal md-effect-1 md-show" id="modal-form">
                <div class="md-content">
                    <h3>
                        User Form</h3>
                    <div>
                        <table class="form-table">
                            <tr>
                                <td class="form-label">
                                    User Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr class="passwordfields">
                                <td class="form-label">
                                    Password:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    User Level:
                                </td>
                                <td>
                                    <asp:DropDownList ID="userTypedrop" runat="server" ClientIDMode="Static">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="clientlevel" style="display: none">
                                <td class="form-label">
                                    Client:
                                </td>
                                <td>
                                    <asp:DropDownList ID="clientDropdown" runat="server" DataValueField="ID"
                                        DataTextField="Name">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="storelevel" style="display: none">
                                <td class="form-label">
                                    Store:
                                </td>
                                <td>
                                    <asp:DropDownList ID="storeDropdown" runat="server" DataTextField="sto_Name" DataValueField="sto_ID">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr class="permissionlevel" style="display: none">
                                <td class="form-label">
                                    Permission:
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="radlPermission" runat="server" RepeatDirection="Horizontal"
                                        RepeatColumns="2">
                                        <asp:ListItem Text="Administrator" Value="0" Selected="True"></asp:ListItem>
                                        <asp:ListItem Text="Report" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Sale" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Sale and Warehouse" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Service" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Technician" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Warehouse" Value="6"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <%--<tr>
            <td>Store:</td>
            <td>
                <asp:DropDownList ID="dropStore" runat="server"></asp:DropDownList>
            </td>
        </tr>--%>
                            <tr>
                                <td class="form-label">
                                    First Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Last Name:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Address:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAddress" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Suburb:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSuburb" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    State:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtState" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Postcode:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPostcode" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Phone:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPhone" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Moble Phone:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMoblePhone" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Email:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="form-label">
                                    Note:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtNote" runat="server" TextMode="MultiLine" Rows="4"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <td colspan="2" align="center">
                                    <asp:Label ID="lblId" runat="server" Visible="false"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="md-footer">
                        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                            UseSubmitBehavior="false" class="btn btn-large btn-block btn-info btn-center"
                            Width="75" Height="30" />
                    </div>
                </div>
            </div>
        </asp:Panel>
        <input type="hidden" id="deluserid" runat="server" clientidmode="Static" value="" />
        <asp:Button ID="delUserBtn" runat="server" Width="0" Height="0" CssClass="displaynone"
            OnClick="Delete" ClientIDMode="Static"></asp:Button>
    </ContentTemplate>
</asp:UpdatePanel>
