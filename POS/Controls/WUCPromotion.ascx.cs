﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using POS.Data;

namespace POS.Controls
{
    public partial class WUCPromotion : ControlBase
    {
        List<Promotion> Promotions
        {
            get
            {
                return GlobalCache.Promotions;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                InitializeDropdowns();
                Bind();
                txtStartDate.Attributes.Add("onFocus", "WdatePicker({lang:'en',dateFmt:'dd-MM-yyyy'})");
                txtEndDate.Attributes.Add("onFocus", "WdatePicker({lang:'en',dateFmt:'dd-MM-yyyy'})");
            }
        }

        public override void Init()
        {
            if (formpanel.Visible)
            {
                BackToList(null, null);
            }
            InitializeDropdowns();
            Bind();
        }

        void InitializeDropdowns()
        {
            GlobalCache.RefreshProducts(0, "", "");
            dropPromotionProduct.DataSource = GlobalCache.Products;
            dropPromotionProduct.DataBind();
            if (GlobalCache.Products.Any())
            {
                dropPromotionProduct.SelectedIndex = 0;
            }
        }

        void Bind()
        {
            this.AspNetPager1.RecordCount = Promotions.Count;
            rptPromotionList.DataSource = null;
            rptPromotionList.DataSource = Promotions.TakeFrom(0, this.AspNetPager1.PageSize);
            rptPromotionList.DataBind();
            this.AspNetPager1.CurrentPageIndex = 1;
            this.AspNetPager1.GoToPage(this.AspNetPager1.CurrentPageIndex);
        }

        protected void AspNetPager1_PageChanged(object sender, EventArgs e)
        {
            int pageSize = AspNetPager1.PageSize;
            int currentPage = AspNetPager1.CurrentPageIndex;
            rptPromotionList.DataSource = null;
            rptPromotionList.DataSource = Promotions.TakeFrom((currentPage - 1) * pageSize, pageSize);
            rptPromotionList.DataBind();
        }

        protected void AddWarranty(object sender, EventArgs e)
        {
            formbuttons.Visible = true;
            listbuttons.Visible = false;
            formpanel.Visible = true;
            listpanel.Visible = false;
            ltlPageTitle.Text = "Promotion / Add";
            if (dropPromotionProduct.Items.Count > 0)
            {
                dropPromotionProduct.SelectedIndex = 0;
            }
            
            txtDiscount.Text = "";
            txtEndDate.Text = "";
            txtStartDate.Text = "";
            txtPromotionDescription.Text = "";
            lblId.Text = "0";
            inpPromotionProductID.Attributes["value"] = "-1";
        }

        protected void BackToList(object sender, EventArgs e)
        {
            listbuttons.Visible = true;
            formbuttons.Visible = false;
            formpanel.Visible = false;
            listpanel.Visible = true;
            ltlPageTitle.Text = "Promotion List";
            inpPromotionProductID.Attributes["value"] = "-1";
            Bind();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtDiscount.Text.Trim()))
            {
                ShowError("Discount is required!");
                return;
            }
            if (string.IsNullOrEmpty(txtStartDate.Text.Trim()))
            {
                ShowError("Start date is required!");
                return;
            }
            if (string.IsNullOrEmpty(txtEndDate.Text.Trim()))
            {
                ShowError("End date is required!");
                return;
            }
            try
            {
                decimal discount = Convert.ToDecimal(txtDiscount.Text.Trim());
            }
            catch
            {
                ShowError("Discount inputted is not in a correct format!");
                return;
            }
            POSService.POSService service = new POSService.POSService();
            Promotion w = new Promotion()
            {
                ProductID = Convert.ToInt32(this.dropPromotionProduct.SelectedValue),
                ID = Convert.ToInt32(lblId.Text),
                ClientID = this.UserClientID,
                UserID = GlobalCache.CurrentUser.usr_ID,
                StoreID = this.UserStoreID,
                IsDeleted=false,
                Description=txtPromotionDescription.Text.Trim(),
                Discount=Convert.ToDecimal(txtDiscount.Text.Trim()),
            };

            w.Product = GlobalCache.Products.FirstOrDefault(pp => pp.pro_Id == w.ProductID);

            if (w.ID > 0)
            {
                w.ServerID = Promotions.FirstOrDefault(wa => wa.ID == w.ID).ServerID;
            }
            if (string.IsNullOrEmpty(txtStartDate.Text))
            {
                w.StartTime = null;
            }
            else
            {
                try
                {
                    w.StartTime = DateTime.Parse(txtStartDate.Text);
                }
                catch
                {
                    w.StartTime = GetDateTime(txtStartDate.Text);
                }
            }
            if (w.StartTime==null)
            {
                ShowError("Start time inputted is not in a correct format!");
                return;
            }

            if (string.IsNullOrEmpty(txtEndDate.Text))
            {
                w.EndTime = null;
            }
            else
            {
                try
                {
                    w.EndTime = DateTime.Parse(txtEndDate.Text);
                }
                catch
                {
                    w.EndTime = GetDateTime(txtEndDate.Text);
                }
            }
            if (w.EndTime == null)
            {
                ShowError("End time inputted is not in a correct format!");
                return;
            }

            //need validation
            if (GlobalCache.Promotions != null && GlobalCache.Promotions.Any(p => p.ProductID == w.ProductID&&((p.StartTime>w.StartTime&&p.StartTime<w.EndTime)||(w.StartTime>p.StartTime&&w.StartTime<p.EndTime))))
            {
                ShowError("There are already promotions for the product during the period! Please check it!");
                return;
            }

            w.LastUpdatedTime = DateTime.Now;
            service.AddPromotion(w);
            GlobalCache.RefreshPromotions();
            BackToList(null, null);
        }

        DateTime? GetDateTime(string s)
        {
            try
            {
                string[] str = s.Split('-');
                int year = Convert.ToInt32(str[2]);
                int month = Convert.ToInt32(str[1]);
                int day = Convert.ToInt32(str[0]);
                return new DateTime(year, month, day, 0, 0, 0);
            }
            catch
            {
                return null;
            }
        }

        protected void rptList_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName == "ItemEdit")
            {
                RepeaterItem item = e.Item;
                formbuttons.Visible = true;
                listbuttons.Visible = false;
                formpanel.Visible = true;
                listpanel.Visible = false;
                ltlPageTitle.Text = "Warranty / Edit";
                dropPromotionProduct.SelectedValue = (item.FindControl("lblProductID") as Label).Text;
                txtDiscount.Text = (item.FindControl("lblDiscount") as Label).Text;
                txtStartDate.Text = (item.FindControl("lblFromDate") as Label).Text;
                txtEndDate.Text = (item.FindControl("lblEndDate") as Label).Text;
                this.txtPromotionDescription.Text = (item.FindControl("lblDescription") as Label).Text;
                lblId.Text = (item.FindControl("lblId") as Label).Text;
                inpPromotionProductID.Attributes["value"] = (item.FindControl("lblProductID") as Label).Text;
            }
            else if (e.CommandName == "ItemDel")
            {
                delpromotionid.Attributes["value"] = e.CommandArgument.ToString();
                ShowConfirm("Are you sure to delete the item?", "deletePromotion");
            }
        }

        protected void Delete(object sender, EventArgs args)
        {
            int id = Convert.ToInt32(delpromotionid.Attributes["value"]);
            POSService.POSService service = new POSService.POSService();
            Promotion w = Promotions.FirstOrDefault(wa => wa.ID == id);
            w.IsDeleted = true;
            w.UserID = GlobalCache.CurrentUser.usr_ID;
            w.LastUpdatedTime = DateTime.Now;
            service.UpdatePromotion(w);
            GlobalCache.RefreshPromotions();
            Bind();
        }
    }
}