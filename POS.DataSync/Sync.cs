﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using POS.Data;
using System.Threading;

namespace POS.DataSync
{
    public class Sync
    {
        private string _clientConn;
        private string _serverConn;
        private int _clientId;
        private int _storeId;

        public Sync()
        {
        }

        public Sync(string clientConn, string serverConn, int clientid, int storeid)
        {
            this._clientConn = clientConn;
            this._serverConn = serverConn;
            this._clientId = clientid;
            this._storeId = storeid;
        }

        public void DoSync()
        {
            WriteLog("Start data sync...");
            Thread.Sleep(2000);
            #region Test connections
            WriteLog("Test connections...");
            bool canConnectToClient = TestConnection(this._clientConn);
            if (!canConnectToClient)
            {
                WriteLog("Test client connection: falied.");
                WriteLog("End.");
                return;
            }
            WriteLog("Test client connection: success.");
            bool canConnectToServer = TestConnection(this._serverConn);
            if (!canConnectToServer)
            {
                WriteLog("Test server connection: falied.");
                WriteLog("End.");
                return;
            }
            WriteLog("Test server connection: success.");
            #endregion

            #region Get client data
            WriteLog("Fetching data from client side...");
            DBInitializer.Initialize(_clientConn);
            WriteLog("Fetching categories...");
            List<Category> categories = Category.Select(new Category.Parms() { ClientID=this._clientId});
            WriteLog("Fetching products...");
            List<Product> products = Product.Select(new Product.Parms() { ClientID = this._clientId });
            WriteLog("Fetching invoices...");
            List<Invoice> invoices = Invoice.Select(new Invoice.Parms() { ClientID = this._clientId,StoreID=this._storeId });
            WriteLog("Fetching inventories...");
            List<Inventory> inventories = Inventory.Select(new Inventory.Parms() { ClientID = this._clientId, StoreID = this._storeId });
            WriteLog("Fetching suppliers...");
            List<Supplier> suppliers = Supplier.Select(new Supplier.Parms() { ClientID = this._clientId });
            WriteLog("Fetching customers...");
            List<Customer> customers = Customer.Select(new Customer.Parms() { ClientID = this._clientId });
            WriteLog("Fetching warraties...");
            List<Warranty> warranties = Warranty.Select(new Warranty.Parms() { ClientID = this._clientId,StoreID=this._storeId });
            WriteLog("Fetching promotions...");
            List<Promotion> promotions = Promotion.Select(new Promotion.Parms() { ClientID = this._clientId, StoreID = this._storeId });
            WriteLog("Fetching users...");
            List<User> users = User.Select(new User.Parms() { RetrieveType = User.RetrieveType.AllUsers, clientId = this._clientId, storeId = this._storeId });
            WriteLog("Fetching clients...");
            List<Client> clients = Client.Select(new Client.Parms() { });
            WriteLog("Fetching stores...");
            List<Store> stores = Store.Select(new Store.Parms() { ClientID = this._clientId });
            #endregion

            #region Get server data
            WriteLog("Fetching data from client side...");
            DBInitializer.Initialize(_serverConn);
            WriteLog("Fetching categories...");
            List<Category> s_categories = Category.Select(new Category.Parms() { ClientID = this._clientId });
            WriteLog("Fetching products...");
            List<Product> s_products = Product.Select(new Product.Parms() { ClientID = this._clientId });
            WriteLog("Fetching invoices...");
            List<Invoice> s_invoices = Invoice.Select(new Invoice.Parms() { ClientID = this._clientId, StoreID = this._storeId });
            WriteLog("Fetching inventories...");
            List<Inventory> s_inventories = Inventory.Select(new Inventory.Parms() { ClientID = this._clientId, StoreID = this._storeId });
            WriteLog("Fetching suppliers...");
            List<Supplier> s_suppliers = Supplier.Select(new Supplier.Parms() { ClientID = this._clientId });
            WriteLog("Fetching customers...");
            List<Customer> s_customers = Customer.Select(new Customer.Parms() { ClientID = this._clientId });
            WriteLog("Fetching warranties...");
            List<Warranty> s_warranties = Warranty.Select(new Warranty.Parms() { ClientID = this._clientId, StoreID = this._storeId });
            WriteLog("Fetching promotions...");
            List<Promotion> s_promotions = Promotion.Select(new Promotion.Parms() { ClientID = this._clientId, StoreID = this._storeId });
            WriteLog("Fetching users...");
            List<User> s_users = User.Select(new User.Parms() { RetrieveType=User.RetrieveType.AllUsers,clientId=this._clientId,storeId=this._storeId});
            WriteLog("Fetching clients...");
            List<Client> s_clients = Client.Select(new Client.Parms() { });
            WriteLog("Fetching stores...");
            List<Store> s_stores = Store.Select(new Store.Parms() { ClientID=this._clientId });
            #endregion

            WriteLog("Start uploading data from client to server...");
            DBInitializer.Initialize(this._clientConn);
            UploadConfig.ConnectionString = this._serverConn;
            UploadConfig.IsOn = true;
            #region upload categories
            WriteLog("Start uploading categories...");
            if (categories.Any(c => !c.UploadFlag))
            {
                WriteLog("Total " + categories.Count(c => !c.UploadFlag).ToString()+" records");
                foreach (Category cat in categories.Where(c => !c.UploadFlag))
                {
                    WriteLog("Uploading category: " + cat.cat_Name);
                    Category s_cat = s_categories.FirstOrDefault(c => c.cat_Id == cat.ServerID);
                    if (s_cat==null||s_cat.LastUpdatedTime.CompareTo(cat.LastUpdatedTime)<0)
                    {
                        cat.Update();
                        WriteLog("Upload successfully! Category: " + cat.cat_Name);
                    }
                    else if (s_cat.LastUpdatedTime.CompareTo(cat.LastUpdatedTime) > 0)
                    {
                        WriteLog("Conflict found! Category: "+cat.cat_Name,LogType.Error);
                    }
                    WriteLog("End uploading category: " + cat.cat_Name);
                }
            }
            WriteLog("End uploading categories...");
            #endregion

            #region upload products
            WriteLog("Start uploading products...");
            if (products.Any(c => !c.UploadFlag))
            {
                WriteLog("Total " + products.Count(c => !c.UploadFlag).ToString() + " records");
                foreach (Product prod in products.Where(c => !c.UploadFlag))
                {
                    WriteLog("Uploading product: " + prod.pro_Name);
                    Product s_prod = s_products.FirstOrDefault(p=> p.pro_Id == prod.ServerID);
                    if (s_prod == null || s_prod.LastUpdatedTime.CompareTo(prod.LastUpdatedTime) < 0)
                    {
                        prod.Update();
                        WriteLog("Upload successfully! Product: " + prod.pro_Name);
                    }
                    else if (s_prod.LastUpdatedTime.CompareTo(prod.LastUpdatedTime) > 0)
                    {
                        WriteLog("Conflict found! Product: " + prod.pro_Name, LogType.Error);
                    }
                    WriteLog("End uploading product: " + prod.pro_Name);
                }
            }
            WriteLog("End uploading products...");
            #endregion

            #region upload invoices
            WriteLog("Start uploading invoices...");
            if (invoices.Any(c => !c.UploadFlag))
            {
                WriteLog("Total " + invoices.Count(c => !c.UploadFlag).ToString() + " records");
                foreach (Invoice inv in invoices.Where(c => !c.UploadFlag))
                {
                    WriteLog("Uploading invoice: " + inv.inv_No);
                    Invoice s_inv = s_invoices.FirstOrDefault(i => i.inv_Id == inv.ServerID);
                    if (s_inv == null || s_inv.LastUpdatedTime.CompareTo(inv.LastUpdatedTime) < 0)
                    {
                        inv.Update();
                        WriteLog("Upload successfully! Invoice: " + inv.inv_No);
                    }
                    else if (s_inv.LastUpdatedTime.CompareTo(inv.LastUpdatedTime) > 0)
                    {
                        WriteLog("Conflict found! Invoice: " + inv.inv_No, LogType.Error);
                    }
                    WriteLog("End uploading invoice: " + inv.inv_No);
                }
            }
            WriteLog("End uploading invoices...");
            #endregion

            #region upload suppliers
            WriteLog("Start uploading suppliers...");
            if (suppliers.Any(c => !c.UploadFlag))
            {
                WriteLog("Total " + suppliers.Count(c => !c.UploadFlag).ToString() + " records");
                foreach (Supplier supp in suppliers.Where(c => !c.UploadFlag))
                {
                    WriteLog("Uploading supplier: " + supp.Name);
                    Supplier s_supp = s_suppliers.FirstOrDefault(s => s.ID == supp.ServerID);
                    if (s_supp == null || s_supp.LastUpdatedTime.CompareTo(supp.LastUpdatedTime) < 0)
                    {
                        supp.Update();
                        WriteLog("Upload successfully! Supplier: " + supp.Name);
                    }
                    else if (s_supp.LastUpdatedTime.CompareTo(supp.LastUpdatedTime) > 0)
                    {
                        WriteLog("Conflict found! Supplier: " + supp.Name, LogType.Error);
                    }
                    WriteLog("End uploading supplier: " + supp.Name);
                }
            }
            WriteLog("End uploading suppliers...");
            #endregion

            #region upload inventories
            WriteLog("Start uploading inventories...");
            if (inventories.Any(c => !c.UploadFlag))
            {
                WriteLog("Total " + inventories.Count(c => !c.UploadFlag).ToString() + " records");
                foreach (Inventory inv in inventories.Where(c => !c.UploadFlag))
                {
                    WriteLog("Uploading inventory: " + inv.Product.pro_Name);
                    Inventory s_inv = s_inventories.FirstOrDefault(i => i.ID == inv.ServerID);
                    if (s_inv == null || s_inv.LastUpdatedTime.CompareTo(inv.LastUpdatedTime) < 0)
                    {
                        inv.Update();
                        WriteLog("Upload successfully! Inventory: " + inv.Product.pro_Name);
                    }
                    else if (s_inv.LastUpdatedTime.CompareTo(inv.LastUpdatedTime) > 0)
                    {
                        WriteLog("Conflict found! Inventory: " + inv.Product.pro_Name, LogType.Error);
                    }
                    WriteLog("End uploading inventory: " + inv.Product.pro_Name);
                }
            }
            WriteLog("End uploading inventories...");
            #endregion

            #region upload customers
            WriteLog("Start uploading customers...");
            if (customers.Any(c => !c.UploadFlag))
            {
                WriteLog("Total " + customers.Count(c => !c.UploadFlag).ToString() + " records");
                foreach (Customer cus in customers.Where(c => !c.UploadFlag))
                {
                    WriteLog("Uploading customer: " + cus.Name);
                    Customer s_cus = s_customers.FirstOrDefault(s => s.ID == cus.ServerID);
                    if (s_cus == null || s_cus.LastUpdatedTime.CompareTo(cus.LastUpdatedTime) < 0)
                    {
                        cus.Update();
                        WriteLog("Upload successfully! Customer: " + cus.Name);
                    }
                    else if (s_cus.LastUpdatedTime.CompareTo(cus.LastUpdatedTime) > 0)
                    {
                        WriteLog("Conflict found! Customer: " + cus.Name, LogType.Error);
                    }
                    WriteLog("End uploading customer: " + cus.Name);
                }
            }
            WriteLog("End uploading customers...");
            #endregion

            #region upload warranties
            WriteLog("Start uploading warranties...");
            if (warranties.Any(c => !c.UploadFlag))
            {
                WriteLog("Total " + warranties.Count(c => !c.UploadFlag).ToString() + " records");
                foreach (Warranty war in warranties.Where(c => !c.UploadFlag))
                {
                    WriteLog("Uploading warranty: " + war.InvoiceNo);
                    Warranty s_war = s_warranties.FirstOrDefault(w => w.ID == war.ServerID);
                    if (s_war == null || s_war.LastUpdatedTime.CompareTo(war.LastUpdatedTime) < 0)
                    {
                        war.Update();
                        WriteLog("Upload successfully! Warranty: " + war.InvoiceNo);
                    }
                    else if (s_war.LastUpdatedTime.CompareTo(war.LastUpdatedTime) > 0)
                    {
                        WriteLog("Conflict found! Warranty: " + war.InvoiceNo, LogType.Error);
                    }
                    WriteLog("End uploading warranty: " + war.InvoiceNo);
                }
            }
            WriteLog("End uploading warranties...");
            #endregion

            #region upload promotions
            WriteLog("Start uploading promotions...");
            if (promotions.Any(c => !c.UploadFlag))
            {
                WriteLog("Total " + promotions.Count(c => !c.UploadFlag).ToString() + " records");
                foreach (Promotion war in promotions.Where(c => !c.UploadFlag))
                {
                    WriteLog("Uploading promotion: " + war.ProductName);
                    Promotion s_war = s_promotions.FirstOrDefault(w => w.ID == war.ServerID);
                    if (s_war == null || s_war.LastUpdatedTime.CompareTo(war.LastUpdatedTime) < 0)
                    {
                        war.Update();
                        WriteLog("Upload successfully! Warranty: " + war.ProductName);
                    }
                    else if (s_war.LastUpdatedTime.CompareTo(war.LastUpdatedTime) > 0)
                    {
                        WriteLog("Conflict found! Warranty: " + war.ProductName, LogType.Error);
                    }
                    WriteLog("End uploading warranty: " + war.ProductName);
                }
            }
            WriteLog("End uploading warranties...");
            #endregion

            WriteLog("End uploading data from client to server...");

            WriteLog("Start downloading data from server to client...");
            UploadConfig.IsOn = false;
            UploadConfig.ConnectionString = string.Empty;
            #region download categories
            WriteLog("downloading categories...");
            IEnumerable<Category> d_categories = s_categories.Where(c => !categories.Any(cat => cat.ServerID == c.cat_Id) || categories.FirstOrDefault(cp => cp.ServerID == c.cat_Id).LastUpdatedTime.CompareTo(c.LastUpdatedTime) < 0);
            if (d_categories != null)
            {
                foreach (Category cat in d_categories)
                {
                    WriteLog("start downloading category: " + cat.cat_Name);
                    if (categories.Any(c => c.ServerID == cat.cat_Id))
                    {
                        cat.ServerID = cat.cat_Id;
                        cat.cat_Id = categories.FirstOrDefault(p => p.ServerID == cat.cat_Id).cat_Id;
                    }
                    else
                    {
                        cat.ServerID = cat.cat_Id;
                        cat.cat_Id = 0;
                    }
                    Category category = categories.FirstOrDefault(c => c.ServerID == cat.ParentID);
                    if (category != null)
                    {
                        cat.ParentID = category.cat_Id;
                    }
                    cat.Update();
                    WriteLog(cat.cat_Name + " is downloaded");
                }
            }
            #endregion
            #region download products
            WriteLog("downloading products...");
            IEnumerable<Product> d_products = s_products.Where(p => !products.Any(pro => pro.ServerID == p.pro_Id)||products.FirstOrDefault(cp=>cp.ServerID==p.pro_Id).LastUpdatedTime.CompareTo(p.LastUpdatedTime)<0);
            if (d_products != null)
            {
                foreach (Product pro in d_products)
                {
                    WriteLog("start downloading product: "+pro.pro_Name);
                    if (products.Any(p => p.ServerID == pro.pro_Id))
                    {
                        pro.ServerID = pro.pro_Id;
                        pro.pro_Id = products.FirstOrDefault(p => p.ServerID == pro.pro_Id).pro_Id;
                    }
                    else
                    {
                        pro.ServerID = pro.pro_Id;
                        pro.pro_Id = 0; 
                    }
                    Supplier supplier = suppliers.FirstOrDefault(s => s.ServerID == pro.SupplierID);
                    if (supplier != null)
                    {
                        pro.SupplierID = supplier.ID;
                    }
                    Category category = categories.FirstOrDefault(c => c.ServerID == pro.Category.cat_Id);
                    if (category != null)
                    {
                        pro.Category = category;
                    }

                    if (pro.Relates != null)
                    {
                        foreach (Product rp in pro.Relates)
                        {
                            Product rponclient = products.FirstOrDefault(ppp => ppp.ServerID == rp.pro_Id);
                            if (rponclient == null)
                            {
                                rp.ServerID = rp.pro_Id;
                                rp.pro_Id = 0;
                                rp.Update();
                                products = Product.Select(new Product.Parms() { ClientID = this._clientId });
                                rponclient = products.FirstOrDefault(ppp => ppp.ServerID == rp.pro_Id);
                            }
                            if (rponclient != null)
                            {
                                rp.pro_Id = rponclient.pro_Id;
                            }
                        }
                    }

                    pro.Update();
                    products = Product.Select(new Product.Parms() { ClientID = this._clientId });
                    WriteLog(pro.pro_Name+" is downloaded");
                }
            }
            #endregion
            #region download customers
            WriteLog("downloading customers...");
            IEnumerable<Customer> d_customers = s_customers.Where(c => !customers.Any(cus => cus.ServerID == c.ID));
            if (d_customers != null)
            {
                foreach (Customer cus in d_customers)
                {
                    cus.ServerID = cus.ID;
                    cus.ID = 0;
                    cus.Update();
                }
            }
            #endregion
            #region download suppliers
            WriteLog("downloading suppliers...");
            IEnumerable<Supplier> d_suppliers = s_suppliers.Where(s => !suppliers.Any(sup => sup.ServerID == s.ID));
            if (d_suppliers != null)
            {
                foreach (Supplier sup in d_suppliers)
                {
                    sup.ServerID = sup.ID;
                    sup.ID = 0;
                    sup.Update();
                }
            }
            #endregion
            #region download users
            WriteLog("downloading users...");
            IEnumerable<User> d_users = s_users.Where(u => !users.Any(user => user.ServerID == u.usr_ID));
            if (d_users != null)
            {
                foreach (User user in d_users)
                {
                    user.ServerID = user.usr_ID;
                    user.usr_ID = 0;
                    user.Update();
                }
            }
            #endregion
            #region download clients if there's no client on client side
            if (clients == null || !clients.Any())
            {
                Client client = s_clients.FirstOrDefault(c => c.ID == this._clientId);
                if (client != null)
                {
                    WriteLog("downloading client...");
                    client.Update();
                }
            }
            #endregion
            #region download stores if there's no store on client side
            if (stores == null || !stores.Any())
            {
                Store store = s_stores.FirstOrDefault(s => s.sto_Id == this._storeId);
                if (store != null)
                {
                    WriteLog("downloading store...");
                    store.Update();
                }
            }
            #endregion
            WriteLog("End data sync...");
        }

        public bool TestConnection(string conn)
        {
            bool isSuccess;
            SqlConnection sqlConn = new SqlConnection(conn);
            try
            {
                sqlConn.Open();
                isSuccess = true;
            }
            catch
            {
                isSuccess = false;
            }
            finally
            {
                sqlConn.Close();
            }
            return isSuccess;
        }

        public void WriteLog(string message)
        {
            WriteLog(message, LogType.Info);
        }
        public virtual void WriteLog(string message, LogType lt)
        {
        }
    }
}
