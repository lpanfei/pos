﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace POS.DataSync
{
    public enum LogType
    {
        Info,
        Warning,
        Error
    }
}
